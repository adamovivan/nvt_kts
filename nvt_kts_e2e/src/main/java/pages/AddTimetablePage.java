package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddTimetablePage {

    @FindBy(id="timetableBtn")
    private WebElement timetableBtn;

    @FindBy(id="addTimetableBtn")
    private WebElement addTimetableBtn;

    @FindBy(id="day")
    private WebElement daySelect;

    @FindBy(id="lines")
    private WebElement linesSelect;

    @FindBy(id="dateFrom")
    private WebElement dateFromPicker;

    @FindBy(id="dateTo")
    private WebElement dateToPicker;

    @FindBy(id="timesTo")
    private WebElement timesToSelect;

    @FindBy(id="timesFrom")
    private WebElement timesFromSelect;

    @FindBy(id="addBtn")
    private WebElement addBtn;

    @FindBy(xpath="//mat-card-title[@class='center mat-card-title']")
    private WebElement title;

    @FindBy(className="mat-simple-snackbar")
    private WebElement snackBarMessage;

    public static String MESSAGE_SUCCESSFULLY_ADDED = "Schedule added successfully.";

    public static String MESSAGE_WRONG_DATE = "End date is before start date.";

    public static String MESSAGE_SCHEDULE_ALREADY_EXISTS = "For that path name and day schedule duration already exist.";

    public static String MESSAGE_EMPTY_FIELDS = "You must fill in all the fields.";

    public WebElement getTimetableBtn() {
        return timetableBtn;
    }

    public WebElement getAddTimetableBtn() {
        return addTimetableBtn;
    }

    public WebElement getDaySelect() {
        return daySelect;
    }

    public WebElement getLinesSelect() {
        return linesSelect;
    }

    public WebElement getDateFromPicker() {
        return dateFromPicker;
    }

    public WebElement getDateToPicker() {
        return dateToPicker;
    }

    public WebElement getTimesToSelect() {
        return timesToSelect;
    }

    public WebElement getTimesFromSelect() {
        return timesFromSelect;
    }

    public WebElement getAddBtn() {
        return addBtn;
    }


    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }

    public WebElement getTitle() {
        return title;
    }

    public void ensureSnackBarIsPresent(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(snackBarMessage,msgTxt));
    }
}
