package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ShowPriceListPage {

    public static String EDIT_SUCCESS = "Price changed successfully.";
    public static String EDIT_FAIL = "Price field cannot be empty.";
    public static String EDIT_NEGATIVE_FAIL = "Please enter a positive number.";


    @FindBy(id="priceListSelect")
    private WebElement priceListSelect;

    @FindBy(tagName = "mat-option")
    private List<WebElement> priceListOptions;

    @FindBy(css="button[aria_label=\"Edit price...\"]")
    private List<WebElement> editPriceButtons;

    @FindBy(css="input[type=\"number\"]")
    private List<WebElement> editPriceFields;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement message;

    public WebElement getPriceListSelect() {
        return priceListSelect;
    }

    public List<WebElement> getPriceListOptions() {
        return priceListOptions;
    }

    public List<WebElement> getEditPriceButtons() {
        return editPriceButtons;
    }

    public List<WebElement> getEditPriceFields() {
        return editPriceFields;
    }

    public WebElement getMessage() {
        return message;
    }

    public void selectLastAddedPriceList(){
        this.priceListSelect.click();
        priceListOptions.get(priceListOptions.size()-1).click();
    }

    public String getLastAddedPriceListOptionText(){
        this.priceListSelect.click();
        if(priceListOptions.size()==0){
            return "";
        }

        WebElement chosenElement  = null;
        int id = 0;
        //poslednji je dodat onaj s najvecim id-om, a zbog pravila preklpanja on traje i najvise u buducnost
        for(WebElement webElement:priceListOptions){
            if(chosenElement==null){
                chosenElement = webElement;
                id = Integer.parseInt(chosenElement.getAttribute("ng-reflect-value"));
            }
            else{
                int newId = Integer.parseInt(webElement.getAttribute("ng-reflect-value"));
                if(newId > id){
                    id = newId;
                    chosenElement = webElement;
                }
            }
        }
        return chosenElement.findElement(By.cssSelector("span")).getText();
    }

    public void ensureMessage(WebDriver webDriver,String msgTxt){
        (new WebDriverWait(webDriver,10)).until(ExpectedConditions.textToBePresentInElement(message,msgTxt));
    }
}
