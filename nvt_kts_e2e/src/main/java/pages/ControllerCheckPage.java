package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ControllerCheckPage {

    @FindBy(id="checkBtn")
    private WebElement checkBtn;

    @FindBy(xpath="//div[@class='mat-form-field-infix']")
    private WebElement linesSelect;

    @FindBy(xpath="//span[contains(text(),'1B')]")
    private WebElement select1B;

    @FindBy(id="tableCheck")
    private WebElement tableCheck;

    public WebElement getCheckBtn() {
        return checkBtn;
    }

    public void setCheckBtn(WebElement checkBtn) {
        this.checkBtn = checkBtn;
    }

    public WebElement getLinesSelect() {
        return linesSelect;
    }

    public void setLinesSelect(WebElement linesSelect) {
        this.linesSelect = linesSelect;
    }

    public WebElement getSelect1B() {
        return select1B;
    }

    public void setSelect1B(WebElement select1B) {
        this.select1B = select1B;
    }

    public WebElement getTableCheck() {
        return tableCheck;
    }

    public void setTableCheck(WebElement tableCheck) {
        this.tableCheck = tableCheck;
    }


    public void clickOnFirstOption(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(select1B,msgTxt));
        select1B.click();
    }

    public void waitForTable(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(tableCheck));
    }
}
