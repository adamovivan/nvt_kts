package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RegistrationPage {

    @FindBy(id="firstName")
    private WebElement firstNameInput;

    @FindBy(id="lastName")
    private WebElement lastNameInput;

    @FindBy(id="username")
    private WebElement usernameInput;

    @FindBy(id="password")
    private WebElement passwordInput;

    @FindBy(id="confirmPassword")
    private WebElement confirmPasswordInput;

    @FindBy(css="button[type=\"submit\"]")
    private WebElement registrationButton;

    @FindBy(className="mat-simple-snackbar")
    private WebElement snackBarMessage;

    public static String MESSAGE_REGISTER = "Successful registration.";

    public static String MESSAGE_USER_EXISTS = "User with that username already exist.";

    public static String MESSAGE_EMPTY_FIELDS = "You must fill in all the fields.";

    public WebElement getFirstNameInput() {
        return firstNameInput;
    }

    public WebElement getLastNameInput() {
        return lastNameInput;
    }

    public WebElement getUsernameInput() {
        return usernameInput;
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public WebElement getConfirmPasswordInput() {
        return confirmPasswordInput;
    }

    public WebElement getRegistrationButton() {
        return registrationButton;
    }

    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }

    public void ensureSnackBarIsPresent(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(snackBarMessage,msgTxt));
    }
}
