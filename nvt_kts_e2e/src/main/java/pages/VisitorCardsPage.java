package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class VisitorCardsPage {

    private WebDriver webDriver;

    @FindBy(id="quickBuyButton")
    private WebElement quickBuyButton;

    @FindBy(id="cardTypeSelect")
    private WebElement cardTypeSelect;

    @FindBy(id="zoneSelect")
    private WebElement zoneSelect;

    @FindBy(id="vehicleSelect")
    private WebElement vehicleSelect;

    @FindBy(id="buyCardButton")
    private WebElement buyCardButton;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement message;

    @FindBy(css="button[aria-label=\"Previous page\"]")
    private WebElement previousPageButton;

    @FindBy(css="button[aria-label=\"Next page\"]")
    private WebElement nextPageButton;

    @FindBy(xpath="//div[@class='mat-paginator-range-label']")
    private WebElement numElemsDiv;

    @FindBy(css="mat-select[aria-label=\"Items per page:\"]")
    private WebElement elementsPerPageSelect;

    @FindBy(css="tbody > tr")
    private List<WebElement> tableRows;

    public static String MESSAGE_BOUGHT = "Card bought.";

    public VisitorCardsPage(WebDriver wbDriver){
        this.webDriver = wbDriver;
    }

    public WebElement getElementsPerPageSelect() {
        return elementsPerPageSelect;
    }

    public WebElement getQuickBuyButton() {
        return quickBuyButton;
    }

    public WebElement getCardTypeSelect() {
        return cardTypeSelect;
    }

    public WebElement getZoneSelect() {
        return zoneSelect;
    }

    public WebElement getVehicleSelect() {
        return vehicleSelect;
    }

    public WebElement getBuyCardButton() {
        return buyCardButton;
    }

    public WebElement getMessage() {
        return message;
    }

    public void fillQuickBuyForm(){
        cardTypeSelect.sendKeys("DAILY");
        vehicleSelect.sendKeys("TROLLEY");
        zoneSelect.sendKeys("TWO");
    }

    public int getNumCardsOnPage(){
        return tableRows.size();
    }

    public void ensureBuyButtonIsEnabled(){
        (new WebDriverWait(webDriver,300)).until(ExpectedConditions.elementToBeClickable(buyCardButton));
    }

    public void ensureCardBought(){
        (new WebDriverWait(webDriver,10)).until(ExpectedConditions.textToBePresentInElement(message,MESSAGE_BOUGHT));
    }

    public WebElement getPreviousPageButton() {
        return previousPageButton;
    }

    public WebElement getNextPageButton() {
        return nextPageButton;
    }

    private String getNumElemsLabel(){
        String pageText = numElemsDiv.getText();
        if(pageText.equals("0 of 0")){
            return "0 - 0 of 0";
        }
        return pageText;
    }

    public int getTotalNumOfCards(){
        String pageText = getNumElemsLabel();
        return Integer.parseInt(pageText.split("of")[1].trim());
    }

    public int getFromNumCards(){
        String pageText = getNumElemsLabel();
        return Integer.parseInt(pageText.split("-")[0].trim());
    }

    public int getToNumberCards(){
        String pageText = getNumElemsLabel();
        return Integer.parseInt(pageText.split("of")[0].split("-")[1].trim());
    }

}
