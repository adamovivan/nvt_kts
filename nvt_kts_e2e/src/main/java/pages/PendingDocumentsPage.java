package pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PendingDocumentsPage {

    public static final String CARD_ID_1 = "card1";
    public static final String CARD_ID_2 = "card2";

    @FindBy(id="pendingDocs")
    private WebElement pendingDocs;

    @FindBy(id="approveBtn1")
    private WebElement approveBtn1;

    @FindBy(id="rejectBtn2")
    private WebElement rejectBtn2;

    @FindBy(id=CARD_ID_1)
    private WebElement card1;

    @FindBy(id=CARD_ID_2)
    private WebElement card2;

    @FindBy(id="noPendingElText")
    private WebElement statusText;

    private WebDriver driver;

    public PendingDocumentsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getApproveBtn1() {
        return approveBtn1;
    }

    public void setApproveBtn1(WebElement approveBtn1) {
        this.approveBtn1 = approveBtn1;
    }

    public WebElement getRejectBtn2() {
        return rejectBtn2;
    }

    public void setRejectBtn2(WebElement rejectBtn2) {
        this.rejectBtn2 = rejectBtn2;
    }

    public WebElement getCard1() {
        return card1;
    }

    public void setCard1(WebElement card1) {
        this.card1 = card1;
    }

    public WebElement getCard2() {
        return card2;
    }

    public void setCard2(WebElement card2) {
        this.card2 = card2;
    }

    public WebElement getStatusText() {
        return statusText;
    }

    public void setStatusText(WebElement statusText) {
        this.statusText = statusText;
    }

    public WebElement getPendingDocs() {
        return pendingDocs;
    }

    public void setPendingDocs(WebElement pendingDocs) {
        this.pendingDocs = pendingDocs;
    }

    public boolean approveButtonIsPresent() {
        try{(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(approveBtn1));}
        catch (TimeoutException e){ return false;}
        return true;
    }

    public boolean rejectButtonIsPresent() {
        try{(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(rejectBtn2));}
        catch (TimeoutException e){ return false;}
        return true;
    }

    public void ensureStatusTextIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(statusText));
    }

    public boolean pendingDocsIsPresent() {
        try{(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(pendingDocs));}
        catch (TimeoutException e){ return false;}
        return true;
    }

}
