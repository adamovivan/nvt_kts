package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddPathPage {

    private WebDriver webDriver;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-add-path/div/div[1]/app-map/agm-map/div[1]/div[1]/div/div[1]/div[3]")
    private WebElement mapDiv;

    @FindBy(xpath = "//*[@id=\"mat-input-0\"]")
    private WebElement pathNameInput;

    @FindBy(xpath = "//mat-select[@placeholder=\"Vehicle type\"]/div/div[1]")
    private WebElement vehicleTypeInput;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-add-path/div/div[2]/mat-card/mat-card-content/form/div/button[1]")
    private WebElement addButton;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-add-path/div/div[2]/mat-card/mat-card-content/form/div/button[2]")
    private WebElement resetButton;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement toastMessage;

    public WebElement getMapDiv() {
        return mapDiv;
    }

    public WebElement getPathNameInput() {
        return pathNameInput;
    }

    public WebElement getVehicleTypeSelect() {
        return vehicleTypeInput;
    }

    public WebElement getAddButton() {
        return addButton;
    }

    public WebElement getResetButton() {
        return resetButton;
    }

    public WebElement getToastMessage() {
        return toastMessage;
    }
}
