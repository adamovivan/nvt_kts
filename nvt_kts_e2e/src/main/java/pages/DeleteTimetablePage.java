package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DeleteTimetablePage {

    @FindBy(id="timetableBtn")
    private WebElement timetableBtn;

    @FindBy(id="deleteTimetableBtn")
    private WebElement deleteTimetableBtn;

    @FindBy(id="Schedule")
    private WebElement scheduleSelect;

    @FindBy(id="deleteButton")
    private WebElement deleteButton;

    @FindBy(xpath="//span[contains(text(),'1B / BUS -> Tue Jan 01 2019 - Fri Feb 01 2019')]")
    private WebElement optionBus;

    @FindBy(xpath="//span[contains(text(),'1M / METROTRAIN -> Tue Jan 01 2019 - Fri Feb 01 20')]")
    private WebElement optionMetrotrain;

    @FindBy(className="mat-simple-snackbar")
    private WebElement snackBarMessage;

    public static String MESSAGE_SUCCESSFULLY_DELETED = "Schedule deleted successfully.";

    public static String MESSAGE_DOES_NOT_EXIST = "Schedule duration with that path name and day does no exist.";

    public static String MESSAGE_EMPTY_FIELD = "You must choose a schedule.";

    public WebElement getTimetableBtn() {
        return timetableBtn;
    }

    public WebElement getDeleteTimetableBtn() {
        return deleteTimetableBtn;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getScheduleSelect() {
        return scheduleSelect;
    }

    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }


    public void clickOnBus(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(optionBus,msgTxt));
        optionBus.click();
    }

    public void clickOnMetrotrain(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(optionMetrotrain,msgTxt));
        optionMetrotrain.click();
    }

    public void ensureSnackBarIsPresent(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(snackBarMessage,msgTxt));
    }
}
