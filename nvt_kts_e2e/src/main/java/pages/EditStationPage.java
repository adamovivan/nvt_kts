package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EditStationPage {

    private WebDriver webDriver;

    public static String MESSAGE_SUCCESS = "Station successfully added!";
    public static String MESSAGE_NO_MAP_SELECT = "Please select station location on map!";

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[1]/app-map/agm-map/div[1]/div/div/div[1]/div[3]")
    private WebElement mapDiv;

//    @FindBy(xpath="//img[@src='https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2.png']")
//    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[1]/app-map/agm-map/div[1]/div/div/div[1]/div[3]/div/div[3]/div[*]/img")
    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[1]/app-map/agm-map/div[1]/div/div/div[1]/div[3]/div/div[3]/div[*]")
    private List<WebElement> stationMarkers;

    @FindBy(xpath = "//*[@id=\"mat-input-3\"]")
    private WebElement nameInput;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[2]/mat-card/mat-card-content/form/div/button[1]")
    private WebElement saveButton;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[2]/mat-card/mat-card-content/form/div/button[2]")
    private WebElement deleteButton;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement toastMessage;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-station/div/div[1]/app-map/agm-map/div[1]/div[2]/table/tr/td[2]/button")
    private WebElement dismissButton;

    public List<WebElement> getStationMarkers() {
        return stationMarkers;
    }

    public WebElement getNameInput() {
        return nameInput;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getToastMessage() {
        return toastMessage;
    }

    public WebElement getDismissButton() {
        return dismissButton;
    }

    public WebElement getMapDiv() {
        return mapDiv;
    }
}
