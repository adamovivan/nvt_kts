package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ViewTimetablePage {

    @FindBy(id = "timetableBtn")
    private WebElement timetableBtn;

    @FindBy(id = "viewTimetableBtn")
    private WebElement viewTimetableBtn;

    @FindBy(id = "day")
    private WebElement daySelect;

    @FindBy(id = "lines")
    private WebElement linesSelect;

    @FindBy(id = "vehicleTypes")
    private WebElement vehicleType;

    @FindBy(id = "viewButton")
    private WebElement viewBtn;

    @FindBy(className = "mat-simple-snackbar")
    private WebElement snackBarMessage;

    @FindBy(id = "dates")
    private WebElement datesSelect;

    @FindBy(className = "timetableCard")
    private WebElement timetableCard;

    public static String MESSAGE_EMPTY_FIELDS = "You must fill in all the fields.";

    public static String MESSAGE_DOES_NOT_EXIST = "For entered criteria schedule does not exist.";

    public WebElement getTimetableBtn() {
        return timetableBtn;
    }

    public WebElement getViewTimetableBtn() {
        return viewTimetableBtn;
    }

    public WebElement getDaySelect() {
        return daySelect;
    }

    public WebElement getLinesSelect() {
        return linesSelect;
    }

    public WebElement getVehicleType() {
        return vehicleType;
    }

    public WebElement getViewBtn() {
        return viewBtn;
    }

    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }

    public WebElement getDatesSelect() {
        return datesSelect;
    }

    public WebElement getTimetableCard() {
        return timetableCard;
    }

    public void waitForDates(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(datesSelect));
    }

    public void waitForCard(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(timetableCard));
    }

    public void waitSnackBar(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(snackBarMessage));
    }
}
