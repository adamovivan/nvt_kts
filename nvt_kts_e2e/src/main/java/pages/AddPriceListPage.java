package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AddPriceListPage {

    private WebDriver webDriver;

    public static String MESSAGE_SUCCESS = "Price list added successfully.";
    public static String MESSAGE_OVERLAP = "An error occurred: Date ranges of price lists cannot overlap.";

    @FindBy(css = "input[placeholder=\"Start date\"]")
    private WebElement startDate;

    @FindBy(css = "input[placeholder=\"End date\"]")
    private WebElement endDate;

    @FindBy(id="addPriceListButton")
    private WebElement addPriceListButton;

    @FindBy(css="input[type=\"number\"]")
    private List<WebElement> modifiers;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement message;

    public AddPriceListPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public WebElement getStartDate() {
        return startDate;
    }

    public WebElement getEndDate() {
        return endDate;
    }

    public WebElement getAddPriceListButton() {
        return addPriceListButton;
    }

    public List<WebElement> getModifiers() {
        return modifiers;
    }

    public WebElement getMessage() {
        return message;
    }

    public void ensurePriceListAdded(){
        (new WebDriverWait(webDriver,10)).until(ExpectedConditions.textToBePresentInElement(message,MESSAGE_SUCCESS));
    }

    public void ensurePriceListOverlap(){
        (new WebDriverWait(webDriver,10)).until(ExpectedConditions.textToBePresentInElement(message,MESSAGE_OVERLAP));
    }
}
