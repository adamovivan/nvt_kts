package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    @FindBy(id="btnPending")
    private WebElement pendingButton;

    @FindBy(id="btnCards")
    private WebElement cardsButton;

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getPendingButton() {
        return pendingButton;
    }

    public void setPendingButton(WebElement pendingButton) {
        this.pendingButton = pendingButton;
    }

    public WebElement getCardsButton() {
        return cardsButton;
    }

    public void setCardsButton(WebElement cardsButton) {
        this.cardsButton = cardsButton;
    }

    public void ensurePendingButtonIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(pendingButton));
    }

    public void ensureCardsButtonIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(cardsButton));
    }

}
