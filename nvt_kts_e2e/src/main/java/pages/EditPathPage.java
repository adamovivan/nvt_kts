package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditPathPage {

    private WebDriver webDriver;

    @FindBy(xpath = "//mat-select[@placeholder=\"Lines\"]/div/div[1]")
    private WebElement lineSelect;

    @FindBy(xpath = "//mat-select[@placeholder=\"Vehicle type\"]/div/div[1]")
    private WebElement vehicleTypeSelect;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-path/div/div[2]/mat-card/mat-card-content/form/div/button[1]")
    private WebElement saveButton;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-edit-path/div/div[2]/mat-card/mat-card-content/form/div/button[2]")
    private WebElement deleteButton;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement toastMessage;

    public WebElement getLineSelect() {
        return lineSelect;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public WebElement getToastMessage() {
        return toastMessage;
    }

    public WebElement getVehicleTypeSelect() {
        return vehicleTypeSelect;
    }
}
