package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CardsPage {

    public static final String SHOW_FILTER_BTN = "showFilterBtn";
    public static final String FILTER_OPTIONS = "filterOptions";

    @FindBy(id = SHOW_FILTER_BTN)
    private WebElement showFilterButton;

    @FindBy(id = FILTER_OPTIONS)
    private WebElement filterOptions;

    @FindBy(id = "btnFilter")
    private WebElement filterButton;

    @FindBy(id = "inputUsername")
    private WebElement inputUsername;

    @FindBy(id = "selectZone")
    private WebElement selectZone;

    @FindBy(id = "selectActive")
    private WebElement selectActive;

    @FindBy(id = "selectCardType")
    private WebElement selectCardType;

    @FindBy(id = "selectVehicleType")
    private WebElement selectVehicleType;

    @FindBy(id = "priceFrom")
    private WebElement priceFrom;

    @FindBy(id = "priceTo")
    private WebElement priceTo;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-cards/div/table/tbody/*")
    private List<WebElement> tableRows;

    @FindBy(css="mat-select[aria-label=\"Items per page:\"]")
    private WebElement pageNum;

    private WebDriver driver;

    public CardsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getShowFilterButton() {
        return showFilterButton;
    }

    public void setShowFilterButton(WebElement showFilterButton) {
        this.showFilterButton = showFilterButton;
    }

    public WebElement getFilterOptions() {
        return filterOptions;
    }

    public void setFilterOptions(WebElement filterOptions) {
        this.filterOptions = filterOptions;
    }

    public List<WebElement> getTableRows() {
        return tableRows;
    }

    public void setTableRows(List<WebElement> tableRows) {
        this.tableRows = tableRows;
    }

    public WebElement getFilterButton() {
        return filterButton;
    }

    public void setFilterButton(WebElement filterButton) {
        this.filterButton = filterButton;
    }

    public WebElement getInputUsername() {
        return inputUsername;
    }

    public void setInputUsername(WebElement inputUsername) {
        this.inputUsername = inputUsername;
    }

    public WebElement getSelectZone() {
        return selectZone;
    }

    public void setSelectZone(WebElement selectZone) {
        this.selectZone = selectZone;
    }

    public WebElement getSelectVehicleType() {
        return selectVehicleType;
    }

    public void setSelectVehicleType(WebElement selectVehicleType) {
        this.selectVehicleType = selectVehicleType;
    }

    public WebElement getPageNum() {
        return pageNum;
    }

    public void setPageNum(WebElement pageNum) {
        this.pageNum = pageNum;
    }

    public void ensureUsernameIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(inputUsername));
    }

    public void ensureShowFilterButtonIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(showFilterButton));
    }

    public void ensureFilterOptionsIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(filterOptions));
    }

    public WebElement getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(WebElement priceFrom) {
        this.priceFrom = priceFrom;
    }

    public WebElement getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(WebElement priceTo) {
        this.priceTo = priceTo;
    }

    public void ensurePriceToIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(priceTo));
    }
    public void ensurePriceFromIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(priceFrom));
    }

    public void ensureActiveIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(selectActive));
    }

    public void ensureCardTypeIsPresent() {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(selectCardType));
    }

    public WebElement getSelectActive() {
        return selectActive;
    }

    public void setSelectActive(WebElement selectActive) {
        this.selectActive = selectActive;
    }

    public WebElement getSelectCardType() {
        return selectCardType;
    }

    public void setSelectCardType(WebElement selectCardType) {
        this.selectCardType = selectCardType;
    }
}
