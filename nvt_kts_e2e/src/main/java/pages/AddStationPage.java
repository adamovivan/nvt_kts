package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddStationPage {

    private WebDriver webDriver;

    public static String MESSAGE_SUCCESS = "Station successfully added!";
    public static String MESSAGE_NO_MAP_SELECT = "Please select station location on map!";

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-add-station/div/div[1]/app-map/agm-map/div[1]/div/div/div[1]/div[3]")
    private WebElement mapDiv;

    @FindBy(xpath = "//*[@id=\"mat-input-0\"]")
    private WebElement stationNameInput;

    @FindBy(xpath = "/html/body/app-root/app-sidenav/mat-sidenav-container/mat-sidenav-content/app-add-station/div/div[2]/mat-card/mat-card-actions/button")
    private WebElement submitButton;

    @FindBy(css="div[class=\"cdk-live-announcer-element cdk-visually-hidden\"]")
    private WebElement toastMessage;

    public WebElement getMapDiv() {
        return mapDiv;
    }

    public WebElement getStationNameInput() {
        return stationNameInput;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public WebElement getToastMessage() {
        return toastMessage;
    }
}
