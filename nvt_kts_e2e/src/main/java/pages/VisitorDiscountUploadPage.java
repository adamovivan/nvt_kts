package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class VisitorDiscountUploadPage {

    @FindBy(id="docUploadButton")
    private WebElement docUploadButton;

    @FindBy(xpath="//div[@class='mat-form-field-infix']")
    private WebElement visitorType;

    @FindBy(xpath="//span[contains(text(),'Student')]")
    private WebElement firstSelectStudent;

    @FindBy(id="document")
    private WebElement document;

    @FindBy(id="uploadDocumentButton")
    private WebElement uploadDocumentButton;

    @FindBy(className="mat-simple-snackbar")
    private WebElement snackBarMessage;

    public static String MESSAGE_SUCCESSFULLY_UPLOADED = "Uploaded.";

    public static String MESSAGE_SOMETHING_WENT_WRONG = "Something went wrong";

    public static String MESSAGE_EMPTY_FIELDS = "You must fill in all the fields.";

    public WebElement getDocUploadButton() {
        return docUploadButton;
    }

    public WebElement getVisitorType() {
        return visitorType;
    }

    public WebElement getDocument() {
        return document;
    }

    public WebElement getUploadDocumentButton() {
        return uploadDocumentButton;
    }

    public WebElement getFirstSelectStudent() {
        return firstSelectStudent;
    }

    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }

    public void clickOnFirstOption(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(firstSelectStudent));
        firstSelectStudent.click();
    }

    public void waitSnackBar(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(snackBarMessage));
    }
}
