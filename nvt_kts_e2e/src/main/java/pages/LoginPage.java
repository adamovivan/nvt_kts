package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    private WebDriver webDriver;

    @FindBy(css="input[ng-reflect-placeholder=\"Username\"]")
    private WebElement usernameField;

    @FindBy(css = "input[ng-reflect-placeholder=\"Password\"]")
    private WebElement passwordField;

    @FindBy(css="button[type=\"submit\"]")
    private WebElement loginButton;

    public LoginPage(){
        this.webDriver = webDriver;
    }

    public WebElement getUsernameField() {
        return usernameField;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }
}
