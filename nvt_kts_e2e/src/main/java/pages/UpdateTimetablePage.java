package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UpdateTimetablePage {

    @FindBy(id="timetableBtn")
    private WebElement timetableBtn;

    @FindBy(id="updateTimetableBtn")
    private WebElement updateTimetableBtn;

    @FindBy(id="Schedule")
    private WebElement scheduleSelect;

    @FindBy(id="timesTo")
    private WebElement timesTo;

    @FindBy(id="timesFrom")
    private WebElement timesFrom;

    @FindBy(id="updateButton")
    private WebElement updateButton;

    @FindBy(xpath="//mat-card-title[@class='center mat-card-title']")
    private WebElement title;

    @FindBy(xpath="//span[contains(text(),'1B / BUS -> Tue Jan 01 2019 - Fri Feb 01 2019')]")
    private WebElement option1;

    @FindBy(className="mat-simple-snackbar")
    private WebElement snackBarMessage;

    public static String MESSAGE_SUCCESSFULLY_UPDATED = "Schedule duration updated successfully.";

    public static String MESSAGE_EMPTY_FIELDS_TIME = "You must enter from and to times.";

    public WebElement getTimetableBtn() {
        return timetableBtn;
    }

    public WebElement getUpdateTimetableBtn() {
        return updateTimetableBtn;
    }

    public WebElement getScheduleSelect() {
        return scheduleSelect;
    }

    public WebElement getTimesTo() {
        return timesTo;
    }

    public WebElement getTimesFrom() {
        return timesFrom;
    }

    public WebElement getUpdateButton() {
        return updateButton;
    }

    public WebElement getSnackBarMessage() {
        return snackBarMessage;
    }

    public WebElement getTitle() {
        return title;
    }

    public void clickOnFirstOption(WebDriver driver, String msgTxt){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.textToBePresentInElement(option1,msgTxt));
        option1.click();
    }

    public void waitSnackBar(WebDriver driver){
        (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(snackBarMessage));
    }
}
