package util;

public class Constants {

    public final static String BASE_URL  = "http://localhost:4200";
    public final static long WAIT_TIME_MS = 3500; //cekanje izmedju nekih akcija
    public final static long RE_RENDER_WAIT_TIME_MS = 1000; // angular page re-render

    public final static long EXPLICIT_WAIT_TIME_SEC = 5; //eksplicitno cekanje
    public final static long IMPLICIT_WAIT_TIME_SEC = 5;

}
