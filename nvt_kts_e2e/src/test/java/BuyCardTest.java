import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import pages.VisitorCardsPage;
import util.Constants;

public class BuyCardTest {

    private VisitorCardsPage visitorCardsPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("mika123","12345");
        visitorCardsPage = PageFactory.initElements(Common.driver, VisitorCardsPage.class);
    }

    @Test
    public void buyCardTest() throws Exception{
        Thread.sleep(Constants.WAIT_TIME_MS);
        Common.driver.navigate().to(Constants.BASE_URL+"/myCards");
        visitorCardsPage.getQuickBuyButton().click();
        visitorCardsPage.fillQuickBuyForm();
        visitorCardsPage.ensureBuyButtonIsEnabled();
        visitorCardsPage.getBuyCardButton().click();
        visitorCardsPage.ensureCardBought();
        Assert.assertTrue(visitorCardsPage.getMessage().getText().equals(VisitorCardsPage.MESSAGE_BOUGHT));
        Thread.sleep(Constants.WAIT_TIME_MS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
