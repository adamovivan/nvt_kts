import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pages.CardsPage;
import pages.HomePage;


public class CardsTest {

    private CardsPage cardsPage;
    private HomePage homePage;

    @Before
    public void init(){
        Common.init();
        Common.setupSeleniumAndLogIn("pera123", "12345");
        cardsPage = PageFactory.initElements(Common.driver, CardsPage.class);
        homePage = PageFactory.initElements(Common.driver, HomePage.class);
    }

    @Test
    public void filterVisibility() throws InterruptedException {
        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        // visible
        Assert.assertTrue(Common.isElementPresent(By.id(CardsPage.FILTER_OPTIONS)));
    }

    @Test
    public void filterTestZoneUsername() throws InterruptedException {
        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        cardsPage.ensureUsernameIsPresent();
        cardsPage.getInputUsername().sendKeys("ana.hello");
        cardsPage.getSelectZone().sendKeys("One");
        cardsPage.getFilterButton().click();

        Thread.sleep(500);
        Assert.assertEquals(4, cardsPage.getTableRows().size());

    }

    @Test
    public void filterTestZoneUsernameVehicleType() throws InterruptedException {

        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        cardsPage.ensureUsernameIsPresent();
        cardsPage.getInputUsername().sendKeys("ana.hello");
        cardsPage.getSelectZone().sendKeys("One");
        cardsPage.getSelectVehicleType().sendKeys("Bus");
        cardsPage.getFilterButton().click();

        Thread.sleep(500);
        Assert.assertEquals(1, cardsPage.getTableRows().size());

        cardsPage.getShowFilterButton().click();

        // invisible
        Assert.assertFalse(Common.isElementPresent(By.id(CardsPage.FILTER_OPTIONS)));
    }

    @Test
    public void filterPriceTo() throws InterruptedException {

        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        cardsPage.ensurePriceToIsPresent();
        cardsPage.getPriceFrom().sendKeys("100");
        cardsPage.getFilterButton().click();

        Thread.sleep(500);
        Assert.assertEquals(10, cardsPage.getTableRows().size());
    }

    @Test
    public void filterPriceFrom() throws InterruptedException {

        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        cardsPage.ensurePriceFromIsPresent();
        cardsPage.getPriceFrom().sendKeys("12000");
        cardsPage.getFilterButton().click();

        Thread.sleep(500);
        Assert.assertEquals(5, cardsPage.getTableRows().size());
    }

    @Test
    public void filterCardTypeVehicleTypeActive() throws InterruptedException {

        homePage.ensureCardsButtonIsPresent();
        homePage.getCardsButton().click();

        cardsPage.ensureShowFilterButtonIsPresent();
        cardsPage.getShowFilterButton().click();

        cardsPage.ensureUsernameIsPresent();
        cardsPage.getSelectCardType().sendKeys("Daily");
        cardsPage.getSelectActive().sendKeys("No");
        cardsPage.getSelectVehicleType().sendKeys("Bus");
        cardsPage.getFilterButton().click();

        Thread.sleep(500);
        Assert.assertEquals(7, cardsPage.getTableRows().size());
    }

    @After
    public void closeSelenium() {
            Common.driver.quit();
        }

}
