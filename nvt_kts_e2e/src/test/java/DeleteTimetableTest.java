import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.DeleteTimetablePage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class DeleteTimetableTest {

    private DeleteTimetablePage deleteTimetablePage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        deleteTimetablePage = PageFactory.initElements(Common.driver, DeleteTimetablePage.class);
        Common.setupSeleniumAndLogIn("pera123","12345");
    }

    @Test
    public void deleteTimetable() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        deleteTimetablePage.getTimetableBtn().click();
        deleteTimetablePage.getDeleteTimetableBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        deleteTimetablePage.getScheduleSelect().click();
        deleteTimetablePage.clickOnMetrotrain(Common.driver, "1M / METROTRAIN -> Tue Jan 01 2019 - Fri Feb 01 20");

        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        deleteTimetablePage.getDeleteButton().click();
        deleteTimetablePage.ensureSnackBarIsPresent(Common.driver, DeleteTimetablePage.MESSAGE_SUCCESSFULLY_DELETED);

        Assert.assertTrue(deleteTimetablePage.getSnackBarMessage().getText().contains(DeleteTimetablePage.MESSAGE_SUCCESSFULLY_DELETED));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @Test
    public void emptyField() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        deleteTimetablePage.getTimetableBtn().click();
        deleteTimetablePage.getDeleteTimetableBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        deleteTimetablePage.getDeleteButton().click();
        deleteTimetablePage.ensureSnackBarIsPresent(Common.driver, DeleteTimetablePage.MESSAGE_EMPTY_FIELD);

        Assert.assertTrue(deleteTimetablePage.getSnackBarMessage().getText().contains(DeleteTimetablePage.MESSAGE_EMPTY_FIELD));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
