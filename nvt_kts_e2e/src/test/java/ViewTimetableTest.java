import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pages.ViewTimetablePage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class ViewTimetableTest {

    private ViewTimetablePage viewTimetablePage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        viewTimetablePage = PageFactory.initElements(Common.driver, ViewTimetablePage.class);
        Common.setupSeleniumAndLogIn("pera123", "12345");
    }

    private void viewTimetable(String day, String pathName, String vehicleType) {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        viewTimetablePage.getTimetableBtn().click();
        viewTimetablePage.getViewTimetableBtn().click();

        viewTimetablePage.getDaySelect().click();
        Common.driver.findElement(By.xpath("//span[contains(text(),'" + day + "')]")).click();

        viewTimetablePage.getLinesSelect().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Common.driver.findElement(By.xpath("//span[contains(text(),'" + pathName + "')]")).click();

        viewTimetablePage.getVehicleType().click();
        Common.driver.findElement(By.xpath("//span[@class='mat-option-text'][contains(text(),'" + vehicleType + "')]")).click();

        viewTimetablePage.getViewBtn().click();
    }

    private void successMessage() {
        viewTimetablePage.waitForDates(Common.driver);

        viewTimetablePage.getDatesSelect().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Common.driver.findElement(By.xpath("//span[contains(text(),'Sat Feb 02 2019 - Fri Mar 01 2019')]")).click();
        viewTimetablePage.waitForCard(Common.driver);

        Assert.assertTrue(viewTimetablePage.getTimetableCard().isDisplayed());
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    private void timetableDoesNotExistMessage() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        Assert.assertTrue(viewTimetablePage.getSnackBarMessage().getText().contains(ViewTimetablePage.MESSAGE_DOES_NOT_EXIST));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @Test
    public void success() {
        viewTimetable("Workday", "1B","Bus");
        successMessage();
    }

    @Test
    public void success_vehicleType_all() {
        viewTimetable("Workday", "1B", "All");
        successMessage();
    }

    @Test
    public void timetableDoesNotExist() {
        viewTimetable("Workday", "2B","Bus");
        timetableDoesNotExistMessage();
    }

    @Test
    public void timetableDoesNotExist_vehicle_all() {
        viewTimetable("Workday", "2B","All");
        timetableDoesNotExistMessage();
    }

    @Test
    public void empty_fields() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        viewTimetablePage.getTimetableBtn().click();
        viewTimetablePage.getViewTimetableBtn().click();
        viewTimetablePage.getViewBtn().click();

        viewTimetablePage.waitSnackBar(Common.driver);

        Assert.assertTrue(viewTimetablePage.getSnackBarMessage().getText().contains(ViewTimetablePage.MESSAGE_EMPTY_FIELDS));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
