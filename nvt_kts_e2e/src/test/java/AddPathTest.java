import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.AddPathPage;
import util.Constants;

public class AddPathTest {

    AddPathPage addPathPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123", "12345");
        addPathPage = PageFactory.initElements(Common.driver, AddPathPage.class);
    }

    @Test
    public void addPathTest() throws Exception {
        Thread.sleep(1000);
        Common.driver.navigate().to(Constants.BASE_URL + "/paths/add");
        Thread.sleep(4000);

        // Click on select
        Actions builder2 = new Actions(Common.driver);
        builder2.moveToElement(addPathPage.getVehicleTypeSelect(), 0, 0).click().build().perform();
        Thread.sleep(300);
        builder2.moveToElement(addPathPage.getVehicleTypeSelect(), 0, 60).click().build().perform();  // select option
        Thread.sleep(100);

        // Add press -> not selected on map
        addPathPage.getAddButton().click();
        Thread.sleep(300);

        // Check message
        Assert.assertTrue(addPathPage.getToastMessage().getText().toLowerCase().contains("please select"));

        // Map clicks
        Actions builder1 = new Actions(Common.driver);
        builder1.moveToElement(addPathPage.getMapDiv(), -10, -10).click().build().perform();
        builder1.moveToElement(addPathPage.getMapDiv(), 0, 0).click().build().perform();
        builder1.moveToElement(addPathPage.getMapDiv(), 50, 50).click().build().perform();
        builder1.moveToElement(addPathPage.getMapDiv(), 100, 100).click().build().perform();
        builder1.moveToElement(addPathPage.getMapDiv(), 120, 120).click().build().perform();

        // Set path name
        addPathPage.getPathNameInput().sendKeys("E2E path");

        // Check message
        // Add press -> success
        addPathPage.getAddButton().click();
        Thread.sleep(400);

        // Check message
        Assert.assertTrue(addPathPage.getToastMessage().getText().toLowerCase().contains("successfully"));
    }

}
