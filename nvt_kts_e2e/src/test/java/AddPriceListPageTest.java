import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pages.AddPriceListPage;
import pages.ShowPriceListPage;
import util.Constants;

import java.time.LocalDate;
import java.util.Random;

public class AddPriceListPageTest {


    private AddPriceListPage addPriceListPage;
    private ShowPriceListPage showPriceListPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123","12345");
        addPriceListPage = PageFactory.initElements(Common.driver,AddPriceListPage.class);
        showPriceListPage = PageFactory.initElements(Common.driver,ShowPriceListPage.class);
    }

    private LocalDate getLastAddedPricelistEndDate() throws Exception {
        Thread.sleep(Constants.WAIT_TIME_MS);
        Common.driver.navigate().to(Constants.BASE_URL+"/priceList/show");
        String dateStr = showPriceListPage.getLastAddedPriceListOptionText();

        if(dateStr.equals("")){
            return LocalDate.now();
        }

        String[] dateContents = dateStr.split("-")[1].trim().split("\\.");
        int dayOfMonth  = Integer.parseInt(dateContents[0]);
        int month = Integer.parseInt(dateContents[1]);
        int year = Integer.parseInt(dateContents[2]);
        return LocalDate.of(year,month,dayOfMonth);
    }

    @Test
    public void testAddPriceList() throws Exception{
        LocalDate startDate = getLastAddedPricelistEndDate().plusYears(1);
        Thread.sleep(Constants.WAIT_TIME_MS);
        Common.driver.navigate().to(Constants.BASE_URL+"/priceList/add");

        for (WebElement elem:addPriceListPage.getModifiers()){
            elem.clear();
            elem.sendKeys(Common.getRandomNumberInRange(1,3)+"");
        }


        addPriceListPage.getStartDate().sendKeys(startDate.getMonth().getValue()+"/"+startDate.getDayOfMonth()+"/"+startDate.getYear());
        LocalDate endDate = startDate.plusYears(1);
        addPriceListPage.getEndDate().sendKeys(endDate.getMonth().getValue()+"/"+endDate.getDayOfMonth()+"/"+endDate.getYear());

        addPriceListPage.getAddPriceListButton().click();
        addPriceListPage.ensurePriceListAdded();
        Assert.assertTrue(addPriceListPage.getMessage().getText().equals(AddPriceListPage.MESSAGE_SUCCESS));


        //
        Thread.sleep(Constants.WAIT_TIME_MS);
        Common.clearElement(addPriceListPage.getStartDate());
        Common.clearElement(addPriceListPage.getEndDate());
        //testiramo preklapanje
        addPriceListPage.getStartDate().sendKeys(startDate.getMonth().getValue()+"/"+startDate.getDayOfMonth()+"/"+startDate.getYear());
        endDate = startDate.plusMonths(6);
        addPriceListPage.getEndDate().sendKeys(endDate.getMonth().getValue()+"/"+endDate.getDayOfMonth()+"/"+endDate.getYear());

        Thread.sleep(Constants.WAIT_TIME_MS);
        addPriceListPage.getAddPriceListButton().click();
        addPriceListPage.ensurePriceListOverlap();
        Assert.assertTrue(addPriceListPage.getMessage().getText().equals(AddPriceListPage.MESSAGE_OVERLAP));

    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }



}
