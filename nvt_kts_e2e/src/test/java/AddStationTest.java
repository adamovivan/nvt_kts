import common.Common;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.AddStationPage;
import util.Constants;

public class AddStationTest {

    private AddStationPage addStationPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123","12345");
        addStationPage = PageFactory.initElements(Common.driver, AddStationPage.class);
    }

    @Test
    public void testAddStation() throws Exception{
        Thread.sleep(2000);
        Common.driver.navigate().to(Constants.BASE_URL+"/stations/add");
        Thread.sleep(2000);
        // Set station name
        addStationPage.getStationNameInput().sendKeys("Test1234");

        // Submit without name -> expect error toast
        addStationPage.getSubmitButton().click();
        Thread.sleep(500);

        Assert.assertTrue(addStationPage.getToastMessage().getText().toLowerCase().contains("please select station location"));

        // Click on map
        Actions builder = new Actions(Common.driver);
        builder.moveToElement(addStationPage.getMapDiv(), 50,50).click().build().perform();

        // Submit without name -> expect success
        addStationPage.getSubmitButton().click();
        Thread.sleep(500);

        Assert.assertTrue(addStationPage.getToastMessage().getText().toLowerCase().contains("successfully"));
    }


}
