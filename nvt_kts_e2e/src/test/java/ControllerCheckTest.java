import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import pages.ControllerCheckPage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class ControllerCheckTest {

    private ControllerCheckPage controllerCheckPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        controllerCheckPage = PageFactory.initElements(Common.driver, ControllerCheckPage.class);
        Common.setupSeleniumAndLogIn("maja123", "123");
    }

    @Test
    public void success() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        controllerCheckPage.getCheckBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        controllerCheckPage.getLinesSelect().click();

        controllerCheckPage.clickOnFirstOption(Common.driver,"1B");

        controllerCheckPage.waitForTable(Common.driver);

        Assert.assertTrue(controllerCheckPage.getTableCheck().isDisplayed());
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
