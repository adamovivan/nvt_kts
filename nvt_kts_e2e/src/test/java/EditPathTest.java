import common.Common;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.EditPathPage;
import util.Constants;

public class EditPathTest {

    private EditPathPage editPathPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123", "12345");
        editPathPage = PageFactory.initElements(Common.driver, EditPathPage.class);
    }

    @Test
    public void editLineTest() throws Exception {
        Thread.sleep(1000);
        Common.driver.navigate().to(Constants.BASE_URL + "/paths/edit");
        Thread.sleep(5000);

        // Select line
        Actions builder1 = new Actions(Common.driver);
        builder1.moveToElement(editPathPage.getLineSelect(), 0, 0).click().build().perform();
        builder1.moveToElement(editPathPage.getLineSelect(), 0, 50).click().build().perform();

        Thread.sleep(600);

        // Change vehicle type
        Actions builder2 = new Actions(Common.driver);
        builder2.moveToElement(editPathPage.getVehicleTypeSelect(), 0, 0).click().build().perform();
        builder2.moveToElement(editPathPage.getVehicleTypeSelect(), 0, 50).click().build().perform();

        // Save
        editPathPage.getSaveButton().click();
        Thread.sleep(200);

        // Check message
        Assert.assertTrue(editPathPage.getToastMessage().getText().toLowerCase().contains("saved successfully"));

        // Select line to delete
        Actions builder3 = new Actions(Common.driver);
        builder3.moveToElement(editPathPage.getLineSelect(), 0, 0).click().build().perform();
        builder3.moveToElement(editPathPage.getLineSelect(), 0, 50).click().build().perform();
        Thread.sleep(600);

        // Delete check
        editPathPage.getDeleteButton().click();
        Thread.sleep(200);

        // Check message
        Assert.assertTrue(editPathPage.getToastMessage().getText().toLowerCase().contains("deleted successfully"));
    }

}
