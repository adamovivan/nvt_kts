import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import pages.RegistrationPage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class RegistrationTest {

    private RegistrationPage registrationPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        registrationPage = PageFactory.initElements(Common.driver, RegistrationPage.class);
    }

    private void register(String firstName, String lastName, String username, String password, String confirmPassword, String message) {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
        Common.driver.navigate().to(Constants.BASE_URL+"/signup");

        registrationPage.getFirstNameInput().sendKeys(firstName);
        registrationPage.getLastNameInput().sendKeys(lastName);
        registrationPage.getUsernameInput().sendKeys(username);
        registrationPage.getPasswordInput().sendKeys(password);
        registrationPage.getConfirmPasswordInput().sendKeys(confirmPassword);
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        registrationPage.getRegistrationButton().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        registrationPage.ensureSnackBarIsPresent(Common.driver, message);

        Assert.assertTrue(registrationPage.getSnackBarMessage().getText().contains(message));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @Test
    public void success() {
       register("Marko", "Markovic", "marko123", "123456", "123456", RegistrationPage.MESSAGE_REGISTER);
    }

    @Test
    public void alreadyExists() {
        register("Marko", "Markovic", "pera123", "123456", "123456", RegistrationPage.MESSAGE_USER_EXISTS);
    }

    @Test
    public void emptyField() {
        register("", "", "", "", "", RegistrationPage.MESSAGE_EMPTY_FIELDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
