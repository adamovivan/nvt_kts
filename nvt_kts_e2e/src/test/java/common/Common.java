package common;

import org.junit.rules.ExpectedException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.LoginPage;
import util.Constants;

import java.util.Random;

public class Common {

    public static WebDriver driver;
    public static LoginPage loginPage;
    public static WebDriverWait wait;

    public static void init() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        wait = new WebDriverWait(driver, Constants.EXPLICIT_WAIT_TIME_SEC);
    }

    public static void setupSeleniumAndLogIn(String username, String password){
        driver.navigate().to(Constants.BASE_URL+"/login");

        loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.getUsernameField().sendKeys(username);
        loginPage.getPasswordField().sendKeys(password);
        loginPage.getLoginButton().click();
    }

    public static boolean isElementPresent(By by){
        try{
            driver.findElement(by);
            return true;
        }
        catch(NoSuchElementException e){
            return false;
        }
    }

    //ako webElement.clear() ne radi
    public static void clearElement(WebElement webElement){
        webElement.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
    }

    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;

    }
}
