import common.Common;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.EditStationPage;
import util.Constants;

public class EditStationTest {

    private EditStationPage editStationPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123","12345");
        editStationPage = PageFactory.initElements(Common.driver, EditStationPage.class);
    }

//    @Test
    public void editSaveTest() throws Exception{
        Thread.sleep(2000);
        Common.driver.navigate().to(Constants.BASE_URL+"/stations/edit");
        Thread.sleep(2000);

        // Dismiss button
//        editStationPage.getDismissButton().click();
//        Thread.sleep(100);

        // Get one marker
        WebElement marker = editStationPage.getStationMarkers().get(0);

        // Click on map to target marker
        Actions builder = new Actions(Common.driver);
        builder.moveToElement(editStationPage.getMapDiv(), 50,50).click().build().perform();
        Thread.sleep(200);

        // Alter station name
        editStationPage.getNameInput().sendKeys("E2E_test");

        // Save
        editStationPage.getSaveButton().click();
        Thread.sleep(200);

        // Check message
        Assert.assertTrue(editStationPage.getToastMessage().getText().toLowerCase().contains("successfully"));

    }

}
