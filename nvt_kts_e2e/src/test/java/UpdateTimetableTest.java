import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.UpdateTimetablePage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class UpdateTimetableTest {

    private UpdateTimetablePage updateTimetablePage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        updateTimetablePage = PageFactory.initElements(Common.driver, UpdateTimetablePage.class);
        Common.setupSeleniumAndLogIn("pera123","12345");
    }

    @Test
    public void updateTimetable() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        updateTimetablePage.getTimetableBtn().click();
        updateTimetablePage.getUpdateTimetableBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        updateTimetablePage.getScheduleSelect().click();
        updateTimetablePage.clickOnFirstOption(Common.driver,"1B / BUS -> Tue Jan 01 2019 - Fri Feb 01 2019")  ;

        updateTimetablePage.getTimesTo().click();
        updateTimetablePage.getTimesTo().sendKeys(Keys.chord(Keys.CONTROL, "a"));

        Actions action = new Actions(Common.driver);
        action.doubleClick(updateTimetablePage.getTitle()).perform();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        updateTimetablePage.getTimesFrom().click();
        updateTimetablePage.getTimesFrom().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        action.doubleClick(updateTimetablePage.getTitle()).perform();

        updateTimetablePage.getUpdateButton().click();
        updateTimetablePage.waitSnackBar(Common.driver);

        Assert.assertTrue(updateTimetablePage.getSnackBarMessage().getText().contains(UpdateTimetablePage.MESSAGE_SUCCESSFULLY_UPDATED));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @Test
    public void emptyFields() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        updateTimetablePage.getTimetableBtn().click();
        updateTimetablePage.getUpdateTimetableBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        updateTimetablePage.getScheduleSelect().click();
        updateTimetablePage.clickOnFirstOption(Common.driver,"1B / BUS -> Tue Jan 01 2019 - Fri Feb 01 2019");

        updateTimetablePage.getUpdateButton().click();
        updateTimetablePage.waitSnackBar(Common.driver);

        Assert.assertTrue(updateTimetablePage.getSnackBarMessage().getText().contains(UpdateTimetablePage.MESSAGE_EMPTY_FIELDS_TIME));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
