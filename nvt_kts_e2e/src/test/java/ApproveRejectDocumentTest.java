import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import pages.HomePage;
import pages.PendingDocumentsPage;
import util.Constants;

public class ApproveRejectDocumentTest {

    private PendingDocumentsPage pendingDocumentsPage;
    private HomePage homePage;

    @Before
    public void init(){
        Common.init();
        Common.setupSeleniumAndLogIn("janko123", "12345");
        pendingDocumentsPage = PageFactory.initElements(Common.driver, PendingDocumentsPage.class);
        homePage = PageFactory.initElements(Common.driver, HomePage.class);
    }

    @Test
    public void approveRejectTest() throws InterruptedException {
        homePage.ensurePendingButtonIsPresent();
        homePage.getPendingButton().click();

        if(pendingDocumentsPage.pendingDocsIsPresent()){

            // approve
            if(pendingDocumentsPage.approveButtonIsPresent()) {
                pendingDocumentsPage.getApproveBtn1().click();

                Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
                Assert.assertFalse(Common.isElementPresent(By.id(PendingDocumentsPage.CARD_ID_1)));
            }

            // reject
            if(pendingDocumentsPage.rejectButtonIsPresent()) {
                pendingDocumentsPage.getRejectBtn2().click();

                Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
                Assert.assertFalse(Common.isElementPresent(By.id(PendingDocumentsPage.CARD_ID_2)));
            }

        } else{
            pendingDocumentsPage.ensureStatusTextIsPresent();
            Assert.assertEquals(pendingDocumentsPage.getStatusText().getText(), "There is no pending documents");
        }

    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }
}
