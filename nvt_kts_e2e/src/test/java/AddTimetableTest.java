import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import pages.AddTimetablePage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class AddTimetableTest {

    private AddTimetablePage addTimetablePage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        addTimetablePage = PageFactory.initElements(Common.driver, AddTimetablePage.class);
        Common.setupSeleniumAndLogIn("pera123","12345");
    }

    private void addTimetable(String day, String pathName, String startDate, String endDate, String message) {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        addTimetablePage.getTimetableBtn().click();
        addTimetablePage.getAddTimetableBtn().click();

        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        addTimetablePage.getDaySelect().click();
        Common.driver.findElement(By.xpath(day)).click();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.getLinesSelect().click();
        Common.driver.findElement(By.xpath(pathName)).click();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.getDateFromPicker().sendKeys(startDate);
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        addTimetablePage.getDateToPicker().sendKeys(endDate);
        addTimetablePage.getTimesToSelect().click();
        addTimetablePage.getTimesToSelect().sendKeys(Keys.chord(Keys.CONTROL, "a"));

        Actions action = new Actions(Common.driver);
        action.doubleClick(addTimetablePage.getTitle()).perform();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.getTimesFromSelect().click();
        addTimetablePage.getTimesFromSelect().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        action.doubleClick(addTimetablePage.getTitle()).perform();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.getAddBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.ensureSnackBarIsPresent(Common.driver, message);

        Assert.assertTrue(addTimetablePage.getSnackBarMessage().getText().contains(message));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @Test
    public void success() {
        addTimetable("//span[contains(text(),'Workday')]", "//span[contains(text(),'1B')]", "09/09/2019", "10/10/2019", AddTimetablePage.MESSAGE_SUCCESSFULLY_ADDED);
    }

    @Test
    public void endDateBeforeStartDate() {
        addTimetable("//span[contains(text(),'Workday')]", "//span[contains(text(),'1B')]", "12/12/2020", "11/11/2020", AddTimetablePage.MESSAGE_WRONG_DATE);
    }

    @Test
    public void alreadyExists() {
        addTimetable("//span[contains(text(),'Workday')]", "//span[contains(text(),'1B')]", "09/09/2019", "10/10/2019", AddTimetablePage.MESSAGE_SCHEDULE_ALREADY_EXISTS);
    }

    @Test
    public void emptyFields() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);

        addTimetablePage.getTimetableBtn().click();
        addTimetablePage.getAddTimetableBtn().click();

        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        addTimetablePage.getAddBtn().click();
        Common.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        addTimetablePage.ensureSnackBarIsPresent(Common.driver, AddTimetablePage.MESSAGE_EMPTY_FIELDS);

        Assert.assertTrue(addTimetablePage.getSnackBarMessage().getText().contains(AddTimetablePage.MESSAGE_EMPTY_FIELDS));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.MILLISECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
