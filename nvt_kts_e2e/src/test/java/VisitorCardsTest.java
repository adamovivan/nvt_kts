import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import pages.VisitorCardsPage;
import util.Constants;

public class VisitorCardsTest {

    private VisitorCardsPage visitorCardsPage;

    @Before
    public void setup(){
        Common.init();
        Common.setupSeleniumAndLogIn("mika123","12345");
        visitorCardsPage = PageFactory.initElements(Common.driver, VisitorCardsPage.class);
    }

    @Test
    public void pageNavigateTest() throws Exception{

        Thread.sleep(Constants.WAIT_TIME_MS);
        Common.driver.navigate().to(Constants.BASE_URL+"/myCards");
        Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
        int totalCardsNum = visitorCardsPage.getTotalNumOfCards();
        int fromCardsNum = visitorCardsPage.getFromNumCards();
        int toCardsNum = visitorCardsPage.getToNumberCards();
        int numCardsPerPage = toCardsNum - fromCardsNum + 1;

        int numPages = 0;
        while(toCardsNum<totalCardsNum){
            visitorCardsPage.getNextPageButton().click();
            toCardsNum += numCardsPerPage;
            Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
            numPages++;
        }
        Assert.assertEquals(visitorCardsPage.getToNumberCards(),totalCardsNum); //da li je izlistao sve stranice
        while(numPages!=0){
            visitorCardsPage.getPreviousPageButton().click();
            Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
            numPages--;
        }

        int maxCardsPerPage = 40;
        Assert.assertEquals(visitorCardsPage.getFromNumCards(),fromCardsNum); //da li se vratio na prvu stranicu
        visitorCardsPage.getElementsPerPageSelect().sendKeys(maxCardsPerPage+"");
        Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
        int numCardsOnPage = visitorCardsPage.getNumCardsOnPage();
        Assert.assertTrue(numCardsOnPage==40 || numCardsOnPage==totalCardsNum);

    }


    @After
    public void closeSelenium(){
        Common.driver.close();
    }
}
