import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import pages.AddPriceListPage;
import pages.ShowPriceListPage;
import util.Constants;

import java.time.LocalDate;

public class EditPriceListPageTest {

    private AddPriceListPage addPriceListPage;
    private ShowPriceListPage showPriceListPage;

    @Before
    public void setupSeleniumAndLogIn() throws Exception {
        Common.init();
        Common.setupSeleniumAndLogIn("pera123","12345");
        addPriceListPage = PageFactory.initElements(Common.driver, AddPriceListPage.class);
        showPriceListPage = PageFactory.initElements(Common.driver,ShowPriceListPage.class);
        Thread.sleep(Constants.WAIT_TIME_MS); //jer cekanje ne radi :/
        Common.driver.navigate().to(Constants.BASE_URL+"/priceList/add");

        for (WebElement elem:addPriceListPage.getModifiers()){
            elem.clear();
            elem.sendKeys(Common.getRandomNumberInRange(1,3)+"");
        }

        LocalDate startDate = LocalDate.now().plusYears(5);
        addPriceListPage.getStartDate().sendKeys(startDate.getMonth().getValue()+"/"+startDate.getDayOfMonth()+"/"+startDate.getYear());
        LocalDate endDate = startDate.plusYears(1);
        addPriceListPage.getEndDate().sendKeys(endDate.getMonth().getValue()+"/"+endDate.getDayOfMonth()+"/"+endDate.getYear());

        addPriceListPage.getAddPriceListButton().click();

        Thread.sleep(Constants.WAIT_TIME_MS);
    }

    @Test
    public void editPriceListPageTest() throws Exception{

        Common.driver.navigate().to(Constants.BASE_URL+"/priceList/show");
        Thread.sleep(Constants.RE_RENDER_WAIT_TIME_MS);
        showPriceListPage.selectLastAddedPriceList();
        Thread.sleep(Constants.WAIT_TIME_MS);

        int randIndex = Common.getRandomNumberInRange(0,showPriceListPage.getEditPriceButtons().size()-1);
        WebElement editField = showPriceListPage.getEditPriceFields().get(randIndex);
        WebElement editButton = showPriceListPage.getEditPriceButtons().get(randIndex);

        Common.clearElement(editField);
        editField.sendKeys(Common.getRandomNumberInRange(500,1000)+"");
        editButton.click();
        showPriceListPage.ensureMessage(Common.driver,ShowPriceListPage.EDIT_SUCCESS);
        Assert.assertEquals(showPriceListPage.getMessage().getText(),ShowPriceListPage.EDIT_SUCCESS);
        Thread.sleep(Constants.WAIT_TIME_MS);


        Common.clearElement(editField);
        editButton.click();
        showPriceListPage.ensureMessage(Common.driver,ShowPriceListPage.EDIT_FAIL);
        Assert.assertEquals(showPriceListPage.getMessage().getText(),ShowPriceListPage.EDIT_FAIL);
        Thread.sleep(Constants.WAIT_TIME_MS);

        editField.sendKeys(Common.getRandomNumberInRange(-1000,-500)+"");
        editButton.click();
        showPriceListPage.ensureMessage(Common.driver,ShowPriceListPage.EDIT_NEGATIVE_FAIL);
        Assert.assertEquals(showPriceListPage.getMessage().getText(),ShowPriceListPage.EDIT_NEGATIVE_FAIL);
        Thread.sleep(Constants.WAIT_TIME_MS);
    }

    @After
    public void closeSelenium(){
        Common.driver.close();
    }
}
