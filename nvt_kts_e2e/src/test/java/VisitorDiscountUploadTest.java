import common.Common;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import pages.ControllerCheckPage;
import pages.VisitorDiscountUploadPage;
import util.Constants;

import java.util.concurrent.TimeUnit;

public class VisitorDiscountUploadTest {

    private VisitorDiscountUploadPage visitorDiscountUploadPage;

    @Before
    public void setupSeleniumAndLogIn() {
        Common.init();
        visitorDiscountUploadPage = PageFactory.initElements(Common.driver, VisitorDiscountUploadPage.class);
        Common.setupSeleniumAndLogIn("mika123", "12345");
    }

    @Test
    public void success() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getDocUploadButton().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getVisitorType().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        visitorDiscountUploadPage.clickOnFirstOption(Common.driver);

        visitorDiscountUploadPage.getDocument().sendKeys("C:\\Users\\Jelena\\Pictures\\EESTEC\\Zatvaranje.png");

        visitorDiscountUploadPage.getUploadDocumentButton().click();

        visitorDiscountUploadPage.waitSnackBar(Common.driver);

        Assert.assertTrue(visitorDiscountUploadPage.getSnackBarMessage().getText().contains(VisitorDiscountUploadPage.MESSAGE_SUCCESSFULLY_UPLOADED));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);
    }

    @Test
    public void empty_fields() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getDocUploadButton().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getUploadDocumentButton().click();

        visitorDiscountUploadPage.waitSnackBar(Common.driver);

        Assert.assertTrue(visitorDiscountUploadPage.getSnackBarMessage().getText().contains(VisitorDiscountUploadPage.MESSAGE_EMPTY_FIELDS));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);
    }

    @Test
    public void something_went_wrong() {
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getDocUploadButton().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        visitorDiscountUploadPage.getVisitorType().click();
        Common.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        visitorDiscountUploadPage.clickOnFirstOption(Common.driver);

        visitorDiscountUploadPage.getDocument().sendKeys("C:\\Users\\Jelena\\Desktop\\podaci.txt");

        visitorDiscountUploadPage.getUploadDocumentButton().click();

        visitorDiscountUploadPage.waitSnackBar(Common.driver);

        System.out.println(visitorDiscountUploadPage.getSnackBarMessage().getText());

        Assert.assertTrue(visitorDiscountUploadPage.getSnackBarMessage().getText().contains(VisitorDiscountUploadPage.MESSAGE_SOMETHING_WENT_WRONG));
        Common.driver.manage().timeouts().implicitlyWait(Constants.WAIT_TIME_MS, TimeUnit.SECONDS);
    }

    @After
    public void closeSelenium() {
        Common.driver.quit();
    }

}
