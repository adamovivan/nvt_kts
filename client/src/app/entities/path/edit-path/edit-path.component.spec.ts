import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MapService } from 'src/app/services/map/map.service';
import { PathService } from 'src/app/services/path/path.service';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { CommonsServiceMock } from 'src/app/services/mocks/commons.service.mock';
import { AgmCoreModule } from '@agm/core';
import { MapMockComponent } from 'src/app/components/mocks/map.component.mock';
import { VehicleServiceMock } from 'src/app/services/mocks/vehicle.service.mock';
import { MapServiceMock } from 'src/app/services/mocks/map.service.mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Path } from 'src/app/model/path.model';
import { EditPathComponent } from './edit-path.component';
import { MapData } from 'src/app/model/mapData.model';
import { of } from 'rxjs';
import { LineSelectMockComponent } from 'src/app/components/mocks/line-select.component.mock';

describe('EditPathComponent', () => {
  let component: EditPathComponent;
  let pathService: PathService;
  let commonService: any;
  let fixture: ComponentFixture<EditPathComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: PathService, useClass: PathServiceMock },
        { provide: MapService, useClass: MapServiceMock },
        { provide: VehicleService, useClass: VehicleServiceMock },
        { provide: CommonsService, useClass: CommonsServiceMock }
      ],
      declarations: [
        LineSelectMockComponent,
        MapMockComponent,
        EditPathComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    pathService = TestBed.get(PathService);
    commonService = TestBed.get(CommonsService);

    fixture = TestBed.createComponent(EditPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load selected path info into mapData', () => {
    // boilerplate code
    const lineNumber = '1A';
    const selectedPath = new Path();
    selectedPath.pathName = '1A';
    pathService.mapData = new MapData(new Path(), [], [], [], [], false);

    spyOn(pathService, 'getLineByNumber').and.returnValue(of(selectedPath));

    // call
    component.onLineSelect(lineNumber);

    // expectations
    expect(component.currentPath.pathName === selectedPath.pathName).toBeTruthy();
    expect(pathService.mapData.currentPath.coords.length === 0).toBeTruthy();
    expect(pathService.mapData.pathEditMarkers.length === 0).toBeTruthy();
    expect(pathService.mapData.stations.length === 0).toBeTruthy();
    expect(pathService.mapData.vehicles.length === 0).toBeTruthy();
    expect(pathService.mapData.stationClickable === false).toBeTruthy();
    expect(component.showForm).toBeTruthy();
  });

  it('should init component', () => {
    // boilerplate code
    spyOn(pathService, 'getLinesByCategory').and.returnValue(of(new Map<string, any[]>()));
    spyOn(commonService, 'resetLineControl').and.returnValue({});
    spyOn(commonService, 'updateLines').and.returnValue({});
    // call
    component.init();

    // expectations
    expect(pathService.getLinesByCategory).toHaveBeenCalled();
    expect(commonService.resetLineControl).toHaveBeenCalled();
    expect(commonService.updateLines).toHaveBeenCalled();
    expect(component.showForm).toBeFalsy();
  });

  it('should delete current path', () => {
    // boilerplate code
    spyOn(component, 'init').and.returnValue({});
    spyOn(commonService, 'openSnackBar').and.returnValue({});
    spyOn(pathService, 'deletePath').and.returnValue(of({}));

    component.currentPath = new Path();
    component.currentPath.id = 0;
    // call
    component.delete();

    // expectations
    expect(component.init).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('should call init', () => {
    // boilerplate code
    spyOn(component, 'init').and.returnValue({});
    // call
    component.reset();

    // expectations
    expect(component.init).toHaveBeenCalled();
  });

  it('should submit path data', () => {
    // boilerplate code
    component.currentPath = new Path();
    spyOn(component, 'init').and.returnValue({});
    spyOn(pathService, 'updatePath').and.returnValue(of({}));
    spyOn(commonService, 'openSnackBar').and.returnValue({});

    // call
    component.submit();

    // expectations
    expect(component.init).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });


});
