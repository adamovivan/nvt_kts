import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PathService } from 'src/app/services/path/path.service';
import { Path } from 'src/app/model/path.model';
import { CommonsService } from 'src/app/services/commons/commons.service';

@Component({
  selector: 'app-edit-path',
  templateUrl: './edit-path.component.html',
  styleUrls: ['./edit-path.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditPathComponent implements OnInit {

  form = new FormGroup({
    'vehicleNumber': new FormControl(null),
    'vehicleType': new FormControl(null),
    'zone': new FormControl(null)
  });
  showForm = false;
  vehicleTypes: string[] = this.pathService.vehicleTypes;
  zones: string[] = this.pathService.zones;
  currentPath: Path;
  constructor(private pathService: PathService, private commons: CommonsService) {
  }

  ngOnInit() {
    this.init();
  }

  onLineSelect(lineNumber: string) {
    this.pathService.getLineByNumber(lineNumber).subscribe(
      (path: Path) => {
        console.log('Path got:', path);
        this.currentPath = path;
        // coords
        this.pathService.mapData.currentPath.coords.length = 0;
        this.pathService.mapData.currentPath.coords.push(...path.coords);
        // markers
        this.pathService.mapData.pathEditMarkers.length = 0;
        this.pathService.mapData.pathEditMarkers.push(...path.coords);
        // stations
        this.pathService.mapData.stations.length = 0;
        this.pathService.mapData.stations.push(...path.stations);
        // vehicles
        this.pathService.mapData.vehicles.length = 0;
        // this.pathService.mapData.vehicles.push(...path.vehicles);
        // flags
        this.pathService.mapData.stationClickable = false;
        // set formControls
        this.form.patchValue({
          vehicleType: path.vehicleType,
          zone: path.zone,
          vehicleNumber: path.vehicles.length
        });
        this.showForm = true;
      }
    );
  }

  init() {
    this.pathService.getLinesByCategory().subscribe();
    this.commons.resetLineControl();
    this.commons.updateLines();
    this.showForm = false;
  }

  delete() {
    this.pathService.deletePath(this.currentPath.id).subscribe(
      () => {
        this.init();
        this.commons.openSnackBar('Path deleted successfully!', 'Ok');
      }
    );
  }

  reset() {
    this.init();
  }

  submit() {
    console.log('To submit:', this.currentPath);
    this.currentPath.zone = this.form.get('zone').value;
    this.currentPath.vehicleType = this.form.get('vehicleType').value;

    this.pathService.updatePath(this.currentPath, this.form.get('vehicleNumber').value)
      .subscribe(
        response => {
          console.log('RETponse: ', response);
          this.commons.openSnackBar('Path saved successfully!', 'Great!');
        }
      );
    this.init();
  }

}
