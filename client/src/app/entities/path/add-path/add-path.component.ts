import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { PathService } from 'src/app/services/path/path.service';
import { StationService } from 'src/app/services/station/station.service';
import { Station } from 'src/app/model/station.model';
import { Path } from 'src/app/model/path.model';
import { Coord } from 'src/app/model/coord.model';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-path',
  templateUrl: './add-path.component.html',
  styleUrls: ['./add-path.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddPathComponent implements OnInit, OnDestroy {

  currentPath: Path;
  pathSet = false;

  form = new FormGroup({
    'lineName': new FormControl('', [Validators.required]),
    'vehicleNumber': new FormControl(0),
    'vehicleType': new FormControl(''),
    'zone': new FormControl('ONE')
  });

  vehicleTypes: string[] = ['BUS', 'TROLLEY', 'METROTRAIN'];
  zones: string[] = ['ONE', 'TWO', 'THREE'];

  constructor(
    private pathService: PathService,
    private stationService: StationService,
    private commons: CommonsService) {
    this.currentPath = this.pathService.mapData.currentPath;
    this.pathService.mapData.stationClickable = true;
    this.stationService.loadAllStations();
    this.pathService.clearPathCoords();
  }

  ngOnInit() {
    // link for easier use
    this.currentPath = this.pathService.mapData.currentPath;
    // map settings

    this.stationService.loadAllStations();
    this.pathService.clearPathCoords();
  }

  init() {
    this.pathService.clearPathCoords();
  }

  add() {
    // check form
    if (!this.pathSet || this.form.status !== 'VALID') {
      this.commons.openSnackBar('Please select path on map and set line name.', 'Close');
    } else {
      this.currentPath.pathName = this.form.get('lineName').value;
      this.currentPath.vehicleType = this.form.get('vehicleType').value;
      this.pathService.initVehicles(this.currentPath, this.form.get('vehicleNumber').value);
      this.currentPath.zone = this.form.get('zone').value;

      console.log('Path submit check:', this.currentPath);
      // sent rest
      this.pathService.savePath(this.currentPath).subscribe(
        response => {
          console.log('RETponse: ', response);
          this.commons.openSnackBar('Path successfully added!', 'Great!');
        }
      );

      this.form.reset();
      this.init();
    }
  }

  reset() {
    console.log('Form check:', this.form.value, 'status:', this.form.status);
    this.form.reset();
  }

  mapClick(event) {
    this.currentPath = this.pathService.mapData.currentPath;
    this.pathSet = true;
    this.currentPath.coords.push(new Coord(event.coords.lng, event.coords.lat));
  }

  stationClick(station: Station) {
    this.currentPath = this.pathService.mapData.currentPath;
    this.currentPath.coords.push(new Coord(station.coord.e, station.coord.n));
    this.currentPath.stations.push(station);
    // console.log('Path check:', this.currentPath)
  }

  ngOnDestroy() {
    // remove coordinates from map view
    this.pathService.clearPathCoords();
  }

}
