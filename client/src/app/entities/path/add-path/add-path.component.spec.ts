import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MapService } from 'src/app/services/map/map.service';
import { PathService } from 'src/app/services/path/path.service';
import { StationServiceMock } from 'src/app/services/mocks/station.service.mock';
import { StationService } from 'src/app/services/station/station.service';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { AddPathComponent } from './add-path.component';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { CommonsServiceMock } from 'src/app/services/mocks/commons.service.mock';
import { AgmCoreModule } from '@agm/core';
import { MapMockComponent } from 'src/app/components/mocks/map.component.mock';
import { VehicleServiceMock } from 'src/app/services/mocks/vehicle.service.mock';
import { MapServiceMock } from 'src/app/services/mocks/map.service.mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Coord } from 'src/app/model/coord.model';
import { Station } from 'src/app/model/station.model';
import { Path } from 'src/app/model/path.model';

describe('AddPathComponent', () => {
  let component: AddPathComponent;
  let pathService: any;
  let stationService: any;
  let commonService: any;
  let fixture: ComponentFixture<AddPathComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: PathService, useClass: PathServiceMock },
        { provide: MapService, useClass: MapServiceMock },
        { provide: StationService, useClass: StationServiceMock },
        { provide: VehicleService, useClass: VehicleServiceMock },
        { provide: CommonsService, useClass: CommonsServiceMock }
      ],
      declarations: [
        MapMockComponent,
        AddPathComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    pathService = TestBed.get(PathService);
    stationService = TestBed.get(StationService);
    commonService = TestBed.get(CommonsService);

    fixture = TestBed.createComponent(AddPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create', () => {
    spyOn(component, 'init').and.returnValue({});

    component.ngOnInit();

    expect(component.init).toHaveBeenCalled();
  });

  it('addPath init test', async(() => {
    spyOn(stationService, 'loadAllStations').and.returnValue(Promise.resolve());
    spyOn(pathService, 'clearPathCoords').and.returnValue(Promise.resolve());

    component.init();

    expect(pathService.mapData.stationClickable).toBeTruthy();
    expect(stationService.loadAllStations).toHaveBeenCalled();
    expect(pathService.clearPathCoords).toHaveBeenCalled();
  }));

  it('addPath add test (path not selected)', async(() => {
    spyOn(commonService, 'openSnackBar').and.returnValue(Promise.resolve());
    component.pathSet = false;

    component.add();

    expect(commonService.openSnackBar).toHaveBeenCalled();
  }));

  it('should reset add path form', async(() => {
    spyOn(component.form, 'reset').and.returnValue(Promise.resolve());

    component.reset();

    expect(component.form.reset).toHaveBeenCalled();
  }));

  it('should set path with click', async(() => {
    const event = { coords: new Coord(10, 20) };

    component.mapClick(event);

    expect(component.pathSet).toBeTruthy();
  }));

  it('should add station to path coords', async(() => {
    const station = new Station('test');
    station.coord = new Coord(10, 20);
    // clear current path
    component.currentPath = new Path();

    component.stationClick(station);

    expect(component.currentPath.coords.length === 1).toBeTruthy();
    expect(component.currentPath.coords[0].e = station.coord.e);
    expect(component.currentPath.coords[0].n = station.coord.n);
    expect(component.currentPath.stations.length === 1).toBeTruthy();
    expect(component.currentPath.stations[0].name === station.name).toBeTruthy();
  }));

  it('ngOnDestry test', async(() => {
    spyOn(pathService, 'clearPathCoords').and.returnValue({});

    component.ngOnDestroy();

    expect(pathService.clearPathCoords).toHaveBeenCalled();
  }));

  // impossible to set form control status [program]
  // it('addPath add test [path selected]', async(() => {
  //   component.pathSet = true;
  //   let formGroup = new FormGroup({});
  //   formGroup.setErrors({ 'incorrect': false })
  //   //spyOnProperty(component.form, 'status', 'get').and.returnValue('VALID')

  //   spyOn(component.form, 'reset').and.returnValue({});
  //   spyOn(component, 'init').and.returnValue({});
  //   // spyOn(pathService, 'savePath').and.returnValue(true);

  //   component.add();

  //   expect(component.form.reset).toHaveBeenCalled();
  //   expect(component.init).toHaveBeenCalled();
  // }));



});
