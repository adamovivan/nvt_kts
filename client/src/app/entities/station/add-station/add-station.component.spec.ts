import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MapService } from 'src/app/services/map/map.service';
import { StationService } from 'src/app/services/station/station.service';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { CommonsServiceMock } from 'src/app/services/mocks/commons.service.mock';
import { AgmCoreModule } from '@agm/core';
import { MapMockComponent } from 'src/app/components/mocks/map.component.mock';
import { MapServiceMock } from 'src/app/services/mocks/map.service.mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddStationComponent } from './add-station.component';
import { StationServiceMock } from 'src/app/services/mocks/station.service.mock';
import { Coord } from 'src/app/model/coord.model';

describe('AddStationComponent', () => {
  let component: AddStationComponent;
  let stationService: StationService;
  let commonService: CommonsService;
  let fixture: ComponentFixture<AddStationComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: MapService, useClass: MapServiceMock },
        { provide: CommonsService, useClass: CommonsServiceMock },
        { provide: StationService, useClass: StationServiceMock },
      ],
      declarations: [
        MapMockComponent,
        AddStationComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    stationService = TestBed.get(StationService);
    commonService = TestBed.get(CommonsService);

    fixture = TestBed.createComponent(AddStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set station marker', () => {
    // boilerplate code
    spyOn(stationService, 'setStationMarker').and.returnValue({});
    component.stationCoordSet = false;
    // call
    component.mapClick({ 'coords': new Coord(10, 20) });

    // expectations
    expect(component.stationCoordSet).toBeTruthy();
    expect(stationService.setStationMarker).toHaveBeenCalled();
  });

  it('should submit station [coords not set]', () => {
    // boilerplate code
    component.stationCoordSet = false;
    spyOn(commonService, 'openSnackBar').and.returnValue({});
    // call
    component.submit();

    // expectations
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('should submit station [coords set]', () => {
    // boilerplate code
    component.stationCoordSet = true;
    spyOn(stationService, 'addStation').and.returnValue({});
    spyOn(stationService, 'clearStationMarkers').and.returnValue({});
    spyOn(component.name, 'reset').and.returnValue({});
    // call
    component.submit();

    // expectations
    expect(component.stationCoordSet).toBeFalsy();
    expect(stationService.addStation).toHaveBeenCalled();
    expect(component.name.reset).toHaveBeenCalled();
    expect(stationService.clearStationMarkers).toHaveBeenCalled();
  });

});
