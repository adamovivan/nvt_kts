import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StationService } from 'src/app/services/station/station.service';
import { Coord } from 'src/app/model/coord.model';
import { FormControl, Validators } from '@angular/forms';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { Station } from 'src/app/model/station.model';


@Component({
  selector: 'app-add-station',
  templateUrl: './add-station.component.html',
  styleUrls: ['./add-station.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddStationComponent implements OnInit {

  stationCoordSet = false;
  name = new FormControl(null, [Validators.required]);

  constructor(private stationService: StationService, private commons: CommonsService) { }

  ngOnInit() {
  }

  mapClick(event: any) {
    this.stationCoordSet = true;
    this.stationService.setStationMarker(new Coord(event.coords.lng, event.coords.lat));
  }

  submit() {
    if (!this.stationCoordSet) {
      this.commons.openSnackBar('Please select station location on map!', 'Close');
      return;
    }
    this.stationService.addStation(new Station(this.name.value));
    this.name.reset();
    this.stationCoordSet = false;
    this.stationService.clearStationMarkers();
  }

}
