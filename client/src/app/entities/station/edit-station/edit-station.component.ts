import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Station } from 'src/app/model/station.model';
import { StationService } from 'src/app/services/station/station.service';
import { Path } from 'src/app/model/path.model';
import { PathService } from 'src/app/services/path/path.service';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { CommonsService } from 'src/app/services/commons/commons.service';

@Component({
  selector: 'app-edit-station',
  templateUrl: './edit-station.component.html',
  styleUrls: ['./edit-station.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditStationComponent implements OnInit {
  currentStation: Station = new Station('');
  displayForm = false;
  allPaths: Path[] = [];

  form: FormGroup;

  constructor(
    private stationService: StationService,
    private pathService: PathService,
    private commons: CommonsService) { }

  ngOnInit() {
    this.form = new FormGroup({
      'controlList': new FormArray([])
    });
    this.initData();
  }

  initData() {
    this.form.setControl('controlList', new FormArray([]));
    this.stationService.loadAllStations();
    this.pathService.getAllLines().subscribe(
      (data: Path[]) => {
        this.allPaths = data;
        this.allPaths.forEach(e => { this.addControl(e); });
      });
    this.displayForm = false;
  }

  addControl(e: Path) {
    const control = new FormControl(undefined);
    control['data'] = e;
    (<FormArray>this.form.get('controlList')).push(control);
  }

  stationClick(station: Station) {
    // console.log('[' + station.name + ']: ', station.coord.n.toFixed(5) + ' , ' + station.coord.e.toFixed(5))
    this.displayForm = true;
    this.currentStation = station;  // load clicked station to (2-way bind)

    this.checkControls();
  }

  // FOR DB INPUT PURPOSE
  mapClickTest() {
    // console.log(event.coords.lat.toFixed(5) + ' , ' + event.coords.lng.toFixed(5));
  }


  checkControls() {
    // check / uncheck boxes
    (<FormArray>this.form.get('controlList')).controls.forEach(control => {
      control.setValue(this.isStationPath(control['data']));
    });
  }

  isStationPath(path: any) {
    if (this.currentStation.paths.some(e => e.id === path.id)) {
      return true;
    }
    return false;
  }

  delete() {
    this.stationService.deleteStation(this.currentStation.id).subscribe(
      () => {
        this.initData();
        this.commons.openSnackBar('Station successfully deleted!', 'Great!');
      }
    );
  }

  save(name: string) {
    const station = new Station(name);
    station.coord = this.currentStation.coord;
    station.id = this.currentStation.id;

    for (let i = 0; i < this.allPaths.length; i++) {
      if (this.form.value.controlList[i]) {
        station.paths.push(this.allPaths[i]);
      }
    }
    this.stationService.saveStation(station).subscribe(
      () => {
        this.initData();
        this.commons.openSnackBar('Station successfully saved!', 'Great!');
      }
    );
  }

}
