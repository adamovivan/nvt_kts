import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { StationService } from 'src/app/services/station/station.service';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { CommonsServiceMock } from 'src/app/services/mocks/commons.service.mock';
import { AgmCoreModule } from '@agm/core';
import { MapMockComponent } from 'src/app/components/mocks/map.component.mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StationServiceMock } from 'src/app/services/mocks/station.service.mock';
import { EditStationComponent } from './edit-station.component';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { PathService } from 'src/app/services/path/path.service';
import { of } from 'rxjs';
import { Station } from 'src/app/model/station.model';
import { Coord } from 'src/app/model/coord.model';

describe('EditStationComponent', () => {
  let component: EditStationComponent;
  let pathService: PathService;
  let stationService: StationService;
  let fixture: ComponentFixture<EditStationComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: PathService, useClass: PathServiceMock },
        { provide: CommonsService, useClass: CommonsServiceMock },
        { provide: StationService, useClass: StationServiceMock },
      ],
      declarations: [
        MapMockComponent,
        EditStationComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    stationService = TestBed.get(StationService);
    pathService = TestBed.get(PathService);

    fixture = TestBed.createComponent(EditStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call init', () => {
    // boilerplate code
    spyOn(component, 'initData').and.returnValue({});
    // call
    component.ngOnInit();

    // expectations
    expect(component.initData).toHaveBeenCalled();
  });

  it('should init component data', () => {
    // boilerplate code
    spyOn(stationService, 'loadAllStations').and.returnValue({});
    spyOn(pathService, 'getAllLines').and.returnValue(of(new Array(5)));
    spyOn(component, 'addControl').and.returnValue({});
    spyOn(component.form, 'setControl').and.returnValue({});
    // call
    component.initData();

    // expectations
    expect(stationService.loadAllStations).toHaveBeenCalled();
    expect(pathService.getAllLines).toHaveBeenCalled();
    expect(component.form.setControl).toHaveBeenCalled();
  });

  it('should log + load clicked station', () => {
    // boilerplate code
    const station = new Station('');
    station.coord = new Coord(10, 20);
    spyOn(component, 'checkControls').and.returnValue({});
    // call
    component.stationClick(station);

    // expectations
    expect(component.displayForm).toBeTruthy();
    expect(component.currentStation.name === station.name);
    expect(component.checkControls).toHaveBeenCalled();
  });

});
