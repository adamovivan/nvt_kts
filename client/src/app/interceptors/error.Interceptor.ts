import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { CommonsService } from '../services/commons/commons.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private commons: CommonsService) { }

    handleError(error: Error | HttpErrorResponse) {
        if (error instanceof HttpErrorResponse) {
            // Server or connection error happened
            if (!navigator.onLine) {
                // Handle offline error
                console.log('Offline error se desio!');
            } else {
                // Handle Http Error (error.status === 403, 404...)
                console.log('Greska status: ', error.status);
            }
        } else {
            // Handle Client Error (Angular Error, ReferenceError...)
            return; // SnackBar below shadows snackbars from client error handling code.
        }

        console.error('[Global] Error: ', error);
        this.commons.openSnackBar('Unexpected error occured, please try again later!', '');

    }
}
