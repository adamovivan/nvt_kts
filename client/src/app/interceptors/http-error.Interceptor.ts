import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { CommonsService } from '../services/commons/commons.service';
import { throwError } from 'rxjs';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor(private commons: CommonsService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).do(
            () => {
                // console.log('Some event!')
            }, (error: any) => {
                // console.log('Some error!')

                if (error instanceof HttpErrorResponse) {
                    if (error.error instanceof ErrorEvent) {
                        // A client-side or network error occurred. Handle it accordingly.
                        this.commons.openSnackBar('An error occurred:' + error.error.message + ' please try again later.', 'Close');
                    } else if (error.status >= 500 && error.status <= 511) {
                        // The backend returned an unsuccessful response code.
                        // The response body may contain clues as to what went wrong,
                        console.error(
                            `Backend returned code ${error.status}, ` +
                            `body was: ${error.error}`);
                        this.commons.openSnackBar('Server error ' + error.status + ' please try again later.', 'Close');
                    } else if (error.status === 403 || error.status === 400) {
                        // skip 403 and 400
                        return;
                    }
                    // return an observable with a user-facing error message
                    this.commons.openSnackBar('Unexpected error, some functions may not work!', 'Close');

                    return throwError('Something bad happened!');

                }
            });
    }
}
