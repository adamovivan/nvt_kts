import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Schedule } from '../../model/schedule.model';
import { JsogService } from 'jsog-typescript';
import 'rxjs/add/operator/map';
import { CommonsService } from '../commons/commons.service';

@Injectable()
export class TimetableService {

  day: string[] = ['Workday', 'Saturday', 'Sunday'];
  times: string[] = [];

  constructor(private http: HttpClient, private commonsService: CommonsService, private jsog: JsogService) { }

  getSchedule(day: string, pathName: string, vehicleType: string) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json'});
    const params = new HttpParams().set('pathName', pathName).set('day', day).set('vehicleType', vehicleType);

    return this.http.get(location['apiUrl'] + '/schedule', {headers: reqHeader, params: params}).map(
    (response: Response) => {
    if (response != null) {
          return response;
        }
      }
    );
  }

  getSchedules() {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json'});

    return this.http.get(location['apiUrl'] + '/schedule/all', {headers: reqHeader}).map(
    (response: any[]) => {
      if (response != null) {
          return response;
        }
      }
    );
  }

  getAllSchedule(day: string, pathName: string) {

    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/json'});
    const params = new HttpParams().set('pathName', pathName).set('day', day);

    return this.http.get(location['apiUrl'] + '/schedule/allTypes', {headers: reqHeader, params: params}).map(
    (response: Response) => {
      if (response != null) {
          return response;
        }
      }
    );
  }

  addSchedule(schedule: Schedule) {

    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post(location['apiUrl'] + '/schedule/add', this.jsog.serialize(schedule), {headers, responseType: 'text'})
    .subscribe((response) => {
      this.commonsService.openSnackBar(response, 'Ok'); },
      (err) => {
        this.commonsService.openSnackBar(err.error, 'Ok');
      });
  }

  updateSchedule(schedule: any) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    return this.http.put(location['apiUrl'] + '/schedule/update', this.jsog.serialize(schedule), {headers, responseType: 'text'})
    .subscribe((response) => {
      this.commonsService.openSnackBar(response, 'Ok'); },
      (err) => {
        this.commonsService.openSnackBar(err.error, 'Ok');
      });
  }

  deleteSchedule(schedule: any) {
    const headerData = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    const params = new HttpParams().set('pathName', schedule['pathName']).set('day', schedule['day']).
    set('vehicleType', schedule['vehicleType']).set('startDate', schedule['startDate']).set('endDate', schedule['endDate']);

    return this.http.delete(location['apiUrl'] + '/schedule/delete', {headers: headerData, responseType: 'text', params: params})
    .subscribe((response) => {
      this.commonsService.openSnackBar(response, 'Ok');
    },
      (err) => {
        this.commonsService.openSnackBar(err.error, 'Ok');
      });
  }

  generateTimes() {
    if (this.times.length === 0) {
      const now = new Date(Date.now());
      let t = '';

      for (let index = 0; index < 24; index++) {
        for (let i = 0; i < 60; i = i + 5) {
          now.setHours(index, i);

          if ( index < 10) {
          t = '0' + now.getHours() + ':';
          } else {
            t = now.getHours() + ':';
          }

          if (i < 10 ) {
            t = t + '0' + now.getMinutes();
          } else if (i === 60) {
            continue;
          } else {
            t = t + now.getMinutes();
          }
          this.times.push(t);
        }
      }
    }
    return this.times;
  }
}
