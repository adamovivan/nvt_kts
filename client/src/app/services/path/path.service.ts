import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { JsogService } from 'jsog-typescript';
import { MapData } from '../../model/mapData.model';
import { Path } from '../../model/path.model';
import { Vehicle } from '../../model/vehicle.model';
import { Coord } from '../../model/coord.model';
import { CommonsService } from '../commons/commons.service';

@Injectable({
    providedIn: 'root'
  })
export class PathService {

    constructor(private http: Http, private commons: CommonsService, private jsog: JsogService) {

     }

    mapData: MapData;
    vehicleTypes: string[] = ['BUS', 'TROLLEY', 'METROTRAIN'];
    zones: string[] = ['ONE', 'TWO', 'THREE'];

    setData(data: any) {
        this.mapData = data;
    }

    savePath(path: Path) {
        console.log('Body:', this.jsog.serialize(path));

        return this.http.post(location['apiUrl'] + '/lines', this.jsog.serialize(path)).pipe(map(
            (response: Response) => {
                return response;
            }
        ),
            // catchError(this.commons.handleError)
        );
    }

    updatePath(path: Path, vNumber: number) {
        const dto = {
            'path': this.jsog.serialize(path),
            'vehicleNumber': vNumber
        };

        console.log(dto);

        return this.http.post(location['apiUrl'] + '/lines/update', dto).pipe(map(
            (response: Response) => {
                return response;
            }
        ),
            // catchError(this.commons.handleError)
        );
    }

    deletePath(id: number) {
        return this.http.delete(location['apiUrl'] + '/lines/' + id).pipe(map(
            (response: Response) => {
                return response;
            }
        ));
    }

    getAllLineNumbers() {
        return this.http.get(location['apiUrl'] + '/lines/lineNumbers').pipe(map(
            (response: Response) => {
                const data = this.jsog.deserialize(response.json());
                this.mapData.allPathNumbers = <string[]>data;       // update path line numbers
                return data;
            }
        ));
    }

    getLineByNumber(lineNumber: string) {
        return this.http.get(location['apiUrl'] + '/lines/' + lineNumber).pipe(map(
            (response: Response) => {
                const data = this.jsog.deserialize(response.json());
                return data;
            }
        ),
        );
    }

    // NOT IN USE -> USE: getAllLineNumbers()
    getAllLines() {
        return this.http.get(location['apiUrl'] + '/lines').pipe(map(
            (response: Response) => {
                const data = this.jsog.deserialize(response.json());
                return data;
            }
        ),
        );
    }

    getLinesByCategory() {
        return this.http.get(location['apiUrl'] + '/lines/lineNumbers').pipe(map(
            (response: Response) => {
                const data = response.json();
                const m = new Map<string, any[]>();

                m.set('BUS', data['buses']);
                m.set('METROTRAIN', data['metroTrains']);
                m.set('TROLLEY', data['trolleys']);

                return m;
            }
        ),
        );
    }

    getPathById(id: string) {
        return this.http.get(location['apiUrl'] + '/lines' + id).pipe(map(
            (response: Response) => {
                return response.json();
            },
            (error: ErrorEvent) => {
                this.commons.openSnackBar(error.message, '');
            }
        ),
        );
    }

    initVehicles(path: Path, number: number) {
        for (let i = 0; i < number; i++) {
            const v = new Vehicle();
            v.coord = new Coord(0, 0);
            v.vehicleType = path.vehicleType;
            v.path = path;
            path.vehicles.push(v);
        }
    }

    clearPathCoords() {
        this.mapData.currentPath.coords.length = 0;
    }
}
