import { TestBed } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { fakeAsync, tick } from '@angular/core/testing';
import { PathService } from './path.service';
import { Path } from 'src/app/model/path.model';
import { CommonsService } from '../commons/commons.service';
import { CommonsServiceMock } from '../mocks/commons.service.mock';
import { JsogService } from 'jsog-typescript';

describe('PathService', () => {

    let pathService: PathService;
    let backend;
    let lastConnection;

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                { provide: ConnectionBackend, useClass: MockBackend },
                { provide: RequestOptions, useClass: BaseRequestOptions },
                { provide: CommonsService, useClass: CommonsServiceMock },
                JsogService,
                Http,
                PathService]
        });

        pathService = TestBed.get(PathService);
        backend = TestBed.get(ConnectionBackend);
        backend.connections.subscribe((connection: any) =>
            lastConnection = connection);

    });

    it('should pass simple test', () => {
        expect(true).toBe(true);
    });

    it('savePath() should query url and save a path', fakeAsync(() => {
        const path = new Path();
        let res: Path;
        path.id = 1;
        path.pathName = '1';


        pathService.savePath(path).subscribe((data: Response) => res = data.json());

        expect(lastConnection).toBeDefined('no http service connection at all?');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(
                {
                    id: 1,
                    pathName: '1'
                })
        })));
        tick();
        expect(path).toBeDefined();
        expect(res.id).toEqual(1);
        expect(res.pathName).toEqual('1');
    }));


    it('savePath() should query url and save a path', fakeAsync(() => {
        const path = new Path();
        let res: Path;
        path.id = 1;
        path.pathName = '1';


        pathService.savePath(path).subscribe((data: Response) => res = data.json());

        expect(lastConnection).toBeDefined('no http service connection at all?');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(
                {
                    id: 1,
                    pathName: '1'
                })
        })));
        tick();
        expect(path).toBeDefined();
        expect(res.id).toEqual(1);
        expect(res.pathName).toEqual('1');
    }));

    it('updatePath() should update path', fakeAsync(() => {
        const path = new Path();
        let res: Path;
        path.id = 1;
        path.pathName = '1';


        pathService.updatePath(path, 1).subscribe((data: Response) => res = data.json());

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Post, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(
                {
                    id: 1,
                    pathName: '1'
                })
        })));
        tick();
        expect(path).toBeDefined();
        expect(res.id).toEqual(1);
        expect(res.pathName).toEqual('1');
    }));

    it('deletePath() should query url and delete path', () => {
        pathService.deletePath(4);

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Delete, 'invalid http method');
    });

    it('getAllLineNumbers() should get all line numbers', fakeAsync(() => {
        const lineNumbers  = ['1', '2'];
        let res;

        pathService.setData({});
        pathService.getAllLineNumbers().subscribe((data: Response) => res = data);

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Get, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(
                {
                    res: ['1', '2']
                })
        })));
        tick();
        expect(res).toBeDefined();
        expect(res.res[0]).toEqual(lineNumbers[0]);
        expect(res.res[1]).toEqual(lineNumbers[1]);
    }));

    it('getLineByNumber() should get line by number', fakeAsync(() => {
        const lineNumber = '1';
        let res: Path = new Path();
        res.pathName = '1';

        pathService.getLineByNumber(lineNumber).subscribe((data: Path) => res = data);

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Get, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(res)
        })));
        tick();
        expect(res).toBeDefined();
        expect(res.pathName).toEqual(lineNumber);
    }));

    it('getLinesByCategory() should get lines by category', fakeAsync(() => {
        let res: Map<string, any[]> = new Map<string, any[]>();

        pathService.getLinesByCategory().subscribe((data: Map<string, any[]>) => res = data);

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Get, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(res)
        })));
        tick();
        expect(res).toBeDefined();
        expect(res instanceof Map).toBeTruthy();
    }));

    it('initVehicles() intialize Vehicles on Path', fakeAsync(() => {
        const path = new Path();
        path.vehicleType = 'BUS';
        const vehicleNum = 5;

        pathService.initVehicles(path, vehicleNum);

        expect(path).toBeDefined();
        expect(path.vehicles.length).toBeDefined();
        expect(path.vehicles.length === vehicleNum).toBeTruthy();
        expect(path.vehicles[0].vehicleType === path.vehicleType).toBeTruthy();
    }));

});
