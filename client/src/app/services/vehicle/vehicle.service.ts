import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Vehicle } from '../../model/vehicle.model';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Sockets
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { JsogService } from 'jsog-typescript';
import { CommonsService } from '../commons/commons.service';


@Injectable()
export class VehicleService {

    constructor(
        private http: HttpClient,
        private commons: CommonsService,
        private jsog: JsogService) { }

    // Vehicles stream
    private vehicleLocationsSource = new Subject<any[]>();
    vehicleLocationsStream$ = this.vehicleLocationsSource.asObservable();

    // Sockets
    public serverUrl = location['apiUrl'] + '/socket';
    public stompClient;

    initializeWebSocketConnection() {

        const ws = new SockJS(this.serverUrl);
        this.stompClient = Stomp.over(ws);
        this.stompClient.debug = null;
        const that = this;
        this.stompClient.connect({}, function () {
            that.stompClient.subscribe('/chat', (message) => {
                if (message.body) {
                    that.vehicleLocationsSource.next(that.jsog.deserializeArray(
                        JSON.parse(message.body), Vehicle));
                }
            });
        });

        this.getRealtimeLocations(-1).subscribe();
    }

    // no return (initiates/closes socket stream)
    getRealtimeLocations(pathId: number) {

        return this.http.get('http://localhost:8080/vehicles/realtime/' + pathId, this.commons.httpOptions).pipe(map(
            (response: Response) => {
                if (response != null) {
                    return response.json();
                }
            }
        ));
    }

}
