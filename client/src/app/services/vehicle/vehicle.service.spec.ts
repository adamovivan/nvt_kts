import { TestBed } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { CommonsServiceMock } from '../mocks/commons.service.mock';
import { JsogService } from 'jsog-typescript';
import { VehicleService } from './vehicle.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CommonsService } from '../commons/commons.service';

describe('StationService', () => {

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                { provide: ConnectionBackend, useClass: MockBackend },
                { provide: RequestOptions, useClass: BaseRequestOptions },
                { provide: CommonsService, useClass: CommonsServiceMock },
                JsogService,
                Http,
                HttpHandler,
                HttpClient,
                VehicleService]
        });

    });

    it('should pass simple test', () => {
        expect(true).toBe(true);
    });

});
