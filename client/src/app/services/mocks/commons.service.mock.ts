import { CommonsService } from '../commons/commons.service';

export class CommonsServiceMock extends CommonsService {

  constructor() {
    super(null);
  }

  openSnackBar() {
  }

}
