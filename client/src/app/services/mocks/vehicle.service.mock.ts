import { VehicleService } from '../vehicle/vehicle.service';

export class VehicleServiceMock extends VehicleService {

    constructor() {
        super(null, null, null);
    }

    initializeWebSocketConnection() {
    }
}
