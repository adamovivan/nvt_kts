import { CommonsService } from '../commons/commons.service';

export class CommonsServiceMocked extends CommonsService {

  constructor() {
    super(null);
  }

  public openSnackBar() {
  }
}
