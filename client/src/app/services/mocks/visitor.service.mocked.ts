import { VisitorService } from '../visitor/visitor.service';

export class VisitorServiceMocked extends VisitorService {

  constructor() {
    super(null, null, null);
  }
}
