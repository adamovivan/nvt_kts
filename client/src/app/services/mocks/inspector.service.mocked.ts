import { of } from 'rxjs';

export class InspectorServiceMocked  {

    constructor() { }

    getPendingDocuments() {
        const doc1 = { id: 1, visitorType: 'REGULAR', documentName: 'Licna karta'};
        const doc2 = { id: 2, visitorType: 'PENSIONER', documentName: 'Pasos'};
        const doc3 = { id: 3, visitorType: 'REGULAR', documentName: 'Identity card'};
        const doc4 = { id: 4, visitorType: 'STUDENT', documentName: 'Zdravstvena knjizica'};

        return of([doc1, doc2, doc3, doc4]);
    }

    getDocumentImage() {
        return of();
    }

    approveDocument() {
        return of({ id: 1, visitorType: 'REGULAR', documentName: 'Licna karta'});
    }

    rejectDocument() {
        return of({ id: 2, visitorType: 'PENSIONER', documentName: 'Pasos'});
    }

  }
