import {CardService} from '../card/card.service';
import {CardDTO} from '../../model/card.model';
import {Observable, of} from 'rxjs';

class MockResponse {

  value: string;
  constructor(val: string) {
    this.value = val;
  }
  public toString(): string {
    return this.value;
  }
}

class VisitorCardsResponse {
  content: CardDTO[] = [];
  totalElements: number;
}

export class CardServiceMocked extends CardService {

  private readonly allCards: CardDTO[];

  constructor() {
    super(null, null);
    this.allCards = [];
    for (let i = 0; i < 20; i++) {
      const cardDTO = new CardDTO();
      cardDTO.id = i;
      cardDTO.zone = 'ONE';
      cardDTO.vehicleType = 'BUS';
      cardDTO.active = true;
      cardDTO.startDate = '11.11.2019.';
      cardDTO.endDate = '11.11.2020.';
      cardDTO.price = 456;
      cardDTO.cardType = 'YEARLY';
      this.allCards.push(cardDTO);
    }

  }

  buyCard(cardDTO: CardDTO): Observable<object> {
    return of(cardDTO);
  }

  updateCurrentPrice(cardDTO: CardDTO): Observable<object> {
    if (!cardDTO) {
      return of(new MockResponse('Invalid object'));
    }
     const response = new MockResponse('456');
     return of(response);
  }

  getVisitorCards(pageOptions: any): Observable<object> {
    const pageIndex = pageOptions['page'];
    const pageSize = pageOptions['size'];
    console.log(' Page size: ' + pageSize);
    const retVal = new VisitorCardsResponse();
    retVal.totalElements = this.allCards.length;
    const startIndex = pageSize * pageIndex;
    for (let i = startIndex; i < startIndex + pageSize; i++) {
      retVal.content.push(this.allCards[i]);
    }
    return of(retVal);
  }

  getCards(_params) {
    const totalElements = 100;
    const cards = {'content': [], 'totalElements': totalElements};

    for ( let i = 0; i < totalElements; i++) {
      cards.content.push(new CardDTO());
    }

    return of(cards);
  }

}
