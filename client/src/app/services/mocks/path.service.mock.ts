import { PathService } from '../path/path.service';
import { MapData } from 'src/app/model/mapData.model';
import { Path } from 'src/app/model/path.model';
import { of } from 'rxjs';
import { Response, ResponseOptions } from '@angular/http';


export class PathServiceMock extends PathService {

  constructor() {
    super(null, null, null);
    super.setData(new MapData(new Path(), [], [], [], [], false));
  }

  getLinesByCategory() {
    return of(new Map<string, any[]>());
  }

  getLineByNumber() {
    return of(new Path());
  }

  getAllLines() {
    return of([]);
  }

  clearPathCoords() {
  }

  deletePath() {
    return of(new Response(new ResponseOptions()));
  }

  savePath() {
    return Response.bind({});
  }

}
