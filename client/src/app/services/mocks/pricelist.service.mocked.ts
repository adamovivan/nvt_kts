import {PricelistService} from '../pricelist/pricelist.service';
import {PriceListInfo} from '../../model/pricelistData.model';
import {Observable, of} from 'rxjs';

export class PricelistServiceMocked extends PricelistService {

  priceListInfos: PriceListInfo[] = [];

  constructor() {
    super(null, null);
    for (let i = 9; i > 0; i-- ) {
      const priceListInfo = new PriceListInfo();
      priceListInfo.id = i + 1;
      priceListInfo.editable = false;
      priceListInfo.startDate = '11.01.201' + i + '.';
      priceListInfo.endDate = '11.01.201' + (i - 1) + '.';
      this.priceListInfos.push(priceListInfo);
    }
  }

  addNewPriceList(priceListDTO: any): Observable<Object> {
    if (!priceListDTO) {
      return of({'message': 'Invalid object.'});
    }
    return of({});
  }

  getPriceListsInfo() {
    return of(this.priceListInfos);
  }

  getPriceListData(id: number, vehicleType: string) {
    const priceListTabData = [];
    for (const cardType of ['ONEDRIVE', 'DAILY', 'MONTHLY', 'YEARLY']) {
      for (const visitorType of ['REGULAR', 'STUDENT', 'PENSIONER']) {
          for (const zone of ['ONE', 'TWO', 'THREE']) {
            const priceListItem =  {};
            priceListItem['cardType'] = cardType;
            priceListItem['visitorType'] = visitorType;
            priceListItem['zone'] = zone;
            priceListItem['vehicleType'] = vehicleType;
            priceListItem['price'] = 55;
            priceListItem['priceListId'] = id;
            priceListTabData.push(priceListItem);
          }
      }
    }
    return of(priceListTabData);
  }

}
