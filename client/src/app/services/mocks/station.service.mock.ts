import { StationService } from '../station/station.service';
import { Subscription } from 'rxjs';

export class StationServiceMock extends StationService {

    constructor() {
        super(null, null, null);
    }

    loadAllStations() {
        return new Subscription();
    }
}
