import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { JsogService } from 'jsog-typescript';
import { CommonsService } from '../commons/commons.service';


@Injectable()
export class VisitorService {

  visitorTypes: string[] = ['REGULAR', 'STUDENT', 'PENSIONER'];

  constructor(private http: HttpClient,
    private commonsService: CommonsService,
    private jsog: JsogService) { }

  addVisitor(visitor: any) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json;');

    return this.http.post(location['apiUrl'] + '/register', this.jsog.serialize(visitor), { headers, responseType: 'text' })
      .subscribe((response) => {
        this.commonsService.openSnackBar(response, 'Ok');
      },
        (err) => {
          this.commonsService.openSnackBar(err.error, 'Ok');
        });
  }

  uploadDocument(formdata: FormData) {

    return this.http.post(location['apiUrl'] + '/uploadDocument', formdata, { responseType: 'json' })
      .subscribe((response: Response) => {

        this.commonsService.openSnackBar(response['message'], 'Ok');
      },
        () => {
          this.commonsService.openSnackBar('Something went wrong.', 'Ok');
        });
  }

}
