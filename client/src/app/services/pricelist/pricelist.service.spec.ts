import { TestBed } from '@angular/core/testing';
import {PricelistService} from './pricelist.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {CommonsService} from '../commons/commons.service';
import {CommonsServiceMocked} from '../mocks/commons.service.mocked';
import {PriceListInfo, PriceListItemData} from '../../model/pricelistData.model';

function createPriceListItemData() {
  const pricelistItemData: PriceListItemData = new PriceListItemData({'price': 455,
    'zone': 'ONE',
    'visitorType': 'REGULAR'});
  pricelistItemData.cardType = 'DAILY';
  pricelistItemData.priceListId = 1;
  pricelistItemData.vehicleType = 'BUS';
  return pricelistItemData;
}

function createDummyPriceListData(vehicleType: string) {
  const priceListTabData = [];
  for (const cardType of ['ONEDRIVE', 'DAILY', 'MONTHLY', 'YEARLY']) {
    for (const visitorType of ['REGULAR', 'STUDENT', 'PENSIONER']) {
      for (const zone of ['ONE', 'TWO', 'THREE']) {
        const priceListItem =  {};
        priceListItem['cardType'] = cardType;
        priceListItem['visitorType'] = visitorType;
        priceListItem['zone'] = zone;
        priceListItem['vehicleType'] = vehicleType;
        priceListItem['price'] = 55;
        priceListTabData.push(priceListItem);
      }
    }
  }
  return priceListTabData;
}

describe('PricelistService', () => {

  let service: PricelistService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers:    [
        HttpClient,
        PricelistService,
        {provide: CommonsService, useClass: CommonsServiceMocked},
      ]
    });

    service = TestBed.get(PricelistService);
    httpMock = TestBed.get(HttpTestingController);
  });



  it('addNewPriceList should query the right url', () => {
    const priceListDTO = {};
    service.addNewPriceList(priceListDTO).subscribe(() => {
    });
    const req = httpMock.expectOne({ method: 'POST'});
    expect(req.request.url).toEqual(location['apiUrl'] + service.baseURL);
  });

  it('should respond successfully', () => {

    const priceListDTO = {};
    const dummyResponse = {'message': 'Pricelist added successfully.', 'success': true};
    service.addNewPriceList(priceListDTO).subscribe((response) => {
      expect(response['message']).toEqual(dummyResponse['message']);
      expect(response['success']).toBeTruthy();
    });
    const req = httpMock.expectOne({ method: 'POST'});
    expect(req.request.url).toEqual(location['apiUrl'] + service.baseURL);
    req.flush(dummyResponse);
  });

  it('should return error', () => {

    const priceListDTO = {'startDate': '11/11/2021', 'endDate': '11/12/2021'};
    const dummyResponse = {'message': 'An error occured: Pricelist dates should not overlap.', 'success': false};
    service.addNewPriceList(priceListDTO).subscribe(() => {}, (err) => {
      expect(err['message']).toEqual(dummyResponse['message']);
      expect(err['success']).toBeFalsy();
    });

    const req = httpMock.expectOne({ method: 'POST'});
    expect(req.request.url).toEqual(location['apiUrl'] + service.baseURL);
    httpMock.expectNone(location['apiUrl'] + service.baseURL);
    req.flush(dummyResponse);
  });

  it('should return pricelists info observable ', () => {

    const prInfo: PriceListInfo = new PriceListInfo();
    prInfo.startDate = '11/11/2021';
    prInfo.endDate = '11/12/2021';
    prInfo.id = 1;
    prInfo.editable = true;

    service.getPriceListsInfo().subscribe((data) => {
      expect((<Array<PriceListInfo>>data).length).toBe(1);
      expect(data).toEqual([prInfo]);
    });
    const req = httpMock.expectOne(location['apiUrl'] + service.baseURL + '/all');
    expect(req.request.method).toBe('GET');
    req.flush([prInfo]);
  });

  it('should return priceList data for chosen vehicleType', () => {
    const vehicleType = 'BUS';
    const id = 1;
    const priceListTabData = createDummyPriceListData(vehicleType);
    service.getPriceListData(id, vehicleType).subscribe( (data) => {
      expect(data).toEqual(priceListTabData);
    });
    const req = httpMock.expectOne(location['apiUrl'] + service.baseURL + '/id/' + id + '/' + vehicleType);
    expect(req.request.method).toBe('GET');
    req.flush(priceListTabData);
  });

  it('should return price cannot be empty', () => {
    const priceListItemData: PriceListItemData = createPriceListItemData();
    priceListItemData.price = null;
    service.changePriceListItemPrice(priceListItemData).subscribe( (data) => {
      expect(data['message']).toEqual('Price field cannot be empty.');
      expect(data['success']).toBeFalsy();
    });

    httpMock.expectNone(location['apiUrl'] + service.baseURL + '/editPriceListItem');

  });

  it('should return that price field must be positive', () => {
    const priceListItemData: PriceListItemData = createPriceListItemData();
    priceListItemData.price = -45;
    service.changePriceListItemPrice(priceListItemData).subscribe( (data) => {
      expect(data['message']).toEqual('Please enter a positive number.');
      expect(data['success']).toBeFalsy();
    });

    httpMock.expectNone(location['apiUrl'] + service.baseURL + '/editPriceListItem');
  });

  it('should return Price changed successfully.', () => {

    const priceListItemData: PriceListItemData = createPriceListItemData();
    const dummyAnswer = {'message': 'Price changed successfully.', 'success': true};
    service.changePriceListItemPrice(priceListItemData).subscribe( (data) => {
      expect(data['message']).toEqual(dummyAnswer['message']);
      expect(data['success']).toBeTruthy();
    });
    const req = httpMock.expectOne(location['apiUrl'] + service.baseURL + '/editPriceListItem');
    expect(req.request.method).toEqual('PUT');
    req.flush(dummyAnswer);
  });

});
