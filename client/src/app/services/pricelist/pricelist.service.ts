import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/catch';
import {throwError, of} from 'rxjs';
import { PriceListItemData} from '../../model/pricelistData.model';
import { CommonsService } from '../commons/commons.service';

@Injectable()
export class PricelistService {

  baseURL = '/priceList';

  constructor(private http: HttpClient , private commonsService: CommonsService ) {

  }

  addNewPriceList(priceListDTO: any) {
      const headers = {'Content-Type' : 'application/json'};
      return this.http.post(location['apiUrl'] + this.baseURL, JSON.stringify(priceListDTO), {headers: headers })
        .catch((err: HttpErrorResponse) => {
          this.commonsService.openSnackBar('An error occurred: ' +  err.error['message'], 'Ok');
          return throwError(err.statusText);
        });

  }

    changePriceListItemPrice(priceListItem: PriceListItemData) {

    if (priceListItem.price == null || priceListItem.price === undefined) {
      return of({'message': 'Price field cannot be empty.', 'success': false});
    }
    if (priceListItem.price  <= 0 ) {
      return of({'message': 'Please enter a positive number.', 'success': false});
    }
      const headers = {'Content-Type' : 'application/json'};
      return this.http.put(location['apiUrl'] + this.baseURL + '/editPriceListItem', JSON.stringify(priceListItem), {headers: headers })
        .catch((err: HttpErrorResponse) => {
          this.commonsService.openSnackBar('An error occurred: ' +  err.error['message'], '');
          return throwError(err.statusText);
        });
   }

  getPriceListData(id: number, vehicleType: string) {

    if ( id == null || id === undefined) {
      return;
    }

    return this.http.get(location['apiUrl'] + this.baseURL + '/id/' + id + '/' + vehicleType)
      .catch((err: HttpErrorResponse) => {
        this.commonsService.openSnackBar('An error occurred: ' +  err.error, 'Ok');
        return throwError(err.statusText);
      });
  }

  getPriceListsInfo() {
    return this.http.get(location['apiUrl'] + this.baseURL + '/all')
      .catch((err: HttpErrorResponse) => {
        this.commonsService.openSnackBar('An error occurred: ' +  err.error, 'Ok');
        return throwError(err.statusText);
      });
  }


}
