import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('AuthenticationService', () => {

  const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkanVyYS5kanVyaWMiLCJjcmVhdGVkIjo5OTk5OTk5OSwicm9\
  sZXMiOlt7ImF1dGhvcml0eSI6IlJPTEVfVklTSVRPUiJ9XSwiZXhwIjoxMDAwMDAwMDAwfQ.kVvhvoTl0ujuS2uN9K4GkbZt\
  BCbjMd21-BpRLOcFJa5CKXCS8VQ_-qsa_-9_ElGxO34PGy72-SJoj89a5_BHJA';
  // 14.03.2255.
  const longerExpTimetoken = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkanVyYS5kanVyaWMiLCJjcmVhdGVkIjo5OTk5O\
  Tk5OSwicm9sZXMiOlt7ImF1dGhvcml0eSI6IlJPTEVfVklTSVRPUiJ9XSwiZXhwIjo5MDAwMDAwMDAwfQ.LcCx0uqFxfRO1Khk3\
  Y9iZrkvtz5L-Zi7un_EP32yOMc0ZArzkqnxdIVA7038haOIT4KrnDDFGKPRx8S0RB4z1w';
  const TOKEN_KEY = 'authToken';

  let service: AuthenticationService;

  beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [],
        providers: [
          HttpClient,
          HttpHandler
        ]
      });

      service = TestBed.get(AuthenticationService);
      service.saveToken(token);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get token from local storage', () => {
    expect(localStorage.getItem(TOKEN_KEY)).toBe(token);
  });

  it('should delete token from local storage', () => {
    expect(localStorage.getItem(TOKEN_KEY)).toBe(token);

    service.clearStorage();
    expect(localStorage.getItem(TOKEN_KEY)).toBe(null);
  });

  it('should return token from local storage', () => {
    expect(service.getToken()).toBe(token);
  });

  it('should return decoded jwt token', () => {
    const decodedToken = service.getDecodedToken();
    expect(decodedToken.created).toBe(99999999);
    expect(decodedToken.exp).toBe(1000000000);
    expect(decodedToken.roles[0].authority).toBe('ROLE_VISITOR');
    expect(decodedToken.sub).toBe('djura.djuric');
  });

  it('should claim that token expired', () => {
    expect(service.isTokenExpired()).toBe(true);

    service.saveToken(longerExpTimetoken);
    expect(service.isTokenExpired()).toBe(false);
  });

  it('should return Visitor role', () => {
    expect(service.getRoles().length).toBe(1);
    expect(service.getRoles()[0]).toBe('ROLE_VISITOR');
  });

  it('should return false, user is not authenticated', () => {
      expect(service.isAuthenticated()).toBe(false);
  });

  it('should return true, user is authenticated', () => {
    service.saveToken(longerExpTimetoken);
    expect(service.isAuthenticated()).toBe(true);
  });

});
