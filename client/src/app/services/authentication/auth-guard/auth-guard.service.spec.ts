import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthGuardService', () => {
  const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkanVyYS5kanVyaWMiLCJjcmVhdGVkIjo5OTk5OTk5OS\
                wicm9sZXMiOlt7ImF1dGhvcml0eSI6IlJPTEVfVklTSVRPUiJ9XSwiZXhwIjoxMDAwMDAwMDAwfQ.\
                kVvhvoTl0ujuS2uN9K4GkbZtBCbjMd21-BpRLOcFJa5CKXCS8VQ_-qsa_-9_ElGxO34PGy72-SJoj89a5_BHJA';
   // 14.03.2255.
  const longerExpTimetoken = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkanVyYS5kanVyaWMiLCJjcmVhdGVkIj\
                              o5OTk5OTk5OSwicm9sZXMiOlt7ImF1dGhvcml0eSI6IlJPTEVfVklTSVRPUiJ9XS\
                              wiZXhwIjo5MDAwMDAwMDAwfQ.LcCx0uqFxfRO1Khk3Y9iZrkvtz5L-Zi7un_EP32y\
                              OMc0ZArzkqnxdIVA7038haOIT4KrnDDFGKPRx8S0RB4z1w';

  let service: AuthGuardService;
  const TOKEN_KEY = 'authToken';

  beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule
        ],
        providers: [
          HttpClient,
          HttpHandler
        ]
      });

      service = TestBed.get(AuthGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should not allow access to page', () => {
    localStorage.setItem(TOKEN_KEY, token);
    expect(service.canActivate()).toBe(false);
  });

  it('should allow access to page', () => {
    localStorage.setItem(TOKEN_KEY, longerExpTimetoken);
    expect(service.canActivate()).toBe(true);
  });

});
