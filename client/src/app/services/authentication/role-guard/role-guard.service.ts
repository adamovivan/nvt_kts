import { Injectable } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService {

  constructor(public auth: AuthenticationService, public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {

    const expectedRoles = route.data.expectedRoles;
    const roles = this.auth.getRoles();

    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['home']);
      return false;
    }
    // check role
    for (const role of roles) {
      if (expectedRoles.indexOf(role) !== -1) {
        return true;
      }
    }

    return false;
  }
}
