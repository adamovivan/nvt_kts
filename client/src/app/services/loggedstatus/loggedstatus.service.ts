
// Napravljeno kako bi status o tome ko je ulogovan bio vidljiv svuda gde treba, ne samo u navbaru
import {Injectable} from '@angular/core';

@Injectable()
export class LoggedStatusService {

  loggedIn: boolean;
  visitorLogged: boolean;
  adminLogged: boolean;
  inspectorLogged: boolean;
  controllerLogged: boolean;

}
