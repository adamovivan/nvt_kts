import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Station } from '../../model/station.model';
import { MapData } from '../../model/mapData.model';
import { Coord } from '../../model/coord.model';
import { JsogService } from 'jsog-typescript';
import { map } from 'rxjs/operators';
import { CommonsService } from '../commons/commons.service';


@Injectable()
export class StationService {

    constructor(
        private http: Http,
        private jsog: JsogService,
        private commons: CommonsService
    ) { }

    mapData: MapData;

    setMapData(data: any) {
        this.mapData = data;
    }

    loadAllStations() {
        this.mapData.stationClickable = true;
        return this.http.get(location['apiUrl'] + '/stations').pipe(map(
            (response: Response) => {
                const data = this.jsog.deserialize(response.json());
                return data;
            }
        )).subscribe(
            (data: Station[]) => {
                console.log('[Rest] Stations:', data);
                this.mapData.stations.length = 0;
                this.mapData.stations.push(...data);
            }
        );
    }

    clearStationMarkers() {
        this.mapData.stations.length = 0;
    }

    setStationMarker(coord: Coord) {
        this.mapData.stationDraggable = false;
        const station = new Station('');
        station.coord = coord;
        this.mapData.stations[0] = station;
        console.log(station.coord.n, '    ', station.coord.e);
    }

    getAllLines() {
        return this.http.get(location['apiUrl'] + '/stations').pipe(map(
            (response: Response) => {
                const data = response.json();
                return data;
            }
        ));
    }

    addStation(station: Station) {
        station.coord = this.mapData.stations[0].coord;
        return this.http.post(location['apiUrl'] + '/stations', station)
            .pipe(map(
                (response: Response) => {
                    return response;
                }
            )).subscribe(
                data => {
                    console.log(data);
                    this.commons.openSnackBar('Station successfully added!', 'Great!');
                }
            );
    }

    saveStation(station: Station) {
        console.log('Save station!');
        console.log('JSOG:', this.jsog.serialize(station));

        return this.http.post(location['apiUrl'] + '/stations/edit', this.jsog.serialize(station))
            .pipe(map(
                (response: Response) => {
                    return response;
                }
            ));
    }

    deleteStation(id: number) {
        return this.http.delete(location['apiUrl'] + '/stations/' + id)
            .pipe(map(
                (response: Response) => {
                    return response;
                }
            ));
    }

}
