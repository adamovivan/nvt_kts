import { TestBed } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { fakeAsync, tick } from '@angular/core/testing';
import { StationService } from './station.service';
import { Path } from 'src/app/model/path.model';
import { CommonsService } from '../commons/commons.service';
import { CommonsServiceMock } from '../mocks/commons.service.mock';
import { JsogService } from 'jsog-typescript';
import { MapData } from 'src/app/model/mapData.model';
import { Coord } from 'src/app/model/coord.model';
import { Station } from 'src/app/model/station.model';

describe('StationService', () => {

    let stationService: StationService;
    let backend;
    let lastConnection;

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                { provide: ConnectionBackend, useClass: MockBackend },
                { provide: RequestOptions, useClass: BaseRequestOptions },
                { provide: CommonsService, useClass: CommonsServiceMock },
                JsogService,
                Http,
                StationService]
        });

        stationService = TestBed.get(StationService);
        backend = TestBed.get(ConnectionBackend);
        backend.connections.subscribe((connection: any) =>
            lastConnection = connection);

    });

    it('should pass simple test', () => {
        expect(true).toBe(true);
    });

    it('loadAllStations() should load all stations', fakeAsync(() => {
        const map = new MapData(new Path(), [], [], [], [], false);

        stationService.setMapData(map);
        stationService.loadAllStations();

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Get, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(map)
        })));
        tick();
        expect(map).toBeDefined();
        expect(map instanceof MapData).toBeTruthy();
        expect(map.stationClickable === true).toBeTruthy();
        expect(map.stations.length === 0).toBeTruthy();
    }));

    it('clearStationMarkers() slear all station markers', fakeAsync(() => {
        const map = new MapData(new Path(), [], [], [], [], false);

        stationService.setMapData(map);
        stationService.clearStationMarkers();

        expect(map).toBeDefined();
        expect(map instanceof MapData).toBeTruthy();
        expect(map.stations.length === 0).toBeTruthy();
    }));

    it('setStationMarker() set station marker on given coordinate', fakeAsync(() => {
        const map = new MapData(new Path(), [], [], [], [], false);
        const coord = new Coord(10, 20);

        stationService.setMapData(map);
        stationService.setStationMarker(coord);

        expect(map).toBeDefined();
        expect(map instanceof MapData).toBeTruthy();
        expect(map.stationDraggable === false).toBeTruthy();
        expect(map.stations[0].coord.e === coord.e).toBeTruthy();
        expect(map.stations[0].coord.n === coord.n).toBeTruthy();
    }));

    it('addStation(station: Station) add station', fakeAsync(() => {
        const map = new MapData(new Path(), [], [], [], [], false);
        const station = new Station('');
        const coord = new Coord(10, 20);
        station.coord = coord;
        map.stations.push(station);

        const testStation = new Station('');

        stationService.setMapData(map);
        stationService.addStation(testStation);

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Post, 'invalid http method');

        tick();
        expect(map).toBeDefined();
        expect(map instanceof MapData).toBeTruthy();
        expect(map.stations[0].name === testStation.name).toBeTruthy();
        expect(map.stations[0].coord.e === testStation.coord.e).toBeTruthy();
        expect(map.stations[0].coord.n === testStation.coord.n).toBeTruthy();
    }));

    it('saveStation() should save station', fakeAsync(() => {
        const station = new Station('');
        let res: Station;

        stationService.saveStation(station).subscribe((data: Response) => res = data.json());

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Post, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(station)
        })));
        tick();
        expect(res).toBeDefined();
        expect(res.name === station.name).toBeTruthy();
    }));

    it('deleteStation(id) should delete station', fakeAsync(() => {
        const id = 1;
        let res: number;

        stationService.deleteStation(id).subscribe((data: Response) => res = data.json());

        expect(lastConnection).toBeDefined('no http service connection at all?');
        expect(lastConnection.request.method).toEqual(RequestMethod.Delete, 'invalid http method');

        lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify(id)
        })));
        tick();
        expect(res).toBeDefined();
        expect(res === id).toBeTruthy();
    }));

});
