import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class TestService {

  // Observable string sources
  private mapClickSource = new Subject<any>();

  // Observable string streams
  mapClickStream$ = this.mapClickSource.asObservable();

  // Service message commands
  sendMessage(mission: any) {
    this.mapClickSource.next(mission);
  }

}
