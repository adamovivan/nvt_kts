import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Path } from '../../model/path.model';
import { PathService } from '../path/path.service';

@Injectable()
export class MapService {

  constructor(private pathService: PathService) { }

  // Path stream
  private pathRealtimeSource = new Subject<Path>();
  pathRealtimeStream$ = this.pathRealtimeSource.asObservable();

  loadPathToMap(lineNumber: string) {
    this.pathService.getLineByNumber(lineNumber).subscribe(
      (path: Path) => {
        // console.log('Path to load:', path);
        this.pathRealtimeSource.next(path);
      }
    );
  }

}
