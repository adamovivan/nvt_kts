import { TestBed } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { fakeAsync, tick } from '@angular/core/testing';
import { MapService } from './map.service';
import { PathService } from '../path/path.service';
import { PathServiceMock } from '../mocks/path.service.mock';
import { Path } from 'src/app/model/path.model';
import { of } from 'rxjs';

describe('MapService', () => {
    let pathService: PathService;
    let mapService: MapService;

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                { provide: ConnectionBackend, useClass: MockBackend },
                { provide: RequestOptions, useClass: BaseRequestOptions },
                { provide: PathService, useClass: PathServiceMock },
                Http,
                MapService]
        });

        pathService = TestBed.get(PathService);
        mapService = TestBed.get(MapService);


    });

    it('should pass simple test', () => {
        expect(true).toBe(true);
    });

    it('loadPathToMap() should stream line by number ', fakeAsync(() => {
        // Spies / subscriptions
        let path: Path = new Path();
        path.pathName = '1B';
        const lineNumber = '1B';
        mapService.pathRealtimeStream$.subscribe(val => path = val);

        spyOn(pathService, 'getLineByNumber').and.returnValue(of(path));

        // call test method
        mapService.loadPathToMap(lineNumber);

        // tick?
        tick();
        // expextations and call checks
        expect(path.pathName === lineNumber).toBeTruthy();
        expect(pathService.getLineByNumber).toHaveBeenCalled();
    }));

});
