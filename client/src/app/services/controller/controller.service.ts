import {Injectable} from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';


@Injectable()
export class ControllerService {

  constructor(private http: HttpClient) {}

  getControllersLines() {

    const headers = new HttpHeaders().set('Content-Type', 'application/json;');

    return this.http.get(location['apiUrl'] + '/controllersLines', {headers});
  }

  check(zone: string, vehicleType: string, time: string) {

    const headers = new HttpHeaders().set('Content-Type', 'application/json;');

    const params = new HttpParams().set('zone', zone).set('vehicleType', vehicleType).set('time', time);

    return this.http.get(location['apiUrl'] + '/controllerCheck',  {headers, params});
  }
}
