import { Injectable } from '@angular/core';
import { MatSnackBar, MatSidenav } from '@angular/material';
import { Subject } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { InfoWindow } from '@agm/core/services/google-maps-types';
import { FormControl } from '@angular/forms';

@Injectable()
export class CommonsService {
    constructor(private snackBar: MatSnackBar) { }

    private lineSelectControl = new FormControl(null);
    public lineSelectList: Map<string, any[]>;
    private sidenav: MatSidenav;

    // Info windows
    prevInfoWindow: InfoWindow = null;
    currentInfoWindow: InfoWindow = null;

    private resetLineSource = new Subject<any>();
    resetLineStream$ = this.resetLineSource.asObservable();

    public httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    // SnackBar
    public openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            panelClass: ['snack-dark-mode'],
            duration: 5000,
        });
    }

    // open one -> close previous
    public handleInfoWindows(newInfoWindow: InfoWindow) {
        if (this.prevInfoWindow == null) {
            this.prevInfoWindow = newInfoWindow;
        } else {
            this.currentInfoWindow = newInfoWindow;
            this.prevInfoWindow.close();
        }
        this.prevInfoWindow = newInfoWindow;
    }

    public resetInfoHandler() {
        this.currentInfoWindow = null;
        this.prevInfoWindow = null;
    }

    public setSidenav(sidenav: MatSidenav) {
        this.sidenav = sidenav;
    }

    public toggleSideNav() {
        return this.sidenav.toggle();
    }

    public setLineSelectControl(control: FormControl) {
        this.lineSelectControl = control;
    }

    public resetLineControl() {
        this.lineSelectControl.reset();
    }

    public updateLines() {
        this.resetLineSource.next(0);
    }

}
