import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { appConfig } from '../../app.config';
import {CardDTO} from '../../model/card.model';
import {throwError} from 'rxjs';
import {CommonsService} from '../commons/commons.service';

@Injectable()
export class CardService {

  cardTypes: string[] = ['ONEDRIVE', 'DAILY', 'MONTHLY', 'YEARLY'];


  constructor(private http: HttpClient, private commonsService: CommonsService) { }

  getCards(_params) {
    return this.http.get<any>(`${appConfig.apiUrl}/cards/filter`, {
      params: _params
    });
  }

  updateCurrentPrice(cardDTO: CardDTO) {

    const headers = {headers: { 'Content-Type': 'application/json'}};

    return this.http.post(location['apiUrl'] + '/cards/checkPrice', JSON.stringify(cardDTO), headers)
      .catch((err: HttpErrorResponse) => {
        return throwError(err.statusText);
      });
  }

  buyCard(cardDTO: CardDTO) {
    const headers = { headers: { 'Content-Type': 'application/json' } };
    return this.http.post(location['apiUrl'] + '/cards/buy', JSON.stringify(cardDTO), headers)
      .catch((err: HttpErrorResponse) => {
        this.commonsService.openSnackBar(err.message, '');
        return throwError(err.statusText);
      });
  }

  getVisitorCards(pageOptions: any) {
    return this.http.get(location['apiUrl'] + '/cards/myCards', { 'params': pageOptions })
      .catch((err: HttpErrorResponse) => {
        alert('An error occurred: ' +  err.error);
        return throwError(err.statusText);
      });

  }

}
