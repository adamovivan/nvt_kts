import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {HttpClient, HttpRequest} from '@angular/common/http';
import {CommonsService} from '../commons/commons.service';
import {CommonsServiceMocked} from '../mocks/commons.service.mocked';
import {CardService} from '../card/card.service';
import {CardDTO} from '../../model/card.model';
import 'rxjs/add/operator/catch';


describe('CardService', () => {

  let service: CardService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers:    [
        HttpClient,
        CardService,
        {provide: CommonsService, useClass: CommonsServiceMocked},
      ]
    });

    service = TestBed.get(CardService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should buy card', () => {

    const cardDTO = new CardDTO();
    cardDTO.cardType = 'DAILY';
    cardDTO.zone = 'ONE';
    cardDTO.vehicleType = 'BUS';

    service.buyCard(cardDTO).subscribe((data) => {
      expect(data['cardType']).toEqual(cardDTO['cardType']);
      expect(data['zone']).toEqual(cardDTO['zone']);
      expect(data['vehicleType']).toEqual(cardDTO['vehicleType']);
    });

    const req = httpMock.expectOne(location['apiUrl'] + '/cards/buy');
    expect(req.request.method).toEqual('POST');
    req.flush(cardDTO);
  });

  it('should get visitor cards', () => {
    const visitorCards = {'content': [],  'totalElements': 10};
    for (let i = 1; i <= 5; i++) {
      visitorCards['content'].push(new CardDTO());
      visitorCards['content'][i - 1].cardType = 'DAILY';
      visitorCards['content'][i - 1].zone = 'ONE';
      visitorCards['content'][i - 1].vehicleType = 'BUS';
      visitorCards['content'][i - 1].price = 45;
      visitorCards['content'][i - 1].id = i;
      visitorCards['content'][i - 1].startDate = '11/11/2011';
      visitorCards['content'][i - 1].endDate = '11/11/2012';
      visitorCards['content'][i - 1].active = true;
    }
    const dummyParams = {'pageIndex': 0, 'pageSize': 5};
    service.getVisitorCards(dummyParams).subscribe((data) => {
        expect(data['content']).toEqual(visitorCards['content']);
        expect(data['totalElements']).toEqual(visitorCards['totalElements']);
        expect((<CardDTO[]>data['content']).length).toEqual(dummyParams['pageSize']);

    });
    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'GET'
        && request.url === location['apiUrl'] + '/cards/myCards';
    });
    req.flush(visitorCards);
  });

  it('should check price', () => {
    const cardDTO = new CardDTO();
    cardDTO.cardType = 'DAILY';
    cardDTO.zone = 'ONE';
    cardDTO.vehicleType = 'BUS';
    const price = '456';

    service.updateCurrentPrice(cardDTO).subscribe((data) => {
      expect(data).toEqual(price);
    });

    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'POST'
        && request.url === location['apiUrl'] + '/cards/checkPrice';
    });
    req.flush(price);
  });

  it('should get cards', () => {
    const cardDTO1 = new CardDTO();
    cardDTO1.cardType = 'DAILY';
    cardDTO1.zone = 'ONE';
    cardDTO1.vehicleType = 'BUS';

    const cardDTO2 = new CardDTO();
    cardDTO2.cardType = 'DAILY';
    cardDTO2.zone = 'ONE';
    cardDTO2.vehicleType = 'TROLLEY';

    const params = {};
    params['page'] = 0;
    params['size'] = 1;
    params['zone'] = 'ONE';
    params['cardType'] = 'DAILY';

    service.getCards(params).subscribe(data => {
      expect(data.length).toBe(2);
      expect(data[0].cardType).toBe('DAILY');
      expect(data[1].cardType).toBe('DAILY');
      expect(data[0].zone).toBe('ONE');
      expect(data[1].zone).toBe('ONE');
    });

    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'GET' && request.url === location['apiUrl'] + '/cards/filter';
    });
    req.flush([cardDTO1, cardDTO2]);
  });

});
