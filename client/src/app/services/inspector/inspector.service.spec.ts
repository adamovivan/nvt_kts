import { TestBed } from '@angular/core/testing';

import { InspectorService } from './inspector.service';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DocumentModel } from 'src/app/model/document.model';
import { appConfig } from 'src/app/app.config';

describe('InspectorService', () => {

  let service: InspectorService;
  let httpMock: HttpTestingController;

  let document1: DocumentModel;
  let document2: DocumentModel;
  let documents: Array<DocumentModel>;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers:    [
        HttpClient,
        InspectorService
      ]
    });

    service = TestBed.get(InspectorService);
    httpMock = TestBed.get(HttpTestingController);

    document1 = new DocumentModel();
    document1.id = 1;
    document1.username = 'mika123';
    document1.firstName = 'Mika';
    document1.lastName = 'Mikic';
    document1.documentName = 'Licna karta';
    document1.visitorType = 'STUDENT';

    document2 = new DocumentModel();
    document2.id = 2;
    document2.username = 'djura.djuric';
    document2.firstName = 'Djura';
    document2.lastName = 'Djuric';
    document2.documentName = 'Polish identity card';
    document2.visitorType = 'PENSIONER';

    documents = [document1, document2];
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return pending documents', () => {
    service.getPendingDocuments().subscribe(data => {
      expect(data.length).toBe(2);
    });

    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'GET' && request.url === `${appConfig.apiUrl}/discountRequests`;
    });

    req.flush(documents);
  });

  it('should approve document', () => {
    service.approveDocument(document1.id).subscribe(data => {
        expect(data.message).toBe('Successful approval.');
    });

    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'PUT' && request.url === `${appConfig.apiUrl}/approveRequest`;
    });

    req.flush({message: 'Successful approval.'});
  });

  it('should reject document', () => {
    service.rejectDocument(document2.id).subscribe(data => {
      expect(data.message).toBe('Successful rejection.');
    });

    const req = httpMock.expectOne((request: HttpRequest<any>) => {
      return request.method === 'PUT' && request.url === `${appConfig.apiUrl}/rejectRequest`;
    });

    req.flush({message: 'Successful rejection.'});
  });

});
