import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { appConfig } from '../../app.config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InspectorService {

  constructor(private http: HttpClient) { }

  getPendingDocuments(): Observable<any> {
    return this.http.get(`${appConfig.apiUrl}/discountRequests`);
  }

  getDocumentImage(documentId): Observable<Blob> {
    return this.http.get(`${appConfig.apiUrl}/documentImage/${documentId}`, {responseType: 'blob'});
  }

  approveDocument(documentId): Observable<any> {
    return this.http.put(`${appConfig.apiUrl}/approveRequest`, documentId);
  }

  rejectDocument(documentId): Observable<any> {
    return this.http.put(`${appConfig.apiUrl}/rejectRequest`, documentId);
  }
}
