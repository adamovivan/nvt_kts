import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './components/log-in/log-in.component';
import { HomeComponent } from './components/home/home.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { EditStationComponent } from './entities/station/edit-station/edit-station.component';
import { AddStationComponent } from './entities/station/add-station/add-station.component';
import { AddPathComponent } from './entities/path/add-path/add-path.component';
import { EditPathComponent } from './entities/path/edit-path/edit-path.component';
import { AddPriceListComponent } from './components/pricelist/add-price-list/add-price-list.component';
import { RoleGuardService as RoleGuard } from './services/authentication/role-guard/role-guard.service';
import { CardsComponent } from './components/cards/cards.component';
import { ShowPricelistComponent } from './components/pricelist/show-pricelist/show-pricelist.component';
import { VisitorCardsComponent } from './components/visitor-cards/visitor-cards.component';
import { PendingDocsComponent } from './components/pending-docs/pending-docs.component';
import { TimetableComponent } from './components/timetable/view-timetable/view-timetable.component';
import { TimetableMenuComponent } from './components/timetable/timetable-menu/timetable-menu.component';
import { AddTimetableComponent } from './components/timetable/add-timetable/add-timetable.component';
import { UpdateTimetableComponent } from './components/timetable/update-timetable/update-timetable.component';
import { DeleteTimetableComponent } from './components/timetable/delete-timetable/delete-timetable.component';
import { ControllerCheckComponent } from './components/controller-check/controller-check.component';
import { UploadDocumentComponent } from './components/upload-document/upload-document.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'signup',
    component: SignUpComponent
  },
  {
    path: 'timetable',
    component: TimetableComponent
  },
  {
    path: 'timetable/menu',
    component: TimetableMenuComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    },
    children: [
      {
        path: 'add',
        component: AddTimetableComponent
      },
      {
        path: 'view',
        component: TimetableComponent
      },
      {
        path: 'update',
        component: UpdateTimetableComponent
      },
      {
        path: 'delete',
        component: DeleteTimetableComponent
      }
    ]
  },
  {
    path: 'login',
    component: LogInComponent
  },
  {
    path: 'stations/add',
    component: AddStationComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'stations/edit',
    component: EditStationComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'paths/add',
    component: AddPathComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'paths/edit',
    component: EditPathComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'check',
    component: ControllerCheckComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_CONTROLLER']
    }
  },
  {
    path: 'priceList/add',
    component: AddPriceListComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'priceList/show',
    component: ShowPricelistComponent
  },
  {
    path: 'cards',
    component: CardsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_ADMIN']
    }
  },
  {
    path: 'myCards',
    component: VisitorCardsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_VISITOR']
    }
  },
  {
    path: 'pending',
    component: PendingDocsComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_INSPECTOR']
    }
  },
  {
    path: 'uploadDocument',
    component: UploadDocumentComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRoles: ['ROLE_VISITOR']
    }
  },
  {
    path: '**', redirectTo: '/home'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

