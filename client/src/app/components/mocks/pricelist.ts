import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-pricelist-tab-view',
  template: '<p>{{vehicleType}}</p>'
})
export class MockPriceListTabViewComponent implements OnInit {

  @Input() vehicleType: string;
  @Input() priceListEditable: boolean;

  private id: number;

  @Input('selectedPriceListId')
  set selectedPriceListId(id: number) {
    if (this.id !== id) {
      this.id = id;
    }
  }

  ngOnInit() {
  }
}
