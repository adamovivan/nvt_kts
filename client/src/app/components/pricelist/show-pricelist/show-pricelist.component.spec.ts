import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ShowPricelistComponent } from './show-pricelist.component';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../../../material.module';
import {PathService} from '../../../services/path/path.service';
import {PricelistService} from '../../../services/pricelist/pricelist.service';
import {PricelistServiceMocked} from '../../../services/mocks/pricelist.service.mocked';
import {LoggedStatusService} from '../../../services/loggedstatus/loggedstatus.service';
import {MatTabsModule} from '@angular/material';
import {MockPriceListTabViewComponent} from '../../mocks/pricelist';
import {LoggedstatusServiceMocked} from '../../../services/mocks/loggedstatus.service.mocked';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';

describe('ShowPricelistComponent', () => {
  let component: ShowPricelistComponent;
  let fixture: ComponentFixture<ShowPricelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPricelistComponent, MockPriceListTabViewComponent ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
        MatTabsModule
      ],
      providers: [
        {provide: PathService, useClass: PathServiceMock},
        {provide: PricelistService, useClass: PricelistServiceMocked},
        {provide: LoggedStatusService, useClass: LoggedstatusServiceMocked},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPricelistComponent);
    component = fixture.componentInstance;
    component.loggedStatusService.loggedIn = true;
    component.loggedStatusService.adminLogged = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test tabs', fakeAsync(() => {
      fixture.detectChanges();
      tick();
      const i = 1;
      const elems  = fixture.debugElement.queryAll(By.css('.mat-tab-label'));
      elems[i].nativeElement.click();
      fixture.detectChanges();
      tick();
      expect(elems[i].nativeElement.textContent).toEqual(component.vehicleTypes[i]);
  }));

  it('should test price list select', fakeAsync(() => {
      expect(component.priceLists.length).toBeGreaterThan(0);
      component.selectedPriceListId = component.priceLists[1].id;

      const cssSelector = '#priceListSelect > div > div.mat-select-value > span';
      fixture.detectChanges();
      tick();
      const elem = fixture.debugElement.query(By.css(cssSelector)).nativeElement;
      expect(elem.textContent).not.toEqual('');

      component.selectedPriceListId = component.priceLists[2].id;
      fixture.detectChanges();
      tick();
      const elem2 = fixture.debugElement.query(By.css(cssSelector)).nativeElement;
      expect(elem2.textContent).not.toEqual(elem.textContent);

  }));

  it('should test admin and non admin view', fakeAsync(() => {
    component.loggedStatusService.loggedIn = true;
    component.loggedStatusService.adminLogged = true;
    fixture.detectChanges();
    tick();
    let button = fixture.debugElement.query(By.css('#addPriceListButton')).nativeElement;
    expect(button).not.toBeNull();

    component.loggedStatusService.loggedIn = true;
    component.loggedStatusService.adminLogged = false;
    fixture.detectChanges();
    tick();
    button = fixture.debugElement.query(By.css('#addPriceListButton'));
    expect(button).toBe(null);


  }));

});
