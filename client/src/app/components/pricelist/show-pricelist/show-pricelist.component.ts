import { Component, OnInit } from '@angular/core';
import {PathService} from '../../../services/path/path.service';
import {PriceListInfo} from '../../../model/pricelistData.model';
import {PricelistService} from '../../../services/pricelist/pricelist.service';
import {LoggedStatusService} from '../../../services/loggedstatus/loggedstatus.service';

@Component({
  selector: 'app-show-pricelist',
  templateUrl: './show-pricelist.component.html',
  styleUrls: ['./show-pricelist.component.scss']
})
export class ShowPricelistComponent implements OnInit {

  vehicleTypes: string[];

  priceLists: PriceListInfo[] = [];

  selectedPriceListId: number;

  constructor(private pathService: PathService,
   private priceListService: PricelistService, public loggedStatusService: LoggedStatusService) {
    this.vehicleTypes = this.pathService.vehicleTypes;
  }

  ngOnInit() {
    this.priceListService.getPriceListsInfo().subscribe((response) => {
      for (const elem of <PriceListInfo[]>response) {
        this.priceLists.push(elem);
      }

      if (this.priceLists.length > 0) {
        this.selectedPriceListId = this.priceLists[0].id;
      }
    });
  }

  currentPriceListEditable(): boolean {
    for (const priceList of this.priceLists) {
      if (priceList.id === this.selectedPriceListId) {
        return priceList.editable;
      }
    }
    return false;
  }


}
