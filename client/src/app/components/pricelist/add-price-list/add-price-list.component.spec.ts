import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPriceListComponent } from './add-price-list.component';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PathService } from '../../../services/path/path.service';
import { CardService } from '../../../services/card/card.service';
import { VisitorService } from '../../../services/visitor/visitor.service';
import { PricelistService } from '../../../services/pricelist/pricelist.service';
import { DebugElement } from '@angular/core';
import { PricelistServiceMocked } from '../../../services/mocks/pricelist.service.mocked';
import { MaterialModule } from '../../../material.module';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { VisitorServiceMocked } from '../../../services/mocks/visitor.service.mocked';
import { CardServiceMocked } from '../../../services/mocks/card.service.mocked';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonsService } from '../../../services/commons/commons.service';
import { CommonsServiceMocked } from '../../../services/mocks/commons.service.mocked';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';

describe('AddPriceListComponent', () => {
  let component: AddPriceListComponent;
  let fixture: ComponentFixture<AddPriceListComponent>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddPriceListComponent],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MatDatepickerModule,
        MatNativeDateModule
      ],
      providers: [
        { provide: PathService, useClass: PathServiceMock },
        { provide: CardService, useClass: CardServiceMocked },
        { provide: VisitorService, useClass: VisitorServiceMocked },
        { provide: CommonsService, useClass: CommonsServiceMocked },
        { provide: PricelistService, useClass: PricelistServiceMocked }
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(AddPriceListComponent);
        component = fixture.componentInstance;
        de = fixture.debugElement.query(By.css('form'));
        el = de.nativeElement;
      });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPriceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should be invalid', () => {
    component.form.controls['startDate'].setValue('invalidDate');
    component.form.controls['endDate'].setValue('invalidDate');
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.valid).toBeFalsy();
    });

  });

  it('should be valid', () => {
    component.form.controls['startDate'].setValue(new Date());
    component.form.controls['endDate'].setValue(new Date());
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.valid).toBeTruthy();
    });
  });

  it('should call on submit method', () => {

    component.form.controls['startDate'].setValue(new Date());
    component.form.controls['endDate'].setValue(new Date());
    fixture.detectChanges();
    spyOn(component, 'addPriceList');
    el = fixture.debugElement.query(By.css('.submit')).nativeElement;
    el.click();
    fixture.whenStable().then(() => {
      expect(component.addPriceList).toHaveBeenCalledTimes(1);
    });

  });

});

