import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {CardService} from '../../../services/card/card.service';
import {VisitorService} from '../../../services/visitor/visitor.service';
import {PathService} from '../../../services/path/path.service';
import {PricelistService} from '../../../services/pricelist/pricelist.service';
import {CommonsService} from '../../../services/commons/commons.service';


@Component({
  selector: 'app-add-price-list',
  templateUrl: './add-price-list.component.html',
  styleUrls: ['./add-price-list.component.scss']
})
export class AddPriceListComponent implements OnInit {

  cardTypes: string[];
  visitorTypes: string[];
  zones: string[];
  vehicles: string[];

  form = new FormGroup({
    'startDate': new FormControl(),
    'endDate': new FormControl(),
    'basePriceDAILY': new FormControl(1),
    'basePriceONEDRIVE': new FormControl(1),
    'basePriceMONTHLY': new FormControl(1),
    'basePriceYEARLY': new FormControl(1),
    'visitorTypeREGULAR': new FormControl(1),
    'visitorTypeSTUDENT': new FormControl(1),
    'visitorTypePENSIONER': new FormControl(1),
    'zoneONE': new FormControl(1),
    'zoneTWO': new FormControl(1),
    'zoneTHREE': new FormControl(1),
    'vehicleTROLLEY': new FormControl(1),
    'vehicleBUS': new FormControl(1),
    'vehicleMETROTRAIN' : new FormControl(1)
  });

  static formatDate(date: Date): string {
    const months = [
      '01', '02', '03',
      '04', '05', '06', '07',
      '08', '09', '10',
      '11', '12'
    ];

    const day = date.getDate();
    const monthIndex = date.getMonth();
    const year = date.getFullYear();

    return day + '.' + months[monthIndex] + '.' + year + '.';
  }

  constructor(private pathService: PathService, private cardService: CardService, private visitorService: VisitorService,
    private priceListService: PricelistService, private commonsService: CommonsService) {
    this.cardTypes = this.cardService.cardTypes;
    this.visitorTypes = this.visitorService.visitorTypes;
    this.zones = this.pathService.zones;
    this.vehicles = this.pathService.vehicleTypes;
  }



  ngOnInit() {

  }



  addPriceList() {
    const priceListDTO = {};

    priceListDTO['startDate'] = AddPriceListComponent.formatDate(this.form.get('startDate').value);
    priceListDTO['endDate'] = AddPriceListComponent.formatDate(this.form.get('endDate').value);
    priceListDTO['basePrices'] = {};

    priceListDTO['basePrices']['ONEDRIVE'] = this.form.get('basePriceONEDRIVE').value;
    priceListDTO['basePrices']['DAILY'] = this.form.get('basePriceDAILY').value;
    priceListDTO['basePrices']['MONTHLY'] = this.form.get('basePriceMONTHLY').value;
    priceListDTO['basePrices']['YEARLY'] = this.form.get('basePriceYEARLY').value;



    priceListDTO['visitorModifier'] = {};
    priceListDTO['visitorModifier']['REGULAR'] = this.form.get('visitorTypeREGULAR').value;
    priceListDTO['visitorModifier']['STUDENT'] = this.form.get('visitorTypeSTUDENT').value;
    priceListDTO['visitorModifier']['PENSIONER'] = this.form.get('visitorTypePENSIONER').value;

    priceListDTO['zoneModifier'] = {};
    priceListDTO['zoneModifier']['ONE'] = this.form.get('zoneONE').value;
    priceListDTO['zoneModifier']['TWO'] = this.form.get('zoneTWO').value;
    priceListDTO['zoneModifier']['THREE'] = this.form.get('zoneTHREE').value;


    priceListDTO['vehicleModifier'] = {};
    priceListDTO['vehicleModifier']['BUS'] = this.form.get('vehicleBUS').value;
    priceListDTO['vehicleModifier']['TROLLEY'] = this.form.get('vehicleTROLLEY').value;
    priceListDTO['vehicleModifier']['METROTRAIN'] = this.form.get('vehicleMETROTRAIN').value;
   this.priceListService.addNewPriceList(priceListDTO)
     .subscribe((response) => {this.commonsService.openSnackBar(response['message'], 'Ok'); } );
  }
}
