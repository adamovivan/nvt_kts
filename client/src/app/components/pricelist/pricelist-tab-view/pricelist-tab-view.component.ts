import {Component, Input, OnInit} from '@angular/core';
import {CardService} from '../../../services/card/card.service';
import {PriceListData, PriceListItemData} from '../../../model/pricelistData.model';
import {PricelistService} from '../../../services/pricelist/pricelist.service';
import {LoggedStatusService} from '../../../services/loggedstatus/loggedstatus.service';
import {CommonsService} from '../../../services/commons/commons.service';


@Component({
  selector: 'app-pricelist-tab-view',
  templateUrl: './pricelist-tab-view.component.html',
  styleUrls: ['./pricelist-tab-view.component.scss']
})
export class PricelistTabViewComponent implements OnInit {

  @Input() vehicleType: string;
  @Input() priceListEditable: boolean;

  priceListId: number;


  @Input('selectedPriceListId')
  set selectedPriceListId(id: number) {
    this.priceListId = id;
    this.setEditColumn();
    this.priceListService.getPriceListData(id, this.vehicleType)
      .subscribe((response) => { this.priceListData.setNewData(response); } );
  }

  cardTypes: string[];
  priceListData: PriceListData = new PriceListData();
  columnsToDisplay: string[] = ['zone', 'visitorType', 'price'];


  constructor(public cardService: CardService, public priceListService: PricelistService,
               public loggedStatusService: LoggedStatusService,
              public commonsService: CommonsService) {
    this.cardTypes =  cardService.cardTypes;
  }

  editPriceListItemPrice(zone: string, visitorType: string, price: number, cardType: string) {

    const newPriceListItem = new PriceListItemData({'price': price,
      'zone': zone,
      'visitorType': visitorType
    });

    newPriceListItem.cardType = cardType;
    newPriceListItem.priceListId = this.priceListId;
    newPriceListItem.vehicleType = this.vehicleType;
    this.priceListService.changePriceListItemPrice(newPriceListItem)
    .subscribe((response) => {this.commonsService.openSnackBar(response['message'], 'Ok'); } );
}


  ngOnInit() {
  }

  setEditColumn() {
    if (this.loggedStatusService.adminLogged && this.priceListEditable) {
      const index = this.columnsToDisplay.indexOf('edit', 0);
      if (index === -1) {
        this.columnsToDisplay.push('edit');
      }
    } else {
      this.columnsToDisplay =  ['zone', 'visitorType', 'price'];
    }
  }

}
