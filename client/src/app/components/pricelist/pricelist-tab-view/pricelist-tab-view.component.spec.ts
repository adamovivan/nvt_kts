import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { PricelistTabViewComponent } from './pricelist-tab-view.component';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../../../material.module';
import {PricelistService} from '../../../services/pricelist/pricelist.service';
import {PricelistServiceMocked} from '../../../services/mocks/pricelist.service.mocked';
import {LoggedStatusService} from '../../../services/loggedstatus/loggedstatus.service';
import {LoggedstatusServiceMocked} from '../../../services/mocks/loggedstatus.service.mocked';
import {CardService} from '../../../services/card/card.service';
import {CardServiceMocked} from '../../../services/mocks/card.service.mocked';
import {CommonsService} from '../../../services/commons/commons.service';
import {CommonsServiceMocked} from '../../../services/mocks/commons.service.mocked';
import {of} from 'rxjs';

describe('PricelistTabViewComponent', () => {
  let component: PricelistTabViewComponent;
  let fixture: ComponentFixture<PricelistTabViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricelistTabViewComponent ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
      ],
      providers: [
        {provide: PricelistService, useClass: PricelistServiceMocked},
        {provide: LoggedStatusService, useClass: LoggedstatusServiceMocked},
        {provide: CardService, useClass: CardServiceMocked},
        {provide: CommonsService, useClass: CommonsServiceMocked}
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricelistTabViewComponent);
    component = fixture.componentInstance;
    component.vehicleType = 'BUS';
    component.selectedPriceListId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assert that data is present in tables', () => {
    const allTables = fixture.debugElement.queryAll(By.css('table'));
    expect(allTables.length).toBeGreaterThan(0);
    for (let i = 0; i < allTables.length; i++) {
      expect(allTables[i].nativeElement.rows.length).toBeGreaterThan(1);
    }
  });

  it('should test edit view', fakeAsync(() => {

    component.priceListEditable = true;
    component.loggedStatusService.adminLogged = true;
    component.selectedPriceListId = 2;
    fixture.detectChanges();
    tick();
    const allPriceEdits  = document.querySelectorAll('input[type="number"]');
    expect(allPriceEdits.length).toBeGreaterThan(0);
    for (let i = 0; i < allPriceEdits.length; i++) {
      expect((<HTMLInputElement>allPriceEdits[i]).value).toEqual( '55');
    }
  }));

  it('should call price edit functionality', fakeAsync(() => {
    spyOn(component.priceListService, 'changePriceListItemPrice')
      .and.returnValue(of({'message': 'Added.', 'success': true }));
    component.priceListEditable = true;
    component.loggedStatusService.adminLogged = true;
    component.selectedPriceListId = 2;
    fixture.detectChanges();
    tick();
    const allPriceEdits  = document.querySelectorAll('input[type="number"]');
    const allButtons  = document.querySelectorAll('button');
    for (let i = 0; i < allPriceEdits.length; i++) {
      (<HTMLInputElement>allPriceEdits[i]).value = '41';
      (<HTMLButtonElement>allButtons[i]).click();
    }
      expect(component.priceListService.changePriceListItemPrice).toHaveBeenCalledTimes(allPriceEdits.length);
  }));

});
