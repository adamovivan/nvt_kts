import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MapMockComponent } from '../mocks/map.component.mock';
import { HomeComponent } from './home.component';
import { MapComponent } from '../map/map.component';
import { LineSelectComponent } from '../line-select/line-select.component';
import { MaterialModule } from 'src/app/material.module';
import { TestService } from 'src/app/services/test/test.service';
import { LineSelectMockComponent } from '../mocks/line-select.component.mock';

describe('HomeComponent', () => {
    let fixture: ComponentFixture<HomeComponent>;

    beforeEach((() => {

        TestBed.configureTestingModule({
            providers: [
                { provide: MapComponent, useClass: MapMockComponent },
                { provide: LineSelectComponent, useClass: LineSelectMockComponent },
                TestService
            ],
            declarations: [
                HomeComponent,
                MapMockComponent,
                LineSelectMockComponent
            ],
            imports: [
                MaterialModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HomeComponent);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(true).toBeTruthy();
    });



});
