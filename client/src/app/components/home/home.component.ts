import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TestService } from 'src/app/services/test/test.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  constructor(testService: TestService) {
    testService.mapClickStream$.subscribe(
      (data) => {
        console.log('Map click data:', data);
      }
    );
  }

  ngOnInit() {

  }

}
