import { Component, OnInit, OnDestroy, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { Station } from 'src/app/model/station.model';
import { Path } from 'src/app/model/path.model';
import { Vehicle } from 'src/app/model/vehicle.model';
import { AgmMarker } from '@agm/core';
import { Coord } from 'src/app/model/coord.model';
import { MapService } from 'src/app/services/map/map.service';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import { StationService } from 'src/app/services/station/station.service';
import { PathService } from 'src/app/services/path/path.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnInit, OnDestroy {

  @Output() mapClickEvent: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
  @Output() markerClickEvent: EventEmitter<Station> = new EventEmitter<Station>();

  markerInfo: string;
  path: Path = new Path();
  allPathNumbers: string[] = [];
  stations: Station[] = [];
  vehicles: Vehicle[] = [];
  pathEditMarkers: AgmMarker[] = [];

  // Complete data (pass by ref to services)
  mapData = {
    currentPath: this.path,
    stations: this.stations,
    vehicles: this.vehicles,

    pathEditMarkers: this.pathEditMarkers,
    allPathNumbers: this.allPathNumbers,

    stationDraggable: false,
    stationClickable: true
  };

  // subscription refs
  mapRealtimeSubscription;
  vehicleRealtimeSubscription;

  constructor(
    private mapService: MapService,
    private vehicleService: VehicleService,
    private stationService: StationService,
    private pathService: PathService
  ) {

  }


  ngOnInit() {
    this.stationService.setMapData(this.mapData);
    this.pathService.setData(this.mapData);
    // Initialize sockets
    this.vehicleService.initializeWebSocketConnection();

    // Realtime
    this.mapRealtimeSubscription = this.mapService.pathRealtimeStream$.subscribe(
      (path: Path) => {
        this.vehicles = [];  // instant real time refresh
        this.path = path;
        this.stations = path.stations;
        this.vehicleService.getRealtimeLocations(path.id).subscribe();
      }
    );
    // Realtime
    this.vehicleRealtimeSubscription = this.vehicleService.vehicleLocationsStream$.subscribe(
      (newVehicleList: Vehicle[]) => {
        this.vehicles = newVehicleList;
      }
    );
  }

  mapClick(event: MouseEvent) {
    console.log('Map Click:', event);
    this.mapClickEvent.emit(event);
  }

  markerDragEnd(m, i, $event: any) {
    m.n = $event.coords.lat;
    m.e = $event.coords.lng;
    console.log(i);
    // console.log("Drag at index: ", i, " Marker now:", m)
    // console.log("Marker list:", this.pathEditMarkers)

    this.path.coords.length = 0;
    this.pathEditMarkers.forEach(element => {
      this.path.coords.push(new Coord(element['e'], element['n']));  // mora ovako budzeno jer Agm Mape imaju bag
    });
  }

  stationClicked(station: Station) {
    this.markerClickEvent.emit(station);
  }

  ngOnDestroy() {
    this.mapRealtimeSubscription.unsubscribe();                 // close subscription
    this.vehicleService.getRealtimeLocations(-1).subscribe();   // close socket stream
    this.mapData.vehicles.length = 0;                             // clear markers on map
  }

}
