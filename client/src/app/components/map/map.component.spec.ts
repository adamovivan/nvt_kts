import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './map.component';
import { MaterialModule } from 'src/app/material.module';
import { MapService } from 'src/app/services/map/map.service';
import { PathService } from 'src/app/services/path/path.service';
import { StationServiceMock } from 'src/app/services/mocks/station.service.mock';
import { StationService } from 'src/app/services/station/station.service';
import { VehicleService } from 'src/app/services/vehicle/vehicle.service';
import { VehicleServiceMock } from 'src/app/services/mocks/vehicle.service.mock';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { MapServiceMock } from 'src/app/services/mocks/map.service.mock';
import { Coord } from 'src/app/model/coord.model';
import { Observable } from 'rxjs';

describe('MapComponent', () => {
  let component: MapComponent;
  let vehicleService: any;
  let mapService: any;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: MapService, useClass: MapServiceMock },
        { provide: PathService, useClass: PathServiceMock },
        { provide: StationService, useClass: StationServiceMock },
        { provide: VehicleService, useClass: VehicleServiceMock },
      ],
      declarations: [
        MapComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MapComponent);
    vehicleService = TestBed.get(VehicleService);
    mapService = TestBed.get(MapService);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('map component constructor test', async(() => {
    spyOn(vehicleService, 'initializeWebSocketConnection').and.returnValue(Promise.resolve());
    spyOn(vehicleService.vehicleLocationsStream$, 'subscribe').and.returnValue(null);
    spyOn(mapService.pathRealtimeStream$, 'subscribe').and.returnValue(null);

    component.ngOnInit();

    // Socket init method call
    expect(vehicleService.initializeWebSocketConnection).toHaveBeenCalled();

    // Subscriptions
    expect(vehicleService.vehicleLocationsStream$.subscribe).toHaveBeenCalled();
    expect(mapService.pathRealtimeStream$.subscribe).toHaveBeenCalled();
  }));

  it('markerDragEnd() should refresh marker list', async(() => {
    const m = new Coord(10, 20);
    const event = { coords: new Coord(40, 50) };

    component.markerDragEnd(m, event);

    expect(component.path.coords.length === 0).toBeTruthy();
  }));

  it('map component constructor test', async(() => {
    spyOn(vehicleService, 'getRealtimeLocations').and.returnValue(new Observable());
    spyOn(component.mapRealtimeSubscription, 'unsubscribe').and.returnValue(null);

    component.ngOnDestroy();

    expect(component.mapData.vehicles.length === 0).toBeTruthy();
    expect(vehicleService.getRealtimeLocations).toHaveBeenCalled();
  }));

});
