import { Component, OnInit } from '@angular/core';
import { VisitorService } from '../../services/visitor/visitor.service';
import { CommonsService } from '../../services/commons/commons.service';

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.scss']
})
export class UploadDocumentComponent implements OnInit {
  visitorTypes: any[] = [];
  selectedVisitorType: any = '';
  selectedFile: File;

  constructor(private visitorService: VisitorService,
              private commonsService: CommonsService) {

    visitorService.visitorTypes.forEach(element => {
      if (element !== 'REGULAR') {
        this.visitorTypes.push(element);
      }
    });

   }

  ngOnInit() {
  }

  onChange(event) {
    this.selectedVisitorType = event.value;
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onFormSubmit() {
    if (this.selectedFile === undefined || this.selectedVisitorType === '') {
      this.commonsService.openSnackBar('You must fill in all the fields.', 'Ok');
      return;
    }

    const formdata: FormData = new FormData();
    formdata.append('file', this.selectedFile);
    formdata.append('documentName', this.selectedFile.name);
    formdata.append('discount', this.selectedVisitorType);
    this.visitorService.uploadDocument(formdata);
  }
}
