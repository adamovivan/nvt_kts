import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDocumentComponent } from './upload-document.component';
import { CommonsService } from '../../services/commons/commons.service';
import { VisitorService } from '../../services/visitor/visitor.service';
import { JsogService } from 'jsog-typescript';
import { MaterialModule } from '../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';

describe('UploadDocumentComponent', () => {
  let component: UploadDocumentComponent;
  let fixture: ComponentFixture<UploadDocumentComponent>;
  let visitorsService: VisitorService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadDocumentComponent ],
      providers: [CommonsService, VisitorService, JsogService],

      imports: [
        MaterialModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDocumentComponent);
    visitorsService = TestBed.get(VisitorService);
    spyOn(visitorsService, 'uploadDocument').and.returnValue('Uploaded.');
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('empty fields', () => {
    fixture.debugElement.query(By.css('#uploadDocumentButton')).nativeElement.click();
    expect(component.selectedFile === undefined && component.selectedVisitorType === '').toBeTruthy();
  });

  it('visitor type is not choosen', () => {
    component.selectedFile = new File([''], 'filename', { type: 'image/png' });
    fixture.debugElement.query(By.css('#uploadDocumentButton')).nativeElement.click();
    expect(component.selectedVisitorType === '').toBeTruthy();
  });

  it('file is not upload', () => {
    component.selectedVisitorType = 'Student';
    fixture.debugElement.query(By.css('#uploadDocumentButton')).nativeElement.click();
    expect(component.selectedFile === undefined).toBeTruthy();
  });

  it('file uploaded', () => {
    component.selectedVisitorType = 'Student';
    component.selectedFile = new File([''], 'filename', { type: 'image/png' });
    fixture.debugElement.query(By.css('#uploadDocumentButton')).nativeElement.click();
    fixture.detectChanges();
    expect(visitorsService.uploadDocument).toHaveBeenCalled();
  });
});
