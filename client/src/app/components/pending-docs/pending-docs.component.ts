import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { InspectorService } from 'src/app/services/inspector/inspector.service';

@Component({
  selector: 'app-pending-docs',
  templateUrl: './pending-docs.component.html',
  styleUrls: ['./pending-docs.component.scss']
})
export class PendingDocsComponent implements OnInit {

  documents = [];

  url = {};

  showCard = {};

  constructor(private inspectorService: InspectorService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
      this.inspectorService.getPendingDocuments().subscribe(data => {

        for (const document of data) {
            this.documents.push(document);
            this.url[document.id] = undefined;
        }

        const docImageLoop = (documentId: number) => {
          this.inspectorService.getDocumentImage(documentId).subscribe(
            res => {

              this.url[documentId] = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(new Blob([res])));

              if (data.length) {
                docImageLoop(data.shift().id);
              }
            }
          );
        };
        if (data.length) {
          docImageLoop(data.shift().id);
        }
      });
  }

  onApprove(documentId) {
    this.inspectorService.approveDocument(documentId).subscribe(() => {
      this.documents = this.documents.filter(el => el.id !== documentId);
    });

  }

  onReject(documentId) {
    this.inspectorService.rejectDocument(documentId).subscribe(() => {
      this.documents = this.documents.filter(el => el.id !== documentId);
    });
  }

}
