import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingDocsComponent } from './pending-docs.component';
import { MaterialModule } from 'src/app/material.module';
import { InspectorService } from 'src/app/services/inspector/inspector.service';
import { InspectorServiceMocked } from 'src/app/services/mocks/inspector.service.mocked';
import { FormsModule } from '@angular/forms';

describe('PendingDocsComponent', () => {
  let component: PendingDocsComponent;
  let fixture: ComponentFixture<PendingDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MaterialModule
      ],
      declarations: [ PendingDocsComponent ],
      providers: [
        { provide: InspectorService, useClass: InspectorServiceMocked }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove document from documents (approve)', () => {
    const docId = 1;
    expect((component.documents.find(el => el.id === docId)).id).toBe(docId);

    component.onApprove(docId);
    expect(component.documents.find(el => el.id === docId)).toBe(undefined);
  });

  it('should remove document from documents (reject)', () => {
    const docId = 2;
    expect((component.documents.find(el => el.id === docId)).id).toBe(docId);

    component.onReject(docId);
    expect(component.documents.find(el => el.id === docId)).toBe(undefined);
  });

});
