import { ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MapService } from 'src/app/services/map/map.service';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { CommonsServiceMock } from 'src/app/services/mocks/commons.service.mock';
import { AgmCoreModule } from '@agm/core';
import { MapMockComponent } from 'src/app/components/mocks/map.component.mock';
import { MapServiceMock } from 'src/app/services/mocks/map.service.mock';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PathService } from 'src/app/services/path/path.service';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { LineSelectComponent } from 'src/app/components/line-select/line-select.component';
import { of } from 'rxjs';

describe('LineSelectComponent', () => {
  let component: LineSelectComponent;
  let pathService: PathService;
  let mapService: MapService;
  let commonService: CommonsService;
  let fixture: ComponentFixture<LineSelectComponent>;

  beforeEach((() => {

    TestBed.configureTestingModule({
      providers: [
        { provide: MapService, useClass: MapServiceMock },
        { provide: CommonsService, useClass: CommonsServiceMock },
        { provide: PathService, useClass: PathServiceMock },
      ],
      declarations: [
        MapMockComponent,
        LineSelectComponent,
      ],
      imports: [
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
        }),
        MaterialModule,
        BrowserAnimationsModule
      ]
    }).compileComponents();

    pathService = TestBed.get(PathService);
    commonService = TestBed.get(CommonsService);
    mapService = TestBed.get(MapService);

    fixture = TestBed.createComponent(LineSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init component', fakeAsync(() => {
    // boilerplate code
    spyOn(commonService, 'setLineSelectControl').and.returnValue({});
    spyOn(component, 'loadLines').and.returnValue({});
    spyOn(commonService.resetLineStream$, 'subscribe').and.returnValue(of({}));

    // call
    component.ngOnInit();
    commonService.updateLines();
    tick();

    // expectations
    expect(commonService.setLineSelectControl).toHaveBeenCalled();
    expect(component.loadLines).toHaveBeenCalled();
  }));

  it('should init selections to map', fakeAsync(() => {
    // boilerplate code
    // can't make force valueChange to fire from program
    spyOn(component.selectControl, 'valueChanges').and.returnValue(of({}));
    spyOn(mapService, 'loadPathToMap').and.returnValue(of({}));
    spyOn(component, 'loadLines').and.returnValue({});
    component.autoLoad = true;
    // call
    component.ngAfterViewInit();
    tick();

    // expectations
    expect(component.loadLines).toHaveBeenCalled();
  }));

  it('should load line map', fakeAsync(() => {
    // boilerplate code
    const map = new Map<string, any[]>([['1', ['test']]]);
    spyOn(pathService, 'getLinesByCategory').and.returnValue(of(map));

    // call
    component.loadLines();
    tick();

    // expectations
    expect(component.allLinesMap).toBeDefined();
    expect(component.allLinesMap === map).toBeTruthy();
  }));

  it('should load line map', fakeAsync(() => {
    // boilerplate code
    const map = new Map<string, any[]>([['1', ['test']]]);

    // call
    const keys = component.getKeys(map);

    // expectations
    expect(keys[0] === '1').toBeTruthy();
  }));

});
