import { Component, OnInit, AfterViewInit, Output, EventEmitter, Input, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PathService } from 'src/app/services/path/path.service';
import { MapService } from 'src/app/services/map/map.service';
import { CommonsService } from 'src/app/services/commons/commons.service';


export interface LineSelection {
  value: string;
  viewValue: string;
}

export interface VehicleGroup {
  disabled?: boolean;
  name: string;
  lines: LineSelection[];
}

@Component({
  selector: 'app-line-select',
  templateUrl: './line-select.component.html',
  styleUrls: ['./line-select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LineSelectComponent implements OnInit, AfterViewInit {

  selectControl = new FormControl();
  allLinesMap = new Map<string, string[]>();
  @Output() lineSelectedEvent: EventEmitter<string> = new EventEmitter<string>();
  @Input() autoLoad = false;

  constructor(
    private pathService: PathService,
    private mapService: MapService,
    private commonsService: CommonsService) { }

  ngOnInit() {
    this.commonsService.setLineSelectControl(this.selectControl);
    this.commonsService.lineSelectList = this.allLinesMap;

    this.commonsService.resetLineStream$.subscribe(
      () => this.loadLines()
    );
  }

  ngAfterViewInit() {

    this.selectControl.valueChanges.subscribe(
      (pathSelected) => {
        if (pathSelected === null) {
          return;
        }
        // console.log('Value change:', pathSelected);
        if (this.autoLoad) {
          this.mapService.loadPathToMap(pathSelected); // autoLoad
        } else {

        }
        this.lineSelectedEvent.emit(pathSelected);  // emit only
      }
    );

    this.loadLines();
  }

  loadLines() {
    this.pathService.getLinesByCategory()
      .subscribe(
        (lineMap) => {
          this.allLinesMap = lineMap;
        },
        (error) => console.log(error)
      );
  }

  getKeys(map) {
    return Array.from(map.keys());
  }

}
