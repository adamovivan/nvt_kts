import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControllerCheckComponent } from './controller-check.component';
import { CommonsService } from '../../services/commons/commons.service';
import { ControllerService } from '../../services/controller/controller.service';
import { JsogService } from 'jsog-typescript';
import { MaterialModule } from '../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('ControllerCheckComponent', () => {
  let component: ControllerCheckComponent;
  let fixture: ComponentFixture<ControllerCheckComponent>;
  let controllerService: ControllerService;
  let commonService: CommonsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControllerCheckComponent ],
      providers: [CommonsService, ControllerService, JsogService, DatePipe],

      imports: [
        MaterialModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    controllerService = TestBed.get(ControllerService);
    commonService = TestBed.get(CommonsService);
    spyOn(controllerService, 'getControllersLines').and.returnValue(of(['1B', '2B']));
    spyOn(commonService, 'openSnackBar');
    fixture = TestBed.createComponent(ControllerCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should choose line', () => {
    spyOn(controllerService, 'check').and.returnValue(of([]));
    const lines = fixture.debugElement.query(By.css('#lines')).nativeElement;
    lines.value = '1B';
    component.onChange({value: {zone: 'ONE', vehicleType: 'BUS'}});
    fixture.detectChanges();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('line is not choosen', () => {
    spyOn(controllerService, 'check').and.returnValue(of([{}, {}]));
    const lines = fixture.debugElement.query(By.css('#lines')).nativeElement;
    lines.value = '1B';
    component.onChange({value: {zone: 'ONE', vehicleType: 'BUS'}});
    fixture.detectChanges();
    expect(component.showTable).toBeTruthy();
  });

});
