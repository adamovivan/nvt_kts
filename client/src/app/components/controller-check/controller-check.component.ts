import { Component, OnInit } from '@angular/core';
import { ControllerService } from '../../services/controller/controller.service';
import { DatePipe } from '@angular/common';
import { CommonsService } from '../../services/commons/commons.service';

@Component({
  selector: 'app-controller-check',
  templateUrl: './controller-check.component.html',
  styleUrls: ['./controller-check.component.scss']
})
export class ControllerCheckComponent implements OnInit {
  lines: any;
  displayedColumns: string[] = ['firstName', 'lastName', 'cardType', 'startDate', 'endDate', 'activeCard', 'paid'];
  dataSource: any;
  showTable = false;

  constructor(private controllerService: ControllerService,
              private datePipe: DatePipe,
              private commonsService: CommonsService) {
                this.controllerService.getControllersLines().subscribe((data) => {
                  this.lines = data;
                });
              }

  ngOnInit() {
  }

  onChange(event) {
    const now = new Date();
    this.controllerService.check(event.value.zone, event.value.vehicleType,
      this.datePipe.transform(now, 'EEE MMM dd yyyy')).subscribe((data: any[]) => {
      if (data.length === 0) {
        this.commonsService.openSnackBar('Vehicle is empty. You must wait for passengers.', 'Ok');
        return;
      }
      this.dataSource = data;
    });
    this.showTable = true;
  }

}
