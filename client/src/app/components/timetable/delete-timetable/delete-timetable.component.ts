import { Component, OnInit } from '@angular/core';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from '../../../services/commons/commons.service';

@Component({
  selector: 'app-delete-timetable',
  templateUrl: './delete-timetable.component.html',
  styleUrls: ['./delete-timetable.component.scss']
})
export class DeleteTimetableComponent implements OnInit {
  scheduleList: any[] = [];
  selectedSchedule: any = '';

  constructor(private timetableService: TimetableService,
    private commonsService: CommonsService) {

    timetableService.getSchedules().subscribe(
      (response: any[]) => {

      if (response.length === 0) {
        this.commonsService.openSnackBar('New schedules does not exist. You must wait for them to be added.', 'Ok');
      } else {
        this.scheduleList = response;
      }
    }); }

  ngOnInit() {
  }

  onChange(event) {
    this.selectedSchedule = event.value;
  }

  onFormSubmit() {
    if (this.selectedSchedule === '') {
      this.commonsService.openSnackBar('You must choose a schedule.', 'Ok');
      return;
    }
    this.timetableService.deleteSchedule(this.selectedSchedule);
  }

}
