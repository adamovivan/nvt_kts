import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTimetableComponent } from './delete-timetable.component';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from '../../../services/commons/commons.service';
import { JsogService } from 'jsog-typescript';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '../../../material.module';
import { MatCardModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('DeleteTimetableComponent', () => {
  let component: DeleteTimetableComponent;
  let fixture: ComponentFixture<DeleteTimetableComponent>;
  let commonService: CommonsService;
  let timetableService: TimetableService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTimetableComponent ],
      providers: [TimetableService, CommonsService, JsogService],
      imports: [
        HttpModule,
        MaterialModule,
        MatCardModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    commonService = TestBed.get(CommonsService);
    timetableService = TestBed.get(TimetableService);
    spyOn(commonService, 'openSnackBar');
    spyOn(timetableService, 'getSchedules').and.returnValue(of([]));
    fixture = TestBed.createComponent(DeleteTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(timetableService.getSchedules).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('deletes schedule', () => {
    component.onChange({ day: 'Workday', endDate: 'Fri Feb 01 2019',
    fromTimes: ['16:02:00', '16:22:00', '16:32:00', '17:02:00', '17:22:00', '17:32:00', '18:02:00', '18:22:00', '18:32:00'],
    pathName: '1B', startDate: 'Tue Jan 01 2019',
    toTimes: ['16:22:00', '16:42:00', '16:52:00', '17:22:00', '17:42:00', '17:52:00', '18:22:00', '18:42:00', '18:52:00'],
    vehicleType: 'BUS'});
    fixture.detectChanges();

    spyOn(component, 'onFormSubmit');
    fixture.debugElement.query(By.css('#deleteButton')).nativeElement.click();
    fixture.detectChanges();

    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('must choose schedule error', () => {
    spyOn(component, 'onFormSubmit');
    fixture.debugElement.query(By.css('#deleteButton')).nativeElement.click();
    fixture.detectChanges();
    expect(commonService.openSnackBar).toHaveBeenCalled();
    expect(component.onFormSubmit).toHaveBeenCalled();
    expect(component.selectedSchedule === '').toBeTruthy();
  });
});
