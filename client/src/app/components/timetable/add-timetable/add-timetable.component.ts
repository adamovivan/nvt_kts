import { Component, OnInit } from '@angular/core';
import { PathService } from '../../../services/path/path.service';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonsService } from '../../../services/commons/commons.service';
import { Schedule } from '../../../model/schedule.model';

@Component({
  selector: 'app-add-timetable',
  templateUrl: './add-timetable.component.html',
  styleUrls: ['./add-timetable.component.scss']
})
export class AddTimetableComponent implements OnInit {
  days: string[] = [];
  lines: any = [];
  times: any[] = [];
  line: any;

  minDate = new Date(Date.now());
  maxDate = new Date(2100, 0, 1);

  timetableForm: FormGroup;

  disableButton = false;

  constructor(pathService: PathService,
              private timetableService: TimetableService,
              private formBuilder: FormBuilder,
              private commonsService: CommonsService) {

    pathService.getAllLines().subscribe(
    (response: any[]) => {

    if (response.length === 0) {
      this.commonsService.openSnackBar('New lines does not exist. You must wait for them to be added.', 'Ok');
      this.disableButton = true;
    } else {
      this.disableButton = false;
    }
    response.forEach(element => {
      let stationsString = '';
      element['stations'].forEach(element1 => {
        stationsString = stationsString + element1.name + ' - ';
      });
      stationsString = stationsString.slice(0, -2);
      this.lines.push({'pathName': element.pathName, 'stations': stationsString, 'vehicleType': element.vehicleType});
      });
    });
    this.days = timetableService.day;
  }

  ngOnInit() {
    this.timetableForm = this.formBuilder.group({
      Day: ['', Validators.required],
      Line: ['', Validators.required],
      toDate: ['', Validators.required],
      fromDate: ['', Validators.required],
      timesTo: ['', Validators.required],
      timesFrom: ['', Validators.required]
    });

    this.times = this.timetableService.generateTimes();
  }

  onChange(event: any) {
    this.line = event.value;
  }

  onFormSubmit() {
    if (this.timetableForm.value['Day'] === '' || this.timetableForm.value['Line'] === '' || this.timetableForm.value['toDate'] === '' ||
    this.timetableForm.value['fromDate'] === '' || this.timetableForm.value['timesFrom'] === [] ||
    this.timetableForm.value['timesTo'] === []) {
    this.commonsService.openSnackBar('You must fill in all the fields.', 'Ok');
      return;
    }

    if (this.timetableForm.value['fromDate'] > this.timetableForm.value['toDate']) {
      this.commonsService.openSnackBar('End date is before start date.', 'Ok');
      return;
    }

    const schedule: Schedule = new Schedule(this.line.pathName, this.timetableForm.value['Day'],
     new Date(this.timetableForm.value['fromDate']).toDateString(),  new Date(this.timetableForm.value['toDate'])
     .toDateString(), this.timetableForm.value['timesTo'], this.timetableForm.value['timesFrom'], this.line.vehicleType);

    this.timetableService.addSchedule(schedule);
  }

}
