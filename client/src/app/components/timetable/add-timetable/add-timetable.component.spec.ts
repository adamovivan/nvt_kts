import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTimetableComponent } from './add-timetable.component';
import { PathService } from '../../../services/path/path.service';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from '../../../services/commons/commons.service';
import { JsogService } from 'jsog-typescript';
import { MaterialModule } from '../../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('AddTimetableComponent', () => {
  let component: AddTimetableComponent;
  let fixture: ComponentFixture<AddTimetableComponent>;
  let commonService: CommonsService;
  let pathService: PathService;
  let lineSelect;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTimetableComponent ],
      providers: [PathService, TimetableService, CommonsService, JsogService],
      imports: [
        HttpModule,
        MaterialModule,
        MatCardModule,
        MatDatepickerModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatNativeDateModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    pathService = TestBed.get(PathService);
    commonService = TestBed.get(CommonsService);
    spyOn(pathService, 'getAllLines').and.returnValue(of([]));
    spyOn(commonService, 'openSnackBar').and.returnValue(of([]));
    fixture = TestBed.createComponent(AddTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 it('should create and pathService.getAllLines to have been called', () => {
    expect(component).toBeTruthy();
    expect(pathService.getAllLines).toHaveBeenCalled();
    // empty lines
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('empty form', () => {
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#addBtn')).nativeElement.click();
    expect(component.timetableForm.valid).toBeFalsy();
  });

  it('valid form', () => {
    spyOn(component, 'onFormSubmit');
    component.disableButton = false;
    component.timetableForm.controls.Day.setValue('Saturday');
    lineSelect = fixture.debugElement.query(By.css('#lines')).nativeElement;
    lineSelect.value = '2B ( mostar 1 - Stanica 6 - Stanica 27 )';
    component.timetableForm.controls.Line.setValue('2B ( mostar 1 - Stanica 6 - Stanica 27 )');
    lineSelect.dispatchEvent(new Event('selectionChange'));
    component.timetableForm.controls.fromDate.setValue('01/01/2020');
    component.timetableForm.controls.toDate.setValue('02/02/2020');
    component.timetableForm.controls.timesFrom.setValue(['11:00', '11:45', '12:00']);
    component.timetableForm.controls.timesTo.setValue(['11:00', '11:45', '12:00']);
    component.line = {pathName: '2B', stations: 'mostar 1 - Stanica 6 - Stanica 27 ', vehicleType: 'BUS'};
    fixture.detectChanges();

    fixture.debugElement.query(By.css('#addBtn')).nativeElement.click();
    fixture.detectChanges();
    expect(component.onFormSubmit).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('all fields must be filled', () => {
    spyOn(component, 'onFormSubmit');
    component.timetableForm.controls.Day.setValue('Saturday');
    lineSelect.value = '2B ( mostar 1 - Stanica 6 - Stanica 27 )';
    component.timetableForm.controls.Line.setValue('2B ( mostar 1 - Stanica 6 - Stanica 27 )');
    component.timetableForm.controls.timesFrom.setValue(['11:00', '11:45', '12:00']);
    component.timetableForm.controls.timesTo.setValue(['11:00', '11:45', '12:00']);
    lineSelect.dispatchEvent(new Event('selectionChange'));
    fixture.debugElement.query(By.css('#addBtn')).nativeElement.click();
    fixture.detectChanges();
    expect(component.timetableForm.valid).toBeFalsy();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('end date is before start date', () => {
    spyOn(component, 'onFormSubmit');
    component.disableButton = false;
    component.timetableForm.controls.Day.setValue('Saturday');
    lineSelect = fixture.debugElement.query(By.css('#lines')).nativeElement;
    lineSelect.value = '2B ( mostar 1 - Stanica 6 - Stanica 27 )';
    component.timetableForm.controls.Line.setValue('2B ( mostar 1 - Stanica 6 - Stanica 27 )');
    lineSelect.dispatchEvent(new Event('selectionChange'));
    component.timetableForm.controls.fromDate.setValue('02/02/2020');
    component.timetableForm.controls.toDate.setValue('01/01/2020');
    component.timetableForm.controls.timesFrom.setValue(['11:00', '11:45', '12:00']);
    component.timetableForm.controls.timesTo.setValue(['11:00', '11:45', '12:00']);
    component.line = {pathName: '2B', stations: 'mostar 1 - Stanica 6 - Stanica 27 ', vehicleType: 'BUS'};
    fixture.detectChanges();

    fixture.debugElement.query(By.css('#addBtn')).nativeElement.click();
    fixture.detectChanges();

    expect(component.onFormSubmit).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
    expect(component.timetableForm.valid).toBeFalsy();
  });
});
