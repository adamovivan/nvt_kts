import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { PathService } from '../../../services/path/path.service';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from 'src/app/services/commons/commons.service';

@Component({
  selector: 'app-timetable',
  templateUrl: './view-timetable.component.html',
  styleUrls: ['./view-timetable.component.scss']
})
export class TimetableComponent implements OnInit {
  days: string[] = [];
  vehicleTypes: string[] = [];
  lines: any = [];
  index;

  timetableForm: FormGroup;

  schedules: any[];
  path: any;
  dates: any[] = [];

  showDate = false;
  showTable = false;
  disableButton = false;

  stationsTo = '';
  stationsFrom = '';
  pathName = '';

  toTimes: any[] = [];
  fromTimes: any[] = [];

  ELEMENT_DATA_toTimes: any[] = [];
  ELEMENT_DATA_fromTimes: any[] = [];

  displayedColumns: string[] = ['time'];

  constructor(pathService: PathService,
              private timetableService: TimetableService,
              private formBuilder: FormBuilder,
              private commonsService: CommonsService) {
    pathService.getAllLines().subscribe(
      (response: any[]) => {
        this.lines = response;
        if (response.length === 0) {
          this.commonsService.openSnackBar('New lines does not exist. You must wait for them to be added.', 'Ok');
          this.disableButton = true;
        } else {
          this.disableButton = false;
        }
      });

    this.days = timetableService.day;
    this.vehicleTypes = pathService.vehicleTypes;
  }

  ngOnInit() {
    this.timetableForm = this.formBuilder.group({
        Day: ['', Validators.required],
        Line: ['', Validators.required],
        vehicleType: ['', Validators.required],
    });
  }

  onFormSubmit() {
    this.showDate = false;
    this.showTable = false;

    this.dates = [];
    let index = 0;

    if (this.timetableForm.value['Day'] !== '' && this.timetableForm.value['Line'] !== ''
    && this.timetableForm.value['vehicleType'] === 'All') {
      this.timetableService.getAllSchedule(this.timetableForm.value['Day'], this.timetableForm.value['Line']).subscribe((data: any) =>  {

        if (data.length === 0) {
          this.commonsService.openSnackBar('For entered criteria schedule does not exist.', 'Ok');
          return;
        }
        console.log(data);
        this.schedules = data;
        this.path = data[0].schedule.path;
        data.forEach(element => {
          this.dates.push({'date' : new Date(element.startDate).toDateString() + ' - ' +
          new Date(element.endDate).toDateString(), 'id' : index});
          index = index + 1;
        });
        this.showDate = true;
      });
      } else if (this.timetableForm.value['Day'] === '' && this.timetableForm.value['Line'] === ''
      && this.timetableForm.value['vehicleType'] === '') {
        this.commonsService.openSnackBar('You must fill in all the fields.', 'Ok');
        return;
      } else if (this.timetableForm['Day'] !== '' && this.timetableForm['Line'] !== '') {
        this.timetableService.getSchedule(this.timetableForm.value['Day'],
        this.timetableForm.value['Line'], this.timetableForm.value['vehicleType']).subscribe((data: any) =>  {

          if (data.length === 0) {
            this.commonsService.openSnackBar('For entered criteria schedule does not exist.', 'Ok');
            return;
          }
          console.log(data);
          this.schedules = data;
          this.path = data[0].schedule.path;
          data.forEach(element => {
            this.dates.push({'date' : new Date(element.startDate).toDateString() + ' - '
            + new Date(element.endDate).toDateString(), 'id' : index});
            index = index + 1;
          });
          this.showDate = true;
        });
      }
    }

    onChange(event: any) {
      this.index = event.value;
      this.stationsTo = '';
      this.stationsFrom = '';
      this.pathName = '';
      this.pathName = this.path.pathName;
      this.ELEMENT_DATA_toTimes = [];
      this.ELEMENT_DATA_fromTimes = [];

      this.toTimes =  this.schedules[this.index].toTimes;
      this.fromTimes =  this.schedules[this.index].fromTimes;

      this.path.stations.forEach(element => {
        this.stationsTo = this.stationsTo + element.name + ' - ';
      });
      this.stationsTo = this.stationsTo.slice(0, -2);

      for (let i =  this.path.stations.length - 1; i >= 0; i--) {
        this.stationsFrom =  this.stationsFrom + this.path.stations[i].name + ' - ';
      }
      this.stationsFrom = this.stationsFrom.slice(0, -2);

      this.toTimes.forEach(element => {
        this.ELEMENT_DATA_toTimes.push({'time': element});
      });

      this.fromTimes.forEach(element => {
        this.ELEMENT_DATA_fromTimes.push({'time': element});
      });

      this.showTable = true;
    }
}
