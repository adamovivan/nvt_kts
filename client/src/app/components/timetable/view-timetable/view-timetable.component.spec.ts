import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetableComponent } from './view-timetable.component';
import { PathService } from '../../../services/path/path.service';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from '../../../services/commons/commons.service';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '../../../material.module';
import { MatCardModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JsogService } from 'jsog-typescript';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('TimetableComponent', () => {
  let component: TimetableComponent;
  let fixture: ComponentFixture<TimetableComponent>;
  let pathService: PathService;
  let commonService: CommonsService;
  let timetableService: TimetableService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetableComponent ],
      providers: [PathService, TimetableService, CommonsService, JsogService],
      imports: [
        HttpModule,
        MaterialModule,
        MatCardModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    pathService = TestBed.get(PathService);
    commonService = TestBed.get(CommonsService);
    timetableService = TestBed.get(TimetableService);

    spyOn(commonService, 'openSnackBar');
    spyOn(timetableService, 'getSchedules').and.returnValue(of([]));
    spyOn(pathService, 'getAllLines').and.returnValue(of([]));

    fixture = TestBed.createComponent(TimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(pathService.getAllLines).toHaveBeenCalled();
    // empty lines
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('all fields must be filled', () => {
    fixture.debugElement.query(By.css('#viewButton')).nativeElement.click();
    fixture.detectChanges();
    expect(component.timetableForm.valid).toBeFalsy();
  });

  it('schedule exists', () => {
    component.timetableForm.controls.Day.setValue('Workday');
    component.timetableForm.controls.Line.setValue('1B');
    component.timetableForm.controls.vehicleType.setValue('BUS');

    fixture.detectChanges();
    spyOn(component, 'onFormSubmit');
    const button = fixture.debugElement.query(By.css('#viewButton')).nativeElement;
    button.click();
    button.dispatchEvent(new Event('click'));

    expect(component.onFormSubmit).toHaveBeenCalled();
    expect(component.timetableForm.valid).toBeTruthy();

    component.index = 1;
    fixture.detectChanges();

    component.schedules = [{active: true, endDate: '2019-02-01',
    fromTimes: ['16:02:00', '16:22:00', '16:32:00', '17:02:00', '17:22:00', '17:32:00', '18:02:00', '18:22:00', '18:32:00'],
    id: '1', schedule: {id: 1, day: 'Workday', active: true,
    path: {pathName: '1B', stations: 'mostar 1 - zs novi beograd', vehicleType: 'BUS'}},
    startDate: '2019-01-01',
    toTimes: ['16:22:00', '16:42:00', '16:52:00', '17:22:00', '17:42:00', '17:52:00', '18:22:00', '18:42:00', '18:52:00']}];
    component.path = {path: {pathName: '1B', stations: 'mostar 1 - zs novi beograd ', vehicleType: 'BUS'},
    stations: [{name: 'zs novi beograd'}, {name: 'mostar 1'}]};
    component.onChange({value: 0});
    fixture.detectChanges();

    expect(component.showTable).toBeTruthy();
  });

});
