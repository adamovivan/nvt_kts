import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-timetable-menu',
  templateUrl: './timetable-menu.component.html',
  styleUrls: ['./timetable-menu.component.scss']
})
export class TimetableMenuComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }

  addTimetable() {
    this.router.navigate(['timetable/menu/add']);
  }

  viewTimetable() {
    this.router.navigate(['timetable/menu/view']);
  }

  updateTimetable() {
    this.router.navigate(['timetable/menu/update']);
  }

  deleteTimetable() {
    this.router.navigate(['timetable/menu/delete']);
  }

}
