import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetableMenuComponent } from './timetable-menu.component';
import { MaterialModule } from '../../../material.module';
import { MatCardModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { By } from '@angular/platform-browser';

describe('TimetableMenuComponent', () => {
  let component: TimetableMenuComponent;
  let fixture: ComponentFixture<TimetableMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetableMenuComponent ],
      providers: [
        {
          provide: Router,
          useClass: class { navigate = jasmine.createSpy('navigate'); }
        }],
      imports: [
        MaterialModule,
        MatCardModule,
        BrowserAnimationsModule,
        RouterModule.forRoot([])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetableMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load add component', () => {
    spyOn(component, 'addTimetable');
    fixture.debugElement.query(By.css('#addTimetableBtn')).nativeElement.click();
    expect(component.addTimetable).toHaveBeenCalled();
  });

  it('should load view component', () => {
    spyOn(component, 'viewTimetable');
    fixture.debugElement.query(By.css('#viewTimetableBtn')).nativeElement.click();
    expect(component.viewTimetable).toHaveBeenCalled();
  });

  it('should load update component', () => {
    spyOn(component, 'updateTimetable');
    fixture.debugElement.query(By.css('#updateTimetableBtn')).nativeElement.click();
    expect(component.updateTimetable).toHaveBeenCalled();
  });

  it('should load delete component', () => {
    spyOn(component, 'deleteTimetable');
    fixture.debugElement.query(By.css('#deleteTimetableBtn')).nativeElement.click();
    expect(component.deleteTimetable).toHaveBeenCalled();
  });
});
