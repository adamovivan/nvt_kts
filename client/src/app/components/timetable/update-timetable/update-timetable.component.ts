import { Component, OnInit } from '@angular/core';
import { TimetableService } from '../../../services/timetable/timetable.service';
import { CommonsService } from '../../../services/commons/commons.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-timetable',
  templateUrl: './update-timetable.component.html',
  styleUrls: ['./update-timetable.component.scss']
})
export class UpdateTimetableComponent implements OnInit {
  scheduleList: any[] = [];
  times: any[] = [];
  showTimes = false;
  selectedSchedule: any;

  timetableForm: FormGroup;

  constructor(private timetableService: TimetableService,
              private commonsService: CommonsService,
              private formBuilder: FormBuilder) {

    timetableService.getSchedules().subscribe(
      (response: any[]) => {
      if (response.length === 0) {
        this.commonsService.openSnackBar('New schedules does not exist. You must wait for them to be added.', 'Ok');
      } else {
        this.scheduleList = response;
      }
    });

    this.times = timetableService.generateTimes();
   }

  ngOnInit() {
    this.timetableForm = this.formBuilder.group({
      timesTo: ['', Validators.required],
      timesFrom: ['', Validators.required]
    });
  }

  onChange(event) {
    this.showTimes = true;
    this.selectedSchedule = event.value;
  }

  onFormSubmit() {
    if (this.timetableForm.value['timesTo'].length === 0 || this.timetableForm.value['timesFrom'].length === 0) {
      this.commonsService.openSnackBar('You must enter from and to times.', 'Ok');
      return;
    }

    this.selectedSchedule['toTimes'] = this.timetableForm.value['timesTo'];
    this.selectedSchedule['fromTimes'] = this.timetableForm.value['timesFrom'];

    this.timetableService.updateSchedule(this.selectedSchedule);
  }
}
