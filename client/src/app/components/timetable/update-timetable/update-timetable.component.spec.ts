import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTimetableComponent } from './update-timetable.component';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '../../../material.module';
import { MatCardModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { JsogService } from 'jsog-typescript';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { TimetableService } from 'src/app/services/timetable/timetable.service';
import { CommonsService } from 'src/app/services/commons/commons.service';

describe('UpdateTimetableComponent', () => {
  let component: UpdateTimetableComponent;
  let fixture: ComponentFixture<UpdateTimetableComponent>;
  let commonService: CommonsService;
  let timetableService: TimetableService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTimetableComponent ],
      providers: [TimetableService, CommonsService, JsogService],
      imports: [
        HttpModule,
        MaterialModule,
        MatCardModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    commonService = TestBed.get(CommonsService);
    timetableService = TestBed.get(TimetableService);
    spyOn(commonService, 'openSnackBar');
    spyOn(timetableService, 'getSchedules').and.returnValue(of([]));
    fixture = TestBed.createComponent(UpdateTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(timetableService.getSchedules).toHaveBeenCalled();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('schedule updated', () => {
    component.ngOnInit();
    component.onChange({ day: 'Workday', endDate: 'Fri Feb 01 2019',
    fromTimes: ['16:02:00', '16:22:00', '16:32:00', '17:02:00', '17:22:00', '17:32:00', '18:02:00', '18:22:00', '18:32:00'],
    pathName: '1B', startDate: 'Tue Jan 01 2019',
    toTimes: ['16:22:00', '16:42:00', '16:52:00', '17:22:00', '17:42:00', '17:52:00', '18:22:00', '18:42:00', '18:52:00'],
    vehicleType: 'BUS'});
    fixture.detectChanges();

    component.timetableForm.controls.timesFrom.setValue(['11:00', '11:45', '12:00']);
    component.timetableForm.controls.timesTo.setValue(['11:00', '11:45', '12:00']);
    fixture.detectChanges();
    spyOn(component, 'onFormSubmit');

    fixture.debugElement.query(By.css('#updateButton')).nativeElement.click();
    fixture.detectChanges();

    expect(component.onFormSubmit).toHaveBeenCalled();
    expect(component.timetableForm.valid).toBeTruthy();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });

  it('schedule - empty fields for times', () => {
    component.ngOnInit();
    component.onChange({ day: 'Workday', endDate: 'Fri Feb 01 2019',
    fromTimes: ['16:02:00', '16:22:00', '16:32:00', '17:02:00', '17:22:00', '17:32:00', '18:02:00', '18:22:00', '18:32:00'],
    pathName: '1B', startDate: 'Tue Jan 01 2019',
    toTimes: ['16:22:00', '16:42:00', '16:52:00', '17:22:00', '17:42:00', '17:52:00', '18:22:00', '18:42:00', '18:52:00'],
    vehicleType: 'BUS'});
    fixture.detectChanges();

    spyOn(component, 'onFormSubmit');
    fixture.debugElement.query(By.css('#updateButton')).nativeElement.click();
    fixture.detectChanges();

    expect(component.timetableForm.valid).toBeFalsy();
    expect(commonService.openSnackBar).toHaveBeenCalled();
  });
});
