import { Component, OnInit } from '@angular/core';
import {PathService} from '../../services/path/path.service';
import {CardService} from '../../services/card/card.service';
import {CardDTO} from '../../model/card.model';
import {MatDialogRef} from '@angular/material';
import {CommonsService} from '../../services/commons/commons.service';

@Component({
  selector: 'app-buy-card',
  templateUrl: './buy-card.component.html',
  styleUrls: ['./buy-card.component.scss']
})
export class BuyCardComponent implements OnInit {

  cardTypes: string[];
  zones: string[];
  vehicles: string[];

  currentPrice = '0';
  currentCardType: string;
  currentZone: string;
  currentVehicleType: string;

  constructor(private pathService: PathService, private cardService: CardService,
    private dialogRef: MatDialogRef<BuyCardComponent>, public commonsService: CommonsService) {
    this.cardTypes = this.cardService.cardTypes;
    this.zones = this.pathService.zones;
    this.vehicles = this.pathService.vehicleTypes;
  }

  ngOnInit() {
  }

  buyCard() {
    const cardDTO = this.getCardDTOFromForm();
    this.cardService.buyCard(cardDTO).subscribe(() => {
      this.commonsService.openSnackBar('Card bought.', 'Ok'); });
    this.dialogRef.close();
  }

  notFormFilled() {
    return !(( this.currentCardType != null || this.currentCardType !== undefined ) &&
      ( this.currentVehicleType != null || this.currentVehicleType !== undefined ) &&
      ( this.currentZone != null || this.currentZone !== undefined ));
  }

  getCardDTOFromForm(): CardDTO {
    const cardDTO = new CardDTO();
    cardDTO.cardType = this.currentCardType;
    cardDTO.zone = this.currentZone;
    cardDTO.vehicleType = this.currentVehicleType;
    return cardDTO;
  }

  checkPrice() {
    const cardDTO = this.getCardDTOFromForm();

    if (cardDTO.cardType == null || cardDTO.vehicleType == null || cardDTO.zone == null) {
      return;
    }

    this.cardService.updateCurrentPrice(cardDTO)
    .subscribe((response) => { this.currentPrice = response.toString(); });
  }

}
