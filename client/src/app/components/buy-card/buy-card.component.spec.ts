import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { BuyCardComponent } from './buy-card.component';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../../material.module';
import {PathService} from '../../services/path/path.service';
import {CardService} from '../../services/card/card.service';
import {CardServiceMocked} from '../../services/mocks/card.service.mocked';
import {MatDialogRef} from '@angular/material';
import {MatDialogMock} from '../../services/mocks/mat.dialog.mock';
import {CommonsServiceMocked} from '../../services/mocks/commons.service.mocked';
import { PathServiceMock } from 'src/app/services/mocks/path.service.mock';
import { CommonsService } from 'src/app/services/commons/commons.service';

describe('BuyCardComponent', () => {

  let component: BuyCardComponent;
  let fixture: ComponentFixture<BuyCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuyCardComponent],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule,
      ],
      providers: [
        {provide: PathService, useClass: PathServiceMock},
        {provide: CardService, useClass: CardServiceMocked},
        {provide: MatDialogRef, useClass: MatDialogMock},
        {provide: CommonsService, useClass: CommonsServiceMocked}
      ]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(BuyCardComponent);
        component = fixture.componentInstance;
      });

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not be filled', fakeAsync(() => {
    component.currentZone = 'ONE';
    component.currentCardType = 'DAILY';
    component.checkPrice();
    fixture.detectChanges();
    tick();
    expect(component.notFormFilled()).toBeTruthy();
    expect(component.currentPrice).toBe('0');

  }));

  it('should be filled', fakeAsync(() => {
    component.currentZone = 'ONE';
    component.currentCardType = 'DAILY';
    component.currentVehicleType = 'BUS';
    component.checkPrice();
    fixture.detectChanges();
    tick();
    expect(component.notFormFilled()).toBeFalsy();
    expect(component.currentPrice !== '0').toBeTruthy();

  }));

  it('should be bought', fakeAsync(() => {
    component.currentZone = 'ONE';
    component.currentCardType = 'DAILY';
    component.currentVehicleType = 'BUS';
    fixture.detectChanges();
    spyOn(component, 'buyCard');
    const el = fixture.debugElement.query(By.css('#buyCardButton')).nativeElement;
    el.click();
    tick();
    expect(component.notFormFilled()).toBeFalsy();
    expect(component.buyCard).toHaveBeenCalledTimes(1);
  }));

});
