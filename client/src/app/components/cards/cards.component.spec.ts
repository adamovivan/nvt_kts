import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsComponent } from './cards.component';
import { CardService } from 'src/app/services/card/card.service';
import { CardServiceMocked } from 'src/app/services/mocks/card.service.mocked';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CardsComponent', () => {
  let component: CardsComponent;
  let fixture: ComponentFixture<CardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,
                 MaterialModule,
                 BrowserAnimationsModule
               ],
      declarations: [ CardsComponent ],
      providers: [
        {provide: CardService, useClass: CardServiceMocked}
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should chekc paginator initial state', () => {
    component.ngOnInit();

    expect(component.totalElements).toBe(100);
    expect(component.paginator.pageIndex).toBe(0);
    expect(component.pageSize).toBe(10);
  });

  it('should check paginator state when page is changed', () => {
    component.paginator.nextPage();
    expect(component.paginator.pageIndex).toBe(1);
  });

  it('should increase page size', () => {
    component.paginator._changePageSize(20);
    expect(component.pageSize).toBe(20);
  });

  it('should create params object with appropriate attributes', () => {

    component.filterUsername = 'user';
    component.filterVisitorType = 'REGULAR';
    component.filterZone = 'ONE';
    component.filterVehicleType = 'BUS';
    component.filterStartDate = '2019-01-01';
    component.filterEndDate = '2020-01-01';
    component.filterPriceFrom = 500;
    component.filterPriceTo = 4000;
    component.filterActive = 'Yes';

    const params = component.getParams(1, 20);

    expect(params['username']).toBe('user');
    expect(params['visitorType']).toBe('REGULAR');
    expect(params['zone']).toBe('ONE');
    expect(params['vehicleType']).toBe('BUS');
    expect(params['startDate']).toBe('2019-01-01');
    expect(params['endDate']).toBe('2020-01-01');
    expect(params['priceFrom']).toBe(500);
    expect(params['priceTo']).toBe(4000);
    expect(params['active']).toBe('Yes');

    expect(params['page']).toBe(1);
    expect(params['size']).toBe(20);
    expect(params['sort']).toBe('id,desc');
  });

  it('should set sortBy attribute to visitorType', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'visitorType';

    component.sortTable(data);
    expect(component.sortBy).toBe('visitor.visitorType');
  });

  it('should set sortBy attribute to priceListItem.item.zone', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'zone';

    component.sortTable(data);
    expect(component.sortBy).toBe('priceListItem.item.zone');
  });

  it('should set sortBy attribute to priceListItem.item.vehicleType', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'vehicleType';

    component.sortTable(data);
    expect(component.sortBy).toBe('priceListItem.item.vehicleType');
  });

  it('should set sortBy attribute to startDate', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'startDate';

    component.sortTable(data);
    expect(component.sortBy).toBe('startDate');
  });

  it('should set sortBy attribute to endDate', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'endDate';

    component.sortTable(data);
    expect(component.sortBy).toBe('endDate');
  });

  it('should set sortBy attribute to active', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'active';

    component.sortTable(data);
    expect(component.sortBy).toBe('active');
  });

  it('should set sortBy attribute to priceListItem.price', () => {
    const data = {};
    data['direction'] = 'asc';
    data['active'] = 'price';

    component.sortTable(data);
    expect(component.sortBy).toBe('priceListItem.price');
  });

});
