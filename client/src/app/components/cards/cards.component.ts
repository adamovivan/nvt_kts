import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { CardService } from 'src/app/services/card/card.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {

  displayedColumns: string[] = ['no', 'username', 'visitorType', 'zone', 'vehicleType',
                                'cardType', 'startDate', 'endDate', 'active', 'price'];
  dataSource = new MatTableDataSource<Card>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  pageOptions: number[] = [5, 10, 20, 40];
  pageSize = 10;
  pageIndex = 0;
  totalElements = 0;

  filterUsername: string;
  filterVisitorType: string;
  filterZone: string;
  filterVehicleType: string;
  filterCardType: string;
  filterPriceFrom: number;
  filterPriceTo: number;
  filterStartDate: string;
  filterEndDate: string;
  filterActive: string;

  sortBy = 'id';
  sortDirection = 'desc';

  showFilter = false;

  zones = [, 'One', 'Two', 'Three'];
  visitorTypes = [, 'Regular', 'Student', 'Pensioner'];
  vehicleTypes = [, 'Bus', 'Metrotrain', 'Trolley'];
  cardTypes = [, 'One Drive', 'Daily', 'Monthly', 'Yearly'];
  activeOptions = [, 'Yes', 'No'];

  constructor(public cardService: CardService) {

  }

  ngOnInit() {
    const params = this.getParams(0, this.pageSize);

    this.cardService.getCards(params).subscribe(data => {
      this.dataSource.data = data['content'];
      this.totalElements = data['totalElements'];
    });

    this.sort.sortChange.subscribe(data => {
      this.sortTable(data);
      this.paginator.pageIndex = 0;
    });
  }

  pageChanged(event) {
    this.pageSize = event['pageSize'];

    const params = this.getParams(event['pageIndex'], event['pageSize']);

    this.cardService.getCards(params).subscribe(data => {
      this.dataSource.data = data['content'];
      this.totalElements = data['totalElements'];
    });
  }

  onFilter() {
    const params = this.getParams(0, this.pageSize);

    this.cardService.getCards(params).subscribe(data => {
      this.dataSource.data = data['content'];
      this.totalElements = data['totalElements'];
    });
  }

  getParams(page, size) {
    const params = {};
    params['page'] = page;
    params['size'] = size;
    params['sort'] = this.sortBy + ',' + this.sortDirection;

    this.filterUsername ? params['username'] = this.filterUsername : params['username'] = '';
    if (this.filterVisitorType) { params['visitorType'] = this.filterVisitorType.toUpperCase(); }
    if (this.filterZone) { params['zone'] = this.filterZone.toUpperCase(); }
    if (this.filterVehicleType) { params['vehicleType'] = this.filterVehicleType.toUpperCase(); }
    if (this.filterCardType) { params['cardType'] = this.filterCardType.replace(/\s/g, '').toUpperCase(); }
    if (this.filterPriceFrom) { params['priceFrom'] = this.filterPriceFrom; }
    if (this.filterPriceTo) { params['priceTo'] = this.filterPriceTo; }
    if (this.filterStartDate) { params['startDate'] = this.filterStartDate; }
    if (this.filterEndDate) { params['endDate'] = this.filterEndDate; }
    if (this.filterActive) { params['active'] = this.filterActive; }

    return params;
  }

  sortTable(data) {
    this.sortDirection = data['direction'];

    data['active'] === 'visitorType' ? this.sortBy = 'visitor.visitorType' :
    data['active'] === 'zone' ? this.sortBy = 'priceListItem.item.zone' :
    data['active'] === 'vehicleType' ? this.sortBy = 'priceListItem.item.vehicleType' :
    data['active'] === 'cardType' ? this.sortBy = 'priceListItem.item.cardType' :
    data['active'] === 'price' ? this.sortBy = 'priceListItem.price' :
    this.sortBy = data['active'];

    const params = this.getParams(0, this.pageSize);

    this.cardService.getCards(params).subscribe(res => {
      this.dataSource.data = res['content'];
      this.totalElements = res['totalElements'];
    });
  }

}

export interface Card {
  username: string;
  visitorType: string;
  zone: string;
  vehicleType: string;
  price: number;
  startDate: string;
  endDate: string;
  active: boolean;
}
