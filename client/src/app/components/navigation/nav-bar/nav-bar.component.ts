import { Component, OnInit } from '@angular/core';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import {Router} from '@angular/router';
import {LoggedStatusService} from '../../../services/loggedstatus/loggedstatus.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(
    private commonsService: CommonsService,
    private authService: AuthenticationService,
    private router: Router,
    public loggedStatusService: LoggedStatusService) {
  }

  ngOnInit() {
    const roles = this.authService.getRoles();
    this.checkRoles(roles);

    this.authService.loginObs.subscribe(res => {
        this.checkRoles(res);
    });
  }

  toggleSideNav() {
    this.commonsService.toggleSideNav();
  }

  showNavbarToogle() {
    return this.loggedStatusService.adminLogged;
  }

  checkRoles(roles) {

    this.loggedStatusService.loggedIn = false;
    this.loggedStatusService.visitorLogged = false;
    this.loggedStatusService.adminLogged = false;
    this.loggedStatusService.inspectorLogged = false;
    this.loggedStatusService.controllerLogged = false;

    if (!this.authService.isAuthenticated()) {
      return;
    }

    if (roles.length === 0) {
      return;
    } else if (roles[0] === 'ROLE_VISITOR') {
      this.loggedStatusService.visitorLogged = true;
    } else if (roles[0] === 'ROLE_ADMIN') {
      this.loggedStatusService.adminLogged = true;
    } else if (roles[0] === 'ROLE_INSPECTOR') {
      this.loggedStatusService.inspectorLogged = true;
    } else if (roles[0] === 'ROLE_CONTROLLER') {
      this.loggedStatusService.controllerLogged = true;
    }

    this.loggedStatusService.loggedIn = this.loggedStatusService.visitorLogged || this.loggedStatusService.adminLogged ||
      this.loggedStatusService.inspectorLogged || this.loggedStatusService.controllerLogged;
  }

  logout() {
    this.authService.logout();
    this.authService.loginSubject.next([]);
    this.loggedStatusService.loggedIn = false;
    this.router.navigate(['/home']);
  }

}
