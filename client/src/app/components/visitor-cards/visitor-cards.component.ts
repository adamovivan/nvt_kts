import {Component, OnInit, ViewChild} from '@angular/core';
import {CardDTO} from '../../model/card.model';
import {CardService} from '../../services/card/card.service';
import {MatDialog, MatDialogConfig, MatPaginator} from '@angular/material';
import {BuyCardComponent} from '../buy-card/buy-card.component';

@Component({
  selector: 'app-show-cards',
  templateUrl: './visitor-cards.component.html',
  styleUrls: ['./visitor-cards.component.scss']
})
export class VisitorCardsComponent implements OnInit {

  columnsToDisplay = ['id', 'cardType', 'price', 'zone', 'vehicleType', 'startDate', 'endDate', 'isActive' ];

  @ViewChild(MatPaginator) matPaginator: MatPaginator;
  pageSizeOptions = [5, 10, 20, 40];
  pageIndex = 0;
  pageSize: number = this.pageSizeOptions[0];
  totalElements: number;
  visitorCards: CardDTO[];

  constructor(public cardService: CardService, public dialog: MatDialog) {
  }

  openQuickBuy() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '300px';
    dialogConfig.height = '400px';
    this.dialog.open( BuyCardComponent, dialogConfig ).afterClosed().subscribe(
      () => {
        this.pageChanged(null);
      }
    );
  }

  getParams(event) {

    if (event == null || event === undefined) {
      return {'page': this.pageIndex, 'size': this.pageSize};
    }
    this.pageIndex = event['pageIndex'];
    this.pageSize = event['pageSize'];
    return {'page': this.pageIndex, 'size': this.pageSize};
  }

  pageChanged(event) {
    this.cardService.getVisitorCards(this.getParams(event))
      .subscribe((response) => {
      this.visitorCards = <CardDTO[]>(response['content']);
      this.totalElements = response['totalElements'];
    } );
  }

  ngOnInit() {
    this.pageChanged(null);
  }

}
