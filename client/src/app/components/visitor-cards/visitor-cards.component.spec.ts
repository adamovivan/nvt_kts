import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { VisitorCardsComponent } from './visitor-cards.component';
import {CardService} from '../../services/card/card.service';
import {CardServiceMocked} from '../../services/mocks/card.service.mocked';
import {MatDialogRef} from '@angular/material';
import {MatDialogMock} from '../../services/mocks/mat.dialog.mock';
import {BrowserModule, By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../../material.module';

function getTable(fixture: ComponentFixture<VisitorCardsComponent>) {
    return fixture.debugElement.query(By.css('table.mat-table')).nativeElement;
}


describe('VisitorCardsComponent', () => {
  let component: VisitorCardsComponent;
  let fixture: ComponentFixture<VisitorCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorCardsComponent ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule
      ],
      providers: [
        {provide: CardService, useClass: CardServiceMocked},
        {provide: MatDialogRef, useClass: MatDialogMock}
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show 5 cards and turn a page', async(() => {
    spyOn(component.cardService, 'getVisitorCards').and.callThrough();
    component.matPaginator._changePageSize(5);
    component.matPaginator.firstPage();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.matPaginator._pageIndex === component.pageIndex).toBeTruthy();
      expect(getTable(fixture).rows.length).toBe((5 + 1));
    });


    component.matPaginator.nextPage();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.matPaginator._pageIndex === component.pageIndex).toBeTruthy();
      expect(getTable(fixture).rows.length).toBe((5 + 1));
      expect(component.cardService.getVisitorCards).toHaveBeenCalledTimes(2);
    });
  }));

  it('should show 10 cards', async(() => {
    spyOn(component.cardService, 'getVisitorCards').and.callThrough();
    component.matPaginator._changePageSize(10);
    component.matPaginator.firstPage();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.matPaginator._pageIndex === component.pageIndex).toBeTruthy();
      expect(component.cardService.getVisitorCards).toHaveBeenCalledTimes(1);
      expect(getTable(fixture).rows.length).toBe((10 + 1));
    });
  }));



});
