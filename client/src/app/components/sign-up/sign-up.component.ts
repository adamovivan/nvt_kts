import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { CommonsService } from '../../services/commons/commons.service';
import { VisitorService } from '../../services/visitor/visitor.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private commonsService: CommonsService,
              private visitorService: VisitorService) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {

    if (form['firstName'] === '' || form['lastName'] === '' || form['username'] === '' ||
    form['password'] === '' || form['confirmPassword'] === '') {
      this.commonsService.openSnackBar('You must fill in all the fields.', 'Ok');
      return;
    }

    if (form['password'] !== form['confirmPassword']) {
      this.commonsService.openSnackBar('The passwords are not the same.', 'Ok');
      this.signUpForm.controls['password'].setErrors({'incorrect': true});
      this.signUpForm.controls['confirmPassword'].setErrors({'incorrect': true});
      return;
    }


    this.visitorService.addVisitor({'firstName': form['firstName'], 'lastName': form['lastName'],
                                     'username': form['username'], 'password': form['password']});

  }

}
