import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';
import { CommonsService } from '../../services/commons/commons.service';
import { VisitorService } from '../../services/visitor/visitor.service';
import { MaterialModule } from '../../material.module';
import { HttpClientModule } from '@angular/common/http';
import { JsogService } from 'jsog-typescript';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignUpComponent],
      providers: [CommonsService, VisitorService, JsogService,
        {
          provide: Router,
          useClass: class { navigate = jasmine.createSpy('navigate'); }
        }],

      imports: [
        MaterialModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('empty form', () => {
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('passwords are not equal', () => {
    component.signUpForm.controls.firstName.setValue('Maja');
    component.signUpForm.controls.lastName.setValue('Pajic');
    component.signUpForm.controls.username.setValue('maja1234');
    component.signUpForm.controls.password.setValue('1234');
    component.signUpForm.controls.confirmPassword.setValue('1234555');
    fixture.debugElement.query(By.css('#signup')).nativeElement.click();
    fixture.detectChanges();
    expect(component.signUpForm.valid).toBeFalsy();
  });

  it('should be signed up', () => {
    component.signUpForm.controls.firstName.setValue('Maja');
    component.signUpForm.controls.lastName.setValue('Pajic');
    component.signUpForm.controls.username.setValue('maja1234');
    component.signUpForm.controls.password.setValue('1234');
    component.signUpForm.controls.confirmPassword.setValue('1234');
    spyOn(component, 'onFormSubmit');
    fixture.debugElement.query(By.css('#signup')).nativeElement.click();
    fixture.whenStable().then(() => {
       expect(component.signUpForm.valid).toBeTruthy();
    });
  });
});
