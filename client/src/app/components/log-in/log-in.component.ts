import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { LogInModel } from 'src/app/model/log-in.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  user: LogInModel = new LogInModel();
  hide = true;
  badCredentials = false;

  constructor(public authService: AuthenticationService, public router: Router) { }

  ngOnInit() {
    this.authService.clearStorage();
  }

  onLoginSubmit() {
    this.authService.login(this.user).subscribe(data => {
      this.authService.saveToken(data.accessToken);

      const roles = this.authService.getRoles();

      if (roles.length > 0) {
        this.authService.loginSubject.next(roles);
        this.router.navigate(['home']);
      }

      this.badCredentials = false;
    },
    error => {
        if (error['status'] === 403) {
          this.badCredentials = true;
        }
      }
    );
  }

}
