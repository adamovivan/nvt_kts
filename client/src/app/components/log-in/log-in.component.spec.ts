import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogInComponent } from './log-in.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';


describe('LogInComponent', () => {
  let component: LogInComponent;
  let fixture: ComponentFixture<LogInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogInComponent ],
      imports: [
                BrowserAnimationsModule,
                FormsModule,
                MaterialModule,
                RouterTestingModule
      ],
      providers: [
                HttpClient,
                HttpHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log in', () => {
    spyOn(component.authService, 'login').and.returnValue(of(''));
    spyOn(component.authService, 'getRoles').and.returnValue(['ROLE_VISITOR']);
    spyOn(component.router, 'navigate').and.returnValue(null);

    fixture.debugElement.query(By.css('#btnLogin')).nativeElement.click();

    expect(component.authService.login).toHaveBeenCalled();
    expect(component.authService.getRoles).toHaveBeenCalled();
    expect(component.router.navigate).toHaveBeenCalledWith(['home']);
  });

});
