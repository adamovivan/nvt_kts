import { Coord } from './coord.model';
import { Station } from './station.model';
import { Vehicle } from './vehicle.model';

export class Path {

    public id: number;
    public pathName = '';
    public vehicleType = '';
    public coords: Coord[] = [];
    public stations: Station[] = [];
    public vehicles: Vehicle[] = [];
    public zone = '';

}
