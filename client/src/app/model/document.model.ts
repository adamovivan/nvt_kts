export class DocumentModel {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    documentName: string;
    visitorType: string;

}
