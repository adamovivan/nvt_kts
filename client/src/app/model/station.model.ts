import { Coord } from './coord.model';
import { Path } from './path.model';

//  default values set for view rendering
export class Station {
    constructor(name: string) {
        this.name = name;
    }
    public id: number;
    public name = '';
    public paths: Path[] = [];
    public coord: Coord;
    public pathsStr = '';

}
