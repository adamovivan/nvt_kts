
export class CardDTO {

    zone: string;
    vehicleType: string;
    cardType: string;
    id: number;
    price: number;
    visitorType: string;
    active: boolean;
    startDate: string;
    endDate: string;

}
