export class Schedule {

    public pathName: string;
    public day: string;
    public startDate: string;
    public endDate: string;
    public toTimes: any[];
    public fromTimes: any[];
    public vehicleType: string;

    constructor(pathName: string, day: string, startDate: string, endDate: string, toTimes: any[], fromTimes: any[], vechileType: string) {
       this.pathName = pathName;
       this.day = day;
       this.startDate = startDate;
       this.endDate = endDate;
       this.toTimes = toTimes;
       this.fromTimes = fromTimes;
       this.vehicleType = vechileType;
    }
}
