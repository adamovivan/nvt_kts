
export class PriceListItemData {

  public zone: string;
  public visitorType: string;
  public price: number;
  public cardType: string;
  public vehicleType: string;
  public priceListId: number;

  constructor(priceListItem: any) {

    this.price = priceListItem['price'];
    this.zone = priceListItem['zone'];
    this.visitorType = priceListItem['visitorType'];
  }


}

export class PriceListInfo {

  startDate: string;
  endDate: string;
  id: number;
  editable: boolean;

  public toString(): string {
    return 'PriceList: ' + this.startDate + ' - ' + this.endDate;
  }

}


export class PriceListData {

  public ONEDRIVE: PriceListItemData[];
  public DAILY: PriceListItemData[];
  public MONTHLY: PriceListItemData[];
  public YEARLY: PriceListItemData[];

  constructor() {

      this.ONEDRIVE =  [];
      this.DAILY = [];
      this.MONTHLY = [];
      this.YEARLY = [];
  }

  setNewData(rawData: any) {
    this.ONEDRIVE =  [];
    this.DAILY = [];
    this.MONTHLY = [];
    this.YEARLY = [];

    for (const priceListItem of rawData) {
      this[priceListItem['cardType']].push(new PriceListItemData(priceListItem));
    }
  }

}
