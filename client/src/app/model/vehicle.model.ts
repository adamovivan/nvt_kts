import { Path } from './path.model';
import { Coord } from './coord.model';

export class Vehicle {

    public id: number;
    public vehicleType: string;
    public path: Path;
    public coord: Coord;

    init(path: Path) {
        this.path = path;
        this.vehicleType = path.vehicleType;
    }

}
