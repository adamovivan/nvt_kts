import { Path } from './path.model';
import { Coord } from './coord.model';
import { Station } from './station.model';
import { Vehicle } from './vehicle.model';

export class MapData {

    constructor(currentPath: Path, pathCoords: Coord[], stations: Station[],
        vehicles: Vehicle[], pathEditMarkers: Coord[], stationDraggable: boolean) {

        this.currentPath = currentPath;
        this.pathCoords = pathCoords;
        this.stations = stations;
        this.vehicles = vehicles;
        this.pathEditMarkers = pathEditMarkers;
        this.stationDraggable = stationDraggable;
    }

    public currentPath: Path;
    public pathCoords: Coord[];
    public stations: Station[];
    public vehicles: Vehicle[];
    public pathEditMarkers: Coord[];
    public allPathNumbers: string[];
    public stationDraggable: boolean;
    public stationClickable: boolean;
}
