import { LatLngLiteral } from '@agm/core';

export class Coord implements LatLngLiteral {

    constructor(e: number, n: number) {
        this.e = e;
        this.n = n;
        this.lat = n;
        this.lng = e;
    }
    // for updating path rute easier
    public id: number;
    public lat: number;
    public lng: number;
    public e: number;
    public n: number;


}
