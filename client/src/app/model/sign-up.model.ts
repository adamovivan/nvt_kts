export class SignUpModel {
    firstName: String;
    lastName: String;
    username: String;
    password: String;
    repeatPassword: String;
}
