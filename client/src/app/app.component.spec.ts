import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {NavBarComponent} from './components/navigation/nav-bar/nav-bar.component';
import {SidenavComponent} from './sidenav/sidenav.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {CommonsService} from './services/commons/commons.service';
import {CommonsServiceMocked} from './services/mocks/commons.service.mocked';
import {HttpClient} from '@angular/common/http';
import {LoggedStatusService} from './services/loggedstatus/loggedstatus.service';
import {LoggedstatusServiceMocked} from './services/mocks/loggedstatus.service.mocked';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        MaterialModule
      ],
      declarations: [
        AppComponent,
        NavBarComponent,
        SidenavComponent
      ],
      providers: [
        {provide: LoggedStatusService, useClass: LoggedstatusServiceMocked},
        {provide: CommonsService, useClass: CommonsServiceMocked },

        {provide: HttpClient}

      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'client'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('client');
  });
});
