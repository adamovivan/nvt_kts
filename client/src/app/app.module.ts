import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { PathService } from './services/path/path.service';
import { HttpModule } from '@angular/http';
import { MapService } from './services/map/map.service';
import { TestService } from './services/test/test.service';
import { CommonsService } from './services/commons/commons.service';
import { StationService } from './services/station/station.service';
import { AuthenticationService } from './services/authentication/authentication.service';
import { HomeComponent } from './components/home/home.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { MapComponent } from './components/map/map.component';
import { LineSelectComponent } from './components/line-select/line-select.component';
import { AuthTokenInterceptor } from './interceptors/auth-token.interceptor';
import { VehicleService } from './services/vehicle/vehicle.service';
import { SidenavComponent } from './sidenav/sidenav.component';
import { AddStationComponent } from './entities/station/add-station/add-station.component';
import { JsogService } from 'jsog-typescript';
import { EditStationComponent } from './entities/station/edit-station/edit-station.component';
import { AddPathComponent } from './entities/path/add-path/add-path.component';
import { EditPathComponent } from './entities/path/edit-path/edit-path.component';
import { AddPriceListComponent } from './components/pricelist/add-price-list/add-price-list.component';
import { MatDatepickerModule, MatMenuModule, MatNativeDateModule, MatTableModule, MatTabsModule} from '@angular/material';
import { CardService} from './services/card/card.service';
import { VisitorService} from './services/visitor/visitor.service';
import { AppRoutingModule } from './app-routing.module';
import { NavBarComponent } from './components/navigation/nav-bar/nav-bar.component';
import { PricelistService} from './services/pricelist/pricelist.service';
import { ShowPricelistComponent } from './components/pricelist/show-pricelist/show-pricelist.component';
import { PricelistTabViewComponent } from './components/pricelist/pricelist-tab-view/pricelist-tab-view.component';
import { CardsComponent } from './components/cards/cards.component';
import { BuyCardComponent } from './components/buy-card/buy-card.component';
import { VisitorCardsComponent } from './components/visitor-cards/visitor-cards.component';
import { LoggedStatusService} from './services/loggedstatus/loggedstatus.service';
import { PendingDocsComponent } from './components/pending-docs/pending-docs.component';
import { RequestInterceptor } from './interceptors/http-error.Interceptor';
import { GlobalErrorHandler } from './interceptors/error.interceptor';
import { TimetableService } from './services/timetable/timetable.service';
import { TimetableComponent } from './components/timetable/view-timetable/view-timetable.component';
import { TimetableMenuComponent } from './components/timetable/timetable-menu/timetable-menu.component';
import { AddTimetableComponent } from './components/timetable/add-timetable/add-timetable.component';
import { UpdateTimetableComponent } from './components/timetable/update-timetable/update-timetable.component';
import { DeleteTimetableComponent } from './components/timetable/delete-timetable/delete-timetable.component';
import { ControllerCheckComponent } from './components/controller-check/controller-check.component';
import { ControllerService } from './services/controller/controller.service';
import { DatePipe } from '@angular/common';
import { UploadDocumentComponent } from './components/upload-document/upload-document.component';

location['apiUrl'] = 'http://localhost:8080';

@NgModule({
  entryComponents: [BuyCardComponent],
  declarations: [
    AppComponent,
    NavBarComponent,
    SignUpComponent,
    LogInComponent,
    SignUpComponent,
    HomeComponent,
    LineSelectComponent,
    MapComponent,
    SidenavComponent,
    AddStationComponent,
    EditStationComponent,
    AddPathComponent,
    EditPathComponent,
    AddPriceListComponent,
    ShowPricelistComponent,
    PricelistTabViewComponent,
    AddPriceListComponent,
    CardsComponent,
    BuyCardComponent,
    VisitorCardsComponent,
    PendingDocsComponent,
    TimetableComponent,
    TimetableMenuComponent,
    AddTimetableComponent,
    UpdateTimetableComponent,
    DeleteTimetableComponent,
    ControllerCheckComponent,
    UploadDocumentComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA4IWyjGqpUmDZCiWfymeLDHfSHwUaocT8'
    }),
    BrowserAnimationsModule,
    MaterialModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AppRoutingModule,
    MatTabsModule,
    MatTableModule,
    MatMenuModule
  ],
  providers: [
    MapService,
    PathService,
    StationService,
    CommonsService,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    }
    ,
    VehicleService,
    CommonsService,
    TestService,
    JsogService,
    CardService,
    VisitorService,
    PricelistService,
    LoggedStatusService,
    TimetableService,
    ControllerService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
