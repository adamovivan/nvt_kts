-- USERS
INSERT INTO user (id, username, password, first_name, last_name, role)
values (1, 'pera123', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Pera', 'Peric', 'ROLE_ADMIN'); --12345
INSERT INTO admin (id) values (1);

INSERT INTO user (id, username, password, first_name, last_name, role)
values (2, 'mika123', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Mika', 'Mikic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (2,'REGULAR');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (3, 'janko123', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Janko', 'Jankic', 'ROLE_INSPECTOR'); --12345
INSERT INTO inspector (id) values (3);

INSERT INTO user (id, username, password, first_name, last_name, role)
values (4, 'djura.djuric', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Djura', 'Djuric', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (4,'REGULAR');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (5, 'nikola_nikolic', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Nikola', 'Nikolic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (5,'PENSIONER');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (6, 'milica.milica', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Milica', 'Milic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (6,'REGULAR');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (7, 'jovana123', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Jovana', 'Jovic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (7,'STUDENT');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (8, 'rajko51', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Rajko', 'Rajic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (8,'REGULAR');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (9, 'ana.hello', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Ana', 'Anic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (9,'PENSIONER');

INSERT INTO user (id, username, password, first_name, last_name, role)
values (10, 'jana123', '$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC', 'Jana', 'Janjic', 'ROLE_VISITOR'); --12345
INSERT INTO visitor (id,visitor_type) values (10,'REGULAR');

-- ZA KORISCENJE ODKOMENTARISATI i PROMENITI PUTANJU DO SLIKA
-- INSERT INTO document (ID, APPROVAL_STATUS, PATH, NAME, DISCOUNT_VISITOR_TYPE, VISITOR_ID )
-- values (1, 'PENDING', 'C:\Users\ASUS\Documents\NVT_KTS\nvt_kts\server\documents\35d2ed9e-b263-4964-b0f1-1f4a3ebd69a6.jpg', 'Licna karta', 'STUDENT',  2);
-- --
-- INSERT INTO document (ID, APPROVAL_STATUS, PATH, NAME, DISCOUNT_VISITOR_TYPE, VISITOR_ID )
-- values (2, 'PENDING', 'C:\Users\ASUS\Documents\NVT_KTS\nvt_kts\server\documents\8e3fd5aa-e12d-4e37-bc5a-0f85991133c2.jpg', 'Polish identity card', 'PENSIONER',  4);
-- --
-- INSERT INTO document (ID, APPROVAL_STATUS, PATH, NAME, DISCOUNT_VISITOR_TYPE, VISITOR_ID)
-- values (3, 'PENDING', 'C:\Users\ASUS\Documents\NVT_KTS\nvt_kts\server\documents\f585f375-6e07-4564-913a-3e04887d79a5.png', 'Student identity card', 'STUDENT', 6);

-- LINIJE
insert into path ( id , path_name , vehicle_type, zone ) values ( 1, '1B', 'BUS', 'ONE');
insert into path ( id , path_name , vehicle_type, zone ) values ( 2, '2B', 'BUS', 'TWO');
insert into path ( id , path_name , vehicle_type, zone ) values ( 3, '3Ba', 'BUS', 'THREE');
insert into path ( id , path_name , vehicle_type, zone ) values ( 4, '3Bb', 'BUS', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 5, '4Ba', 'BUS', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 6, '4Bb', 'BUS', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 7, '5Ba', 'BUS', 'THREE');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 8, '5Bb', 'BUS', 'THREE');

insert into path ( id , path_name , vehicle_type, zone ) values ( 101, '1M', 'METROTRAIN', 'ONE');
insert into path ( id , path_name , vehicle_type, zone ) values ( 102, '2M', 'METROTRAIN', 'TWO');
insert into path ( id , path_name , vehicle_type, zone ) values ( 103, '3M', 'METROTRAIN', 'THREE');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 104, '4M', 'METROTRAIN', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 106, '5M', 'METROTRAIN', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 107, '6Ma', 'METROTRAIN', 'THREE');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 108, '6Mb', 'METROTRAIN', 'THREE');

insert into path ( id , path_name , vehicle_type, zone ) values ( 201, '1Ta', 'TROLLEY', 'ONE');
insert into path ( id , path_name , vehicle_type, zone ) values ( 202, '1Tb', 'TROLLEY', 'TWO');
insert into path ( id , path_name , vehicle_type, zone ) values ( 203, '2T', 'TROLLEY', 'THREE');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 204, '3T', 'TROLLEY', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 205, '4T', 'TROLLEY', 'TWO');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 207, '5Ta', 'TROLLEY', 'THREE');
-- insert into path ( id , path_name , vehicle_type, zone ) values ( 208, '5Tb', 'TROLLEY', 'THREE');


-- KOORDINATE LINIJA
-- 1B
insert into path_coordinates ( path_id , n, e) values ( 1, 44.798575 , 20.448888 );
insert into path_coordinates ( path_id , n, e) values ( 1, 44.799569 , 20.447055 );
insert into path_coordinates ( path_id , n, e) values ( 1, 44.800422 , 20.445381 );
insert into path_coordinates ( path_id , n, e) values ( 1, 44.811416 , 20.422341 );
insert into path_coordinates ( path_id , n, e) values ( 1, 44.806383 , 20.418706 );

-- 2B  (mostar 1 , 27 , 6)
insert into path_coordinates ( path_id , n, e) values ( 2, 44.794204    , 20.448201 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.79791     , 20.4502613 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.798575    , 20.448888 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.802548210 , 20.441183 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.806       , 20.43344 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.80607153  , 20.432966919 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.805340768 , 20.43206569 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.804762240 , 20.43110010 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.80395533  , 20.42762395 );
insert into path_coordinates ( path_id , n, e) values ( 2, 44.803881    , 20.427 );

-- 3Ba (4,16,3)
insert into path_coordinates ( path_id , n, e) values ( 3, 44.81431  , 20.425037 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.811493 , 20.4315 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.80994  , 20.43 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.808509 , 20.4288 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.806    , 20.43344 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.8113   , 20.42237685 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.81745  , 20.42804168 );
insert into path_coordinates ( path_id , n, e) values ( 3, 44.819437 , 20.4237501 );

-- 3Bb (4,16)
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81481 , 20.43461 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81749 , 20.42800 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81444 , 20.42516 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81149 , 20.43150 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.80994 , 20.43000 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81030 , 20.42877 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81152 , 20.42225 );
insert into path_coordinates ( path_id , n, e) values ( 4, 44.81341 , 20.41787 );

-- 1M
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80151 , 20.43379 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80441 , 20.40370 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80400 , 20.40790 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80394 , 20.41654 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80354 , 20.42370 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80370 , 20.42564 );
insert into path_coordinates ( path_id , n, e) values ( 101, 44.80388 , 20.42700 );
-- 2M
insert into path_coordinates ( path_id , n, e) values ( 102, 44.79360 , 20.45320 );
insert into path_coordinates ( path_id , n, e) values ( 102, 44.79462 , 20.44842 );
insert into path_coordinates ( path_id , n, e) values ( 102, 44.79526 , 20.44477 );
insert into path_coordinates ( path_id , n, e) values ( 102, 44.79581 , 20.44262 );
insert into path_coordinates ( path_id , n, e) values ( 102, 44.79620 , 20.44200 );
-- 3M
insert into path_coordinates ( path_id , n, e) values ( 103, 44.80322 , 20.45098 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.80426 , 20.45162 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.80578 , 20.45308 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.80813 , 20.45475 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.80850 , 20.45500 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.81226 , 20.45345 );
insert into path_coordinates ( path_id , n, e) values ( 103, 44.81499 , 20.45072 );
-- 1T
insert into path_coordinates ( path_id , n, e) values ( 201, 44.81809 , 20.40700 );
insert into path_coordinates ( path_id , n, e) values ( 201, 44.81600 , 20.41160 );
insert into path_coordinates ( path_id , n, e) values ( 201, 44.81303 , 20.40900 );
insert into path_coordinates ( path_id , n, e) values ( 201, 44.81159 , 20.40765 );
insert into path_coordinates ( path_id , n, e) values ( 201, 44.80638 , 20.41871 );
-- 2T
insert into path_coordinates ( path_id , n, e) values ( 202, 44.81360 , 20.40330 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.81168 , 20.40742 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.80952 , 20.41214 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.80638 , 20.41871 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.80489 , 20.42238 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.80276 , 20.43074 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.80126 , 20.43426 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.79928 , 20.43697 );
insert into path_coordinates ( path_id , n, e) values ( 202, 44.79620 , 20.44200 );
-- 3T
insert into path_coordinates ( path_id , n, e) values ( 203, 44.81900 , 20.41458 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.81620 , 20.42090 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.80904 , 20.41388 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.80794 , 20.41328 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.80325 , 20.42332 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.80179 , 20.42470 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.80136 , 20.42504 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.79114 , 20.42710 );
insert into path_coordinates ( path_id , n, e) values ( 203, 44.79382 , 20.43100 );
-- STANICE
insert into station ( id , name , n , e ) values ( 0, 'mostar 1', 44.798575 , 20.448888);
insert into station ( id , name , n , e ) values ( 1, 'sava centar', 44.807116 , 20.431208);
insert into station ( id , name , n , e ) values ( 2, 'zs novi beograd', 44.806383 , 20.418706);
insert into station ( id , name , n , e ) values ( 3, 'Stanica 3', 44.808509  , 20.4288);
insert into station ( id , name , n , e ) values ( 4, 'Stanica 4', 44.811493  , 20.4315);
insert into station ( id , name , n , e ) values ( 5, 'Stanica 5', 44.8053432 , 20.4260);
insert into station ( id , name , n , e ) values ( 6, 'Stanica 6', 44.803881  , 20.4270);
insert into station ( id , name , n , e ) values ( 7, 'Stanica 7', 44.80354   , 20.4237);
insert into station ( id , name , n , e ) values ( 8, 'Stanica 8', 44.79114   , 20.4271);
insert into station ( id , name , n , e ) values ( 9, 'Stanica 9', 44.8039    , 20.4172);
insert into station ( id , name , n , e ) values ( 10,'Stanica 10', 44.80441  , 20.4037);
insert into station ( id , name , n , e ) values ( 11,'Stanica 11', 44.81315  , 20.4089);
insert into station ( id , name , n , e ) values ( 12,'Stanica 12', 44.81918  , 20.4145);
------------------------------------------NOVE STANICE------------------------------------
insert into station ( id , name , n , e ) values ( 13, 'Stanica 13', 44.80446  , 20.41731);
insert into station ( id , name , n , e ) values ( 14, 'Stanica 14', 44.80394  , 20.41654);
insert into station ( id , name , n , e ) values ( 15, 'Stanica 15', 44.8040   , 20.4079 );
insert into station ( id , name , n , e ) values ( 16, 'Stanica 16', 44.80994  , 20.430  );
insert into station ( id , name , n , e ) values ( 17, 'Stanica 17', 44.8162   , 20.4209 );
insert into station ( id , name , n , e ) values ( 18, 'Stanica 18', 44.8190   , 20.41458);
insert into station ( id , name , n , e ) values ( 19, 'Stanica 19', 44.816    , 20.4116 );
insert into station ( id , name , n , e ) values ( 20, 'Stanica 20', 44.81303  , 20.4090 );
insert into station ( id , name , n , e ) values ( 21, 'Stanica 21', 44.8149   , 20.4046 );
insert into station ( id , name , n , e ) values ( 22, 'Stanica 22', 44.81809  , 20.407  );
insert into station ( id , name , n , e ) values ( 23, 'Stanica 23', 44.8085   , 20.455  );
insert into station ( id , name , n , e ) values ( 24, 'Stanica 24', 44.7936   , 20.4532 );
insert into station ( id , name , n , e ) values ( 25, 'Stanica 25', 44.7962   , 20.4420 );
insert into station ( id , name , n , e ) values ( 26, 'Stanica 26', 44.79382  , 20.4310 );
insert into station ( id , name , n , e ) values ( 27, 'Stanica 27', 44.806    , 20.43344);


-- STANICE - LINIJE
-- BUS
-- 1B
insert into paths_stations ( path_id , station_id )  values ( 1, 0);
insert into paths_stations ( path_id , station_id )  values ( 1, 2);
-- 2B
insert into paths_stations ( path_id , station_id )  values ( 2, 0);
insert into paths_stations ( path_id , station_id )  values ( 2, 6);
insert into paths_stations ( path_id , station_id )  values ( 2, 27);
-- 3Ba
insert into paths_stations ( path_id , station_id )  values ( 3, 4);
insert into paths_stations ( path_id , station_id )  values ( 3, 16);
insert into paths_stations ( path_id , station_id )  values ( 3, 3);
-- 3Bb
insert into paths_stations ( path_id , station_id )  values ( 4, 4);
insert into paths_stations ( path_id , station_id )  values ( 4, 16);
-- METRO
-- 1M ( 6,7,10,14,15)
insert into paths_stations ( path_id , station_id )  values ( 101, 6);
insert into paths_stations ( path_id , station_id )  values ( 101, 7);
insert into paths_stations ( path_id , station_id )  values ( 101, 10);
insert into paths_stations ( path_id , station_id )  values ( 101, 14);
insert into paths_stations ( path_id , station_id )  values ( 101, 15);
-- 2M
insert into paths_stations ( path_id , station_id )  values ( 102, 25);
insert into paths_stations ( path_id , station_id )  values ( 102, 24);
-- 3M
insert into paths_stations ( path_id , station_id )  values ( 103, 23);
-- VOZ
-- 1T
insert into paths_stations ( path_id , station_id )  values ( 201, 2);
insert into paths_stations ( path_id , station_id )  values ( 201, 19);
insert into paths_stations ( path_id , station_id )  values ( 201, 20);
insert into paths_stations ( path_id , station_id )  values ( 201, 22);
-- 2T
insert into paths_stations ( path_id , station_id )  values ( 202, 2);
insert into paths_stations ( path_id , station_id )  values ( 202, 25);
-- 3T
insert into paths_stations ( path_id , station_id )  values ( 203, 17);
insert into paths_stations ( path_id , station_id )  values ( 203, 18);
insert into paths_stations ( path_id , station_id )  values ( 203, 8 );
insert into paths_stations ( path_id , station_id )  values ( 203, 26);
---------------------------------------------------------------------

-- VOZILA
-- 1B
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 1, 1, 'BUS', 44.811961, 20.452162);
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 2, 1, 'BUS', 44.811961, 20.452162);
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 3, 1, 'BUS', 44.811961, 20.452162);
-- 2B
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 4, 2, 'BUS', 44.811961, 20.452162);
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 5, 2, 'BUS', 44.811961, 20.452162);
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 6, 2, 'BUS', 44.811961, 20.452162);
-- 3Ba
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 7, 3, 'BUS', 44.811961, 20.452162);
-- 3Bb
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 8, 3, 'BUS', 44.811961, 20.452162);
-- 1M
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 9, 101, 'METROTRAIN', 44.811961, 20.452162);
-- 2M
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 10, 102, 'METROTRAIN', 44.811961, 20.452162);
-- 3M
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 11, 103, 'METROTRAIN', 44.811961, 20.452162);
-- 1T
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 12, 201, 'TROLLEY', 44.811961, 20.452162);
-- 2T
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 13, 202, 'TROLLEY', 44.811961, 20.452162);
-- 3T
insert into vehicle ( id , path_id , vehicle_type , n , e) values ( 14, 203, 'TROLLEY', 44.811961, 20.452162);
--

-- ITEM - ONE DRIVE - BUS
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (1, 'ONEDRIVE', 'REGULAR', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (2, 'ONEDRIVE', 'REGULAR', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (3, 'ONEDRIVE', 'REGULAR', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (4, 'ONEDRIVE', 'STUDENT', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (5, 'ONEDRIVE', 'STUDENT', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (6, 'ONEDRIVE', 'STUDENT', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (7, 'ONEDRIVE', 'PENSIONER', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (8, 'ONEDRIVE', 'PENSIONER', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (9, 'ONEDRIVE', 'PENSIONER', 'THREE','BUS');

-- ITEM - ONE DRIVE - TROLLEY
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (10, 'ONEDRIVE', 'REGULAR', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (11, 'ONEDRIVE', 'REGULAR', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (12, 'ONEDRIVE', 'REGULAR', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (13, 'ONEDRIVE', 'STUDENT', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (14, 'ONEDRIVE', 'STUDENT', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (15, 'ONEDRIVE', 'STUDENT', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (16, 'ONEDRIVE', 'PENSIONER', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (17, 'ONEDRIVE', 'PENSIONER', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (18, 'ONEDRIVE', 'PENSIONER', 'THREE','TROLLEY');

-- ITEM - ONE DRIVE - METROTRAIN
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (19, 'ONEDRIVE', 'REGULAR', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (20, 'ONEDRIVE', 'REGULAR', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (21, 'ONEDRIVE', 'REGULAR', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (22, 'ONEDRIVE', 'STUDENT', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (23, 'ONEDRIVE', 'STUDENT', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (24, 'ONEDRIVE', 'STUDENT', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (25, 'ONEDRIVE', 'PENSIONER', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (26, 'ONEDRIVE', 'PENSIONER', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (27, 'ONEDRIVE', 'PENSIONER', 'THREE','METROTRAIN');

-- ITEM - DAILY - BUS
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (28, 'DAILY', 'REGULAR', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (29, 'DAILY', 'REGULAR', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (30, 'DAILY', 'REGULAR', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (31, 'DAILY', 'STUDENT', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (32, 'DAILY', 'STUDENT', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (33, 'DAILY', 'STUDENT', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (34, 'DAILY', 'PENSIONER', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (35, 'DAILY', 'PENSIONER', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (36, 'DAILY', 'PENSIONER', 'THREE','BUS');

-- ITEM - DAILY - TROLLEY
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (37, 'DAILY', 'REGULAR', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (38, 'DAILY', 'REGULAR', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (39, 'DAILY', 'REGULAR', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (40, 'DAILY', 'STUDENT', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (41, 'DAILY', 'STUDENT', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (42, 'DAILY', 'STUDENT', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (43, 'DAILY', 'PENSIONER', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (44, 'DAILY', 'PENSIONER', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (45, 'DAILY', 'PENSIONER', 'THREE','TROLLEY');

-- ITEM - DAILY - METROTRAIN
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (46, 'DAILY', 'REGULAR', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (47, 'DAILY', 'REGULAR', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (48, 'DAILY', 'REGULAR', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (49, 'DAILY', 'STUDENT', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (50, 'DAILY', 'STUDENT', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (51, 'DAILY', 'STUDENT', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (52, 'DAILY', 'PENSIONER', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (53, 'DAILY', 'PENSIONER', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (54, 'DAILY', 'PENSIONER', 'THREE','METROTRAIN');

-- ITEM - MONTHLY - BUS
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (55, 'MONTHLY', 'REGULAR', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (56, 'MONTHLY', 'REGULAR', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (57, 'MONTHLY', 'REGULAR', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (58, 'MONTHLY', 'STUDENT', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (59, 'MONTHLY', 'STUDENT', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (60, 'MONTHLY', 'STUDENT', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (61, 'MONTHLY', 'PENSIONER', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (62, 'MONTHLY', 'PENSIONER', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (63, 'MONTHLY', 'PENSIONER', 'THREE','BUS');

-- ITEM - MONTHLY - TROLLEY
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (64, 'MONTHLY', 'REGULAR', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (65, 'MONTHLY', 'REGULAR', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (66, 'MONTHLY', 'REGULAR', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (67, 'MONTHLY', 'STUDENT', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (68, 'MONTHLY', 'STUDENT', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (69, 'MONTHLY', 'STUDENT', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (70, 'MONTHLY', 'PENSIONER', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (71, 'MONTHLY', 'PENSIONER', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (72, 'MONTHLY', 'PENSIONER', 'THREE','TROLLEY');

-- ITEM - MONTHLY - METROTRAIN
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (73, 'MONTHLY', 'REGULAR', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (74, 'MONTHLY', 'REGULAR', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (75, 'MONTHLY', 'REGULAR', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (76, 'MONTHLY', 'STUDENT', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (77, 'MONTHLY', 'STUDENT', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (78, 'MONTHLY', 'STUDENT', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (79, 'MONTHLY', 'PENSIONER', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (80, 'MONTHLY', 'PENSIONER', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (81, 'MONTHLY', 'PENSIONER', 'THREE','METROTRAIN');

-- ITEM - YEARLY - BUS
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (82, 'YEARLY', 'REGULAR', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (83, 'YEARLY', 'REGULAR', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (84, 'YEARLY', 'REGULAR', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (85, 'YEARLY', 'STUDENT', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (86, 'YEARLY', 'STUDENT', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (87, 'YEARLY', 'STUDENT', 'THREE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (88, 'YEARLY', 'PENSIONER', 'ONE','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (89, 'YEARLY', 'PENSIONER', 'TWO','BUS');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (90, 'YEARLY', 'PENSIONER', 'THREE','BUS');

-- ITEM - YEARLY - TROLLEY
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (91, 'YEARLY', 'REGULAR', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (92, 'YEARLY', 'REGULAR', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (93, 'YEARLY', 'REGULAR', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (94, 'YEARLY', 'STUDENT', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (95, 'YEARLY', 'STUDENT', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (96, 'YEARLY', 'STUDENT', 'THREE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (97, 'YEARLY', 'PENSIONER', 'ONE','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (98, 'YEARLY', 'PENSIONER', 'TWO','TROLLEY');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (99, 'YEARLY', 'PENSIONER', 'THREE','TROLLEY');

-- ITEM - YEARLY - METROTRAIN
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (100, 'YEARLY', 'REGULAR', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (101, 'YEARLY', 'REGULAR', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (102, 'YEARLY', 'REGULAR', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (103, 'YEARLY', 'STUDENT', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (104, 'YEARLY', 'STUDENT', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (105, 'YEARLY', 'STUDENT', 'THREE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (106, 'YEARLY', 'PENSIONER', 'ONE','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (107, 'YEARLY', 'PENSIONER', 'TWO','METROTRAIN');
insert into item (id, card_type, visitor_type, zone,vehicle_type) values (108, 'YEARLY', 'PENSIONER', 'THREE','METROTRAIN');

-- PRICE LIST
insert into price_list (id, start_date, end_date) values (1, '2016-01-01', '2018-01-01');
insert into price_list (id, start_date, end_date) values (2, '2018-01-01', '2020-01-01');

-- PRICE LIST ITEM - ONE DRIVE 2017 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (1, 1, 1, 100);
insert into price_list_item (id, price_list_id, item_id, price) values (2, 1, 2, 120);
insert into price_list_item (id, price_list_id, item_id, price) values (3, 1, 3, 140);
insert into price_list_item (id, price_list_id, item_id, price) values (4, 1, 4, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (5, 1, 5, 90);
insert into price_list_item (id, price_list_id, item_id, price) values (6, 1, 6, 105);
insert into price_list_item (id, price_list_id, item_id, price) values (7, 1, 7, 55);
insert into price_list_item (id, price_list_id, item_id, price) values (8, 1, 8, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (9, 1, 9, 75);

-- PRICE LIST ITEM - ONE DRIVE 2017 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (10, 1, 10, 100);
insert into price_list_item (id, price_list_id, item_id, price) values (11, 1, 11, 120);
insert into price_list_item (id, price_list_id, item_id, price) values (12, 1, 12, 140);
insert into price_list_item (id, price_list_id, item_id, price) values (13, 1, 13, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (14, 1, 14, 90);
insert into price_list_item (id, price_list_id, item_id, price) values (15, 1, 15, 105);
insert into price_list_item (id, price_list_id, item_id, price) values (16, 1, 16, 55);
insert into price_list_item (id, price_list_id, item_id, price) values (17, 1, 17, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (18, 1, 18, 75);

-- PRICE LIST ITEM - ONE DRIVE 2017 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (19, 1, 19, 100);
insert into price_list_item (id, price_list_id, item_id, price) values (20, 1, 20, 120);
insert into price_list_item (id, price_list_id, item_id, price) values (21, 1, 21, 140);
insert into price_list_item (id, price_list_id, item_id, price) values (22, 1, 22, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (23, 1, 23, 90);
insert into price_list_item (id, price_list_id, item_id, price) values (24, 1, 24, 105);
insert into price_list_item (id, price_list_id, item_id, price) values (25, 1, 25, 55);
insert into price_list_item (id, price_list_id, item_id, price) values (26, 1, 26, 65);
insert into price_list_item (id, price_list_id, item_id, price) values (27, 1, 27, 75);

-- PRICE LIST ITEM - DAILY 2017 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (28, 1, 28, 300);
insert into price_list_item (id, price_list_id, item_id, price) values (29, 1, 29, 320);
insert into price_list_item (id, price_list_id, item_id, price) values (30, 1, 30, 340);
insert into price_list_item (id, price_list_id, item_id, price) values (31, 1, 31, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (32, 1, 32, 290);
insert into price_list_item (id, price_list_id, item_id, price) values (33, 1, 33, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (34, 1, 34, 255);
insert into price_list_item (id, price_list_id, item_id, price) values (35, 1, 35, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (36, 1, 36, 275);

-- PRICE LIST ITEM - DAILY 2017 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (37, 1, 37, 300);
insert into price_list_item (id, price_list_id, item_id, price) values (38, 1, 38, 320);
insert into price_list_item (id, price_list_id, item_id, price) values (39, 1, 39, 340);
insert into price_list_item (id, price_list_id, item_id, price) values (40, 1, 40, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (41, 1, 41, 290);
insert into price_list_item (id, price_list_id, item_id, price) values (42, 1, 42, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (43, 1, 43, 255);
insert into price_list_item (id, price_list_id, item_id, price) values (44, 1, 44, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (45, 1, 45, 275);

-- PRICE LIST ITEM - DAILY 2017 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (46, 1, 46, 300);
insert into price_list_item (id, price_list_id, item_id, price) values (47, 1, 47, 320);
insert into price_list_item (id, price_list_id, item_id, price) values (48, 1, 48, 340);
insert into price_list_item (id, price_list_id, item_id, price) values (49, 1, 49, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (50, 1, 50, 290);
insert into price_list_item (id, price_list_id, item_id, price) values (51, 1, 51, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (52, 1, 52, 255);
insert into price_list_item (id, price_list_id, item_id, price) values (53, 1, 53, 265);
insert into price_list_item (id, price_list_id, item_id, price) values (54, 1, 54, 275);

-- PRICE LIST ITEM - MONTHLY 2017 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (55, 1, 55, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (56, 1, 56, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (57, 1, 57, 1400);
insert into price_list_item (id, price_list_id, item_id, price) values (58, 1, 58, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (59, 1, 59, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (60, 1, 60, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (61, 1, 61, 850);
insert into price_list_item (id, price_list_id, item_id, price) values (62, 1, 62, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (63, 1, 63, 950);

-- PRICE LIST ITEM - MONTHLY 2017 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (64, 1, 64, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (65, 1, 65, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (66, 1, 66, 1400);
insert into price_list_item (id, price_list_id, item_id, price) values (67, 1, 67, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (68, 1, 68, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (69, 1, 69, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (70, 1, 70, 850);
insert into price_list_item (id, price_list_id, item_id, price) values (71, 1, 71, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (72, 1, 72, 950);

-- PRICE LIST ITEM - MONTHLY 2017 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (73, 1, 73, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (74, 1, 74, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (75, 1, 75, 1400);
insert into price_list_item (id, price_list_id, item_id, price) values (76, 1, 76, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (77, 1, 77, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (78, 1, 78, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (79, 1, 79, 850);
insert into price_list_item (id, price_list_id, item_id, price) values (80, 1, 80, 900);
insert into price_list_item (id, price_list_id, item_id, price) values (81, 1, 81, 950);

-- PRICE LIST ITEM - YEARLY 2017 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (82, 1, 82, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (83, 1, 83, 12000);
insert into price_list_item (id, price_list_id, item_id, price) values (84, 1, 84, 14000);
insert into price_list_item (id, price_list_id, item_id, price) values (85, 1, 85, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (86, 1, 86, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (87, 1, 87, 11000);
insert into price_list_item (id, price_list_id, item_id, price) values (88, 1, 88, 8500);
insert into price_list_item (id, price_list_id, item_id, price) values (89, 1, 89, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (90, 1, 90, 9500);

-- PRICE LIST ITEM - YEARLY 2017 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (91, 1, 91, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (92, 1, 92, 12000);
insert into price_list_item (id, price_list_id, item_id, price) values (93, 1, 93, 14000);
insert into price_list_item (id, price_list_id, item_id, price) values (94, 1, 94, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (95, 1, 95, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (96, 1, 96, 11000);
insert into price_list_item (id, price_list_id, item_id, price) values (97, 1, 97, 8500);
insert into price_list_item (id, price_list_id, item_id, price) values (98, 1, 98, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (99, 1, 99, 9500);

-- PRICE LIST ITEM - YEARLY 2017 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (100, 1, 100, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (101, 1, 101, 12000);
insert into price_list_item (id, price_list_id, item_id, price) values (102, 1, 102, 14000);
insert into price_list_item (id, price_list_id, item_id, price) values (103, 1, 103, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (104, 1, 104, 10000);
insert into price_list_item (id, price_list_id, item_id, price) values (105, 1, 105, 11000);
insert into price_list_item (id, price_list_id, item_id, price) values (106, 1, 106, 8500);
insert into price_list_item (id, price_list_id, item_id, price) values (107, 1, 107, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (108, 1, 108, 9500);

-- PRICE LIST ITEM - ONE DRIVE 2018 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (109, 2, 1, 105);
insert into price_list_item (id, price_list_id, item_id, price) values (110, 2, 2, 125);
insert into price_list_item (id, price_list_id, item_id, price) values (111, 2, 3, 145);
insert into price_list_item (id, price_list_id, item_id, price) values (112, 2, 4, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (113, 2, 5, 95);
insert into price_list_item (id, price_list_id, item_id, price) values (114, 2, 6, 110);
insert into price_list_item (id, price_list_id, item_id, price) values (115, 2, 7, 60);
insert into price_list_item (id, price_list_id, item_id, price) values (116, 2, 8, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (117, 2, 9, 80);

-- PRICE LIST ITEM - ONE DRIVE 2018 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (118, 2, 10, 106);
insert into price_list_item (id, price_list_id, item_id, price) values (119, 2, 11, 125);
insert into price_list_item (id, price_list_id, item_id, price) values (120, 2, 12, 145);
insert into price_list_item (id, price_list_id, item_id, price) values (121, 2, 13, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (122, 2, 14, 95);
insert into price_list_item (id, price_list_id, item_id, price) values (123, 2, 15, 110);
insert into price_list_item (id, price_list_id, item_id, price) values (124, 2, 16, 60);
insert into price_list_item (id, price_list_id, item_id, price) values (125, 2, 17, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (126, 2, 18, 80);

-- PRICE LIST ITEM - ONE DRIVE 2018 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (127, 2, 19, 107);
insert into price_list_item (id, price_list_id, item_id, price) values (128, 2, 20, 125);
insert into price_list_item (id, price_list_id, item_id, price) values (129, 2, 21, 145);
insert into price_list_item (id, price_list_id, item_id, price) values (130, 2, 22, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (131, 2, 23, 95);
insert into price_list_item (id, price_list_id, item_id, price) values (132, 2, 24, 110);
insert into price_list_item (id, price_list_id, item_id, price) values (133, 2, 25, 60);
insert into price_list_item (id, price_list_id, item_id, price) values (134, 2, 26, 70);
insert into price_list_item (id, price_list_id, item_id, price) values (135, 2, 27, 80);

-- PRICE LIST ITEM - DAILY 2018 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (136, 2, 28, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (137, 2, 29, 325);
insert into price_list_item (id, price_list_id, item_id, price) values (138, 2, 30, 345);
insert into price_list_item (id, price_list_id, item_id, price) values (139, 2, 31, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (140, 2, 32, 295);
insert into price_list_item (id, price_list_id, item_id, price) values (141, 2, 33, 310);
insert into price_list_item (id, price_list_id, item_id, price) values (142, 2, 34, 260);
insert into price_list_item (id, price_list_id, item_id, price) values (143, 2, 35, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (144, 2, 36, 280);

-- PRICE LIST ITEM - DAILY 2018 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (145, 2, 37, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (146, 2, 38, 325);
insert into price_list_item (id, price_list_id, item_id, price) values (147, 2, 39, 345);
insert into price_list_item (id, price_list_id, item_id, price) values (148, 2, 40, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (149, 2, 41, 295);
insert into price_list_item (id, price_list_id, item_id, price) values (150, 2, 42, 310);
insert into price_list_item (id, price_list_id, item_id, price) values (151, 2, 43, 260);
insert into price_list_item (id, price_list_id, item_id, price) values (152, 2, 44, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (153, 2, 45, 280);

-- PRICE LIST ITEM - DAILY 2018 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (154, 2, 46, 305);
insert into price_list_item (id, price_list_id, item_id, price) values (155, 2, 47, 325);
insert into price_list_item (id, price_list_id, item_id, price) values (156, 2, 48, 345);
insert into price_list_item (id, price_list_id, item_id, price) values (157, 2, 49, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (158, 2, 50, 295);
insert into price_list_item (id, price_list_id, item_id, price) values (159, 2, 51, 310);
insert into price_list_item (id, price_list_id, item_id, price) values (160, 2, 52, 260);
insert into price_list_item (id, price_list_id, item_id, price) values (161, 2, 53, 270);
insert into price_list_item (id, price_list_id, item_id, price) values (162, 2, 54, 280);

-- PRICE LIST ITEM - MONTHLY 2018 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (163, 2, 55, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (164, 2, 56, 1300);
insert into price_list_item (id, price_list_id, item_id, price) values (165, 2, 57, 1500);
insert into price_list_item (id, price_list_id, item_id, price) values (166, 2, 58, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (167, 2, 59, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (168, 2, 60, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (169, 2, 61, 950);
insert into price_list_item (id, price_list_id, item_id, price) values (170, 2, 62, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (171, 2, 63, 1050);

-- PRICE LIST ITEM - MONTHLY 2018 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (172, 2, 64, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (173, 2, 65, 1300);
insert into price_list_item (id, price_list_id, item_id, price) values (174, 2, 66, 1500);
insert into price_list_item (id, price_list_id, item_id, price) values (175, 2, 67, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (176, 2, 68, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (177, 2, 69, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (178, 2, 70, 950);
insert into price_list_item (id, price_list_id, item_id, price) values (179, 2, 71, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (180, 2, 72, 1050);

-- PRICE LIST ITEM - MONTHLY 2018 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (181, 2, 73, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (182, 2, 74, 1300);
insert into price_list_item (id, price_list_id, item_id, price) values (183, 2, 75, 1500);
insert into price_list_item (id, price_list_id, item_id, price) values (184, 2, 76, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (185, 2, 77, 1100);
insert into price_list_item (id, price_list_id, item_id, price) values (186, 2, 78, 1200);
insert into price_list_item (id, price_list_id, item_id, price) values (187, 2, 79, 950);
insert into price_list_item (id, price_list_id, item_id, price) values (188, 2, 80, 1000);
insert into price_list_item (id, price_list_id, item_id, price) values (189, 2, 81, 1050);

-- PRICE LIST ITEM - YEARLY 2018 - BUS
insert into price_list_item (id, price_list_id, item_id, price) values (190, 2, 82, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (191, 2, 83, 12500);
insert into price_list_item (id, price_list_id, item_id, price) values (192, 2, 84, 14500);
insert into price_list_item (id, price_list_id, item_id, price) values (193, 2, 85, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (194, 2, 86, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (195, 2, 87, 11500);
insert into price_list_item (id, price_list_id, item_id, price) values (196, 2, 88, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (197, 2, 89, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (198, 2, 90, 10000);

-- PRICE LIST ITEM - YEARLY 2018 - TROLLEY
insert into price_list_item (id, price_list_id, item_id, price) values (199, 2, 91, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (200, 2, 92, 12500);
insert into price_list_item (id, price_list_id, item_id, price) values (201, 2, 93, 14500);
insert into price_list_item (id, price_list_id, item_id, price) values (202, 2, 94, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (203, 2, 95, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (204, 2, 96, 11500);
insert into price_list_item (id, price_list_id, item_id, price) values (205, 2, 97, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (206, 2, 98, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (207, 2, 99, 10000);

-- PRICE LIST ITEM - YEARLY 2018 - METROTRAIN
insert into price_list_item (id, price_list_id, item_id, price) values (208, 2, 100, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (209, 2, 101, 12500);
insert into price_list_item (id, price_list_id, item_id, price) values (210, 2, 102, 14500);
insert into price_list_item (id, price_list_id, item_id, price) values (211, 2, 103, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (212, 2, 104, 10500);
insert into price_list_item (id, price_list_id, item_id, price) values (213, 2, 105, 11500);
insert into price_list_item (id, price_list_id, item_id, price) values (214, 2, 106, 9000);
insert into price_list_item (id, price_list_id, item_id, price) values (215, 2, 107, 9500);
insert into price_list_item (id, price_list_id, item_id, price) values (216, 2, 108, 10000);

-- CARDS
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (1, True, '2019-02-20', '2019-02-21', 40, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (2, True, '2018-01-01', '2019-01-01', 87, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (3, False, '2018-08-01', '2018-09-01', 66, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (4, False, '2017-06-24', '2017-06-25', 35, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (5, True, '2019-05-06', '2019-05-07', 52, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (6, False, '2018-08-01', '2018-09-01', 66, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (7, True, '2019-03-08', '2019-03-09', 19, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (8, False, '2017-08-20', '2017-08-21', 8, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (9, False, '2017-01-01', '2018-01-01', 83, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (10, False, '2017-01-01', '2018-01-01', 82, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (11, False, '2018-03-10', '2018-03-11', 5, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (12, False, '2018-08-08', '2018-08-09', 30, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (13, True, '2019-01-23', '2019-01-24', 12, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (14, False, '2017-06-01', '2017-07-01', 77, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (15, True, '2019-09-06', '2019-09-07', 37, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (16, False, '2018-11-05', '2018-11-06', 38, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (17, False, '2017-04-06', '2017-04-07', 21, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (18, False, '2017-08-01', '2017-09-01', 64, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (19, True, '2019-01-01', '2020-01-01', 105, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (20, False, '2017-01-01', '2018-01-01', 106, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (21, True, '2019-12-04', '2019-12-05', 49, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (22, True, '2018-01-01', '2019-01-01', 90, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (23, True, '2018-01-01', '2019-01-01', 105, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (24, True, '2019-09-23', '2019-09-24', 39, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (25, False, '2017-03-03', '2017-03-04', 37, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (26, False, '2017-11-29', '2017-11-30', 31, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (27, True, '2019-07-01', '2019-08-01', 81, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (28, False, '2018-05-01', '2018-06-01', 77, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (29, False, '2018-09-14', '2018-09-15', 10, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (30, False, '2017-01-01', '2018-01-01', 84, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (31, True, '2019-02-19', '2019-02-20', 38, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (32, False, '2017-01-01', '2018-01-01', 86, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (33, True, '2019-01-01', '2020-01-01', 93, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (34, False, '2018-02-18', '2018-02-19', 49, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (35, True, '2018-11-01', '2018-12-01', 78, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (36, False, '2018-01-12', '2018-01-13', 35, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (37, True, '2019-06-23', '2019-06-24', 37, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (38, False, '2017-05-01', '2017-06-01', 74, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (39, False, '2017-06-01', '2017-07-01', 71, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (40, False, '2017-10-01', '2017-11-01', 60, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (41, True, '2018-01-01', '2019-01-01', 90, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (42, False, '2018-05-01', '2018-06-01', 70, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (43, False, '2017-01-01', '2018-01-01', 86, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (44, False, '2017-08-30', '2017-08-31', 36, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (45, True, '2019-05-26', '2019-05-27', 5, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (46, False, '2017-02-22', '2017-02-23', 31, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (47, False, '2017-06-03', '2017-06-04', 17, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (48, False, '2017-10-22', '2017-10-23', 48, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (49, True, '2019-01-01', '2020-01-01', 100, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (50, True, '2019-12-31', '2020-01-01', 51, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (51, False, '2017-03-26', '2017-03-27', 48, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (52, True, '2018-01-01', '2019-01-01', 96, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (53, True, '2019-01-01', '2020-01-01', 101, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (54, False, '2018-03-18', '2018-03-19', 3, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (55, False, '2018-03-16', '2018-03-17', 11, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (56, True, '2019-10-12', '2019-10-13', 3, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (57, True, '2019-01-01', '2020-01-01', 88, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (58, True, '2019-07-01', '2019-08-01', 69, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (59, True, '2019-08-01', '2019-09-01', 56, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (60, True, '2019-02-14', '2019-02-15', 23, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (61, True, '2018-01-01', '2019-01-01', 95, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (62, True, '2019-03-30', '2019-03-31', 37, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (63, True, '2018-12-29', '2018-12-30', 37, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (64, True, '2019-09-01', '2019-10-01', 75, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (65, True, '2019-12-05', '2019-12-06', 6, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (66, False, '2017-09-28', '2017-09-29', 49, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (67, False, '2018-07-22', '2018-07-23', 54, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (68, False, '2018-09-23', '2018-09-24', 40, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (69, False, '2018-05-01', '2018-06-01', 65, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (70, False, '2018-10-01', '2018-11-01', 64, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (71, True, '2018-01-01', '2019-01-01', 86, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (72, False, '2017-06-01', '2017-07-01', 73, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (73, True, '2019-01-01', '2019-02-01', 66, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (74, True, '2018-12-23', '2018-12-24', 12, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (75, False, '2017-06-15', '2017-06-16', 29, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (76, False, '2017-03-01', '2017-04-01', 55, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (77, False, '2018-01-01', '2018-01-02', 39, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (78, False, '2017-01-01', '2018-01-01', 82, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (79, True, '2019-02-13', '2019-02-14', 54, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (80, False, '2017-09-29', '2017-09-30', 5, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (81, False, '2018-01-28', '2018-01-29', 39, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (82, False, '2018-06-01', '2018-07-01', 55, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (83, False, '2017-03-30', '2017-03-31', 54, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (84, False, '2017-11-01', '2017-12-01', 73, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (85, True, '2019-01-01', '2019-02-01', 67, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (86, False, '2017-11-23', '2017-11-24', 47, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (87, True, '2019-08-01', '2019-09-01', 64, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (88, True, '2018-01-01', '2019-01-01', 82, 2);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (89, False, '2017-01-01', '2018-01-01', 99, 5);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (90, True, '2018-01-01', '2019-01-01', 102, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (91, True, '2019-10-13', '2019-10-14', 21, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (92, False, '2017-01-01', '2018-01-01', 105, 7);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (93, False, '2018-09-10', '2018-09-11', 40, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (94, True, '2019-11-01', '2019-12-01', 75, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (95, False, '2017-01-01', '2018-01-01', 86, 4);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (96, True, '2019-04-03', '2019-04-04', 38, 8);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (97, False, '2018-10-22', '2018-10-23', 51, 6);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (98, False, '2018-04-29', '2018-04-30', 38, 10);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (99, False, '2017-03-01', '2017-04-01', 63, 9);
insert into card (id, active, start_date, end_date, price_list_item_id, visitor_id) values (100, False, '2018-09-01', '2018-10-01', 74, 8);