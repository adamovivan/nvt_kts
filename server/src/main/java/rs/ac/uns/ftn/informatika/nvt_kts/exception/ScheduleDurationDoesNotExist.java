package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class ScheduleDurationDoesNotExist extends MessageException {
    public ScheduleDurationDoesNotExist() {
        message = "Schedule duration with that path name and day does no exist.";
    }
}
