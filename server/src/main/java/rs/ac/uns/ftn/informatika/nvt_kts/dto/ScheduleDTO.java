package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class ScheduleDTO {

    private Day day;
    private List<LocalTime> toTimes;
    private List<LocalTime> fromTimes;
    private String pathName;
    private String startDate;
    private String endDate;
    private VehicleType vehicleType;

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public List<LocalTime> getToTimes() {
        return toTimes;
    }

    public void setToTimes(List<LocalTime> toTimes) {
        this.toTimes = toTimes;
    }

    public List<LocalTime> getFromTimes() {
        return fromTimes;
    }

    public void setFromTimes(List<LocalTime> fromTimes) {
        this.fromTimes = fromTimes;
    }

    @Override
    public String toString() {
        return "ScheduleDTO{" +
                "day=" + day +
                ", toTimes=" + toTimes +
                ", fromTimes=" + fromTimes +
                ", pathName='" + pathName + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", vehicleType=" + vehicleType +
                '}';
    }
}
