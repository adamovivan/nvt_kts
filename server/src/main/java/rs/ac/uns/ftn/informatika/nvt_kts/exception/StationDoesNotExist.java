package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class StationDoesNotExist extends MessageException {
    public StationDoesNotExist(){
        message = "Station with that id does not exist.";
    }
}
