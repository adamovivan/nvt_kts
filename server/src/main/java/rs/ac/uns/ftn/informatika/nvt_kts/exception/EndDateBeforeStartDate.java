package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class EndDateBeforeStartDate extends MessageException {
    public EndDateBeforeStartDate(){
        message = "Cannot set end date before start date.";
    }
}
