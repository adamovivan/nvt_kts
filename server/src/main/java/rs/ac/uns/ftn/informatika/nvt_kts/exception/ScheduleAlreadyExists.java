package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class ScheduleAlreadyExists extends MessageException {
    public ScheduleAlreadyExists() {
        message = "Schedule with that path name and day already exist.";
    }
}
