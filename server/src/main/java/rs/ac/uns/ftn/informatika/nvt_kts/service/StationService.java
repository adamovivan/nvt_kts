package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.StationDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.StationPathPairDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.StationRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private PathRepository pathRepository;

    public void saveNewStation(Station station){
        stationRepository.save(station);
    }

    @Transactional
    public Station saveStation(Station station) {
        // ARRAYS SHOULD BE SETS HERE !!! (it works, but in case of duplicates -> more sql)
        Station persisted = stationRepository.findById(station.getId()).orElse(null);
        persisted.setName(station.getName());
        // Set arrays
        List<Path> persistedPaths = persisted.getPaths();
        List<Path> newPaths = station.getPaths();
        // Removed paths
        List<Path> removedPaths = new ArrayList<>(persistedPaths);
        removedPaths.removeAll(newPaths);
        // Added paths
        ArrayList<Path> addedPaths = new ArrayList<>(newPaths);
        addedPaths.removeAll(persistedPaths);

        // Remove refs from removed paths
        for (Path p : removedPaths) {
            Path path = pathRepository.findPathById(p.getId());
            path.getStations().remove(persisted);
            pathRepository.save(path);
        }

        // Add refs to added paths
        for (Path p : addedPaths) {
            Path path = pathRepository.findPathById(p.getId());
            path.getStations().add(persisted);
            pathRepository.save(path);
        }

        persisted.setPaths(station.getPaths());
        persisted.pathsToStr();
        stationRepository.save(persisted);  // save edited station
        return station;
    }

    public void addStation(Station station) {
        saveStation(station);
    }

    public Optional<Station> deleteStation(Long id) throws StationDoesNotExist {

        Optional<Station> station = stationRepository.findById(id);
        if (!station.isPresent()) {
            throw new StationDoesNotExist();
        }
        for (Path p : pathRepository.findAllPathsForStation(station.get().getId())) {
            p.getStations().remove(station.get());
            pathRepository.save(p);
        }
        stationRepository.delete(station.get());
        return station;
    }

    public void addStationToPath(String stationName, String lineNumber) throws StationDoesNotExist, PathDoesNotExist {

        Station station = stationRepository.findByName(stationName);
        if (station == null) {
            throw new StationDoesNotExist();
        }
        Path path = pathRepository.findPathByPathName(lineNumber);
        if (path == null) {
            throw new PathDoesNotExist();
        }
        // bidirectional link
        path.getStations().add(station);
        station.getPaths().add(path);
        pathRepository.save(path);
        stationRepository.save(station);
    }

    public void removeStationFromPath(StationPathPairDTO stationPathPairDTO) throws StationDoesNotExist, PathDoesNotExist {

        Optional<Station> station = stationRepository.findById(stationPathPairDTO.stationId);
        if (!station.isPresent()) {
            throw new StationDoesNotExist();
        }
        Optional<Path> path = pathRepository.findById(stationPathPairDTO.pathId);
        if (!path.isPresent()) {
            throw new PathDoesNotExist();
        }
        // bidirectional link
        path.get().getStations().remove(station.get());
        station.get().getPaths().remove(path.get());
        pathRepository.save(path.get());
        stationRepository.save(station.get());
    }

    public List<Station> getAllStations() {
        List<Station> retVal = stationRepository.findAll();
        retVal.forEach(station -> station.pathsToStr());
        return retVal;
    }

    public Station getStationById(Long id) throws StationDoesNotExist {

        Optional<Station> station = stationRepository.findById(id);
        if (!station.isPresent()) {
            throw new StationDoesNotExist();
        }
        return station.get().pathsToStr();
    }

}
