package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ControllerCheckDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathForControllerDto;
import rs.ac.uns.ftn.informatika.nvt_kts.service.ControllerService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.UserService;

import java.util.List;

@RestController
public class ControllerController {

    @Autowired
    UserService userService;

    @Autowired
    private ControllerService controllerService;

    @PreAuthorize("hasRole('ROLE_CONTROLLER')")
    @RequestMapping(value = "/controllerCheck", method = RequestMethod.GET)
    public List<ControllerCheckDTO> controllerCheck(@RequestParam Zone zone, @RequestParam VehicleType vehicleType, @RequestParam String time) {
        return controllerService.controllerCheck(zone, vehicleType, time);
    }

    @PreAuthorize("hasRole('ROLE_CONTROLLER')")
    @RequestMapping(value = "/controllersLines", method = RequestMethod.GET)
    public List<PathForControllerDto> controllersLines() {
        Controller c = (Controller)userService.getLoggedUser();
        return controllerService.findPaths(c);
    }

}
