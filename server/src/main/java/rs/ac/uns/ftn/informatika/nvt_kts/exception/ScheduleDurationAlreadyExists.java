package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class ScheduleDurationAlreadyExists extends MessageException {
    public ScheduleDurationAlreadyExists() {
        message = "For that path name and day schedule duration already exist.";
    }
}
