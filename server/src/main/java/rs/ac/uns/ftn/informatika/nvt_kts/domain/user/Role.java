package rs.ac.uns.ftn.informatika.nvt_kts.domain.user;

public enum Role {
    ROLE_VISITOR,
    ROLE_INSPECTOR,
    ROLE_ADMIN,
    ROLE_CONTROLLER
}
