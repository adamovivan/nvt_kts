package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class PriceListDoesNotExist extends MessageException {
    public PriceListDoesNotExist(){
        message = "Pricelist with that id does not exist.";
    }
}
