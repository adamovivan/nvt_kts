package rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Where(clause = "is_active='TRUE'")
public class ScheduleDuration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Schedule schedule;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    @Column(name = "is_active")
    private Boolean active;

    @ElementCollection
    private List<LocalTime> toTimes;

    @ElementCollection
    private List<LocalTime> fromTimes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<LocalTime> getToTimes() {
        return toTimes;
    }

    public void setToTimes(List<LocalTime> toTimes) {
        this.toTimes = toTimes;
    }

    public List<LocalTime> getFromTimes() {
        return fromTimes;
    }

    public void setFromTimes(List<LocalTime> fromTimes) {
        this.fromTimes = fromTimes;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ScheduleDuration{" +
                "id=" + id +
                ", schedule=" + schedule +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", toTimes=" + toTimes +
                ", fromTimes=" + fromTimes +
                '}';
    }
}
