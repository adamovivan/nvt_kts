package rs.ac.uns.ftn.informatika.nvt_kts.domain.path;

import com.fasterxml.jackson.annotation.*;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.Coord;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.InvalidVehicleType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Path {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String pathName;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VehicleType vehicleType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Zone zone;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "path_coordinates")
    private List<Coord> coords = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "paths_stations",
            joinColumns = {@JoinColumn(name = "path_id")},
            inverseJoinColumns = {@JoinColumn(name = "station_id")})
    private List<Station> stations = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(
            mappedBy = "path",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Vehicle> vehicles = new ArrayList<>();

    @Column(columnDefinition = "int default 0")
    @JsonIgnore
    private int coordIndex = 0;

    public Path updateStationStrings() {
        for (Station s : stations) {
            s.pathsToStr();
        }
        return this;
    }

    public Path updateVehicleCoords() {

        if (coords.size() == 0 || vehicles.size() == 0)
            return this;

        // Roll coordinates
        for (int i = 0; i < vehicles.size(); i++) {
            vehicles.get(i).setCoord(coords.get(coordIndex));
            coordIndex = (coordIndex + 1) % coords.size();
        }
        return this;
    }

    public boolean addVehicle(Vehicle v) throws InvalidVehicleType {
        if (vehicleType == null) {
            vehicleType = v.getVehicleType();
            return this.vehicles.add(v);
        } else {
            if (vehicleType == v.getVehicleType()) {
                return this.vehicles.add(v);
            }
        }
        throw new InvalidVehicleType(this);
    }

    public int getCoordIndex() {
        return coordIndex;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Coord> getCoords() {
        return coords;
    }

    public void setCoords(List<Coord> coords) {
        this.coords = coords;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public Long getId() {
        return id;
    }

    public String vehiclesToStr() {
        StringBuilder retVal = new StringBuilder("[");
        for (Vehicle v : this.vehicles) {
            retVal.append(" , ").append(v.getId());
        }
        return retVal.append("]").toString();
    }

    public String stationsToStr() {
        StringBuilder retVal = new StringBuilder();
        for (Station s : this.stations) {
            retVal.append(" , ").append(s.getName());
        }
        return retVal.append("]").toString();
    }

    @Override
    public String toString() {
        return "Path{" +
                "id=" + id +
                ", pathName='" + pathName + '\'' +
                ", vehicleType=" + vehicleType +
//                ", coords=" + coords +
                ", stations=" + stationsToStr() +
                ", vehicles=" + vehiclesToStr() +
                ", coordIndex=" + coordIndex +
                '}';
    }
}
