package rs.ac.uns.ftn.informatika.nvt_kts.domain.document;

public enum ApprovalStatus {
    PENDING,
    APPROVED,
    REJECTED
}
