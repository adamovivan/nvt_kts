package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.ScheduleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleDurationRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleRepository;

import java.util.List;

@Service
public class ScheduleService {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    ScheduleDurationRepository scheduleDurationRepository;

    @Autowired
    PathRepository pathRepository;

    public boolean addSchedule(ScheduleDTO scheduleDTO) throws PathDoesNotExist {

        Path path = pathRepository.findPathByPathNameAndVehicleType(scheduleDTO.getPathName(), scheduleDTO.getVehicleType());
        if (path == null) {
            throw new PathDoesNotExist();
        }


        Schedule scheduleToCheck = scheduleRepository.findByPathAndDayAndVehicleType(path.getPathName(), scheduleDTO.getDay(), scheduleDTO.getVehicleType());

        if (scheduleToCheck == null) {
            Schedule schedule = new Schedule();
            schedule.setDay(scheduleDTO.getDay());
            schedule.setPath(path);
            schedule.setActive(true);
            scheduleRepository.save(schedule);
        }

        return true;

    }

    public List<Schedule> getSchedules() {
        return scheduleRepository.findAll();
    }

    public boolean deleteSchedule(String pathName, Day day, VehicleType vehicleType) throws ScheduleDoesNotExist {
        Schedule s = scheduleRepository.findByPathAndDayAndVehicleType(pathName, day, vehicleType);

        if(s == null) {
            throw new ScheduleDoesNotExist();
        }

        List<ScheduleDuration> scheduleDurations = scheduleDurationRepository.findByScheduleId(s.getId());

        if(scheduleDurations.isEmpty()) {
            s.setActive(false);
            scheduleRepository.save(s);
        }

        return true;
    }
}
