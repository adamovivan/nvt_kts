package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public abstract class MessageException extends Exception {
    protected String message = "";

    public MessageException(){

    }
    @Override
    public String getMessage() {
        return message;
    }
}
