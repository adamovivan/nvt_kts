package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import java.util.ArrayList;
import java.util.List;

public class PathNumbersDTO {

    public List<String> buses = new ArrayList<>();

    public List<String> metroTrains = new ArrayList<>();

    public List<String> trolleys = new ArrayList<>();

}
