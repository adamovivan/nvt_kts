package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.document.ApprovalStatus;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.document.Document;

import java.util.List;

public interface DocumentRepository extends JpaRepository<Document, Long> {

    List<Document> findByApprovalStatus(ApprovalStatus approvalStatus);

    Document findByVisitor_Username(String username);
}
