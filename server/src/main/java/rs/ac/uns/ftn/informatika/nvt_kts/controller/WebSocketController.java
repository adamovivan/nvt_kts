package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.VehicleService;

import java.util.ArrayList;
import java.util.List;

@EnableScheduling
@Controller
public class WebSocketController {

    @Autowired
    VehicleService vehicleService;

    private final SimpMessagingTemplate template;

    @Autowired
    WebSocketController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Scheduled(fixedRate = 1500)
    public void onReceivedMessage() throws PathDoesNotExist, InterruptedException {
        if (vehicleService.currentPathId != -1L) {
            System.out.println("Current Path:" + vehicleService.currentPathId);
            List<Vehicle> vehicles = vehicleService.getVehicleLocations(vehicleService.currentPathId);
            System.out.println("Socket load:" + vehicles.toString());
            this.template.convertAndSend("/chat", vehicles);
        }
    }

}
