package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class PriceListNotSet extends MessageException {
    public PriceListNotSet(){
        message = "Price list is not set. Please contact an administrator.";
    }
}
