package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.*;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleDurationRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleDurationService {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    ScheduleDurationRepository scheduleDurationRepository;

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    public boolean addScheduleDuration(ScheduleDTO scheduleDTO) throws ScheduleDurationAlreadyExists {

        LocalDate startDate = LocalDate.parse(scheduleDTO.getStartDate(), dtf);
        LocalDate endDate = LocalDate.parse(scheduleDTO.getEndDate(), dtf);

        ScheduleDuration scheduleDurationCheck = scheduleDurationRepository.findByDuration(scheduleDTO.getPathName(), scheduleDTO.getVehicleType(), scheduleDTO.getDay(), startDate, endDate);

        if(scheduleDurationCheck != null) {
            throw new ScheduleDurationAlreadyExists();
        }

        Schedule schedule = scheduleRepository.findByPathAndDayAndVehicleType(scheduleDTO.getPathName(), scheduleDTO.getDay(), scheduleDTO.getVehicleType());

        ScheduleDuration scheduleDuration = new ScheduleDuration();
        scheduleDuration.setSchedule(schedule);
        scheduleDuration.setStartDate(startDate);
        scheduleDuration.setEndDate(endDate);
        scheduleDuration.setToTimes(scheduleDTO.getToTimes());
        scheduleDuration.setFromTimes(scheduleDTO.getFromTimes());
        scheduleDuration.setActive(true);

        scheduleDurationRepository.save(scheduleDuration);
        return true;

    }

    public List<ScheduleDuration> getScheduleDurations(String pathName, Day day, VehicleType vehicleType) {
        return scheduleDurationRepository.findScheduleDurations(pathName, day, vehicleType);
    }

    public List<ScheduleDuration> getAllScheduleDurations(String pathName, Day day) {
        return scheduleDurationRepository.findAllScheduleDurations(pathName, day);
    }

    public List<ScheduleDTO> getScheduleDurations() {
        List<ScheduleDuration> scheduleDurations =  scheduleDurationRepository.findAll();
        List<ScheduleDTO> scheduleDTOs = new ArrayList<>();

        for(ScheduleDuration sd: scheduleDurations) {
            ScheduleDTO sdDTO = new ScheduleDTO();
            sdDTO.setPathName(sd.getSchedule().getPath().getPathName());
            sdDTO.setStartDate(dtf.format(sd.getStartDate()));
            sdDTO.setEndDate(dtf.format(sd.getEndDate()));
            sdDTO.setToTimes(sd.getToTimes());
            sdDTO.setFromTimes(sd.getFromTimes());
            sdDTO.setVehicleType(sd.getSchedule().getPath().getVehicleType());
            sdDTO.setDay(sd.getSchedule().getDay());
            scheduleDTOs.add(sdDTO);
        }
        return scheduleDTOs;
    }

    public boolean updateScheduleDuration(ScheduleDTO scheduleDTO) throws ScheduleDurationDoesNotExist {
        ScheduleDuration scheduleDurationCheck = findScheduleDuration(scheduleDTO);

        scheduleDurationCheck.setToTimes(scheduleDTO.getToTimes());
        scheduleDurationCheck.setFromTimes(scheduleDTO.getFromTimes());
        scheduleRepository.save(scheduleDurationCheck.getSchedule());
        scheduleDurationRepository.save(scheduleDurationCheck);
        return true;
    }

    public boolean deleteScheduleDuration(String pathName, Day day, VehicleType vehicleType, String startDate, String endDate) throws ScheduleDurationDoesNotExist {
        ScheduleDTO sDTO = new ScheduleDTO();
        sDTO.setPathName(pathName);
        sDTO.setDay(day);
        sDTO.setVehicleType(vehicleType);
        sDTO.setStartDate(startDate);
        sDTO.setEndDate(endDate);

        ScheduleDuration scheduleDurationCheck = findScheduleDuration(sDTO);
        scheduleDurationCheck.setActive(false);

        scheduleDurationRepository.save(scheduleDurationCheck);
        return true;
    }

    private ScheduleDuration findScheduleDuration(ScheduleDTO scheduleDTO) throws ScheduleDurationDoesNotExist {
        LocalDate startDate = LocalDate.parse(scheduleDTO.getStartDate(), dtf);
        LocalDate endDate = LocalDate.parse(scheduleDTO.getEndDate(), dtf);

        ScheduleDuration scheduleDurationCheck = scheduleDurationRepository.findByExactDuration(scheduleDTO.getPathName(), scheduleDTO.getVehicleType(), scheduleDTO.getDay(), startDate, endDate);

        if (scheduleDurationCheck == null) {
            throw new ScheduleDurationDoesNotExist();
        }

        return scheduleDurationCheck;
    }
}
