package rs.ac.uns.ftn.informatika.nvt_kts.dto;

public class StationPathPairDTO {
    public Long stationId;
    public Long pathId;
}
