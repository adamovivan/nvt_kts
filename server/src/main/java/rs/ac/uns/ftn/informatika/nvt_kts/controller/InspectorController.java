package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.DiscountRequestDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.response.SimpleResponse;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.DocumentDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.DocumentService;

import javax.activation.FileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
public class InspectorController {

    @Autowired
    private DocumentService documentService;

    @PreAuthorize("hasRole('INSPECTOR')")
    @RequestMapping(value = "/discountRequests",method = RequestMethod.GET)
    public @ResponseBody Iterable<DiscountRequestDTO> getRequests() {
        return documentService.findDocsToApprove();
    }

    @PreAuthorize("hasRole('INSPECTOR')")
    @RequestMapping(value = "/approveRequest", method = RequestMethod.PUT)
    public ResponseEntity<SimpleResponse> approveRequest(@RequestBody Long id) {
        try {
            documentService.approveDocument(id);
        } catch (DocumentDoesNotExist documentDoesNotExist) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, documentDoesNotExist.getMessage()));
        }
        return ResponseEntity.ok(new SimpleResponse("Successful approval."));
    }

    @PreAuthorize("hasRole('INSPECTOR')")
    @RequestMapping(value = "/rejectRequest", method = RequestMethod.PUT)
    public ResponseEntity<SimpleResponse> rejectRequest(@RequestBody Long id) {
        try {
            documentService.rejectDocument(id);
        } catch (DocumentDoesNotExist documentDoesNotExist) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, documentDoesNotExist.getMessage()));
        }
        return ResponseEntity.ok(new SimpleResponse("Successful rejection."));
    }

    @PreAuthorize("hasRole('INSPECTOR')")
    @RequestMapping(value = "/documentImage/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable("id") Long id) throws IOException, DocumentDoesNotExist {
        File image = documentService.getImage(id);

        if (image == null)
            return ResponseEntity.ok().body(null);

        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(image)))
                .body(Files.readAllBytes(image.toPath()));
    }
}
