package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.VehicleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.InvalidVehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.VehicleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VehicleRepository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private PathService pathService;

    public Long currentPathId = -1L;

    public void save(Vehicle v){
        vehicleRepository.save(v);
    }

    public List<Vehicle> getVehicleLocations(Long pathId) {
        List<Vehicle> vehicles = new ArrayList<>();
        for (Path path : pathService.updateVehicleLocations()) {
            if (path.getId().compareTo(pathId) == 0 ) {
                vehicles = path.getVehicles();
            }
        }
        return vehicles;
    }

    public boolean addVehicle(VehicleDTO vDTO) throws PathDoesNotExist, InvalidVehicleType {
        Path p = pathRepository.findPathById(vDTO.getPathId());
        if (p == null) {
            throw new PathDoesNotExist();
        }

        if (p.getVehicleType() != vDTO.getVehicleType()) {
            throw new InvalidVehicleType(p);
        }
        Vehicle v = new Vehicle();
        v.setVehicleType(vDTO.getVehicleType());
        p.getVehicles().add(v);
        v.setPath(p);
        vehicleRepository.save(v);
        pathRepository.save(p);
        return true;
    }

    public Iterable<VehicleDTO> getAllVehicles(Pageable pageable) {
        LinkedList<VehicleDTO> vehicleDTOs = new LinkedList<>();
        for (Vehicle v : vehicleRepository.findAll(pageable)) {
            VehicleDTO vDTO = new VehicleDTO();
            vDTO.setId(v.getId());
            vDTO.setPathId(v.getPath().getId());
            vDTO.setVehicleType(v.getVehicleType());
            vehicleDTOs.add(vDTO);
        }
        return vehicleDTOs;
    }

    public void deleteVehicle(Long id) throws VehicleDoesNotExist {
        Optional<Vehicle> v = vehicleRepository.findById(id);
        if (!v.isPresent()) {
            throw new VehicleDoesNotExist();
        }
        Path p = pathRepository.findPathByVehicle(v.get().getId());
        p.getVehicles().remove(v.get());
        pathRepository.save(p);
        vehicleRepository.delete(v.get());
    }
}
