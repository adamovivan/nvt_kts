package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Card;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ControllerCheckDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathForControllerDto;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ControllerRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VehicleRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VisitorRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ControllerService {

    @Autowired
    VisitorRepository visitorRepository;

    @Autowired
    PathRepository pathRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    ControllerRepository controllerRepository;

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    public List<ControllerCheckDTO> controllerCheck(Zone zone, VehicleType vehicleType, String time){
        List<Visitor> visitors = visitorRepository.findAll();

        LocalDate dateNow = LocalDate.parse(time, dtf);
        boolean foundCard = false;

        List<ControllerCheckDTO> controllerCheckDTOs = new ArrayList<>();
        for(Visitor v: visitors) {
            ControllerCheckDTO controllerCheckDTO = new ControllerCheckDTO();
            controllerCheckDTO.setFirstName(v.getFirstName());
            controllerCheckDTO.setLastName(v.getLastName());

            if(v.getCards().isEmpty()) {
                controllerCheckDTO.setActiveCard(false);
                controllerCheckDTO.setPaid(false);
                controllerCheckDTOs.add(controllerCheckDTO);
            } else {
                for (Card c : v.getCards()) {
                    if (c.getPriceListItem().getItem().getZone().equals(zone) && c.getPriceListItem().getItem().getVehicleType().equals(vehicleType)) {

                        controllerCheckDTO.setCardType(c.getPriceListItem().getItem().getCardType());
                        controllerCheckDTO.setStartDate(c.getStartDate());
                        controllerCheckDTO.setEndDate(c.getEndDate());

                        foundCard = true;

                        if(c.isActive() && (dateNow.isAfter(c.getStartDate()) && dateNow.isBefore(c.getEndDate()))) {
                            controllerCheckDTO.setActiveCard(true);
                            controllerCheckDTO.setPaid(true);
                            controllerCheckDTOs.add(controllerCheckDTO);
                            break;
                        } else {
                            controllerCheckDTO.setActiveCard(false);
                            controllerCheckDTO.setPaid(false);
                            controllerCheckDTOs.add(controllerCheckDTO);
                            break;
                        }
                    }
                }
                if(!foundCard) {
                    controllerCheckDTO.setActiveCard(false);
                    controllerCheckDTO.setPaid(false);
                    controllerCheckDTOs.add(controllerCheckDTO);
                }
            }
        }
        return controllerCheckDTOs;
    }

    public List<PathForControllerDto> findPaths(Controller controller) {
        List<Vehicle> vehicles = vehicleRepository.findAll();
        List<Path> paths = pathRepository.findAll();

        List<PathForControllerDto> pathForControllerDtos = new ArrayList<>();
        for(Path p: paths) {
            PathForControllerDto pathForControllerDto = new PathForControllerDto();

            for(Vehicle v: vehicles) {
                if(v.getVehicleType().equals(p.getVehicleType())) {
                    pathForControllerDto.setPathName(p.getPathName());
                    pathForControllerDto.setVehicleType(p.getVehicleType());
                    pathForControllerDto.setZone(p.getZone());
                    pathForControllerDtos.add(pathForControllerDto);
                    break;
                }
            }
        }
        return pathForControllerDtos;
    }
}
