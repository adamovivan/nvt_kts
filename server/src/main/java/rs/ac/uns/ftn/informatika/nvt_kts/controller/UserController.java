package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.JwtTokenDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.LoginDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.UserAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.security.SecurityConfiguration;
import rs.ac.uns.ftn.informatika.nvt_kts.security.TokenUtils;
import rs.ac.uns.ftn.informatika.nvt_kts.service.UserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    TokenUtils tokenUtils;

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtTokenDTO> login(@RequestBody LoginDTO loginDTO) {
            // Perform the authentication
            UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(
                            loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Reload user details so we can generate token
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());

            return ResponseEntity.ok(new JwtTokenDTO(tokenUtils.generateToken(details)));
    }
}