package rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle;

import com.fasterxml.jackson.annotation.*;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import org.hibernate.annotations.Where;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.Coord;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VehicleType vehicleType;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "path_id")
    private Path path;

    @Column
    private Coord coord;

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) { //za potrebe testiranja :/
        this.id = id;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", vehicleType=" + vehicleType +
                ", path=" + path +
//                ", coord=" + coord +
                '}';
    }
}
