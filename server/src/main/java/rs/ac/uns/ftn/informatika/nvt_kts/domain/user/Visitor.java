package rs.ac.uns.ftn.informatika.nvt_kts.domain.user;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Card;

import javax.persistence.*;
import java.util.List;

@Entity
public class Visitor extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "visitor_type", nullable = false)
    private VisitorType visitorType;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "visitor_id")
    private List<Card> cards;

    public Long getId() {
        return id;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public VisitorType getVisitorType() {
        return visitorType;
    }

    public void setVisitorType(VisitorType visitorType) {
        this.visitorType = visitorType;
    }
}
