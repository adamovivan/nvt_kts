package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.StationDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.StationPathPairDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.PathService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.StationService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/stations")
public class StationController {

    @Autowired
    StationService stationService;

    @Autowired
    PathService pathService;

    // GET ALL
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Station> getAllStations() {
        return stationService.getAllStations();
    }

    // GET BY ID
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Station getStationById(@PathVariable("id") Long id) {
        try {
            return stationService.getStationById(id);
        } catch (StationDoesNotExist e) {
            return null;
        }
    }

    // ADD STATION
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addStation(@RequestBody Station station) {
        System.out.println("Station got:" + station.toString());
        stationService.saveNewStation(station);
    }

    // EDIT STATION
    @RequestMapping(value = "/edit",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void editStation(@RequestBody Station station) {
        System.out.println("Station got:" + station.toString());
        stationService.saveStation(station);
    }
    // NOT IN USE
    // ADD STATION TO LINE
    @RequestMapping(value = "{stationName}/addTo/{lineName}", method = RequestMethod.POST
            , consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addStationToPath(@PathVariable("stationName") String stationName
            , @PathVariable("lineName") String lineNumber) {
        try {
            stationService.addStationToPath(stationName, lineNumber);
            return new ResponseEntity<>("Station added to path successfully.", HttpStatus.OK);
        } catch (StationDoesNotExist | PathDoesNotExist e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    // NOT IN USE
    // REMOVE FROM LINE
    @RequestMapping(value = "/removeFromLine", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> removeStationFromPath(@RequestBody StationPathPairDTO stationPathPairDTO) {
        try {
            stationService.removeStationFromPath(stationPathPairDTO);
            return new ResponseEntity<>("Station removed from path.", HttpStatus.OK);
        } catch (StationDoesNotExist | PathDoesNotExist e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    // DELETE BY ID
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteStation(@PathVariable("id") Long id) throws StationDoesNotExist {
            stationService.deleteStation(id);
    }

}
