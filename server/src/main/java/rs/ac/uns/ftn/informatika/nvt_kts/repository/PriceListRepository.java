package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.PriceList;

import java.time.LocalDate;

@Repository
public interface PriceListRepository extends JpaRepository<PriceList,Long> {

    @Query("select pl from PriceList pl where :dateParam between pl.startDate and pl.endDate")
    PriceList getPriceListForDate(@Param("dateParam")LocalDate dateParam);
}
