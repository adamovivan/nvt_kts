package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.time.LocalDate;
import java.util.List;


@Repository
public interface PriceListItemRepository extends JpaRepository<PriceListItem,Long> {


    PriceListItem findByItemAndPriceList(Item item, PriceList priceList);

    List<PriceListItem> findByPriceList(PriceList priceList);

    @Query("select plItem from PriceListItem plItem where plItem.priceList = :priceList and " +
            " plItem.item.zone = :zone and plItem.item.vehicleType = :vehicleType and plItem.item.cardType = :cardType " +
            "and plItem.item.visitorType = :visitorType")
    PriceListItem findByPriceListAndItemFields(@Param("priceList") PriceList priceList, @Param("zone") Zone zone, @Param("vehicleType") VehicleType vehicleType, @Param("cardType") CardType cardType, @Param("visitorType") VisitorType visitorType);

}
