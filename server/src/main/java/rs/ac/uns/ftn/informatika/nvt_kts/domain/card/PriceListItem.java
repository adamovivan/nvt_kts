package rs.ac.uns.ftn.informatika.nvt_kts.domain.card;

import javax.persistence.*;

@Entity
public class PriceListItem {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) //morao sam ovo da stavim. Rucni unosi u bazu su poremetili generisanje :/
    @Column(name="id", updatable=false, nullable=false)
    private Long id;

    @Column(nullable = false)
    private Long price;

    @OneToOne
    private Item item;

    @ManyToOne
    private PriceList priceList;

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public void setPriceList(PriceList priceList) {
        this.priceList = priceList;
    }
}
