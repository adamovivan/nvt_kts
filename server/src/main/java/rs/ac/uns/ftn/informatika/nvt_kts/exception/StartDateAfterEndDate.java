package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class StartDateAfterEndDate extends MessageException {
    public StartDateAfterEndDate() {
        message = "Start date is after end date.";
    }
}