package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListItemDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.*;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Optional;

@Service
public class PriceListService {

    @Autowired
    PriceListRepository priceListRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    PriceListItemRepository priceListItemRepository;

    public void addNewPriceList(PriceListDTO priceListDTO) throws PriceListInRangeAlreadyExists,EndDateBeforeStartDate{
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d.MM.yyyy.");
        LocalDate newPriceListStartDate = LocalDate.parse(priceListDTO.getStartDate(),dateTimeFormatter);
        LocalDate newPriceListEndDate = LocalDate.parse(priceListDTO.getEndDate(),dateTimeFormatter);

        if(newPriceListEndDate.isBefore(newPriceListStartDate)){
            throw new EndDateBeforeStartDate();
        }

        if(priceListRepository.getPriceListForDate(newPriceListStartDate)!=null
          || priceListRepository.getPriceListForDate(newPriceListEndDate)!=null){
            throw new PriceListInRangeAlreadyExists();
        }

        PriceList priceList = new PriceList();
        priceList.setPriceListItems(new LinkedList<>());
        priceList.setStartDate(newPriceListStartDate);
        priceList.setEndDate(newPriceListEndDate);

        //Za svaku mogucu vrstu itema, (recimo obican korisnik u zoni 1, mesecna karta) , se generise cena
        for(CardType cardType:priceListDTO.getBasePrices().keySet()){
            for(VisitorType visitorType:priceListDTO.getVisitorModifier().keySet()){
                for(Zone zone:priceListDTO.getZoneModifier().keySet()){
                    for(VehicleType vehicleType:priceListDTO.getVehicleModifier().keySet()){
                        Item item = itemRepository.findOneByCardTypeAndVisitorTypeAndZoneAndVehicleType(cardType,visitorType,zone,vehicleType);
                        PriceListItem priceListItem = new PriceListItem();
                        priceListItem.setItem(item);
                        priceListItem.setPrice(priceListDTO.getBasePrices().get(cardType)
                                *priceListDTO.getVisitorModifier().get(visitorType)*priceListDTO.getZoneModifier().get(zone));

                        priceListItem.setPriceList(priceList);
                        priceList.getPriceListItems().add(priceListItem);
                    }

                }
            }
        }

        priceListRepository.save(priceList);
    }

    public PriceList getCurrentPriceList(){
        LocalDate currentDay = LocalDate.now();
        return priceListRepository.getPriceListForDate(currentDay);
    }

    public void changePriceListItemPrice(PriceListItemDTO priceListItemDTO) throws PriceListDoesNotExist,PriceListNotEditable {

        Optional<PriceList> priceListOpt = priceListRepository.findById(priceListItemDTO.getPriceListId());
        if(!priceListOpt.isPresent()){
            throw new PriceListDoesNotExist();
        }
        if(!checkIfEditable(priceListOpt.get())){
            throw new PriceListNotEditable();
        }
        PriceListItem prItem =
        priceListItemRepository.findByPriceListAndItemFields(priceListOpt.get(),priceListItemDTO.getZone(),priceListItemDTO.getVehicleType(),priceListItemDTO.getCardType(),priceListItemDTO.getVisitorType());
        prItem.setPrice(priceListItemDTO.getPrice());
        priceListItemRepository.save(prItem);
    }

    public LinkedList<PriceListItemDTO> getSelectedPriceListItems(Long id,VehicleType vehicleType) throws PriceListDoesNotExist{

        LinkedList<PriceListItemDTO> retVal = new LinkedList<>();
        Optional<PriceList> priceListOpt = priceListRepository.findById(id);

        if(!priceListOpt.isPresent()){
            throw new PriceListDoesNotExist();
        }
        PriceList priceList = priceListOpt.get();

        for(PriceListItem prItem:priceListItemRepository.findByPriceList(priceList)){
            if(vehicleType==prItem.getItem().getVehicleType()){
                PriceListItemDTO priceListItemDTO = new PriceListItemDTO();
                priceListItemDTO.setCardType(prItem.getItem().getCardType());
                priceListItemDTO.setVisitorType(prItem.getItem().getVisitorType());
                priceListItemDTO.setZone(prItem.getItem().getZone());
                priceListItemDTO.setVehicleType(prItem.getItem().getVehicleType());
                priceListItemDTO.setPrice(prItem.getPrice());
                retVal.add(priceListItemDTO);

            }

        }
        return retVal;
    }

    public LinkedList<PriceListDTO> getAllPriceLists(){

        PriceList currentPriceList = getCurrentPriceList();
        Long currentPriceListId = null;
        if(currentPriceList!=null){
            currentPriceListId = currentPriceList.getId();
        }


        LinkedList<PriceListDTO> retVal = new LinkedList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d.MM.yyyy.");
        for(PriceList priceList:priceListRepository.findAll()){

            PriceListDTO priceListDTO = new PriceListDTO();
            priceListDTO.setStartDate(dateTimeFormatter.format(priceList.getStartDate()));
            priceListDTO.setEndDate(dateTimeFormatter.format(priceList.getEndDate()));
            priceListDTO.setId(priceList.getId());
            priceListDTO.setEditable(checkIfEditable(priceList));
            if(priceList.getId().equals(currentPriceListId)){
                retVal.addFirst(priceListDTO); //trenutni pricelist da bude prvi,ako ga ima
            }
            else{
                retVal.add(priceListDTO);
            }

        }

        return retVal;

    }

    // provera da li je cenovnik buduci
    private boolean checkIfEditable(PriceList priceList){

        if(LocalDate.now().isBefore(priceList.getStartDate())){
            return true;
        }
        return false;
    }

}
