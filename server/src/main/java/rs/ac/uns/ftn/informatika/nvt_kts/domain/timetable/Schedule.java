package rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable;

import org.hibernate.annotations.Where;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;

import javax.persistence.*;

@Entity
@Where(clause = "is_active='TRUE'")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "day", nullable = false)
    private Day day;

    @Column(name = "is_active")
    private Boolean active;

    @OneToOne
    private Path path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", day=" + day +
                ", path=" + path +
                '}';
    }
}
