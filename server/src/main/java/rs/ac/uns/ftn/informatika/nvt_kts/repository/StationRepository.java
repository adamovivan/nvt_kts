package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;

import java.util.List;
import java.util.Set;

@Repository
public interface StationRepository extends JpaRepository<Station,Long> {

    @Query("select distinct p.id from Path p   where :stationId = any(select s.id from p.stations s)")
    Set<Long> findAllPathsIdsForStation(@Param("stationId") Long stationId);

    Station findByName(String name);

}
