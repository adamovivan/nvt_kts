package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Card;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.time.LocalDate;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {

    @Query("select c from Card c inner join User v on c.visitor.id = v.id " +
            "where (v.id = ?1) order by c.id desc ")
    Page<Card> findAllVisitorCards(Pageable pageable,Long visitorId);

    @Modifying
    @Query("update Card c set c.active = false " +
            "where c.id = ?2 and " +
            "(select u.username from User u where u.id = c.visitor.id) = ?1")
    int checkCard(String username, Long id);

    @Query("select c from Card c inner join User u on c.visitor.id = u.id " +
            "where (u.username like %?1%) " +
            "and (c.visitor.visitorType = ?2 or ?2 is null) " +
            "and (c.priceListItem.item.cardType = ?3 or ?3 is null) " +
            "and (c.priceListItem.item.zone = ?4 or ?4 is null) " +
            "and (c.priceListItem.item.vehicleType = ?5 or ?5 = null) " +
            "and (c.priceListItem.price >= ?6 or ?6 is null) " +
            "and (c.priceListItem.price <= ?7 or ?7 is null) " +
            "and (c.active = ?8 or ?8 is null) " +
            "and (c.startDate >= ?9 or ?9 is null) " +
            "and (c.endDate <= ?10 or ?10 is null)")
    Page<Card> filterCards(Pageable page, String username, VisitorType visitorType, CardType cardType, Zone zone, VehicleType vehicleType,
                           Long priceFrom, Long priceTo, Boolean active, LocalDate startDate, LocalDate endDate);
}
