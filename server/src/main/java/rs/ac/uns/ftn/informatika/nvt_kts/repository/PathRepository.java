package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.util.List;

@Repository
public interface PathRepository extends JpaRepository<Path, Long> {

    Path findPathById(long id);

    Path findByPathName(String pathName);

    @Query(value = "select p from Path p where :vehicleId = any(select v from Vehicle v where v.path=p) ")
    Path findPathByVehicle(@Param("vehicleId") Long vehicleId);

    @Query("select distinct p from Path p   where :stationId = any(select s.id from p.stations s)")
    List<Path> findAllPathsForStation(@Param("stationId") Long stationId);

    Path findPathByPathName(String lineNumber);

    @Query("select p.pathName from Path p where :vType = p.vehicleType")
    List<String> findAllPathNames(@Param("vType") VehicleType vType);

    Path findPathByPathNameAndVehicleType(String lineNumber, VehicleType vehicleType);
}
