package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;

import java.util.List;
import java.util.Set;

public class StationDTO {

    public StationDTO(Station s) {
        this.station = s;
        this.stationPaths = s.getPaths();
    }

    public Station station;
    public List<Path> stationPaths;
}
