package rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable;


public enum Day {
    Workday,
    Saturday,
    Sunday
}
