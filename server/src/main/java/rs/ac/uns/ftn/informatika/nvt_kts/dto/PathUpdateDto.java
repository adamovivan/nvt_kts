package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;

@JsonIdentityInfo(generator = JSOGGenerator.class)
public class PathUpdateDto {

    public PathUpdateDto() {
    }

    public PathUpdateDto(Path path, int vehicleNumber) {
        this.path = path;
        this.vehicleNumber = vehicleNumber;
    }

    private Path path;
    private int vehicleNumber;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public int getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(int vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    @Override
    public String toString() {
        return "PathUpdateDto{" +
                "path=" + path +
                ", vehicleNumber=" + vehicleNumber +
                '}';
    }
}
