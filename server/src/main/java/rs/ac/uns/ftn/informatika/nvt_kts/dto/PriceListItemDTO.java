package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceListItemDTO {

    private Long price;

    private CardType cardType;

    private VisitorType visitorType;

    private Zone zone;

    private VehicleType vehicleType;

    private Long priceListId;

    public PriceListItemDTO(){

    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public VisitorType getVisitorType() {
        return visitorType;
    }

    public void setVisitorType(VisitorType visitorType) {
        this.visitorType = visitorType;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }
}
