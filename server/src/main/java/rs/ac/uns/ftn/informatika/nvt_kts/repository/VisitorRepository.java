package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;

public interface VisitorRepository extends JpaRepository<Visitor,Long> {

    @Query(value = "select i from User i where :username = i.username")
    Visitor findVisitorByUsername(@Param("username") String username);
}
