package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;

public interface ControllerRepository extends JpaRepository<Controller, Long> {
    Controller findByUsername(String username);
}
