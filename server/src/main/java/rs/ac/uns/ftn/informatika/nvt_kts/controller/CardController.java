package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.CardDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PageDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListNotSet;
import rs.ac.uns.ftn.informatika.nvt_kts.service.CardService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.UserService;

import java.time.LocalDate;

@RestController
@RequestMapping("/cards")
public class CardController {

    @Autowired
    UserService userService;

    @Autowired
    CardService cardService;

    @RequestMapping(value="/buy",method = RequestMethod.POST,consumes= MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('ROLE_VISITOR')")
    public ResponseEntity<Object> buyCard(@RequestBody CardDTO cardDTO){
        try{
            Visitor v = (Visitor)userService.getLoggedUser();
            return new ResponseEntity<>(cardService.buyCard(v,cardDTO),HttpStatus.OK);
        }
        catch(PriceListNotSet | JsonProcessingException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/checkPrice",method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('ROLE_VISITOR')")
    public ResponseEntity<String> checkCardPrice(@RequestBody CardDTO cardDTO){
        try{
            Visitor v = (Visitor)userService.getLoggedUser();
            return new ResponseEntity<>(cardService.checkCardPrice(v,cardDTO),HttpStatus.OK);
        }
        catch(PriceListNotSet e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }


    }

    @RequestMapping(value = "/myCards", method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('ROLE_VISITOR')")
    public PageDTO getMyCards(Pageable pageable){
        Visitor v = (Visitor)userService.getLoggedUser();
        return cardService.getMyCards(pageable,v);
    }

    @RequestMapping(value="/check/{id}", method=RequestMethod.PUT)
    public ResponseEntity<String> checkCard(@PathVariable("id") Long id){
        try{
            if(cardService.checkCard(id) == 1)
                return new ResponseEntity<>("Card successfully checked.", HttpStatus.OK);
            else
                return new ResponseEntity<>("Card unsuccessfully checked.", HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value="/filter", method=RequestMethod.GET)
    public PageDTO getFilteredCards(Pageable pageable,
                                    @RequestParam(value = "visitorType", required = false) VisitorType visitorType,
                                    @RequestParam(value = "cardType", required = false) CardType cardType,
                                    @RequestParam(value = "zone", required = false) Zone zone,
                                    @RequestParam(value = "vehicleType", required = false) VehicleType vehicleType,
                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                              @RequestParam(value = "startDate", required = false) LocalDate startDate,
                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                              @RequestParam(value = "endDate", required = false) LocalDate endDate,
                                    @RequestParam(value = "priceFrom", required = false) Long priceFrom,
                                    @RequestParam(value = "priceTo", required = false) Long priceTo,
                                    @RequestParam(value = "active", required = false) Boolean active,
                                    @RequestParam(value = "username", required = false) String username){

        return cardService.filterCards(pageable, username, visitorType, cardType, zone, vehicleType, priceFrom,
                                       priceTo, active, startDate, endDate);
    }
}
