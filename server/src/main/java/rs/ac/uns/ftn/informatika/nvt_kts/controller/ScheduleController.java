package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.*;
import rs.ac.uns.ftn.informatika.nvt_kts.service.ScheduleDurationService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.ScheduleService;

import java.util.List;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    ScheduleService scheduleService;

    @Autowired
    ScheduleDurationService scheduleDurationService;

    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    @RequestMapping(value="/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> addSchedule(@RequestBody ScheduleDTO scheduleDTO) {
        boolean done, doneDuration;
        try {

            done = scheduleService.addSchedule(scheduleDTO);
            doneDuration = scheduleDurationService.addScheduleDuration(scheduleDTO);

        } catch (PathDoesNotExist e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (ScheduleDurationAlreadyExists scheduleDurationAlreadyExists) {
            return new ResponseEntity<>(scheduleDurationAlreadyExists.getMessage(), HttpStatus.BAD_REQUEST);
        }

        if(done && doneDuration) {
            return new ResponseEntity<>("Schedule added successfully.", HttpStatus.CREATED);
        }

        return new ResponseEntity<>("Something went wrong.", HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value="/allTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody @ResponseStatus(HttpStatus.OK)
    List<ScheduleDuration> getAllScheduleDurations(@RequestParam("pathName") String pathName, @RequestParam("day") Day day) {
        return scheduleDurationService.getAllScheduleDurations(pathName, day);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody @ResponseStatus(HttpStatus.OK)
    List<ScheduleDuration> getScheduleDurations(@RequestParam("pathName") String pathName, @RequestParam("day") Day day, @RequestParam("vehicleType") VehicleType vehicleType) {
        return scheduleDurationService.getScheduleDurations(pathName, day, vehicleType);
    }

    @RequestMapping(value="/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody @ResponseStatus(HttpStatus.OK)
    List<ScheduleDTO> getScheduleDuration() {
        return scheduleDurationService.getScheduleDurations();
    }

    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    @RequestMapping(value="/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> updateSchedule(@RequestBody ScheduleDTO scheduleDTO) {
        try {
            scheduleDurationService.updateScheduleDuration(scheduleDTO);
        } catch (ScheduleDurationDoesNotExist scheduleDurationDoesNotExist) {
            return new ResponseEntity<>(scheduleDurationDoesNotExist.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Schedule duration updated successfully.", HttpStatus.OK);
    }

    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteSchedule(@RequestParam("pathName") String pathName, @RequestParam("day") Day day, @RequestParam("vehicleType") VehicleType vehicleType, @RequestParam("endDate") String endDate, @RequestParam("startDate") String startDate) {
        try {
            scheduleDurationService.deleteScheduleDuration(pathName, day, vehicleType, startDate, endDate);
            scheduleService.deleteSchedule(pathName, day, vehicleType);
        } catch (ScheduleDurationDoesNotExist scheduleDurationDoesNotExist) {
            return new ResponseEntity<>(scheduleDurationDoesNotExist.getMessage(), HttpStatus.NOT_FOUND);
        } catch (ScheduleDoesNotExist scheduleDoesNotExist) {
            scheduleDoesNotExist.printStackTrace();
            return new ResponseEntity<>(scheduleDoesNotExist.getMessage(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>("Schedule deleted successfully.", HttpStatus.OK);
    }
}
