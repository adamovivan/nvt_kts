package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.PriceList;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListItemDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.response.SimpleResponse;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.EndDateBeforeStartDate;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListInRangeAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListNotEditable;
import rs.ac.uns.ftn.informatika.nvt_kts.service.PriceListService;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;


@RestController
@RequestMapping("/priceList")
public class PriceListController {

    @Autowired
    PriceListService priceListService;


    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ResponseEntity<SimpleResponse> addNewPriceList(@RequestBody PriceListDTO priceListDTO){
        try{
            priceListService.addNewPriceList(priceListDTO);
            return new ResponseEntity<>(new SimpleResponse(true,"Price list added successfully."), HttpStatus.CREATED);
        }
        catch(PriceListInRangeAlreadyExists | EndDateBeforeStartDate | DateTimeParseException e){
            return new ResponseEntity<>(new SimpleResponse(false,e.getMessage()),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT,value="/editPriceListItem",consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('ROLE_ADMIN')")
    public ResponseEntity<SimpleResponse> changePriceListItemPrice(@RequestBody PriceListItemDTO priceListItemDTO){
        try{
            priceListService.changePriceListItemPrice(priceListItemDTO);
            return new ResponseEntity<>(new SimpleResponse(true,"Price changed successfully."), HttpStatus.OK);
        }
        catch(PriceListDoesNotExist | PriceListNotEditable e){
            return new ResponseEntity<>(new SimpleResponse(false,e.getMessage()),HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(method=RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody PriceListDTO getCurrentPriceList(){
        PriceList priceList = priceListService.getCurrentPriceList();
        PriceListDTO retVal = new PriceListDTO();

        if(priceList==null){
            return retVal;
        }

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d.MM.yyyy.");
        retVal.setStartDate(dateTimeFormatter.format(priceList.getStartDate()));
        retVal.setEndDate(dateTimeFormatter.format(priceList.getEndDate()));
        retVal.setId(priceList.getId());
        return retVal;
    }

    @RequestMapping(value="/all",produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody LinkedList<PriceListDTO> getAllPriceLists(){
        return priceListService.getAllPriceLists();
    }

    @RequestMapping(value = "/id/{id}/{vehicleType}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody  LinkedList<PriceListItemDTO> getSelectedPriceListItems(@PathVariable("id")Long id,@PathVariable("vehicleType")VehicleType vehicleType){
        try{
            return priceListService.getSelectedPriceListItems(id,vehicleType);
        }
        catch(PriceListDoesNotExist e){
            return new LinkedList<>();
        }
    }
}
