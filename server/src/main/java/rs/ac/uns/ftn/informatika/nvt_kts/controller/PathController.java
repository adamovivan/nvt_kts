package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathNumbersDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathUpdateDto;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.VehicleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.PathService;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/lines")
public class PathController {

    @Autowired
    PathService pathService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void addPath(@RequestBody Path path) {
        System.out.println("Path got:" + path.toString());
        pathService.save(path);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void addPath(@RequestBody PathUpdateDto pathUpdateDto) {
        System.out.println("PathDTO got:" + pathUpdateDto.toString());
        pathService.updatePath(pathUpdateDto);
    }

    @RequestMapping(value = "/lineNumbers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    PathNumbersDTO getAllPathNames() {
        PathNumbersDTO retVal = new PathNumbersDTO();
        retVal.buses = pathService.getAllPathNumbers(VehicleType.BUS);
        retVal.metroTrains = pathService.getAllPathNumbers(VehicleType.METROTRAIN);
        retVal.trolleys = pathService.getAllPathNumbers(VehicleType.TROLLEY);
        return retVal;
    }

    // Update vehicle coordinates on all paths
    @RequestMapping(value = "/updateLocations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Path> updateVehicleLocations() {
        return pathService.updateVehicleLocations();
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Path> getAllPaths() {
        return pathService.getAllPaths();
    }

    @RequestMapping(value = "/{lineNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Path getPath(@PathVariable("lineNumber") String lineNumber) {
        System.out.println("Line Number requested: " + lineNumber);
        System.out.println("Line: " + pathService.getPath(lineNumber));
        return pathService.getPath(lineNumber);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Long id) throws StationDoesNotExist, VehicleDoesNotExist {
        pathService.deleteById(id);
    }

}
