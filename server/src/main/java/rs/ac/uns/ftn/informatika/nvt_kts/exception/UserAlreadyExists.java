package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class UserAlreadyExists extends MessageException {
    public UserAlreadyExists() {
        message = "User with that username already exist.";
    }
}
