package rs.ac.uns.ftn.informatika.nvt_kts.domain.user;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;

import javax.persistence.*;
import java.util.List;

@Entity
public class Controller extends User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany
    private List<Vehicle> vehicles;

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
