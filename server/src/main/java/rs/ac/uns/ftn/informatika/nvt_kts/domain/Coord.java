package rs.ac.uns.ftn.informatika.nvt_kts.domain;

import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Coord {

    public Coord(){
    }

    public Coord(double n, double e){
        this.e = e;
        this.n = n;
    }

    public Coord(Coord c){
        this.e = c.e;
        this.n = c.n;
    }

    public double getE() {
        return e;
    }

    public void setE(double e) {
        this.e = e;
    }

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    private double e;

    private double n;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coord coord = (Coord) o;
        return Double.compare(coord.e, e) == 0 &&
                Double.compare(coord.n, n) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(e, n);
    }

    @Override
    public String toString() {
        return "Coord(" +
                "e: " + e +
                ", n: " + n +
                ')';
    }
}
