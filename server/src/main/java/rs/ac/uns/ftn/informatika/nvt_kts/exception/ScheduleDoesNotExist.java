package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class ScheduleDoesNotExist extends MessageException {
    public ScheduleDoesNotExist() {
        message = "Schedule with that path name and day does no exist.";
    }
}
