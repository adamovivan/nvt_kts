package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ScheduleDurationRepository extends JpaRepository<ScheduleDuration, Long> {

    @Query(value = "select sd from ScheduleDuration sd where sd.schedule.path.pathName=?1 and sd.schedule.path.vehicleType=?2 and sd.schedule.day=?3 and ((sd.startDate>=?4 and sd.startDate<=?5) or (sd.startDate<=?4 and sd.endDate>=?4))")
    ScheduleDuration findByDuration(String pathName, VehicleType vehicleType, Day day, LocalDate startDate, LocalDate endDate);

    @Query(value = "select sd from ScheduleDuration sd where sd.schedule.path.pathName=?1 and sd.schedule.path.vehicleType=?2 and sd.schedule.day=?3 and (sd.startDate=?4 and sd.endDate=?5)")
    ScheduleDuration findByExactDuration(String pathName, VehicleType vehicleType, Day day, LocalDate startDate, LocalDate endDate);

    @Query(value = "select sd from ScheduleDuration sd where sd.schedule.path.pathName=?1 and sd.schedule.path.vehicleType=?3 and sd.schedule.day=?2")
    List<ScheduleDuration> findScheduleDurations(String pathName, Day day, VehicleType vehicleType);

    @Query(value = "select sd from ScheduleDuration sd where sd.schedule.path.pathName=?1 and sd.schedule.day=?2")
    List<ScheduleDuration> findAllScheduleDurations(String pathName, Day day);

    List<ScheduleDuration> findByScheduleId(Long id);
}
