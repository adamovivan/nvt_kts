package rs.ac.uns.ftn.informatika.nvt_kts.exception;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;

//@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="Invalid vehicle type.")
public class InvalidVehicleType extends MessageException {
    public InvalidVehicleType(Path p) {
        message = "Invalid vehicle type. Vehicle type must be: " + p.getVehicleType().toString();
    }
}
