package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.document.ApprovalStatus;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.document.Document;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.DiscountRequestDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.DocumentDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.DocumentRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VisitorRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private VisitorRepository visitorRepository;

    private final String UPLOAD_DIRECTORY = System.getProperty("user.dir")+"\\documents\\";

    public void addDocument(MultipartFile documentPhoto, String documentName, VisitorType discountVisitorType) {

        String extension = FilenameUtils.getExtension(documentPhoto.getOriginalFilename());
        String filename = UUID.randomUUID().toString() + "." + extension;

        Path path = Paths.get(UPLOAD_DIRECTORY + filename);

        try {
            Files.write(path, documentPhoto.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Visitor visitor = visitorRepository.findVisitorByUsername(username);

        Document document = new Document();
        document.setPath(path.toString());
        document.setName(documentName);
        document.setVisitor(visitor);
        document.setDiscountVisitorType(discountVisitorType);
        document.setApprovalStatus(ApprovalStatus.PENDING);

        documentRepository.save(document);
    }

    public List<DiscountRequestDTO> findDocsToApprove() {
        List<Document> documents = documentRepository.findByApprovalStatus(ApprovalStatus.PENDING);

        List<DiscountRequestDTO> requests = new ArrayList<>();

        for(Document d: documents) {
            DiscountRequestDTO dr = new DiscountRequestDTO();

            dr.setId(d.getId());
            dr.setUsername(d.getVisitor().getUsername());
            dr.setFirstName(d.getVisitor().getFirstName());
            dr.setLastName(d.getVisitor().getLastName());
            dr.setDocumentName(d.getName());
            dr.setVisitorType(d.getDiscountVisitorType());

            requests.add(dr);
        }
        return requests;
    }

    public void approveDocument(Long id) throws DocumentDoesNotExist {
        Document document = documentRepository.findById(id).orElseThrow(DocumentDoesNotExist::new);
        document.setApprovalStatus(ApprovalStatus.APPROVED);
        Visitor visitor = visitorRepository.findVisitorByUsername(document.getVisitor().getUsername());
        visitor.setVisitorType(document.getDiscountVisitorType());
        visitorRepository.save(visitor);
        documentRepository.save(document);
    }

    public void rejectDocument(Long id) throws DocumentDoesNotExist {
        Document document = documentRepository.findById(id).orElseThrow(DocumentDoesNotExist::new);
        document.setApprovalStatus(ApprovalStatus.REJECTED);
        documentRepository.save(document);
    }

    public File getImage(Long id) throws DocumentDoesNotExist {
        Document document = documentRepository.findById(id).orElseThrow(DocumentDoesNotExist::new);
        return new File(document.getPath());
    }
}
