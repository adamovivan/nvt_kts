package rs.ac.uns.ftn.informatika.nvt_kts.domain.user;

public enum VisitorType {
    REGULAR,
    STUDENT,
    PENSIONER
}
