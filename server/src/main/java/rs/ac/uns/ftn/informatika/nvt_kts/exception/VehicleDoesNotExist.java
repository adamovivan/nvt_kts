package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class VehicleDoesNotExist extends MessageException {
    public VehicleDoesNotExist(){
        message = "Vehicle with that id does not exist.";
    }
}
