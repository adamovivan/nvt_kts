package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
