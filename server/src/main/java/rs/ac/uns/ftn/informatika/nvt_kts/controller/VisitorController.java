package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.response.SimpleResponse;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.UserAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.service.DocumentService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.VisitorService;

@RestController
public class VisitorController {
    @Autowired
    private VisitorService visitorService;

    @Autowired
    private DocumentService documentService;

    @PreAuthorize("hasRole('ROLE_VISITOR')")
    @RequestMapping(value = "/uploadDocument",method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<SimpleResponse> uploadDocument(@RequestParam("file") MultipartFile file,
                                                         @RequestParam("documentName") String documentName,
                                                         @RequestParam("discount") VisitorType discountVisitorType) {
        try {
            if (file.getContentType() == null || !(file.getContentType().equals("image/jpeg")
                    || file.getContentType().equals("image/png") || file.getContentType().equals("image/jpg")))
                return ResponseEntity.badRequest().body(new SimpleResponse(false, "Unsupported image type."));

            documentService.addDocument(file, documentName, discountVisitorType);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new SimpleResponse(false, "The field file exceeds its maximum permitted size or it its type is invalid."));
        }


            return ResponseEntity.ok(new SimpleResponse("Uploaded."));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registerUser(@RequestBody RegisterDTO registerDTO) {
        try {
            boolean added = visitorService.addVisitor(registerDTO);
            if (!added) {
                return new ResponseEntity<>("Something went wrong.", HttpStatus.BAD_REQUEST);
            }
                return new ResponseEntity<>("Successful registration.", HttpStatus.CREATED);
        } catch (UserAlreadyExists e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
