package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.User;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.UserAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.security.TokenUtils;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TokenUtils tokenUtils;

    /*  Na osnovu username-a is JWT tokena vraca trenutno ulogovanog korisnika.
        Castovati po potrebi u metodama. Nema nikakvih provera
        vezano za postojanje korisnika, jer ce security da se pobrine da je jwt token validan.
        Ako je token validan, onda postoji username u bazi.             */

    public User getLoggedUser(){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByUsername(username);
    }
}
