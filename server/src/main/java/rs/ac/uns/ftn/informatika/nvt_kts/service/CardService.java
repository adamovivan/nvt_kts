package rs.ac.uns.ftn.informatika.nvt_kts.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.validator.constraints.EAN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.User;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.CardDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PageDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListNotSet;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.CardRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.security.TokenUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.awt.event.ItemEvent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class CardService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    PriceListService priceListService;

    @Autowired
    PriceListItemRepository priceListItemRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    TokenUtils tokenUtils;

    @Transactional
    public CardDTO buyCard(Visitor v, CardDTO cardDTO) throws PriceListNotSet, JsonProcessingException {

        PriceList priceList = priceListService.getCurrentPriceList();
        if(priceList==null){
            throw new PriceListNotSet();
        }

        Card card = new Card();
        card.setActive(true);

        Item item = itemRepository.findOneByCardTypeAndVisitorTypeAndZoneAndVehicleType(cardDTO.getCardType(),v.getVisitorType(),cardDTO.getZone(),cardDTO.getVehicleType());
        PriceListItem plItem = priceListItemRepository.findByItemAndPriceList(item,priceList);

        card.setPriceListItem(plItem);
        setStartAndEndDates(card);
        card.setVisitor(v);
        cardRepository.save(card);

        v.getCards().add(card);
        userRepository.save(v);
        return toCardDTO(card,v);
    }

    public String checkCardPrice(Visitor v,CardDTO cardDTO) throws PriceListNotSet{

        PriceList priceList = priceListService.getCurrentPriceList();
        if(priceList==null){
            throw new PriceListNotSet();
        }
        Item item = itemRepository.findOneByCardTypeAndVisitorTypeAndZoneAndVehicleType(cardDTO.getCardType(),v.getVisitorType(),cardDTO.getZone(),cardDTO.getVehicleType());
        PriceListItem plItem = priceListItemRepository.findByItemAndPriceList(item,priceList);
        return plItem.getPrice().toString();

    }

    public void setStartAndEndDates(Card card){
        CardType cardType = card.getPriceListItem().getItem().getCardType();
        LocalDate today = LocalDate.now();
        card.setStartDate(today);

        if(cardType==CardType.DAILY || cardType== CardType.ONEDRIVE){
            card.setEndDate(today.plusDays(0));
        }
        else if(cardType==CardType.MONTHLY){
            card.setEndDate(today.plusMonths(1));
        }
        else{
            card.setEndDate(today.plusYears(1));
        }

    }

    private CardDTO toCardDTO(Card c,Visitor v){

        CardDTO elem = new CardDTO();
        elem.setId(c.getId());
        elem.setCardType(c.getPriceListItem().getItem().getCardType());
        elem.setPrice(c.getPriceListItem().getPrice());
        elem.setZone(c.getPriceListItem().getItem().getZone());
        elem.setVehicleType(c.getPriceListItem().getItem().getVehicleType());
        elem.setStartDate(c.getStartDate());
        elem.setEndDate(c.getEndDate());
        elem.setActive(c.isActive());
        elem.setVisitorType(v.getVisitorType());
        return elem;
    }

    public PageDTO getMyCards(Pageable pageable,Visitor v){

        LinkedList<CardDTO> cardDTOS = new LinkedList<>();
       Page<Card> cards = cardRepository.findAllVisitorCards(pageable,v.getId());
        for(Card c:cards){
            CardDTO elem = toCardDTO(c,v);
            cardDTOS.add(elem);
        }
        PageDTO retVal = new PageDTO(cardDTOS,cards.getTotalElements(),cards.getTotalPages());
        return retVal;
    }

    @Transactional
    public int checkCard(Long id){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return cardRepository.checkCard(username, id);
    }

    public PageDTO filterCards(Pageable page, String username, VisitorType visitorType, CardType cardType, Zone zone,
                                     VehicleType vehicleType, Long priceFrom, Long priceTo,
                                     Boolean active, LocalDate startDate, LocalDate endDate){
        if(username == null)
            username = "";

        Page<Card> cards = cardRepository.filterCards(page, username, visitorType, cardType, zone, vehicleType, priceFrom, priceTo,
                active, startDate, endDate);

        List<CardDTO> cardsDTO = new ArrayList<>();

        for(Card c: cards){
            CardDTO cardDTO = new CardDTO();

            cardDTO.setId(c.getId());
            cardDTO.setPrice(c.getPriceListItem().getPrice());
            cardDTO.setZone(c.getPriceListItem().getItem().getZone());
            cardDTO.setVisitorType(c.getVisitor().getVisitorType());
            cardDTO.setVehicleType(c.getPriceListItem().getItem().getVehicleType());
            cardDTO.setUsername(c.getVisitor().getUsername());
            cardDTO.setActive(c.isActive());
            cardDTO.setStartDate(c.getStartDate());
            cardDTO.setEndDate(c.getEndDate());
            cardDTO.setCardType(c.getPriceListItem().getItem().getCardType());
            cardsDTO.add(cardDTO);
        }

        return new PageDTO(cardsDTO, cards.getTotalElements(), cards.getTotalPages());
    }
}
