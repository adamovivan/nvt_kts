package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;

import java.util.Set;

public class PathDTO {

    public PathDTO(Path path, Set<Station> stations) {
        this.path = path;
        this.stations = stations;
    }

    public Path path;
    public Set<Station> stations;

    // when adding new Path (get stations from DB)
    public Set<Long> stationIds;

}
