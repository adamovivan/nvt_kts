package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class DocumentDoesNotExist extends MessageException {
    public DocumentDoesNotExist() {
        message = "Document with provided id does not exist.";
    }
}