package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;

import java.time.LocalDate;

public class ControllerCheckDTO {

    private String firstName;
    private String lastName;
    private CardType cardType;
    private boolean activeCard;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean paid;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public boolean isActiveCard() {
        return activeCard;
    }

    public void setActiveCard(boolean activeCard) {
        this.activeCard = activeCard;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public String toString() {
        return "ControllerCheckDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cardType=" + cardType +
                ", activeCard=" + activeCard +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", paid=" + paid +
                '}';
    }
}
