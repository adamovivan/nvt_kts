package rs.ac.uns.ftn.informatika.nvt_kts.domain.card;

public enum CardType {
    ONEDRIVE,
    DAILY,
    MONTHLY,
    YEARLY
}
