package rs.ac.uns.ftn.informatika.nvt_kts.domain.card;

public enum Zone {
    ONE, TWO, THREE
}
