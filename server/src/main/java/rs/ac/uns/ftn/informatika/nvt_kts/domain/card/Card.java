package rs.ac.uns.ftn.informatika.nvt_kts.domain.card;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Card {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) //morao sam ovo da stavim. Rucni unosi u bazu su poremetili generisanje :/
    @Column(name="id", updatable=false, nullable=false)
    private Long id;

    @Column(nullable = false)
    private boolean active;

    @OneToOne
    private Visitor visitor;

    @OneToOne
    private PriceListItem priceListItem;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public PriceListItem getPriceListItem() {
        return priceListItem;
    }

    public void setPriceListItem(PriceListItem priceListItem) {
        this.priceListItem = priceListItem;
    }

    public Long getId() {
        return id;
    }

    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", active=" + active +
                ", visitor=" + visitor +
                ", priceListItem=" + priceListItem +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
