package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class PriceListInRangeAlreadyExists extends MessageException{
    public PriceListInRangeAlreadyExists(){
        message = "Date ranges of price lists cannot overlap.";
    }
}
