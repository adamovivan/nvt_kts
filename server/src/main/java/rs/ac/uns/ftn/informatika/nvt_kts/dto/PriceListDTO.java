package rs.ac.uns.ftn.informatika.nvt_kts.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import java.util.HashMap;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceListDTO {

    private String startDate;
    private String endDate;
    private Long id;

    private HashMap<CardType,Long> basePrices;
    private HashMap<VisitorType,Long> visitorModifier;
    private HashMap<Zone,Long> zoneModifier;
    private HashMap<VehicleType,Long> vehicleModifier;
    private boolean editable;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public HashMap<CardType, Long> getBasePrices() {
        return basePrices;
    }

    public void setBasePrices(HashMap<CardType, Long> basePrices) {
        this.basePrices = basePrices;
    }

    public HashMap<VisitorType, Long> getVisitorModifier() {
        return visitorModifier;
    }

    public void setVisitorModifier(HashMap<VisitorType, Long> visitorModifier) {
        this.visitorModifier = visitorModifier;
    }

    public HashMap<Zone, Long> getZoneModifier() {
        return zoneModifier;
    }

    public void setZoneModifier(HashMap<Zone, Long> zoneModifier) {
        this.zoneModifier = zoneModifier;
    }

    public HashMap<VehicleType, Long> getVehicleModifier() {
        return vehicleModifier;
    }

    public void setVehicleModifier(HashMap<VehicleType, Long> vehicleModifier) {
        this.vehicleModifier = vehicleModifier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
