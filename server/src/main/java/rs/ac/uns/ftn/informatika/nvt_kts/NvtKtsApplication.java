package rs.ac.uns.ftn.informatika.nvt_kts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Item;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ItemRepository;


@SpringBootApplication
@ComponentScan(basePackages = "rs.ac.uns.ftn.informatika.nvt_kts")
public class NvtKtsApplication {

    @Autowired
    ItemRepository itemRepository; //komunikacija s bazom kad se generisu itemi

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {

        //Generisi i ubaci sve moguce Item-e(kojih ima fiksan broj) u bazu, ako vec nisu tu.
        for(CardType cardType:CardType.values()){
            for(VisitorType visitorType:VisitorType.values()){
                for(Zone zone:Zone.values()){
                    for(VehicleType vehicleType:VehicleType.values()){
                        if(itemRepository.findOneByCardTypeAndVisitorTypeAndZoneAndVehicleType(cardType,visitorType,zone,vehicleType)==null){
                            Item item = new Item();
                            item.setCardType(cardType);
                            item.setVisitorType(visitorType);
                            item.setVehicleType(vehicleType);
                            item.setZone(zone);
                            itemRepository.save(item);
                        }
                    }

                }
            }
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(NvtKtsApplication.class, args);
    }
}
