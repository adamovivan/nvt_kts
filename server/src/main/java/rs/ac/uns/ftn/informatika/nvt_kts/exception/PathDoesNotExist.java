package rs.ac.uns.ftn.informatika.nvt_kts.exception;



//@ResponseStatus(value = HttpStatus.BAD_REQUEST,reason="Path with that id does not exist.")
public class PathDoesNotExist extends MessageException {
    public PathDoesNotExist(){
        message = "Path does not exist.";
    }
}
