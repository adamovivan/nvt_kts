package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.Coord;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathUpdateDto;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.StationDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.VehicleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.StationRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VehicleRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PathService {

    @Autowired
    PathRepository pathRepository;

    @Autowired
    StationService stationService;

    @Autowired
    VehicleService vehicleService;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    StationRepository stationRepository;

//    @Autowired
//    ControllerService controllerService;

    public List<String> getAllPathNumbers(VehicleType vehicleType) {
        return pathRepository.findAllPathNames(vehicleType);
    }

    public PathUpdateDto updatePath(PathUpdateDto dto) {



        // removed vehicles
        for (Vehicle v : dto.getPath().getVehicles()) {
//            for(Controller c: controllerService)
            vehicleRepository.deleteById(v.getId());
        }
        dto.getPath().setVehicles(new ArrayList<Vehicle>());
        // added vehicles
        for (int i = 0; i < dto.getVehicleNumber(); i++) {
            Vehicle v = new Vehicle();
            v.setCoord(new Coord(44.811961, 20.452162));
            v.setVehicleType(dto.getPath().getVehicleType());
            dto.getPath().getVehicles().add(v);
        }
        save(dto.getPath());
        return dto;
    }

    @Transactional
    public Path save(Path p) {
        ArrayList<Station> stations = (ArrayList<Station>) p.getStations();
        ArrayList<Vehicle> vehicles = (ArrayList<Vehicle>) p.getVehicles();
        p.setVehicles(new ArrayList<>());
        p.setStations(new ArrayList<>());

        pathRepository.save(p);
        // Test
        Path savedPath = pathRepository.findByPathName(p.getPathName());
        System.out.println("Saved path:" + savedPath.toString());

        saveVehicles(vehicles, p);
        saveStations(stations, p);

        // save path
        pathRepository.save(p);
        return p;
    }

    // Could be in vehicle service
    public void saveVehicles(ArrayList<Vehicle> vehicles, Path p) {
        for (Vehicle v : vehicles) {
            v.setPath(p);
            p.getVehicles().add(v);
            vehicleRepository.save(v);
        }
    }

    // Could be in station service
    public void saveStations(ArrayList<Station> stations, Path p) {
        for (Station s : stations) {
            s.getPaths().add(p);
            p.getStations().add(s);
            stationRepository.save(s);
        }
    }

    // DEPRECATED
    public void addPath(PathDTO pathDTO) {
        // needed for persisting station
        pathRepository.save(pathDTO.path);

        // Add stations from DB
        List<Station> stationsFromPath = new ArrayList<>();
        for (Long stationId : pathDTO.stationIds) {
            try {
                // get and update station paths
                Station station = stationService.getStationById(stationId);
                stationsFromPath.add(station);
                station.getPaths().add(pathDTO.path); // update path
                stationService.saveStation(station); // persist station

            } catch (StationDoesNotExist stationDoesNotExist) {
                stationDoesNotExist.printStackTrace();
            }
        }
        pathDTO.path.setStations(stationsFromPath); // set path stations
        pathRepository.save(pathDTO.path);          // persist path finally
    }

    public Path getPath(String lineNumber) {
        return pathRepository.findPathByPathName(lineNumber);
    }

    public List<Path> getAllPaths() {
        List<Path> paths = pathRepository.findAll();
        paths.forEach(path -> path.updateStationStrings());
        return paths;
    }

    public List<Path> updateVehicleLocations() {
        List<Path> allPaths = pathRepository.findAll();
        for (Path p : allPaths) {
            p.updateStationStrings().updateVehicleCoords();
            pathRepository.save(p);
        }
        System.out.println("UPDATED LOCATIONS!");
        return allPaths;
    }

    public void deleteById(Long id) throws StationDoesNotExist {
        Path pathToDelete = pathRepository.findPathById(id);
        // remove path ref from stations
        for (Station s : pathToDelete.getStations()) {
            Station pathStation = stationService.getStationById(s.getId());
            pathStation.getPaths().remove(pathToDelete);
            stationService.saveStation(pathStation);
        }
        // delete vehicles
        for (Vehicle v : pathToDelete.getVehicles()) {
            vehicleRepository.deleteById(v.getId());
        }
        // delete path finally
        pathRepository.deleteById(id);
    }

}
