package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class PriceListNotEditable extends MessageException {
    public PriceListNotEditable(){
        message = "Price list is not editable";
    }
}
