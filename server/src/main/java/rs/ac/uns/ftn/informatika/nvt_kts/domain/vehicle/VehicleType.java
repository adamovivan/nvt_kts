package rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle;

public enum VehicleType {
    BUS,
    TROLLEY,
    METROTRAIN
}
