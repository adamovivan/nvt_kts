package rs.ac.uns.ftn.informatika.nvt_kts.exception;

public class UserDoesNotExist extends MessageException {
    public UserDoesNotExist() {
        message = "User with that username does not exist.";
    }
}