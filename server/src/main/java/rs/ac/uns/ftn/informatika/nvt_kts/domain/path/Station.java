package rs.ac.uns.ftn.informatika.nvt_kts.domain.path;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.Coord;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = JSOGGenerator.class)
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "stations",
    cascade = {CascadeType.PERSIST})
    private List<Path> paths = new ArrayList<>();

    @Column(nullable = true)
    private Coord coord;

    private String pathsStr;

    // IMPORTANT
    public Station pathsToStr() {
        StringBuilder sb = new StringBuilder("[");
        for (Path p : paths) {
            sb.append(" ").append(p.getPathName());
        }
        pathsStr = sb.append(" ]").toString();
        return this;
    }

    public String getPathsStr() {
        return pathsStr;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Station{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", paths=" + paths +
                ", coord=" + coord +
                '}';
    }
}
