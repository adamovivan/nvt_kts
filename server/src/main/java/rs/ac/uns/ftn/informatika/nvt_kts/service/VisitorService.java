package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.User;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.UserAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VisitorRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.security.SecurityConfiguration;

@Service
public class VisitorService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    VisitorRepository visitorRepository;

    public boolean addVisitor(RegisterDTO registerDTO) throws UserAlreadyExists {
        User user = userRepository.findByUsername(registerDTO.getUsername());

        if(user != null) {
            throw new UserAlreadyExists();
        }

        SecurityConfiguration secCong = new SecurityConfiguration();
        PasswordEncoder encoder = secCong.passwordEncoder();
        String hashedPassword = encoder.encode(registerDTO.getPassword());

        Visitor newVisitor = new Visitor();
        newVisitor.setUsername(registerDTO.getUsername());
        newVisitor.setPassword(hashedPassword);
        newVisitor.setFirstName(registerDTO.getFirstName());
        newVisitor.setLastName(registerDTO.getLastName());
        newVisitor.setRole(Role.ROLE_VISITOR);
        newVisitor.setVisitorType(VisitorType.REGULAR);

        visitorRepository.save(newVisitor);

        return true;
    }

}
