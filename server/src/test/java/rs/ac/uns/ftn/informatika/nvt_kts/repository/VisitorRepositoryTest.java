package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class VisitorRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VisitorRepository visitorRepository;

    @Test
    @Transactional
    @Rollback
    public void findVisitorByUsername() {
        Visitor v = new Visitor();
        v.setFirstName("Visitor");
        v.setLastName("Visitorić");
        v.setVisitorType(VisitorType.REGULAR);
        v.setUsername("visitor123");
        v.setPassword("12345");
        v.setRole(Role.ROLE_VISITOR);

        entityManager.persist(v);
        entityManager.flush();

        Visitor visitor = visitorRepository.findVisitorByUsername(v.getUsername());

        assertEquals(v.getUsername(), visitor.getUsername());
    }

    @Test
    @Transactional
    @Rollback
    public void findVisitorByUsername_does_not_exist() {
        Visitor visitor = visitorRepository.findVisitorByUsername("maja");

        assertNull(visitor);
    }
}