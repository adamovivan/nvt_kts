package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class ControllerRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ControllerRepository controllerRepository;

    @Test
    @Transactional
    @Rollback
    public void findControllerByUsername() {
        Controller c = new Controller();
        c.setFirstName("Controler");
        c.setLastName("Controleric");
        c.setUsername("controler123");
        c.setPassword("12345");
        c.setRole(Role.ROLE_CONTROLLER);

        entityManager.persist(c);
        entityManager.flush();

        Controller controller = controllerRepository.findByUsername(c.getUsername());

        assertEquals(c.getUsername(), controller.getUsername());
    }

    @Test
    @Transactional
    @Rollback
    public void findControllerByUsername_does_not_exist() {
        Controller controller = controllerRepository.findByUsername("mija");

        assertNull(controller);
    }
}