package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.results.ResultMatchers;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Admin;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.LoginDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListItemDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.response.SimpleResponse;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListNotEditable;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.util.DTOFactory;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.LinkedList;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PriceListControllerTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PriceListRepository priceListRepository;


    private static String xAuthToken;
    private static boolean alreadyRun = false;

    @Autowired
    private TestUtils testUtils;

    @Autowired
    private MockMvc mockMvc;

    private static String URL_PREFIX = "/priceList";


    @PostConstruct
    public void setup() throws  Exception{
        if(!alreadyRun){
            Admin a = testUtils.createAdmin();
            userRepository.save(a);
            xAuthToken = testUtils.getAuthToken(a.getUsername(),a.getPassword(),Role.ROLE_ADMIN);
            alreadyRun = true;
        }


    }

    @Test
    @Transactional
    public void addPriceListTests() throws Exception{

        PriceListDTO priceListDTO = DTOFactory.createPriceListDTO();
        mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX)
        .header("Authorization",xAuthToken).contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListDTO)))
         .andExpect(MockMvcResultMatchers.status().isCreated());

        priceListDTO.setStartDate(DTOFactory.getDateTimeFormatter().format(LocalDate.now().plusDays(1)));

        mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX)
                .header("Authorization",xAuthToken).contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        priceListDTO.setStartDate(DTOFactory.getDateTimeFormatter().format(LocalDate.now().plusDays(1)));
        priceListDTO.setEndDate(DTOFactory.getDateTimeFormatter().format(LocalDate.now().minusDays(2)));

        mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX)
                .header("Authorization",xAuthToken).contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void showCurrentPriceList() throws Exception {

        String priceListStr = mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        PriceListDTO priceListDTO = objectMapper.readValue(priceListStr,PriceListDTO.class);

        Assert.assertNotNull(priceListDTO.getStartDate());
        Assert.assertNotNull(priceListDTO.getEndDate());
        Assert.assertNotNull(priceListDTO.getId());
    }

    @Test
    public void getAllPriceListsTest() throws Exception {
        String priceListsStr = mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX+"/all"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        LinkedList<PriceListDTO> priceListsDTO = objectMapper.readValue(priceListsStr,LinkedList.class);
        Assert.assertNotEquals(0,priceListsDTO.size());
    }

    @Test
    public void getSelectedPriceListItemsTest() throws Exception {
        String priceListsItemsStr = mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX+"/id/1/BUS"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        LinkedList<PriceListItemDTO> priceListsDTO = objectMapper.readValue(priceListsItemsStr,LinkedList.class);
        Assert.assertEquals(priceListsDTO.size(), CardType.values().length* VisitorType.values().length* Zone.values().length);

        priceListsItemsStr = mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX+"/id/0/BUS"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
        priceListsDTO = objectMapper.readValue(priceListsItemsStr,LinkedList.class);
        Assert.assertEquals(priceListsDTO.size(), 0);
    }

    @Test
    public void changePriceListItemPriceTest() throws Exception{

        ObjectMapper objectMapper = new ObjectMapper();
        PriceListItemDTO priceListItemDTO = DTOFactory.createPriceListItemDTO();

        priceListItemDTO.setPriceListId(new Long(0)); //nepostojeci priceList
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put(URL_PREFIX+"/editPriceListItem")
        .header("Authorization",xAuthToken)
        .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListItemDTO)))
        .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        SimpleResponse response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),SimpleResponse.class);
        Assert.assertEquals(new PriceListDoesNotExist().getMessage(),response.getMessage());


        String priceListStr = mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
        PriceListDTO priceListDTO = objectMapper.readValue(priceListStr,PriceListDTO.class);

        priceListItemDTO.setPriceListId(priceListDTO.getId()); //trenutni price list
        mvcResult = mockMvc.perform(MockMvcRequestBuilders.put(URL_PREFIX+"/editPriceListItem")
                .header("Authorization",xAuthToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListItemDTO)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(),SimpleResponse.class);
        Assert.assertEquals(new PriceListNotEditable().getMessage(),response.getMessage());

        //prvo kreiramo novi cenovnik
        priceListDTO = DTOFactory.createPriceListDTO();
        mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX)
                .header("Authorization",xAuthToken).contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListDTO)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
        Long newId = priceListRepository.findAll().get((int)priceListRepository.count()-1).getId();

        priceListItemDTO.setPriceListId(newId); //trenutni price list
        mockMvc.perform(MockMvcRequestBuilders.put(URL_PREFIX+"/editPriceListItem")
                .header("Authorization",xAuthToken)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListItemDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        //ciscenje
        priceListRepository.deleteById(newId);


    }


}
