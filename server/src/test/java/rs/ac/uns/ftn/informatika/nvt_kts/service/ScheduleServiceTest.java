package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.ScheduleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScheduleServiceTest {

    @Autowired
    private ScheduleService scheduleService;

    @MockBean
    private ScheduleRepository scheduleRepositoryMocked;

    @MockBean
    private PathRepository pathRepositoryMocked;

    @Before
    public void setUp() {
        Path path = new Path();
        path.setPathName("N1");
        path.setVehicleType(VehicleType.BUS);
        path.setZone(Zone.ONE);

        Path path2 = new Path();
        path2.setPathName("N2");
        path2.setVehicleType(VehicleType.BUS);
        path2.setZone(Zone.ONE);

        Mockito.when(pathRepositoryMocked.findPathByPathNameAndVehicleType(path.getPathName(), path.getVehicleType())).thenReturn(path);
        Mockito.when(pathRepositoryMocked.findPathByPathNameAndVehicleType(path2.getPathName(), path2.getVehicleType())).thenReturn(path2);

        Schedule schedule = new Schedule();
        schedule.setPath(path);
        schedule.setActive(true);

        Mockito.when(scheduleRepositoryMocked.findByPathAndDayAndVehicleType(schedule.getPath().getPathName(), Day.Workday, VehicleType.BUS)).thenReturn(schedule);
        Mockito.when(scheduleRepositoryMocked.findByPathAndDayAndVehicleType("N2", Day.Workday, VehicleType.BUS)).thenReturn(null);

        List<Schedule> schedules = new ArrayList<>();
        schedules.add(schedule);
        Mockito.when(scheduleRepositoryMocked.findAll()).thenReturn(schedules);

    }

    @Test(expected = PathDoesNotExist.class)
    public void addSchedule_pathDoesNotExist() throws PathDoesNotExist {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("NP2");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleService.addSchedule(scheduleDTO);
    }

    @Test
    public void addSchedule_schedule_does_not_exist() throws PathDoesNotExist {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N2");
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setVehicleType(VehicleType.BUS);

        boolean saved = scheduleService.addSchedule(scheduleDTO);

        assertTrue(saved);
    }

    @Test
    public void addSchedule() throws PathDoesNotExist {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N1");
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setVehicleType(VehicleType.BUS);

        boolean saved = scheduleService.addSchedule(scheduleDTO);

        assertTrue(saved);
    }

    @Test(expected = ScheduleDoesNotExist.class)
    public void deleteSchedule_ScheduleDoesNotExist() throws ScheduleDoesNotExist {
        scheduleService.deleteSchedule("HM", Day.Workday, VehicleType.BUS);
    }

    @Test
    public void deleteSchedule() throws ScheduleDoesNotExist {
        boolean deleted = scheduleService.deleteSchedule("N1", Day.Workday, VehicleType.BUS);

        assertTrue(deleted);
    }

    @Test
    public void getSchedules() {
        List<Schedule> sc = scheduleService.getSchedules();

        assertEquals(sc.size(), 1);
    }

}