package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Admin;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.User;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.CardDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.LoginDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.service.CardService;
import rs.ac.uns.ftn.informatika.nvt_kts.service.PriceListService;
import rs.ac.uns.ftn.informatika.nvt_kts.util.DTOFactory;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;
import static org.mockito.Mockito.when;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CardControllerTest {

    @Autowired
    private UserRepository userRepository;


    @MockBean
    private CardService cardService;

    private static String xAuthTokenAdmin;
    private static String xAuthTokenVisitor;

    @Autowired
    private TestUtils testUtils;

    @Autowired
    private MockMvc mockMvc;

    private static String URL_PREFIX = "/cards";

    @Before
    public void setup() throws Exception{

        Admin admin = testUtils.createAdmin();
        Visitor visitor = testUtils.createVisitor();
        saveUserIfDoesNotExist(admin);
        saveUserIfDoesNotExist(visitor);
        xAuthTokenVisitor = testUtils.getAuthToken(visitor.getUsername(), visitor.getPassword(), Role.ROLE_VISITOR);
        xAuthTokenAdmin = testUtils.getAuthToken(admin.getUsername(), admin.getPassword(), Role.ROLE_ADMIN);


        when(cardService.checkCard(1L)).thenReturn(1);
        when(cardService.checkCard(2L)).thenReturn(0);
        when(cardService.checkCardPrice(Mockito.any(Visitor.class),Mockito.any(CardDTO.class))).thenReturn("546");

    }

    private void saveUserIfDoesNotExist(User u){
        if(userRepository.findByUsername(u.getUsername())==null){
            userRepository.save(u);
        }
    }

    @Test
    @Transactional
    @Rollback
    public void buyCardTest() throws Exception{


        CardDTO cardDTO = DTOFactory.createCardDTO();
        //kreiramo cenovnik po kome ce kupiti karte
        PriceListDTO priceListDTO = DTOFactory.createPriceListDTO();
        mockMvc.perform(MockMvcRequestBuilders.post("/priceList")
                .header("Authorization",xAuthTokenAdmin)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(priceListDTO)))
                .andReturn();

        mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX+"/buy")
                .header("Authorization",xAuthTokenVisitor)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(cardDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    @Transactional
    @Rollback
    public void checkCardPriceTest() throws  Exception{


        CardDTO cardDTO = DTOFactory.createCardDTO();
        //probamo da kupimo kartu
       mockMvc.perform(MockMvcRequestBuilders.post(URL_PREFIX+"/checkPrice")
                .header("Authorization",xAuthTokenVisitor)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(testUtils.toJson(cardDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(mvcResult -> {
                    String contentAsStr = mvcResult.getResponse().getContentAsString().replace("\"","");
                    Long.parseLong(contentAsStr);
                });



    }

    @Test
    public void showCardsTest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get(URL_PREFIX+"/myCards")
                .header("Authorization",xAuthTokenVisitor))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @Transactional
    @Rollback
    public void checkCardTest() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.put(URL_PREFIX + "/check/1"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", xAuthTokenVisitor))
                .andExpect(MockMvcResultMatchers.status().isOk());

        mockMvc.perform((MockMvcRequestBuilders.put(URL_PREFIX + "/check/2"))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", xAuthTokenVisitor))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}