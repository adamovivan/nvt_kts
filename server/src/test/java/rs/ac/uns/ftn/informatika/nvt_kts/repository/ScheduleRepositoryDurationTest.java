package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import javax.transaction.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class ScheduleRepositoryDurationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ScheduleDurationRepository scheduleDurationRepository;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    LocalDate startDate = LocalDate.parse("Sat Oct 01 2022", dtf);
    LocalDate endDate = LocalDate.parse("Tue Nov 01 2022", dtf);

    Path path;
    Schedule schedule;
    ScheduleDuration scheduleDuration;


    @Before
    public void setUp() {
        path = new Path();
        path.setPathName("N1");
        path.setVehicleType(VehicleType.BUS);
        path.setZone(Zone.ONE);

        entityManager.persist(path);
        entityManager.flush();

        schedule = new Schedule();
        schedule.setDay(Day.Workday);
        schedule.setPath(path);
        schedule.setActive(true);

        entityManager.persist(schedule);
        entityManager.flush();

        scheduleDuration = new ScheduleDuration();
        scheduleDuration.setSchedule(schedule);
        scheduleDuration.setStartDate(startDate);
        scheduleDuration.setEndDate(endDate);
        scheduleDuration.setActive(true);

        entityManager.persist(scheduleDuration);
        entityManager.flush();
    }

    @Test
    @Transactional
    @Rollback
    public void findByDuration_exists() {

        ScheduleDuration found = scheduleDurationRepository.findByDuration("N1", VehicleType.BUS, Day.Workday, startDate, endDate);
        
        assertEquals(scheduleDuration, found);
    }

    @Test
    @Transactional
    @Rollback
    public void findByExactDuration_doesNotExist() {
        ScheduleDuration found = scheduleDurationRepository.findByDuration("NM1", VehicleType.BUS, Day.Workday, startDate, endDate);

        assertNull(found);
    }

    @Test
    @Transactional
    @Rollback
    public void findByExactDuration_exists() {
        ScheduleDuration found = scheduleDurationRepository.findByExactDuration("N1", VehicleType.BUS, Day.Workday, startDate, endDate);

        assertEquals(scheduleDuration, found);
    }

    @Test
    @Transactional
    @Rollback
    public void findByDuration_doesNotExist() {
        ScheduleDuration found = scheduleDurationRepository.findByExactDuration("NM1", VehicleType.BUS, Day.Workday, startDate, endDate);

        assertNull(found);
    }

    @Test
    @Transactional
    @Rollback
    public void findScheduleDurations_exists() {
        List<ScheduleDuration> found = scheduleDurationRepository.findScheduleDurations("N1", Day.Workday, VehicleType.BUS);

        assertEquals(1, found.size());
    }

    @Test
    @Transactional
    @Rollback
    public void findScheduleDurations_doesNotExist() {
        List<ScheduleDuration> found = scheduleDurationRepository.findScheduleDurations("NM1", Day.Workday, VehicleType.BUS);

        assertEquals(0, found.size());
    }

    @Test
    @Transactional
    @Rollback
    public void findAllScheduleDurations_exists() {
        List<ScheduleDuration> found = scheduleDurationRepository.findAllScheduleDurations("N1", Day.Workday);

        assertFalse(found.isEmpty());
    }

    @Test
    @Transactional
    @Rollback
    public void findAllScheduleDurations_doesNotExist() {
        List<ScheduleDuration> found = scheduleDurationRepository.findAllScheduleDurations("NM1", Day.Workday);

        assertTrue(found.isEmpty());
    }

    @Test
    @Transactional
    @Rollback
    public void findByScheduleId_exists() {
        List<ScheduleDuration> found = scheduleDurationRepository.findByScheduleId(schedule.getId());

        assertFalse(found.isEmpty());
    }

    @Test
    @Transactional
    @Rollback
    public void findByScheduleId_doesNotExist() {
        List<ScheduleDuration> found = scheduleDurationRepository.findByScheduleId(125L);

        assertTrue(found.isEmpty());
    }

}