package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.PriceList;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import javax.transaction.Transactional;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class PriceListRepositoryTest {

    @Autowired
    TestEntityManager testEntityManager;

    @Autowired
    TestUtils testUtils;

    @Autowired
    PriceListRepository priceListRepository;

    @Test
    @Transactional
    @Rollback
    public void getPriceListForDate(){

        PriceList priceList = new PriceList();
        LocalDate startDate = LocalDate.now().plusYears(2); // da  bi bili sigurni, jer ko zna cega ima ili nema u bazi  xD
        priceList.setStartDate(startDate);
        priceList.setEndDate(startDate.plusYears(1));
        testEntityManager.persist(priceList);
        PriceList priceList1 = priceListRepository.getPriceListForDate(startDate.plusDays(1));
        Assert.assertEquals(priceList.getId(),priceList1.getId());
    }
}
