package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Card;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;


import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class CardRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    TestUtils testUtils;

    @Autowired
    CardRepository cardRepository;

    @Test
    @Transactional
    public void getAllVisitorCardsTest(){
        int visitorCardNum = 5;
        Visitor v = testUtils.createVisitor();
        for(int i=0;i<visitorCardNum;i++){
            Card c = new Card();
            c.setActive(true);
            c.setVisitor(v);
            c.setStartDate(LocalDate.now());
            c.setEndDate(LocalDate.now());
            v.getCards().add(c);
        }
        testEntityManager.persist(v);
        testEntityManager.flush();
        Iterable<Card> found = cardRepository.findAllVisitorCards(Pageable.unpaged(),v.getId()).getContent();
        Assert.assertEquals(((List<Card>) found).size(),visitorCardNum);
    }

    @Test
    @Transactional
    public void checkInCard(){
        Card card = new Card();
        Visitor visitor = testUtils.createVisitor();
        card.setVisitor(visitor);
        card.setStartDate(LocalDate.of(2018,1,1));
        card.setEndDate(LocalDate.of(2019,1,1));
        card.setActive(true);

        testEntityManager.persist(visitor);
        Card dbCard = testEntityManager.persist(card);
        testEntityManager.flush();

        int numOfModified = cardRepository.checkCard(visitor.getUsername(), dbCard.getId());
        Assert.assertEquals(1, numOfModified);

        numOfModified = cardRepository.checkCard("badUsername", dbCard.getId());
        Assert.assertEquals(0, numOfModified);

        numOfModified = cardRepository.checkCard(visitor.getUsername(), -1L);
        Assert.assertEquals(0, numOfModified);

        numOfModified = cardRepository.checkCard("badUsername", -1L);
        Assert.assertEquals(0, numOfModified);
    }
}
