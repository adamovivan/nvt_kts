package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.service.PathService;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PathControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PathService pathServiceMock;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private static Path p1 = new Path();
    private static ObjectMapper mapper = new ObjectMapper();

    @BeforeClass
    public static void BeforeAll(){
    }

    @Before
    public void SetUp() throws Exception {
    }

    @Test
    public void addPathTest() throws Exception {
        mockMvc.perform(post("/lines")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE)
                .content(mapper.writeValueAsString(p1)))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPathNamesTest() throws Exception {
        mockMvc.perform(get("/lines/lineNumbers"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPathsTest() throws Exception {
        mockMvc.perform(get("/lines"))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void getPathTest() throws Exception {
        mockMvc.perform(get("/lines/1A"))
                .andExpect(status().isOk());
    }

    @Test
    public void deletePathTest() throws Exception {
        mockMvc.perform(delete("/lines/1"))
                .andExpect(status().isOk());
    }

}
