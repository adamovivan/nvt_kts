package rs.ac.uns.ftn.informatika.nvt_kts.util;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.CardDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListItemDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.VehicleDTO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;

public class DTOFactory {
    private static  DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("d.MM.yyyy.");

    public static PriceListDTO createPriceListDTO(){
        PriceListDTO priceListDTO = new PriceListDTO();
        LocalDate today = LocalDate.now();

        LinkedHashMap<CardType,Long> basePrices = new LinkedHashMap<>();

        basePrices.put(CardType.ONEDRIVE,new Long(10));
        basePrices.put(CardType.DAILY,new Long(45));
        basePrices.put(CardType.MONTHLY,new Long(100));
        basePrices.put(CardType.YEARLY,new Long(150));

        LinkedHashMap<VisitorType,Long> visitorModifier = new LinkedHashMap<>();
        visitorModifier.put(VisitorType.REGULAR,new Long(3));
        visitorModifier.put(VisitorType.STUDENT,new Long(2));
        visitorModifier.put(VisitorType.PENSIONER,new Long(1));

        LinkedHashMap<Zone,Long> zoneModifier = new LinkedHashMap<>();
        zoneModifier.put(Zone.ONE,new Long(1));
        zoneModifier.put(Zone.TWO,new Long(2));
        zoneModifier.put(Zone.THREE,new Long(3));

        LinkedHashMap<VehicleType,Long> vehicleModifier = new LinkedHashMap<>();
        vehicleModifier.put(VehicleType.BUS,new Long(1));
        vehicleModifier.put(VehicleType.TROLLEY,new Long(2));
        vehicleModifier.put(VehicleType.METROTRAIN,new Long(3));


        priceListDTO.setBasePrices(basePrices);
        priceListDTO.setVisitorModifier(visitorModifier);
        priceListDTO.setZoneModifier(zoneModifier);
        priceListDTO.setVehicleModifier(vehicleModifier);
        priceListDTO.setStartDate(dateTimeFormatter.format(today.plusYears(2))); //da se ne poklopi sa ovim za ovu
        priceListDTO.setEndDate(dateTimeFormatter.format(today.plusYears(3)));
        return priceListDTO;
    }

    public static CardDTO createCardDTO(){
        CardDTO cardDTO = new CardDTO();
        cardDTO.setCardType(CardType.DAILY);
        cardDTO.setZone(Zone.ONE);
        cardDTO.setVehicleType(VehicleType.BUS);
        return cardDTO;
    }

    public static VehicleDTO createVehicleDTO(){
        VehicleDTO retVal = new VehicleDTO();
        retVal.setVehicleType(VehicleType.BUS);
        retVal.setPathId(new Long(1));
        return retVal;
    }

    public static PriceListItemDTO createPriceListItemDTO(){
        PriceListItemDTO priceListItemDTO = new PriceListItemDTO();
        priceListItemDTO.setVehicleType(VehicleType.BUS);
        priceListItemDTO.setZone(Zone.ONE);
        priceListItemDTO.setCardType(CardType.DAILY);
        priceListItemDTO.setVisitorType(VisitorType.REGULAR);
        priceListItemDTO.setPrice(new Long(45));
        return priceListItemDTO;
    }

    public static DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }
}
