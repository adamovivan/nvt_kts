package rs.ac.uns.ftn.informatika.nvt_kts.domain;

import org.junit.Test;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.InvalidVehicleType;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class PathTest {

    @Test
    public void updateStationStrings(){
        Path p = new Path(); p.setPathName("Test");

        Station a = new Station(); a.setName("A");
        a.getPaths().add(p);
        Station b = new Station(); b.setName("B");
        b.getPaths().add(p);

        p.getStations().add(a); p.getStations().add(b);

        // if true -> method executed without exceptions
        assertThat(p.updateStationStrings()).isInstanceOf(Path.class);

        // check stations strings
        for (Station s : p.getStations()) {
            assertThat(s.getPathsStr())
                    .startsWith("[")
                    .containsIgnoringCase("Test")
                    .endsWith("]");
        }
    }

    @Test
    public void updateCoordinatesInit(){
        Path p = new Path();
        p.updateVehicleCoords();

        assertThat(p.getCoordIndex()).isEqualTo(0);
        assertThat(p.getVehicles().size()).isEqualTo(0);
    }

    @Test
    public void pathVehicleCoordinatesTest(){
        Path p = new Path();
        Coord c1 = new Coord(0,0);
        Coord c2 = new Coord(1,1);
        Coord c3 = new Coord(2,2);

        p.getCoords().add(c1); p.getCoords().add(c2); p.getCoords().add(c3);

        Vehicle v1 = new Vehicle();
        Vehicle v2 = new Vehicle();

        p.getVehicles().add(v1); p.getVehicles().add(v2);

        p.updateVehicleCoords();
        // vehicle coordinate update check
        assertThat(p.getVehicles().get(0).getCoord()).isEqualTo(new Coord(0,0));
        assertThat(p.getVehicles().get(1).getCoord()).isEqualTo(new Coord(1,1));

        p.updateVehicleCoords();
        // vehicle coordinate update check
        assertThat(p.getVehicles().get(0).getCoord()).isEqualTo(new Coord(2,2));
        assertThat(p.getVehicles().get(1).getCoord()).isEqualTo(new Coord(0,0)); // circle complete

    }

    @Test
    public void pathVehicleTypeTest(){
        Path p = new Path();
        assertThat(p.getVehicleType()).isNull();

        Vehicle v1 = new Vehicle();
        v1.setVehicleType(VehicleType.BUS);
        Vehicle v2 = new Vehicle();
        v1.setVehicleType(VehicleType.BUS);

        try {
            p.addVehicle(v1);
        } catch (InvalidVehicleType invalidVehicleType) {
            fail("This should not throw exception!");
            invalidVehicleType.printStackTrace();
        }

        assertThat(p.getVehicleType()).isEqualTo(VehicleType.BUS);
        assertThat(p.getVehicles()).hasSize(1);

        try {
            p.addVehicle(v2);
        } catch (InvalidVehicleType invalidVehicleType) {
            fail("This should not throw exception!");
            invalidVehicleType.printStackTrace();
        }

        assertThat(p.getVehicleType()).isEqualTo(VehicleType.BUS);
        assertThat(p.getVehicles()).hasSize(2);
    }

    @Test
    public void pathInvalidVehicleTypeTest(){
        Path p = new Path();

        Vehicle bus = new Vehicle();
        bus.setVehicleType(VehicleType.BUS);

        Vehicle metro = new Vehicle();
        metro.setVehicleType(VehicleType.METROTRAIN);

        try {
            p.addVehicle(bus);
        } catch (InvalidVehicleType invalidVehicleType) {
            fail("This should not throw exception!");
        }

        try {
            p.addVehicle(metro);
            fail("This should throw InvalidVehicleType exception!");
        } catch (InvalidVehicleType invalidVehicleType) {
            assertThat(invalidVehicleType.getMessage()).containsIgnoringCase(p.getVehicleType().toString());
        }
    }

}
