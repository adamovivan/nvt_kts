package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.ScheduleDuration;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.ScheduleDurationAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.ScheduleDurationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleDurationRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ScheduleRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScheduleDurationServiceTest {

    @Autowired
    private ScheduleDurationService scheduleDurationService;

    @MockBean
    ScheduleRepository scheduleRepositoryMocked;

    @MockBean
    ScheduleDurationRepository scheduleDurationRepositoryMocked;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    LocalDate startDateAfterEndDate = LocalDate.parse("Thu Dec 01 2022", dtf);

    LocalDate startDate = LocalDate.parse("Sat Oct 01 2022", dtf);
    LocalDate endDate = LocalDate.parse("Tue Nov 01 2022", dtf);


    @Before
    public void setUp() throws Exception {
        Path path = new Path();
        path.setPathName("N1");
        path.setVehicleType(VehicleType.BUS);
        path.setZone(Zone.ONE);

        Schedule schedule = new Schedule();
        schedule.setPath(path);
        schedule.setDay(Day.Workday);
        schedule.setActive(true);

        ScheduleDuration scheduleDurationWrongDate = new ScheduleDuration();
        scheduleDurationWrongDate.setSchedule(schedule);
        scheduleDurationWrongDate.setStartDate(startDateAfterEndDate);
        scheduleDurationWrongDate.setEndDate(endDate);
        scheduleDurationWrongDate.setActive(true);

        ScheduleDuration scheduleDuration = new ScheduleDuration();
        scheduleDuration.setSchedule(schedule);
        scheduleDuration.setStartDate(startDate);
        scheduleDuration.setEndDate(endDate);
        scheduleDuration.setActive(true);

        List<ScheduleDuration> list = new ArrayList<>();
        List<ScheduleDuration> list2 = new ArrayList<>();
        list2.add(scheduleDuration);

        Mockito.when(scheduleRepositoryMocked.findByPathAndDayAndVehicleType(schedule.getPath().getPathName(), Day.Workday, VehicleType.BUS)).thenReturn(schedule);
        Mockito.when(scheduleDurationRepositoryMocked.findByDuration(schedule.getPath().getPathName(), VehicleType.BUS, Day.Workday, startDate, endDate)).thenReturn(scheduleDurationWrongDate);
        Mockito.when(scheduleDurationRepositoryMocked.findScheduleDurations("123", Day.Workday, VehicleType.BUS)).thenReturn((list));
        Mockito.when(scheduleDurationRepositoryMocked.findAll()).thenReturn(list2);
        Mockito.when(scheduleDurationRepositoryMocked.findAllScheduleDurations("123", Day.Workday)).thenReturn((list2));
        Mockito.when(scheduleDurationRepositoryMocked.findByExactDuration(schedule.getPath().getPathName(), VehicleType.BUS, Day.Workday, startDate, endDate)).thenReturn(scheduleDuration);
    }

    @Test(expected = ScheduleDurationAlreadyExists.class)
    public void addScheduleDuration_ScheduleDurationAlreadyExists() throws ScheduleDurationAlreadyExists {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N1");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setStartDate("Sat Oct 01 2022");
        scheduleDTO.setEndDate("Tue Nov 01 2022");

        scheduleDurationService.addScheduleDuration(scheduleDTO);
    }

    @Test
    public void addScheduleDuration() throws ScheduleDurationAlreadyExists {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N11");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setStartDate("Sat Oct 01 2022");
        scheduleDTO.setEndDate("Tue Nov 01 2022");

        boolean added = scheduleDurationService.addScheduleDuration(scheduleDTO);
        assertTrue(added);
    }

    @Test
    public void getScheduleDurations() {
        List<ScheduleDuration> found = scheduleDurationService.getScheduleDurations("123", Day.Workday, VehicleType.BUS);

        assertEquals(0, found.size());
    }

    @Test
    public void getAllScheduleDurations() {
        List<ScheduleDuration> found = scheduleDurationService.getAllScheduleDurations("123", Day.Workday);

        assertEquals(1, found.size());
    }
    @Test
    public void getScheduleDurations_without_params() {
        List<ScheduleDTO> found = scheduleDurationService.getScheduleDurations();

        assertEquals(1, found.size());
    }

    @Test(expected = ScheduleDurationDoesNotExist.class)
    public void updateScheduleDuration_ScheduleDurationDoesNotExist() throws ScheduleDurationDoesNotExist {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N1");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setStartDate("Sat Jan 01 2022");
        scheduleDTO.setEndDate("Tue Feb 01 2022");

        scheduleDurationService.updateScheduleDuration(scheduleDTO);
    }

    @Test
    public void updateScheduleDuration() throws ScheduleDurationDoesNotExist {
        boolean updated = scheduleDurationService.updateScheduleDuration(getScheduleData());
        assertTrue(updated);
    }

    @Test(expected = ScheduleDurationDoesNotExist.class)
    public void deleteScheduleDuration_ScheduleDurationDoesNotExist() throws ScheduleDurationDoesNotExist {
        ScheduleDTO scheduleDTO = getScheduleData();
        scheduleDTO.setStartDate("Sat Jan 01 2022");
        scheduleDTO.setEndDate("Tue Feb 01 2022");

        scheduleDurationService.deleteScheduleDuration(scheduleDTO.getPathName(), scheduleDTO.getDay(), scheduleDTO.getVehicleType(), scheduleDTO.getStartDate(), scheduleDTO.getEndDate());
    }

    @Test
    public void deleteScheduleDuration() throws ScheduleDurationDoesNotExist {
        ScheduleDTO scheduleDTO = getScheduleData();
        boolean deleted = scheduleDurationService.deleteScheduleDuration(scheduleDTO.getPathName(), scheduleDTO.getDay(), scheduleDTO.getVehicleType(), scheduleDTO.getStartDate(), scheduleDTO.getEndDate());
        assertTrue(deleted);
    }

    private ScheduleDTO getScheduleData() {
        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("N1");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setStartDate("Sat Oct 01 2022");
        scheduleDTO.setEndDate("Tue Nov 01 2022");

        return scheduleDTO;
    }
}