package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathUpdateDto;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VehicleRepository;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PathServiceTest {

    @MockBean
    PathRepository pathRepositoryMock;

    @MockBean
    StationService stationServiceMock;

    @MockBean
    VehicleService vehicleServiceMock;

    @MockBean
    VehicleRepository vehicleRepositoryMock;

    @Autowired
    PathService pathService;

    public static VehicleType vt1 = VehicleType.BUS;
    public static Path p1 = new Path();
    public static PathUpdateDto pudto1 = new PathUpdateDto(null, 0);
    public static List<String> list1 = new ArrayList<String>(){{
        add("A");
        add("B");
        add("C");
    }};
    public static List<Path> list2 = new ArrayList<Path>(){{
        add(new Path());
        add(new Path());
        add(new Path());
    }};

    @Before
    public void beforeEachTest(){
        p1.setPathName("test1");
        pudto1.setPath(p1);
        pudto1.setVehicleNumber(5);

        Mockito.when(pathRepositoryMock.findAllPathNames(vt1)).thenReturn(list1);
        Mockito.when(pathRepositoryMock.findByPathName(p1.getPathName())).thenReturn(p1);
        Mockito.when(pathRepositoryMock.findPathByPathName(p1.getPathName())).thenReturn(p1);
        Mockito.when(pathRepositoryMock.findAll()).thenReturn(list2);
    }

    @Test
    public void getAllPathNumbersTest() {
        List<String> res = pathService.getAllPathNumbers(vt1);
        Assert.assertTrue(res.size() == 3);
        Assert.assertTrue(res.get(0).equals("A"));
        Assert.assertTrue(res.get(1).equals("B"));
        Assert.assertTrue(res.get(2).equals("C"));
    }

    @Test
    public void updatePathTest() {
        PathUpdateDto res = pathService.updatePath(pudto1);
        Assert.assertTrue(res.getVehicleNumber() == pudto1.getVehicleNumber());
    }

    @Test
    public void saveTest() {
        Path res = pathService.save(p1);
        Assert.assertTrue(p1.getPathName().equals(res.getPathName()));
    }

    @Test
    public void getPathTest() {
        Path res = pathService.getPath(p1.getPathName());
        Assert.assertTrue(p1.getPathName().equals(res.getPathName()));
    }

    @Test
    public void getAllPathsTest() {
        List<Path> res = pathService.getAllPaths();
        Assert.assertTrue(res.size() == list2.size());
    }

}
