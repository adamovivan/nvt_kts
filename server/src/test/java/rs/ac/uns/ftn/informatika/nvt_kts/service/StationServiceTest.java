package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.Coord;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.StationRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StationServiceTest {

    @MockBean
    PathRepository pathRepositoryMock;

    @MockBean
    StationRepository stationRepositoryMock;

    @Autowired
    StationService stationService;

    private static Station s1 = new Station();
    private static Path p1 = new Path();
    public static List<Station> list1 = new ArrayList<Station>() {{
        add(new Station());
        add(new Station());
        add(new Station());
    }};

    @Before
    public void beforeEachTest() {
        s1.setName("test1");
        s1.setId(1L);
        s1.setPaths(new ArrayList<>());
        s1.setCoord(new Coord(10, 20));

        p1.setStations(new ArrayList<>());
        p1.setVehicles(new ArrayList<>());

        // Mocks
        Mockito.when(stationRepositoryMock.findById(s1.getId())).thenReturn(Optional.of(s1));
        Mockito.when(stationRepositoryMock.findByName("okName")).thenReturn(s1);
        Mockito.when(pathRepositoryMock.findPathByPathName("1A")).thenReturn(p1);
        Mockito.when(stationRepositoryMock.findAll()).thenReturn(list1);
    }

    @Test
    public void saveStationTest() {
        Station res = stationService.saveStation(s1);
        Assert.assertTrue(res.getId() == s1.getId());
        Assert.assertTrue(res.getName() == s1.getName());
    }

    @Test
    public void deleteStationOkTest() throws Exception {
        Optional<Station> res = stationService.deleteStation(s1.getId());
        Assert.assertTrue(res.get().getId() == s1.getId());
        Assert.assertTrue(res.get().getName() == s1.getName());
    }

    @Test(expected = StationDoesNotExist.class)
    public void deleteNoStationExceptionTest() throws Exception {
        Optional<Station> res = stationService.deleteStation(-1L);
    }

    @Test
    public void addStationToPathOkTest() throws Exception {
        stationService.addStationToPath("okName", "1A");
    }

    @Test(expected = StationDoesNotExist.class)
    public void addStationNoStationExceptionTest() throws Exception {
        stationService.addStationToPath("", "1A");
    }

    @Test(expected = PathDoesNotExist.class)
    public void addStationNoPathExceptionTest() throws Exception {
        stationService.addStationToPath("okName", "-1");
    }

    @Test
    public void removeStationFromPathOkTest() throws Exception {
        stationService.addStationToPath("okName", "1A");
    }

    @Test(expected = StationDoesNotExist.class)
    public void removeStationFromPathNoStationExceptionTest() throws Exception {
        stationService.addStationToPath("", "1A");
    }

    @Test(expected = PathDoesNotExist.class)
    public void removeStationFromPathNoPathExceptionTest() throws Exception {
        stationService.addStationToPath("okName", "-1");
    }

    @Test
    public void getAllStationsTest() {
        List<Station> res = stationService.getAllStations();
        Assert.assertTrue(res.size() == list1.size());
    }

    @Test
    public void getStationByIdOkTest() throws Exception {
        Station res = stationService.getStationById(s1.getId());
        Assert.assertSame(res, s1);
    }

    @Test(expected = StationDoesNotExist.class)
    public void getStationByIdNoStationExceptionTest() throws Exception {
        stationService.getStationById(-1L);
    }
}
