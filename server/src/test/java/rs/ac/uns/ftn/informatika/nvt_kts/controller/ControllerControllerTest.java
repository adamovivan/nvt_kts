package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControllerControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestUtils testUtils;

    private String controller_token;

    @Before
    public void setUp() {
        controller_token = testUtils.getAuthToken("maja123", "123", Role.ROLE_CONTROLLER);
    }

    @Test
    public void controllerCheck() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", controller_token);

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List> responseEntity = restTemplate.exchange("/controllerCheck?zone=ONE&vehicleType=BUS&time=Sun Oct 02 2022", HttpMethod.GET,
                httpEntity, List.class);

        assertFalse(responseEntity.getBody().isEmpty());
    }

    @Test
    public void controllersLines() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", controller_token);

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List> responseEntity = restTemplate.exchange("/controllersLines", HttpMethod.GET,
                httpEntity, List.class);

        assertFalse(responseEntity.getBody().isEmpty());
    }
}