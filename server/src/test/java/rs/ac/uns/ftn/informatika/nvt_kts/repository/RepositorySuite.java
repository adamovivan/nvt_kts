package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CardRepositoryTest.class,
        ControllerRepositoryTest.class,
        PriceListItemRepositoryTest.class,
        PriceListRepositoryTest.class,
        ScheduleRepositoryDurationTest.class,
        ScheduleRepositoryTest.class,
        VisitorRepositoryTest.class
})
public class RepositorySuite {
}
