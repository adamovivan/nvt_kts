package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.StationDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.StationService;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static rs.ac.uns.ftn.informatika.nvt_kts.constants.StationConstants.DB_COUNT;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @PostConstruct
    public void setup() {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(webApplicationContext).build();
    }

    @MockBean
    private StationService stationServiceMock;

    private static ObjectMapper mapper = new ObjectMapper();

    private static Station s1 = new Station();
    private static Station s2 = new Station();

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @BeforeClass
    public static void BeforeAll() {
        s2.setId(5L);
        s2.setName("test");
    }

    @Before
    public void SetUp() throws Exception {
    }

    @Test
    public void getAllStationsTest() throws Exception {
        mockMvc.perform(get("/stations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void getStationByIdOkTest() throws Exception {
        mockMvc.perform(get("/stations/0"))
                .andExpect(status().isOk());
    }

    @Test
    public void addStationTest() throws Exception {
        mockMvc.perform(post("/stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(s1)))
                .andExpect(status().isOk());
    }

    @Test
    public void editStationTest() throws Exception{
        mockMvc.perform(post("/stations/edit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(s1)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteStationTest() throws Exception{
        mockMvc.perform(delete("/stations/5")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
