package rs.ac.uns.ftn.informatika.nvt_kts.service;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CardServiceTest.class,
        ControllerServiceTest.class,
        PathServiceTest.class,
        PriceListServiceTest.class,
        ScheduleDurationServiceTest.class,
        ScheduleServiceTest.class,
        StationServiceTest.class,
        UserServiceTest.class,
        VehicleServiceTest.class,
        VisitorServiceTest.class
})
public class ServiceSuite {
}
