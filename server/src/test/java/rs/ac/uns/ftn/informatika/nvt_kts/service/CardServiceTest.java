package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.assertEquals;

import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Card;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PageDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.util.DTOFactory;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CardServiceTest {

    @Autowired
    private CardService cardService;

    @Autowired
    UserService userService;

    @PostConstruct
    public void setup() {
        logUsername("mika123");
    }

    //method that logs the user
    private void logUsername(String username){
        Authentication authentication = Mockito.mock(Authentication.class);
        Mockito.when(authentication.getName()).thenReturn(username);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void testCheckCard(){
        assertEquals(1, cardService.checkCard(55L));
        assertEquals(0, cardService.checkCard(1L));
    }

    @Test
    public void testFilterCardsNone() {

        int f = cardService.filterCards(Pageable.unpaged(), null, null, null,
                null, null, null, null, null, null,
                null).getContent().size();

        assertEquals(100, f);
    }

    @Test
    public void testFilterCardsUsername() {

        long f = cardService.filterCards(PageRequest.of(0, 10), "mika123", null, null,
                null, null, null, null, null, null,
                null).getTotalElements();

        assertEquals(16, f);
    }

    @Test
    public void testFilterCardsVisitorType() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, VisitorType.REGULAR, null,
                null, null, null, null, null, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, VisitorType.STUDENT, null,
                null, null, null, null, null, null,
                null).getTotalElements();

        long f3 = cardService.filterCards(PageRequest.of(0, 10), null, VisitorType.PENSIONER, null,
                null, null, null, null, null, null,
                null).getTotalElements();

        assertEquals(66, f1);
        assertEquals(16, f2);
        assertEquals(18, f3);

        assertEquals(100, f1 + f2 + f3);
    }

    @Test
    public void testFilterCardsCardType() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, CardType.DAILY,
                null, null, null, null, null, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, CardType.MONTHLY,
                null, null, null, null, null, null,
                null).getTotalElements();

        long f3 = cardService.filterCards(PageRequest.of(0, 10), null, null, CardType.YEARLY,
                null, null, null, null, null, null,
                null).getTotalElements();

        long f4 = cardService.filterCards(PageRequest.of(0, 10), null, null, CardType.ONEDRIVE,
                null, null, null, null, null, null,
                null).getTotalElements();

        assertEquals(34, f1);
        assertEquals(26, f2);
        assertEquals(24, f3);
        assertEquals(16, f4);

        assertEquals(100, f1 + f2 + f3 + f4);
    }

    @Test
    public void testFilterCardsZone() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                Zone.ONE, null, null, null, null, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                Zone.TWO, null, null, null, null, null,
                null).getTotalElements();

        long f3 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                Zone.THREE, null, null, null, null, null,
                null).getTotalElements();

        assertEquals(31, f1);
        assertEquals(29, f2);
        assertEquals(40, f3);

        assertEquals(100, f1 + f2 + f3);
    }

    @Test
    public void testFilterCardsVehicleType() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, VehicleType.BUS, null, null, null, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, VehicleType.TROLLEY, null, null, null, null,
                null).getTotalElements();

        long f3 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, VehicleType.METROTRAIN, null, null, null, null,
                null).getTotalElements();

        assertEquals(32, f1);
        assertEquals(35, f2);
        assertEquals(33, f3);

        assertEquals(100, f1 + f2 + f3);
    }

    @Test
    public void testFilterCardsPrice() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, 500L, null, null, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, null, 2500L, null, null,
                null).getTotalElements();

        long f3 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, 500L, 2500L, null, null,
                null).getTotalElements();

        assertEquals(50, f1);
        assertEquals(76, f2);
        assertEquals(26, f3);

    }

    @Test
    public void testFilterCardsActive() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, null, null, true, null,
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, null, null, false, null,
                null).getTotalElements();

        assertEquals(43, f1);
        assertEquals(57, f2);
    }

    @Test
    public void testFilterCardsDate() {

        long f1 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, null, null, null, LocalDate.of(2018,1,1),
                null).getTotalElements();

        long f2 = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, null, null, null, null, null,
                LocalDate.of(2018,12,31)).getTotalElements();

        assertEquals(66, f1);
        assertEquals(60, f2);
    }

    @Test
    public void testFilterCardsVisitorTypeCardTypeZone() {

        long f = cardService.filterCards(PageRequest.of(0, 10), null, VisitorType.STUDENT, CardType.DAILY,
                Zone.ONE, null, null, null, null, null,
                null).getTotalElements();

        assertEquals(7, f);
    }

    @Test
    public void testFilterCardsVehicleTypeEndDate() {

        long f = cardService.filterCards(PageRequest.of(0, 10), null, null, null,
                null, VehicleType.METROTRAIN, null, null, null, null,
                LocalDate.of(2018,7,31)).getTotalElements();

        assertEquals(15, f);
    }

    @Test
    public void testFilterCardsVisitorTypePriceTo() {

        long f = cardService.filterCards(PageRequest.of(0, 10), null, VisitorType.PENSIONER, null,
                null, null, null, 5000L, null, null,
                null).getTotalElements();

        assertEquals(13, f);
    }

    @Test
    @Transactional
    public void buyCardTest() throws  Exception{
        Visitor v = (Visitor) userService.getLoggedUser();
        int numCards = v.getCards().size();
        cardService.buyCard(v, DTOFactory.createCardDTO());
        assertEquals(v.getCards().size(),numCards+1);
    }

    @Test
    @Transactional
    public void checkCardPriceTest() throws  Exception{

        Visitor v = (Visitor) userService.getLoggedUser();
        int numCards = v.getCards().size();
        String strAnswer =  cardService.checkCardPrice(v, DTOFactory.createCardDTO());
        assertEquals(v.getCards().size(),numCards);

        try{
            Long.parseLong(strAnswer);
        }
        catch(NumberFormatException e){
            Assert.fail();
        }

    }

    @Test
    @Transactional
    public void getAllVisitorCardsTest(){
        Visitor v = (Visitor) userService.getLoggedUser();
        PageDTO pageDTO = cardService.getMyCards(Pageable.unpaged(),v);
        int numCards = v.getCards().size();
        Assert.assertEquals(pageDTO.getTotalElements(),numCards);
        Assert.assertEquals(pageDTO.getContent().size(),numCards);
    }

    @Test
    @Transactional
    public void getSomeVisitorCardsTest(){

        Visitor v = (Visitor) userService.getLoggedUser();
        PageRequest p = new PageRequest(0, 2);
        PageDTO pageDTO = cardService.getMyCards(p,v);
        Assert.assertTrue(pageDTO.getContent().size()<=p.getPageSize());
    }

}
