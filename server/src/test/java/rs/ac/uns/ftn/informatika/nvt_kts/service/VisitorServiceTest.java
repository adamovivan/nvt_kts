package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.UserAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VisitorRepository;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VisitorServiceTest {

    @Autowired
    private VisitorService visitorService;

    @MockBean
    private VisitorRepository visitorRepositoryMocked;

    @Before
    public void setUp() {
        Visitor v = getVisitor();

        Mockito.when(visitorRepositoryMocked.findVisitorByUsername("mirko123")).thenReturn(v);
    }

    @Test(expected = UserAlreadyExists.class)
    public void addVisitor_exists() throws UserAlreadyExists {
        RegisterDTO rDTO = new RegisterDTO();
        rDTO.setUsername("pera123");

        visitorService.addVisitor(rDTO);
    }

    @Test
    public void addVisitor() throws UserAlreadyExists {
        RegisterDTO rDTO = new RegisterDTO();
        rDTO.setUsername("mirko1233");
        rDTO.setFirstName("Mirko");
        rDTO.setLastName("Mirkovic");
        rDTO.setUsername("mirko1233");
        rDTO.setPassword("12345");

        boolean added = visitorService.addVisitor(rDTO);
        assertTrue(added);
    }

    private Visitor getVisitor() {
        Visitor v = new Visitor();
        v.setFirstName("Visitor");
        v.setLastName("Visitorić");
        v.setVisitorType(VisitorType.REGULAR);
        v.setUsername("visitor123");
        v.setPassword("12345");
        v.setRole(Role.ROLE_VISITOR);
        return v;
    }

}