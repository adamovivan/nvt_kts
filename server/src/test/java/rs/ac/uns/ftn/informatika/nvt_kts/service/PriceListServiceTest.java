package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PriceListItemDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.EndDateBeforeStartDate;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListInRangeAlreadyExists;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PriceListNotEditable;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListItemRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PriceListRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.util.DTOFactory;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceListServiceTest {

    @MockBean
    PriceListRepository priceListRepositoryMocked;

    @MockBean
    PriceListItemRepository priceListItemRepositoryMocked;

    @MockBean
    ItemRepository itemRepositoryMocked;

    @Autowired
    PriceListService priceListService;


    @Before
    public void setup() {

        PriceList priceList1 = new PriceList();
        priceList1.setId(new Long(1));
        priceList1.setStartDate(LocalDate.now());
        priceList1.setEndDate(LocalDate.now().plusYears(1));
        priceList1.setPriceListItems(new LinkedList<>());

        PriceList priceList2 = new PriceList();
        priceList2.setId(new Long(2));
        priceList2.setStartDate(LocalDate.now().plusYears(2));
        priceList2.setEndDate(LocalDate.now().plusYears(3));
        priceList2.setPriceListItems(new LinkedList<>());

        LinkedList<PriceList> allPriceLists = new LinkedList<>();
        allPriceLists.add(priceList1);
        allPriceLists.add(priceList2);
        Mockito.when(priceListRepositoryMocked.findAll()).thenReturn(allPriceLists);
        Mockito.when(priceListRepositoryMocked.findById(new Long(1)))
                .thenReturn(Optional.of(priceList1));

        Mockito.when(priceListRepositoryMocked.findById(new Long(2)))
                .thenReturn(Optional.of(priceList2));

        PriceListItem priceListItem1 = null;
        PriceListItem priceListItem2 = null;

        for(CardType cardType:CardType.values()){
            for(VisitorType visitorType:VisitorType.values()){
                for(Zone zone:Zone.values()){
                    for(VehicleType vehicleType:VehicleType.values()){
                        Item i = new Item();
                        i.setCardType(cardType);
                        i.setVisitorType(visitorType);
                        i.setVehicleType(vehicleType);
                        i.setZone(zone);

                        priceListItem1 = new PriceListItem();
                        priceListItem2 = new PriceListItem();

                        priceListItem1.setPrice(new Long(45));
                        priceListItem1.setItem(i);
                        priceListItem1.setPriceList(priceList1);
                        priceList1.getPriceListItems().add(priceListItem1);

                        priceListItem2.setPrice(new Long(45));
                        priceListItem2.setItem(i);
                        priceListItem2.setPriceList(priceList2);
                        priceList2.getPriceListItems().add(priceListItem2);
                        Mockito.when(
                                itemRepositoryMocked.findOneByCardTypeAndVisitorTypeAndZoneAndVehicleType
                                (cardType,visitorType,zone,vehicleType)).thenReturn(i);
                        Mockito.when(priceListItemRepositoryMocked
                        .findByPriceListAndItemFields(priceList1,i.getZone(),i.getVehicleType(),i.getCardType(),i.getVisitorType()))
                         .thenReturn(priceListItem1);
                        Mockito.when(priceListItemRepositoryMocked
                                .findByPriceListAndItemFields(priceList2,i.getZone(),i.getVehicleType(),i.getCardType(),i.getVisitorType()))
                                .thenReturn(priceListItem2);
                    }

                }
            }

        }

    }

    @Test
    public void addNewPriceListTestOk() throws Exception{
        PriceListDTO priceListDTO = DTOFactory.createPriceListDTO();
        priceListService.addNewPriceList(priceListDTO);
    }

    @Test(expected = EndDateBeforeStartDate.class)
    public void addNewPriceListEndDateBeforeStartDate() throws Exception{
        PriceListDTO priceListDTO = DTOFactory.createPriceListDTO();
        priceListDTO.setStartDate(DTOFactory.getDateTimeFormatter().format(LocalDate.now().plusDays(1)));
        priceListDTO.setEndDate(DTOFactory.getDateTimeFormatter().format(LocalDate.now().minusDays(2)));
        priceListService.addNewPriceList(priceListDTO);
    }

    @Test(expected = PriceListInRangeAlreadyExists.class)
    public void addNewPriceListAlreadyInRange() throws  Exception{

        PriceListDTO priceListDTO = DTOFactory.createPriceListDTO();
        LocalDate startDate = LocalDate.parse(priceListDTO.getStartDate(),DTOFactory.getDateTimeFormatter());
        LocalDate endDate = LocalDate.parse(priceListDTO.getEndDate(),DTOFactory.getDateTimeFormatter());

        Mockito.when(priceListRepositoryMocked.getPriceListForDate(startDate)).thenReturn(new PriceList());
        Mockito.when(priceListRepositoryMocked.getPriceListForDate(endDate)).thenReturn(new PriceList());
        priceListService.addNewPriceList(priceListDTO);
    }

    @Test
    public void getAllPriceListsTest(){
        LinkedList<PriceListDTO> allPriceLists = priceListService.getAllPriceLists();
        Assert.assertTrue(allPriceLists.size()>0);
        for(PriceListDTO pDTO:allPriceLists){
            Assert.assertNotNull(pDTO.getStartDate());
            Assert.assertNotNull(pDTO.getEndDate());
            Assert.assertNotNull(pDTO.getId());
        }
    }

    @Test
    public void getSelectedPriceListItemsOk() throws Exception{
        LinkedList<PriceListItemDTO> prItems = priceListService.getSelectedPriceListItems(new Long(1),VehicleType.BUS);
        for(PriceListItemDTO pr:prItems){
            Assert.assertNotNull(pr.getPrice());
            Assert.assertTrue(pr.getVehicleType()==VehicleType.BUS);
        }
    }

    @Test(expected = PriceListDoesNotExist.class)
    public void getSelectedPriceListItemsPriceListDoesNotExist() throws Exception{
        priceListService.getSelectedPriceListItems(new Long(0),VehicleType.BUS);
    }

    @Test(expected = PriceListDoesNotExist.class)
    public void changePriceListItemPriceNotExistTest() throws Exception{
        PriceListItemDTO priceListItemDTO = DTOFactory.createPriceListItemDTO();
        priceListItemDTO.setPriceListId(new Long(0));
        priceListService.changePriceListItemPrice(priceListItemDTO);
    }

    @Test(expected = PriceListNotEditable.class)
    public void changePriceListItemPriceNotEditable() throws Exception{
        PriceListItemDTO priceListItemDTO = DTOFactory.createPriceListItemDTO();
        priceListItemDTO.setPriceListId(new Long(1));
        priceListService.changePriceListItemPrice(priceListItemDTO);
    }

    @Test
    public void changePriceListItemPrice() throws Exception{
        PriceListItemDTO priceListItemDTO = DTOFactory.createPriceListItemDTO();
        priceListItemDTO.setPriceListId(new Long(2));
        priceListItemDTO.setPrice(new Long(65));
        priceListService.changePriceListItemPrice(priceListItemDTO);
    }
}
