package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.VehicleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.InvalidVehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.VehicleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VehicleRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.util.DTOFactory;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleServiceTest {

    @MockBean
    VehicleRepository vehicleRepositoryMocked;

    @MockBean
    PathRepository pathRepositoryMocked;

    @Autowired
    VehicleService vehicleService;

    private LinkedList<Vehicle> allVehicles; //za potrebe mockovanja save metode


    @Before
    public void beforeEachTest(){
        //Mock objekti se resetuju posle svakog testa :/
        Path dummyPath = new Path();
        dummyPath.setVehicles(new LinkedList<>());
        dummyPath.setVehicleType(VehicleType.BUS);
        dummyPath.setPathName("Dummy path");
        Mockito.when(pathRepositoryMocked.findPathById(1)).thenReturn(dummyPath);
        allVehicles = new LinkedList<>();

        //Mockovanje find patha po id
        Mockito.doAnswer(invocation->dummyPath)
        .when(pathRepositoryMocked).findPathByVehicle(Mockito.any(Long.class));

        //Mockovanje save metode :D
        Mockito.doAnswer(invocation->{
            Vehicle v = invocation.getArgument(0);
            v.setId(new Long(allVehicles.size()+1));
            allVehicles.add(v);
            return null;
        }).when(vehicleRepositoryMocked).save(Mockito.any(Vehicle.class));

        //Mockovanje delete metode
        Mockito.doAnswer(invocationOnMock -> {
            allVehicles.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(vehicleRepositoryMocked).delete(Mockito.any(Vehicle.class));

        Mockito.doAnswer(invocationOnMock -> {
            for(Vehicle v:allVehicles){
                if(invocationOnMock.getArgument(0).equals(v.getId())){
                    return Optional.of(v);
                }
            }
            return Optional.empty();
        }).when(vehicleRepositoryMocked).findById(Mockito.any(Long.class));

        //Mockovanje findAll metode
        Mockito.doAnswer(invocationOnMock -> {
            Page<Vehicle> page = new Page<Vehicle>() {
                @Override
                public int getTotalPages() {
                    return 1;
                }

                @Override
                public long getTotalElements() {
                    return allVehicles.size();
                }

                @Override
                public <U> Page<U> map(Function<? super Vehicle, ? extends U> function) {
                    return null;
                }

                @Override
                public int getNumber() {
                    return 0;
                }

                @Override
                public int getSize() {
                    return allVehicles.size();
                }

                @Override
                public int getNumberOfElements() {
                    return allVehicles.size();
                }

                @Override
                public List<Vehicle> getContent() {
                    return allVehicles;
                }

                @Override
                public boolean hasContent() {
                    return false;
                }

                @Override
                public Sort getSort() {
                    return null;
                }

                @Override
                public boolean isFirst() {
                    return false;
                }

                @Override
                public boolean isLast() {
                    return false;
                }

                @Override
                public boolean hasNext() {
                    return false;
                }

                @Override
                public boolean hasPrevious() {
                    return false;
                }

                @Override
                public Pageable nextPageable() {
                    return null;
                }

                @Override
                public Pageable previousPageable() {
                    return null;
                }

                @Override
                public Iterator<Vehicle> iterator() {
                    return allVehicles.iterator();
                }
            };
            return page;
        })
        .when(vehicleRepositoryMocked).findAll(Mockito.any(Pageable.class));

    }

    @Test
    public void addVehicleTestOk() throws Exception{

        VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
        vehicleService.addVehicle(vehicleDTO);
    }

    @Test(expected = PathDoesNotExist.class)
    public void addVehicleTestPathDoesNotExist() throws Exception{

        VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
        vehicleDTO.setPathId(new Long(3)); //path koji ne postoji
        vehicleService.addVehicle(vehicleDTO);
    }

    @Test(expected = InvalidVehicleType.class)
    public void addVehicleTestInvalidVehicleType()throws Exception{
        VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
        vehicleDTO.setVehicleType(VehicleType.METROTRAIN); //BUS je tip linije
        vehicleService.addVehicle(vehicleDTO);
    }

    @Test
    public void getAllVehicles() throws Exception{
        int numVehicles = 5;
        for(int i=0;i<numVehicles;i++){
            VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
            vehicleService.addVehicle(vehicleDTO);
        }
        List<VehicleDTO> vehicleDTOS = (List<VehicleDTO>) vehicleService.getAllVehicles(Pageable.unpaged());
        Assert.assertEquals(numVehicles,vehicleDTOS.size());
    }

    @Test
    public void deleteVehicleTestOk() throws Exception{

        VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
        vehicleService.addVehicle(vehicleDTO);
        vehicleService.deleteVehicle(new Long(allVehicles.size()));
    }

    @Test(expected = VehicleDoesNotExist.class)
    public void deleteVehicleTestVehicleDoesNotExist() throws Exception{

        VehicleDTO vehicleDTO = DTOFactory.createVehicleDTO();
        vehicleService.addVehicle(vehicleDTO);
        vehicleService.deleteVehicle(new Long(allVehicles.size()+2));
    }
}
