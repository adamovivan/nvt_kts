package rs.ac.uns.ftn.informatika.nvt_kts.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Admin;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.security.TokenUtils;

import java.util.LinkedList;


@Component("testUtils")
public class TestUtils {

    @Autowired
    private TokenUtils tokenUtils;

    public String getAuthToken(String username, String password, Role role){
        UserDetails details = new org.springframework.security.core.userdetails.User(
                username,
                password,
                AuthorityUtils.createAuthorityList(role.toString()));

        return "Bearer " + tokenUtils.generateToken(details);
    }

    public String toJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(obj);
    }

    public Admin  createAdmin(){
        Admin a = new Admin();
        a.setFirstName("Pera");
        a.setLastName("peric");
        a.setUsername("pera124");
        a.setPassword("$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC"); //12345
        a.setRole(Role.ROLE_ADMIN);
        return a;
    }

    public Visitor createVisitor(){
        Visitor v = new Visitor();
        v.setFirstName("Mika");
        v.setLastName("Mikic");
        v.setUsername("mika124");
        v.setPassword("$2a$10$kx6ymttdiBQ/3NAz1ssxoeOF8Vwm3LSKFnEkSADPc5x8kgaj/vKnC"); //12345
        v.setRole(Role.ROLE_VISITOR);
        v.setVisitorType(VisitorType.REGULAR);
        v.setCards(new LinkedList<>());
        return v;
    }
}
