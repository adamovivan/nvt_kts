package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.RegisterDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import java.io.FileInputStream;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class VisitorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestUtils testUtils;

    private String visitor_token;


    @Before
    public void setUp() {
        visitor_token = testUtils.getAuthToken("mika123", "12345", Role.ROLE_VISITOR);
    }

    @Test
    public void uploadDocument() throws Exception {
        FileInputStream fis = new FileInputStream("filesForTest\\35d2ed9e-b263-4964-b0f1-1f4a3ebd69a6.jpg");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "filesForTest\\35d2ed9e-b263-4964-b0f1-1f4a3ebd69a6.jpg", "image/jpg", fis);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadDocument")
                .file(multipartFile)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .header("Authorization",visitor_token)
                .param("documentName", "35d2ed9e-b263-4964-b0f1-1f4a3ebd69a6.jpg").param("discount", "STUDENT"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void uploadDocument_unsuportedImageType() throws Exception {
        FileInputStream fis = new FileInputStream("filesForTest\\test.txt");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "filesForTest\\test.txt", "text/plain", fis);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/uploadDocument")
                .file(multipartFile)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .header("Authorization",visitor_token)
                .param("documentName", "test.txt").param("discount", "STUDENT"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void registerUser() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<RegisterDTO> httpEntity = new HttpEntity<>(getRegisterDTO(), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/register", HttpMethod.POST,
                httpEntity, String.class);

        assertEquals("Successful registration.", responseEntity.getBody());
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    private RegisterDTO getRegisterDTO() {
        RegisterDTO registerDTO = new RegisterDTO();
        registerDTO.setUsername("marija123");
        registerDTO.setPassword("123");
        registerDTO.setFirstName("Marija");
        registerDTO.setLastName("Marijanovic");
        return registerDTO;
    }
}