package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CardControllerTest.class,
        ControllerControllerTest.class,
        PathControllerTest.class,
        PriceListControllerTest.class,
        ScheduleControllerTest.class,
        StationControllerTest.class,
        VehicleControllerTest.class,
        VisitorControllerTest.class
})
public class ControllerSuite {
}
