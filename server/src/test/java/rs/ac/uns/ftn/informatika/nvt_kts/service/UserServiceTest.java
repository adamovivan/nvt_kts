package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.UserRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.security.TokenUtils;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @MockBean
    UserRepository userRepositoryMocked;

    @Autowired
    TestUtils testUtils;

    @Autowired
    UserService userService;




    @Test
    public void getLoggedUserTest(){

        Visitor v = testUtils.createVisitor();
        Mockito.when(userRepositoryMocked.findByUsername(v.getUsername())).thenReturn(v);

        Authentication authentication = Mockito.mock(Authentication.class);
        Mockito.when(authentication.getName()).thenReturn(v.getUsername());
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);

        SecurityContextHolder.setContext(securityContext);
        Assert.assertEquals(userService.getLoggedUser().getUsername(),v.getUsername());
    }
}
