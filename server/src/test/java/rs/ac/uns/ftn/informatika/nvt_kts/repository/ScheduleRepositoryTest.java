package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Schedule;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class ScheduleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Test
    @Transactional
    @Rollback
    public void findByPathAndDayAndVehicleType_exists() {
        Path path = new Path();
        path.setPathName("N1");
        path.setVehicleType(VehicleType.BUS);
        path.setZone(Zone.ONE);

        entityManager.persist(path);
        entityManager.flush();

        Schedule schedule = new Schedule();
        schedule.setDay(Day.Workday);
        schedule.setPath(path);
        schedule.setActive(true);

        entityManager.persist(schedule);
        entityManager.flush();

        Schedule found = scheduleRepository.findByPathAndDayAndVehicleType(path.getPathName(), schedule.getDay(), path.getVehicleType());

        assertEquals(schedule, found);
    }

    @Test
    @Transactional
    @Rollback
    public void findByPathAndDayAndVehicleType_doesNotExist() {

        Schedule found = scheduleRepository.findByPathAndDayAndVehicleType("NM1", Day.Workday, VehicleType.BUS);

        assertNull(found);
    }
}