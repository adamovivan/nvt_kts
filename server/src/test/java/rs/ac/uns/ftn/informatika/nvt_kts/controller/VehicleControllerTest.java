package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.VehicleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.InvalidVehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.PathDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.exception.VehicleDoesNotExist;
import rs.ac.uns.ftn.informatika.nvt_kts.service.VehicleService;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VehicleService vehicleServiceMock;

    private static VehicleDTO dtoInvalidType = new VehicleDTO();
    private static VehicleDTO dtoPathDoesNotExist = new VehicleDTO();
    private static VehicleDTO dtoOk = new VehicleDTO();
    private static Path p1 = new Path();
    private static ObjectMapper mapper = new ObjectMapper();

    @BeforeClass
    public static void BeforeAll(){
        dtoInvalidType.setPathId(1L);
        dtoPathDoesNotExist.setPathId(2L);
        dtoOk.setPathId(3L);
        p1.setVehicleType(VehicleType.BUS);
    }

    @Before
    public void SetUp() throws Exception {
        // Add tests
        doThrow(new InvalidVehicleType(p1)).when(vehicleServiceMock).addVehicle(dtoInvalidType);
        doThrow(new PathDoesNotExist()).when(vehicleServiceMock).addVehicle(dtoPathDoesNotExist);
        doReturn(Boolean.TRUE).when(vehicleServiceMock).addVehicle(dtoOk);
        doNothing().when(vehicleServiceMock).deleteVehicle(1L);
        doThrow(new VehicleDoesNotExist()).when(vehicleServiceMock).deleteVehicle(-1L);
    }

    @Test
    public void addVehicleInvalidVehicleTypeTest() throws Exception {

        mockMvc.perform(post("/vehicles")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE)
                .content(mapper.writeValueAsString(dtoInvalidType)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addVehiclePathDoesNotExistTest() throws Exception {

        mockMvc.perform(post("/vehicles")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE)
                .content(mapper.writeValueAsString(dtoPathDoesNotExist)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteVehicleOk() throws Exception{
        mockMvc.perform(delete("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteVehicleNotExists() throws Exception{
        mockMvc.perform(delete("/vehicles/-1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addVehicleOk() throws Exception {

        mockMvc.perform(post("/vehicles")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.TEXT_PLAIN_VALUE)
                .content(mapper.writeValueAsString(dtoOk)))
                .andExpect(status().isCreated());
    }


}
