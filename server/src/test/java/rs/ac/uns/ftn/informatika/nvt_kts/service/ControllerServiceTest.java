package rs.ac.uns.ftn.informatika.nvt_kts.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.*;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Controller;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Visitor;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.Vehicle;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ControllerCheckDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.PathForControllerDto;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.ControllerRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.PathRepository;
import rs.ac.uns.ftn.informatika.nvt_kts.repository.VisitorRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerServiceTest {

    @Autowired
    private ControllerService controllerService;

    @MockBean
    private ControllerRepository controllerRepositoryMocked;

    @MockBean
    private VisitorRepository visitorRepositoryMocked;

    @MockBean
    private PathRepository pathRepositoryMocked;

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    @Before
    public void setUp() {
        Visitor v = getVisitor();
        Visitor v2 = getVisitor();
        v2.setUsername("v2");
        Visitor v3 = getVisitor();
        v3.setUsername("v3");

        Card card = new Card();
        List<Card> cards = new ArrayList<>();
        List<Card> cards2 = new ArrayList<>();
        List<Card> cards3 = new ArrayList<>();

        PriceListItem priceListItem = new PriceListItem();

        Item item = new Item();
        item.setZone(Zone.ONE);
        item.setCardType(CardType.DAILY);
        item.setVehicleType(VehicleType.BUS);
        item.setVisitorType(VisitorType.REGULAR);

        priceListItem.setItem(item);
        priceListItem.setPrice(120L);

        PriceList priceList = new PriceList();
        LocalDate startDate = LocalDate.parse("Sat Oct 01 2022", dtf);
        LocalDate endDate = LocalDate.parse("Tue Nov 01 2022", dtf);
        priceList.setStartDate(startDate);
        priceList.setEndDate(endDate);

        priceListItem.setPriceList(priceList);
        priceListItem.setPriceList(priceList);

        card.setStartDate(startDate);
        card.setEndDate(endDate);
        card.setActive(true);
        card.setVisitor(v);
        card.setPriceListItem(priceListItem);
        cards.add(card);

        v.setCards(cards);
        v2.setCards(cards2);
        v3.setCards(cards3);


        List<Visitor> visitors = new ArrayList<>();
        visitors.add(v);
        visitors.add(v2);
        visitors.add(v3);

        Path path = new Path();
        path.setPathName("N1");
        path.setVehicleType(VehicleType.BUS);
        path.setZone(Zone.ONE);

        Path path2 = new Path();
        path2.setPathName("N2");
        path2.setVehicleType(VehicleType.BUS);
        path2.setZone(Zone.ONE);
        List<Path> paths = new ArrayList<>();
        paths.add(path);
        paths.add(path2);

        Controller c = getController();
        Controller c2 = getController();
        c2.setUsername("c2");

        List<Vehicle> vehicles = new ArrayList<>();
        List<Vehicle> vehicles2 = new ArrayList<>();

        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleType(VehicleType.BUS);
        vehicle.setPath(path);

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setVehicleType(VehicleType.BUS);
        vehicle1.setPath(path2);

        vehicles.add(vehicle);
        vehicles.add(vehicle1);

        c.setVehicles(vehicles);
        c2.setVehicles(vehicles2);

        Mockito.when(visitorRepositoryMocked.findAll()).thenReturn(visitors);
        Mockito.when(pathRepositoryMocked.findAll()).thenReturn(paths);
        Mockito.when(controllerRepositoryMocked.findByUsername("controler123")).thenReturn(c);
        Mockito.when(controllerRepositoryMocked.findByUsername("c2")).thenReturn(c2);

    }

    @Test
    public void controllerCheck() {
        List<ControllerCheckDTO> checkDTOS = controllerService.controllerCheck(Zone.ONE, VehicleType.BUS, "Sun Oct 02 2022");

        assertEquals(3, checkDTOS.size());
    }

    @Test
    public void findPaths() {
        Controller c = getController();
        List<PathForControllerDto> pathForControllerDtos = controllerService.findPaths(c);

        assertEquals(2, pathForControllerDtos.size());
    }

    @Test
    public void findPaths_empty_list_vehicles() {
        Controller c = getController();
        c.setUsername("c2");
        List<PathForControllerDto> pathForControllerDtos = controllerService.findPaths(c);

        assertTrue(pathForControllerDtos.isEmpty());
    }

    private Visitor getVisitor() {
        Visitor v = new Visitor();
        v.setFirstName("Visitor");
        v.setLastName("Visitorić");
        v.setVisitorType(VisitorType.REGULAR);
        v.setUsername("visitor123");
        v.setPassword("12345");
        v.setRole(Role.ROLE_VISITOR);
        return v;
    }

    private Controller getController() {
        Controller c = new Controller();
        c.setFirstName("Controler");
        c.setLastName("Controleric");
        c.setUsername("controler123");
        c.setPassword("12345");
        c.setRole(Role.ROLE_CONTROLLER);
        return c;
    }

}