package rs.ac.uns.ftn.informatika.nvt_kts.domain;

import org.junit.Test;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Path;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.path.Station;

import static org.assertj.core.api.Assertions.assertThat;

public class StationTest {

    @Test
    public void stationPathsStrTest() {
        Path p1 = new Path();
        p1.setPathName("A");

        Path p2 = new Path();
        p2.setPathName("B");

        Station s = new Station();
        s.getPaths().add(p1);
        s.getPaths().add(p2);

        s.pathsToStr();

        assertThat(s.getPathsStr())
                .startsWith("[")
                .contains("A")
                .contains("B")
                .endsWith("]");
    }

}
