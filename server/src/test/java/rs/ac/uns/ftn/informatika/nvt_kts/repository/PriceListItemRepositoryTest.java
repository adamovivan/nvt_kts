package rs.ac.uns.ftn.informatika.nvt_kts.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.CardType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.PriceList;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.PriceListItem;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.card.Zone;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.VisitorType;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
public class PriceListItemRepositoryTest {

    @Autowired
    TestEntityManager testEntityManager;

    @Autowired
    TestUtils testUtils;

    @Autowired
    PriceListRepository priceListRepository;

    @Autowired
    PriceListItemRepository priceListItemRepository;

    @Test
    public void findByPriceListAndItemFieldsTestOk(){
        PriceList priceList = priceListRepository.findById(new Long(1)).get();
        for(Zone zone:Zone.values()){
            for(VehicleType vehicleType:VehicleType.values()){
                for(CardType cardType:CardType.values()){
                    for(VisitorType visitorType:VisitorType.values()){
                        PriceListItem priceListItem = priceListItemRepository
                        .findByPriceListAndItemFields
                         (priceList,zone,vehicleType,cardType,visitorType);
                        Assert.assertNotNull(priceListItem);
                    }

                }

            }
        }
    }

    @Test
    public void findByPriceListAndItemFieldsTestFail(){

        PriceList priceList = new PriceList();
        priceList.setId(new Long(0));
        priceList.setEndDate(LocalDate.now());
        priceList.setStartDate(LocalDate.now());
        Assert.assertNull(priceListItemRepository.findByPriceListAndItemFields
        (priceList,Zone.ONE,VehicleType.BUS,CardType.DAILY,VisitorType.REGULAR));
    }
}
