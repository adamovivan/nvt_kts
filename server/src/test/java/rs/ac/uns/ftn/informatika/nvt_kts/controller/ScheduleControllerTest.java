package rs.ac.uns.ftn.informatika.nvt_kts.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.timetable.Day;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.user.Role;
import rs.ac.uns.ftn.informatika.nvt_kts.domain.vehicle.VehicleType;
import rs.ac.uns.ftn.informatika.nvt_kts.dto.ScheduleDTO;
import rs.ac.uns.ftn.informatika.nvt_kts.util.TestUtils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ScheduleControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestUtils testUtils;

    private String admin_token;

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");

    @Before
    public void setUp() {
        admin_token = testUtils.getAuthToken("pera123", "12345", Role.ROLE_ADMIN);
    }

    @Test
    public void addSchedule() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);
        ScheduleDTO scheduleDTO = getScheduleDTO();

        HttpEntity<ScheduleDTO> httpEntity = new HttpEntity<>(scheduleDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange("/schedule/add", HttpMethod.POST, httpEntity, String.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), "Schedule added successfully.");
    }

    @Test
    public void addSchedule_pathDoesNotExist() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        ScheduleDTO scheduleDTO = getScheduleDTO();
        scheduleDTO.setPathName("N1");

        HttpEntity<ScheduleDTO> httpEntity = new HttpEntity<>(scheduleDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange("/schedule/add", HttpMethod.POST, httpEntity, String.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), "Path does not exist.");
    }

    @Test
    public void addSchedule_scheduleDurationAlreadyExists() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        ScheduleDTO scheduleDTO = getScheduleDTO();
        scheduleDTO.setPathName("1B");
        scheduleDTO.setDay(Day.Workday);
        scheduleDTO.setStartDate("Tue Jan 01 2019");
        scheduleDTO.setEndDate("Fri Feb 01 2019");

        HttpEntity<ScheduleDTO> httpEntity = new HttpEntity<>(scheduleDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange("/schedule/add", HttpMethod.POST, httpEntity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(responseEntity.getBody(), "For that path name and day schedule duration already exist.");
    }


    @Test
    public void getAllScheduleDurations() {
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<ScheduleDTO> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List> responseEntity = restTemplate.exchange("/schedule/allTypes?pathName=N1117&day=Workday", HttpMethod.GET,
                httpEntity, List.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(0, responseEntity.getBody().size());
    }

    @Test
    public void getScheduleDurations() {
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List> responseEntity = restTemplate.exchange("/schedule/allTypes?pathName=N1117&day=Workday&vehicleType=BUS", HttpMethod.GET,
                httpEntity, List.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(0, responseEntity.getBody().size());
    }

    @Test
    public void getScheduleDurations_ScheduleDTO() {
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List> responseEntity = restTemplate.exchange("/schedule/all", HttpMethod.GET,
                httpEntity, List.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseEntity.getBody().isEmpty());
    }


    @Test
    public void updateSchedule() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        ScheduleDTO scheduleDTO = getScheduleDTO();

        List<LocalTime> timesTo = new ArrayList<>();

        timesTo.add(LocalTime.of(15, 45));
        timesTo.add(LocalTime.of(16, 00));
        timesTo.add(LocalTime.of(17, 45));
        timesTo.add(LocalTime.of(18, 45));

        List<LocalTime> timesFrom = new ArrayList<>();

        timesFrom.add(LocalTime.of(10, 45));
        timesFrom.add(LocalTime.of(11, 00));
        timesFrom.add(LocalTime.of(18, 45));
        timesFrom.add(LocalTime.of(19, 45));

        scheduleDTO.setToTimes(timesTo);
        scheduleDTO.setFromTimes(timesFrom);

        HttpEntity<Object> httpEntity = new HttpEntity<>(getScheduleDTO(), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/schedule/update", HttpMethod.PUT,
                httpEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Schedule duration updated successfully.", responseEntity.getBody());
    }

    @Test
    public void updateSchedule_exception() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        ScheduleDTO scheduleDTO = getScheduleDTO();
        scheduleDTO.setStartDate( "Tue Nov 12 2030");
        scheduleDTO.setEndDate("Thu Dec 12 2030");

        HttpEntity<Object> httpEntity = new HttpEntity<>(scheduleDTO, headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/schedule/update", HttpMethod.PUT,
                httpEntity, String.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals("Schedule duration with that path name and day does no exist.", responseEntity.getBody());
    }

    @Test
    public void deleteSchedule() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/schedule/delete?pathName=1B&day=Workday&vehicleType=BUS&endDate=Fri Feb 01 2019&startDate=Tue Jan 01 2019", HttpMethod.DELETE,
                httpEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("Schedule deleted successfully.", responseEntity.getBody());
    }

    @Test
    public void deleteSchedule_scheduleDurationDoesNotExist() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", admin_token);

        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/schedule/delete?pathName=1B&day=Workday&vehicleType=BUS&endDate=Thu Dec 12 2030&startDate=Tue Nov 12 2030", HttpMethod.DELETE,
                httpEntity, String.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertEquals("Schedule duration with that path name and day does no exist.", responseEntity.getBody());
    }

    private ScheduleDTO getScheduleDTO() {
        List<LocalTime> timesTo = new ArrayList<>();

        timesTo.add(LocalTime.of(11, 45));
        timesTo.add(LocalTime.of(13, 00));
        timesTo.add(LocalTime.of(13, 45));
        timesTo.add(LocalTime.of(14, 45));

        List<LocalTime> timesFrom = new ArrayList<>();

        timesFrom.add(LocalTime.of(11, 45));
        timesFrom.add(LocalTime.of(13, 00));
        timesFrom.add(LocalTime.of(13, 45));
        timesFrom.add(LocalTime.of(14, 45));

        ScheduleDTO scheduleDTO = new ScheduleDTO();
        scheduleDTO.setPathName("1B");
        scheduleDTO.setVehicleType(VehicleType.BUS);
        scheduleDTO.setDay(Day.Saturday);
        scheduleDTO.setStartDate("Sat Oct 01 2022");
        scheduleDTO.setEndDate("Tue Nov 01 2022");
        scheduleDTO.setToTimes(timesTo);
        scheduleDTO.setFromTimes(timesFrom);

        return scheduleDTO;
    }
}